package org.immport.data.shared.model;

import java.math.BigDecimal;

public class BiosampleStudyTime {




	private String experimentAccessionOrPanelName;
	private String title;
	private BigDecimal studyTimeCollected;
	private String plannedVisitAccession;
	private long countOfBioSamples;


	public BiosampleStudyTime() {
	
	}

	public BiosampleStudyTime(String experimentAccessionOrPanelName,String title,BigDecimal studyTimeCollected,long countOfBioSamples
	                )
	{
			this.title = title;
	        this.experimentAccessionOrPanelName =experimentAccessionOrPanelName;
	        this.studyTimeCollected = studyTimeCollected;
	        this.countOfBioSamples = countOfBioSamples;       
	}
	
	public BiosampleStudyTime(String experimentAccessionOrPanelName,String title,String plannedVisitAccession,long countOfBioSamples
            )
	{
		this.title = title;
	    this.experimentAccessionOrPanelName =experimentAccessionOrPanelName;
	    this.plannedVisitAccession = plannedVisitAccession;
	    this.countOfBioSamples = countOfBioSamples;       
	}
	
	public BiosampleStudyTime(String experimentAccessionOrPanelName,BigDecimal studyTimeCollected,long countOfBioSamples
            )
	{
	
		this.experimentAccessionOrPanelName =experimentAccessionOrPanelName;
		this.studyTimeCollected = studyTimeCollected;
		this.countOfBioSamples = countOfBioSamples;       
	}

	public BiosampleStudyTime(String experimentAccessionOrPanelName,String plannedVisitAccession,long countOfBioSamples
            )
	{
	
		this.experimentAccessionOrPanelName =experimentAccessionOrPanelName;
		this.plannedVisitAccession = plannedVisitAccession;
		this.countOfBioSamples = countOfBioSamples;       
	}

	public void setExperimentAccessionOrPanelName(String experimentAccessionOrPanelName) {
	        this.experimentAccessionOrPanelName = experimentAccessionOrPanelName;
	}
	
	public String getExperimentAccessionOrPanelName() {
	        return this.experimentAccessionOrPanelName;
	}
	
	public void setTitle(String title) {
        this.title = title;
	}

	public String getTitle() {
        return this.title;
	}
	
	public void setPlannedVisitAccession(String plannedVisitAccession) {
        this.plannedVisitAccession = plannedVisitAccession;
	}

	public String getPlannedVisitAccession() {
        return this.plannedVisitAccession;
	}
	
	public void setStudyTimeCollected(BigDecimal studyTimeCollected) {
        this.studyTimeCollected = studyTimeCollected;
	}

	public BigDecimal getStudyTimeCollected() {
        return this.studyTimeCollected;
	}
	
	
	public void setCountOfBioSamples(long countOfBioSamples) {
        this.countOfBioSamples = countOfBioSamples;
	}

	public long getCountOfBioSamples() {
        return this.countOfBioSamples;
	}
	
	
}

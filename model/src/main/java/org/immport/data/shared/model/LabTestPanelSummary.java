/**
 * 
 */
package org.immport.data.shared.model;

/**
 * @author fmonteiro
 *
 */
public class LabTestPanelSummary {
	private String  panelNameReported;
	    private String armAccession;	    
	    private long subjAccNo;
	    private long bioSampleNo;
	    private long labTestPanelNo;
	    

	    public LabTestPanelSummary() {
	    }

	    public LabTestPanelSummary(String armAccession,String panelNameReported, 
	    		 		long bioSampleNo,
	                    long subjAccNo,	                   
	                    long labTestPanelNo) {
	    	 	this.armAccession = armAccession;
	            this.panelNameReported = panelNameReported;
	            this.subjAccNo = subjAccNo;
	            this.bioSampleNo = bioSampleNo;
	            this.labTestPanelNo = labTestPanelNo;
	            
	    }

	    public void setPanelNameReported(String labTestPanelName) {
	            this.panelNameReported = labTestPanelName;
	    }
	    
	    public String getPanelNameReported() {
	            return this.panelNameReported;
	    }

	    public void setArmAccession(String armAccession) {
	            this.armAccession = armAccession;
	    }

	    public String getArmAccession() {
	            return this.armAccession;
	    }


	    public void setLabTestPanelNo(long labTestPanelNo) {
	            this.labTestPanelNo = labTestPanelNo;
	    }
	    
	    public long getLabTestPanelNo() {
	            return this.labTestPanelNo;
	    }

	   

	    public void setBioSampleNo(long bioSampleNo) {
	            this.bioSampleNo = bioSampleNo;
	    }
	    
	    public long getBioSampleNo() {
	            return this.bioSampleNo;
	    }

	    public void setSubjAccNo(long subjAccNo) {
	            this.subjAccNo = subjAccNo;
	    }

	    public long getSubjAccNo() {
	            return this.subjAccNo;
	    }

}

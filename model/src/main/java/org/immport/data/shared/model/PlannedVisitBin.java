package org.immport.data.shared.model;

import java.math.BigDecimal;

import javax.persistence.Column;

public class PlannedVisitBin implements java.io.Serializable {
	
	private double minStartDay;
	private double maxStartDay;
	
	public PlannedVisitBin(double minStartDay,double maxStartDay)
	{
		
		this.minStartDay = minStartDay;
		this.maxStartDay = maxStartDay;
	}
	
	public double getMaxStartDay() {
		return this.maxStartDay;
	}

	public void setMaxStartDay(double maxStartDay) {
		this.maxStartDay = maxStartDay;
	}

	
	public double getMinStartDay() {
		return this.minStartDay;
	}

	public void setMinStartDay(double minStartDay) {
		this.minStartDay = minStartDay;
	}
	
	
	
	
	public int isValueInBetween( double studyTimeCollected ) {
		  int retValue = 0;
		  
	      if (studyTimeCollected > minStartDay && studyTimeCollected <= maxStartDay) {
	    	  retValue = 1;
	      }
	      
	      if (studyTimeCollected <= minStartDay) {
	    	  retValue = -1;
	      }
	      
	      if (studyTimeCollected > maxStartDay) {
	    	  retValue = 2;
	      }
	      
	      return retValue;
	    	  
	  }   
	

}

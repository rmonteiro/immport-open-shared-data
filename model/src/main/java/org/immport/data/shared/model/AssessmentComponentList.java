/**
 * 
 */
package org.immport.data.shared.model;

import java.util.ArrayList;
import java.util.List;

import org.immport.data.shared.model.GenericPivotData;

/**
 * @author fmonteiro
 *
 */
public class AssessmentComponentList extends GenericPivotData implements java.io.Serializable {

  
   // private String panelNameReported;
   // private String componentNameReported;   
   // private String verbatimQuestion; 
    

    public AssessmentComponentList() {
    }

    public AssessmentComponentList(String studyAccession,String panelNameReported,
    					     String componentNameReported) {
           
          //  this.panelNameReported = panelNameReported;
          //  this.componentNameReported = componentNameReported;
          //  this.verbatimQuestion = verbatimQuestion;     
            this.setStudyAccession(studyAccession);
            List<String> objBaseData = new ArrayList<String>();
	        List<String> objBaseNames = new ArrayList<String>();
	        List<String> objBaseNamesSort = new ArrayList<String>();
			List<String> baseNamesSortParameter = new ArrayList<String>();
			
	        objBaseNames.add("Assessment");
	        objBaseNames.add("Assessment Component");
	        
	        objBaseNamesSort.add("true");
	        objBaseNamesSort.add("true");
	        
	        baseNamesSortParameter.add("panelNameReported");
	        baseNamesSortParameter.add("componentNameReported");
	        
	        objBaseData.add(panelNameReported);
	        objBaseData.add(componentNameReported);
	        
	        this.setBaseData(objBaseData);
	        this.setBaseNames(objBaseNames);
	        this.setBaseNamesSort(objBaseNamesSort);
	        this.setBaseNamesSortParameter(baseNamesSortParameter);
	        
            
    }
    
    
    
   /* public void setPanelNameReported(String panelNameReported) {
        this.panelNameReported = panelNameReported;
    }

    public String getPanelNameReported() {
        return this.panelNameReported;
    }

    
    public void setComponentNameReported(String componentNameReported) {
        this.componentNameReported = componentNameReported;
    }

    public String getComponentNameReported() {
        return this.componentNameReported;
    }
    
    public void setVerbatimQuestion(String verbatimQuestion) {
        this.verbatimQuestion = verbatimQuestion;
    }

    public String getVerbatimQuestion() {
        return this.verbatimQuestion;
    }*/
    
}

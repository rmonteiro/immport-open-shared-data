/**
 * 
 */
package org.immport.data.shared.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fmonteiro
 *
 */
public class GenericPivotData {
	 private String studyAccession;
	 private List<String> armData;
	 private List<String> armNames;
	 private Map armCoValues;	
	 private List<String> baseData;
	 private List<String> baseNames;
	 private List<String> baseNamesSort;
	 private List<String> baseNamesSortParameter;
	 
	 public GenericPivotData()	 {
		 
	 }
	 
	 public GenericPivotData(String studyAccession, List<String>armData) {
		 this.studyAccession = studyAccession;	         
	     this.armData = armData;
	 }
	 
	 public GenericPivotData(String studyAccession) {
		 this.studyAccession = studyAccession;
	 }
	 
	 public GenericPivotData(String studyAccession, List<String>armData, List<String>armNames) {
		 this.studyAccession = studyAccession;	         
	     this.armData = armData;
	     this.armNames = armNames;
	 }
	 
	 public void setStudyAccession(String studyAccession) {
		 this.studyAccession = studyAccession;
     }

	 public String getStudyAccession() {
		 return this.studyAccession;
	 }
	 
	 public void setArmData(List<String> armData) {
		 this.armData = armData;
	 }

	 public List<String>  getArmData() {
		 return this.armData;
	 }
	
	 public void setArmNames(List<String> armNames) {
		 this.armNames = armNames;
	 }

	 public List<String>  getArmNames() {
		 return this.armNames;
	 }
	 
	 public void setArmCoValues(Map armCoValues) {
		 this.armCoValues = armCoValues;
	 }

	 public Map  getArmCoValues() {
		 return this.armCoValues;
	 }

	 public void setBaseData(List<String> baseData) {
		 this.baseData = baseData;
	 }

	 public List<String> getBaseData() {
		 return this.baseData;
	 }
	
	 public void setBaseNames(List<String> baseNames) {
		 this.baseNames = baseNames;
	 }

	 public List<String>  getBaseNames() {
		 return this.baseNames;
	 }
	 
	 public void setBaseNamesSort(List<String> baseNamesSort) {
		 this.baseNamesSort = baseNamesSort;
	 }

	 public List<String>  getBaseNamesSort() {
		 return this.baseNamesSort;
	 }
	 
	 public void setBaseNamesSortParameter(List<String> baseNamesSortParameter) {
		 this.baseNamesSortParameter = baseNamesSortParameter;
	 }

	 public List<String>  getBaseNamesSortParameter() {
		 return this.baseNamesSortParameter;
	 }
	 
}

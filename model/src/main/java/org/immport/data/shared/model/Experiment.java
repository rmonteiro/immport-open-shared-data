package org.immport.data.shared.model;

import java.util.ArrayList;

// Generated Dec 19, 2016 4:14:57 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Experiment generated by hbm2java
 */
@Entity
@Table(name = "experiment")
@NamedQueries({
	   // @NamedQuery(name = "Experiment.getByStudy",
	   //                 query = "SELECT e FROM Experiment e, Biosample2Expsample b1, Biosample b2 \n" +
	   //                         " WHERE e.experimentAccession = b1.experiment.experimentAccession \n" +
	   //                 		"   AND b1.biosample.biosampleAccession = b2.biosampleAccession \n" +
	   //                         "   AND b2.study.studyAccession = :studyAccession \n" +
	   //                 		"ORDER BY e.experimentAccession"),
	   @NamedQuery(name = "Experiment.getByStudy",
	                             query = "SELECT c FROM Experiment c " +
	                                     " WHERE c.study.studyAccession = :studyAccession " +
	                             		"ORDER BY c.experimentAccession"),                		
	    @NamedQuery(name = "Experiment.getCount",
	            query = "SELECT count(distinct e.experimentAccession) FROM Experiment e")
	})
public class Experiment implements java.io.Serializable {

	private String experimentAccession;
	private Workspace workspace;
	private Study study;
	//private LkExpMeasurementTech lkExpMeasurementTech;
	//private LkExperimentPurpose lkExperimentPurpose;
	private String measurementTechnique;
	private String purpose;
	private String description;
	private String name;
	private Set<ElispotResult> elispotResults = new HashSet<ElispotResult>(0);
	private Set<FcsAnalyzedResult> fcsAnalyzedResults = new HashSet<FcsAnalyzedResult>(
			0);
	private Set<KirTypingResult> kirTypingResults = new HashSet<KirTypingResult>(
			0);
	private Set<HaiResult> haiResults = new HashSet<HaiResult>(0);
	private Set<NeutAbTiterResult> neutAbTiterResults = new HashSet<NeutAbTiterResult>(
			0);
	private Set<ElisaResult> elisaResults = new HashSet<ElisaResult>(0);
	private List<Protocol> protocols = new ArrayList<Protocol>(0);
	private Set<StandardCurve> standardCurves = new HashSet<StandardCurve>(0);
	private Set<PcrResult> pcrResults = new HashSet<PcrResult>(0);
	private Set<HlaTypingResult> hlaTypingResults = new HashSet<HlaTypingResult>(
			0);
	private Set<MbaaResult> mbaaResults = new HashSet<MbaaResult>(0);
	private Set<Expsample> expsamples = new HashSet<Expsample>(0);
	private Set<ControlSample> controlSamples = new HashSet<ControlSample>(0);
	
	private List <Reagent> reagents = new ArrayList<Reagent>(0);
	private List <Treatment> treatments = new ArrayList<Treatment>(0);

	public Experiment() {
	}

	public Experiment(String experimentAccession, Workspace workspace,
			//LkExpMeasurementTech lkExpMeasurementTech
			String measurementTechnique) {
		this.experimentAccession = experimentAccession;
		this.workspace = workspace;
		//this.lkExpMeasurementTech = lkExpMeasurementTech;
		this.measurementTechnique = measurementTechnique;
	}

	public Experiment(String experimentAccession, Workspace workspace,
			Study study, 
			//LkExpMeasurementTech lkExpMeasurementTech,
			String measurementTechinque,
			//LkExperimentPurpose lkExperimentPurpose, 
			String purpose,
			String description,
			String name, Set<ElispotResult> elispotResults,
			Set<FcsAnalyzedResult> fcsAnalyzedResults,
			Set<KirTypingResult> kirTypingResults, Set<HaiResult> haiResults,
			Set<NeutAbTiterResult> neutAbTiterResults,
			Set<ElisaResult> elisaResults, List<Protocol> protocols,
			Set<StandardCurve> standardCurves, Set<PcrResult> pcrResults,
			Set<HlaTypingResult> hlaTypingResults, Set<MbaaResult> mbaaResults,
			Set<Expsample> expsamples, Set<ControlSample> controlSamples) {
		this.experimentAccession = experimentAccession;
		this.workspace = workspace;
		this.study = study;
		//this.lkExpMeasurementTech = lkExpMeasurementTech;
		//this.lkExperimentPurpose = lkExperimentPurpose;
		this.measurementTechnique = measurementTechnique;
		this.purpose = purpose;
		this.description = description;
		this.name = name;
		this.elispotResults = elispotResults;
		this.fcsAnalyzedResults = fcsAnalyzedResults;
		this.kirTypingResults = kirTypingResults;
		this.haiResults = haiResults;
		this.neutAbTiterResults = neutAbTiterResults;
		this.elisaResults = elisaResults;
		this.protocols = protocols;
		this.standardCurves = standardCurves;
		this.pcrResults = pcrResults;
		this.hlaTypingResults = hlaTypingResults;
		this.mbaaResults = mbaaResults;
		this.expsamples = expsamples;
		this.controlSamples = controlSamples;
	}

	@Id
	@Column(name = "experiment_accession", unique = true, nullable = false, length = 15)
	public String getExperimentAccession() {
		return this.experimentAccession;
	}

	public void setExperimentAccession(String experimentAccession) {
		this.experimentAccession = experimentAccession;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workspace_id", nullable = false)
	public Workspace getWorkspace() {
		return this.workspace;
	}

	public void setWorkspace(Workspace workspace) {
		this.workspace = workspace;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "study_accession")
	public Study getStudy() {
		return this.study;
	}

	public void setStudy(Study study) {
		this.study = study;
	}

	/*
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "measurement_technique", nullable = false)
	public LkExpMeasurementTech getLkExpMeasurementTech() {
		return this.lkExpMeasurementTech;
	}

	public void setLkExpMeasurementTech(
			LkExpMeasurementTech lkExpMeasurementTech) {
		this.lkExpMeasurementTech = lkExpMeasurementTech;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "purpose")
	public LkExperimentPurpose getLkExperimentPurpose() {
		return this.lkExperimentPurpose;
	}

	public void setLkExperimentPurpose(LkExperimentPurpose lkExperimentPurpose) {
		this.lkExperimentPurpose = lkExperimentPurpose;
	}
	*/

	@Column(name = "measurement_technique", length = 50)
	public String getMeasurementTechnique() {
		return this.measurementTechnique;
	}

	public void setMeasurementTechnique(String measurementTechnique) {
		this.measurementTechnique = measurementTechnique;
	}
	
	@Column(name = "purpose", length = 50)
	public String getPurpose() {
		return this.purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	@Column(name = "description", length = 4000)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "name", length = 500)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<ElispotResult> getElispotResults() {
		return this.elispotResults;
	}

	public void setElispotResults(Set<ElispotResult> elispotResults) {
		this.elispotResults = elispotResults;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<FcsAnalyzedResult> getFcsAnalyzedResults() {
		return this.fcsAnalyzedResults;
	}

	public void setFcsAnalyzedResults(Set<FcsAnalyzedResult> fcsAnalyzedResults) {
		this.fcsAnalyzedResults = fcsAnalyzedResults;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<KirTypingResult> getKirTypingResults() {
		return this.kirTypingResults;
	}

	public void setKirTypingResults(Set<KirTypingResult> kirTypingResults) {
		this.kirTypingResults = kirTypingResults;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<HaiResult> getHaiResults() {
		return this.haiResults;
	}

	public void setHaiResults(Set<HaiResult> haiResults) {
		this.haiResults = haiResults;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<NeutAbTiterResult> getNeutAbTiterResults() {
		return this.neutAbTiterResults;
	}

	public void setNeutAbTiterResults(Set<NeutAbTiterResult> neutAbTiterResults) {
		this.neutAbTiterResults = neutAbTiterResults;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<ElisaResult> getElisaResults() {
		return this.elisaResults;
	}

	public void setElisaResults(Set<ElisaResult> elisaResults) {
		this.elisaResults = elisaResults;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "experiment_2_protocol", joinColumns = { @JoinColumn(name = "experiment_accession", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "protocol_accession", nullable = false, updatable = false) })
	public List<Protocol> getProtocols() {
		return this.protocols;
	}

	public void setProtocols(List<Protocol> protocols) {
		this.protocols = protocols;
	}
	
	@Transient
	public List<Reagent> getReagents() {
		return this.reagents;
	}
	
	public void setReagents(List<Reagent> reagents) {
		this.reagents = reagents;
	}
	
	@Transient
	public List<Treatment> getTreatments() {
		return this.treatments;
	}
	
	public void setTreatments(List<Treatment> treatments) {
		this.treatments = treatments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<StandardCurve> getStandardCurves() {
		return this.standardCurves;
	}

	public void setStandardCurves(Set<StandardCurve> standardCurves) {
		this.standardCurves = standardCurves;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<PcrResult> getPcrResults() {
		return this.pcrResults;
	}

	public void setPcrResults(Set<PcrResult> pcrResults) {
		this.pcrResults = pcrResults;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<HlaTypingResult> getHlaTypingResults() {
		return this.hlaTypingResults;
	}

	public void setHlaTypingResults(Set<HlaTypingResult> hlaTypingResults) {
		this.hlaTypingResults = hlaTypingResults;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<MbaaResult> getMbaaResults() {
		return this.mbaaResults;
	}

	public void setMbaaResults(Set<MbaaResult> mbaaResults) {
		this.mbaaResults = mbaaResults;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<Expsample> getExpsamples() {
		return this.expsamples;
	}

	public void setExpsamples(Set<Expsample> expsamples) {
		this.expsamples = expsamples;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "experiment")
	public Set<ControlSample> getControlSamples() {
		return this.controlSamples;
	}

	public void setControlSamples(Set<ControlSample> controlSamples) {
		this.controlSamples = controlSamples;
	}

}

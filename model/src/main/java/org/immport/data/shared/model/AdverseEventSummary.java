package org.immport.data.shared.model;

/**
 * @author fmonteiro
 *
 */
public class AdverseEventSummary {
	
	private String dataType;   
    private String armAccession;
    private String studyAccession;  
    private Integer sortOrder;
    private String orderToSort;
    private long dataCount;
    
    
    public AdverseEventSummary() {
    }

    public AdverseEventSummary
    				(String dataType,
    				 String armAccession,
    				 String studyAccession,
    				 String orderToSort,
    				 long dataCount) {            
            this.dataType = dataType;
            this.armAccession = armAccession;
            this.studyAccession = studyAccession;  
            this.setOrderToSort(orderToSort);
            this.sortOrder = Integer.parseInt(orderToSort);
            this.dataCount = dataCount;            
    }

    public void setDataType(String dataType) {
            this.dataType = dataType;
    }
    
    public String getDataType() {
            return this.dataType;
    }
    
    
	public void setArmAccession(String armAccession) {
		this.armAccession = armAccession;
    }

	public String getArmAccession() {
	        return this.armAccession;
	}
	
	public void setStudyAccession(String studyAccession) {
		 this.studyAccession = studyAccession;
    }

	public String getStudyAccession() {
	        return this.studyAccession;
	}
	
	public void setSortOrder(Integer sortOrder) {
		 this.sortOrder = sortOrder;
   }

	public Integer getSortOrder() {
	        return this.sortOrder;
	}
	 
	 public void setDataCount(long dataCount) {
		 this.dataCount = dataCount;
	 }

	 public long getDataCount() {
         return this.dataCount;
	 }

	public String getOrderToSort() {
		return orderToSort;
	}

	public void setOrderToSort(String orderToSort) {
		this.orderToSort = orderToSort;
	}

}

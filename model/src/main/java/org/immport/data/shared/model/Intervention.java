package org.immport.data.shared.model;

// Generated Dec 19, 2016 4:14:57 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Intervention generated by hbm2java
 */
@Entity
@Table(name = "intervention")
@NamedQueries({
    @NamedQuery(name = "Intervention.getByStudy",
                    query = "SELECT s FROM Intervention s WHERE s.study.studyAccession = :studyAccession ORDER BY s.interventionAccession"),
    
    @NamedQuery(name = "Intervention.getCount",
            query = "SELECT count(distinct s.interventionAccession) FROM Intervention s"),
     
    @NamedQuery(name = "Intervention.getConcomitantMedication",
            query = "SELECT NEW org.immport.data.shared.model.ConcomitantMedicationSummary(m.name, m.armAccession, m.study.studyAccession, s.compoundRole " +
                    " , COUNT(DISTINCT s.compoundNameReported) as compReportNo, COUNT(DISTINCT s.subject.subjectAccession) as subjAccNo) " +
                    " FROM Intervention s JOIN s.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
                    " WHERE s.compoundRole in ('Concomitant Medication') AND m.study.studyAccession = :studyAccession " +
                    " AND s.study.studyAccession = :studyAccession " +
                    " GROUP BY m.name, m.armAccession, m.study.studyAccession, s.compoundRole " +
                    " ORDER BY m.study.studyAccession,m.armAccession"),
   @NamedQuery(name = "Intervention.getSubstanceUse",
                    query = "SELECT NEW org.immport.data.shared.model.ConcomitantMedicationSummary(m.name, m.armAccession, m.study.studyAccession, s.compoundRole " +
                            " , COUNT(DISTINCT s.compoundNameReported) as compReportNo, COUNT(DISTINCT s.subject.subjectAccession) as subjAccNo) " +
                            " FROM Intervention s JOIN s.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
                            " WHERE s.compoundRole in ('Substance Use') AND m.study.studyAccession = :studyAccession " +
                            " AND s.study.studyAccession = :studyAccession " +
                            " GROUP BY m.name, m.armAccession, m.study.studyAccession, s.compoundRole " +
                            " ORDER BY m.study.studyAccession,m.armAccession"),                 

   @NamedQuery(name = "Intervention.getConcomitantMedicationDetail",
            query = "SELECT NEW org.immport.data.shared.model.ConcomitantMedicationDetail(m.name, m.armAccession, m.study.studyAccession, s.compoundNameReported, s.compoundRole " +
                    " , COUNT(DISTINCT s.compoundNameReported) as compReportNo, COUNT(DISTINCT s.subject.subjectAccession) as subjAccNo) " +
                    " FROM Intervention s JOIN s.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
                    "WHERE s.compoundRole in  ('Concomitant Medication')   AND m.study.studyAccession = :studyAccession " +
                    " and s.study.studyAccession = :studyAccession " +
                    " and s.compoundNameReported IN :compoundName " +
                    " GROUP BY m.name, m.armAccession, m.study.studyAccession, s.compoundNameReported, s.compoundRole " +
                    " ORDER BY s.compoundNameReported,m.armAccession"),
   @NamedQuery(name = "Intervention.getSubstanceUseDetail",
                    query = "SELECT NEW org.immport.data.shared.model.ConcomitantMedicationDetail(m.name, m.armAccession, m.study.studyAccession, s.compoundNameReported, s.compoundRole " +
                            " , COUNT(DISTINCT s.compoundNameReported) as compReportNo, COUNT(DISTINCT s.subject.subjectAccession) as subjAccNo) " +
                            " FROM Intervention s JOIN s.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
                            "WHERE s.compoundRole in  ('Substance Use')   AND m.study.studyAccession = :studyAccession " +
                            " and s.study.studyAccession = :studyAccession " +
                            " and s.compoundNameReported IN :compoundName " +
                            " GROUP BY m.name, m.armAccession, m.study.studyAccession, s.compoundNameReported, s.compoundRole " +
                            " ORDER BY s.compoundNameReported,m.armAccession"),                 

   @NamedQuery(name = "Intervention.getConcoMedicationDetailCompNameCnt",
            query = "SELECT    COUNT(DISTINCT s.compoundNameReported) " +
                    "FROM Intervention s JOIN s.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
                    "where s.compoundRole in ('Concomitant Medication') AND m.study.studyAccession = :studyAccession " +
                    " and s.study.studyAccession = :studyAccession " +
                    " GROUP BY s.compoundRole "),
   @NamedQuery(name = "Intervention.getSubstanceUseDetailCompNameCnt",
                    query = "SELECT    COUNT(DISTINCT s.compoundNameReported) " +
                            "FROM Intervention s JOIN s.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
                            "where s.compoundRole in ('Substance Use') AND m.study.studyAccession = :studyAccession " +
                            " and s.study.studyAccession = :studyAccession " +
                            " GROUP BY s.compoundRole "),                 
    
    @NamedQuery(name = "Intervention.getStudyInterventionDetailCount",
  	query = 
  			" SELECT  DISTINCT c.compoundRole,c.compoundNameReported,c.nameReported " +
  			" FROM Intervention c JOIN c.subject j JOIN j.arm2Subjects b JOIN b.armOrCohort a " +
              " WHERE c.study.studyAccession = :studyAccession " +           
              " AND  a.study.studyAccession = :studyAccession "  +
              " AND c.compoundRole in ('Intervention')"
              ) , 
    /*
    @NamedQuery(name = "Intervention.getStudyInterventionDetailCount",
  	query = 
  			" SELECT  DISTINCT c.compoundRole,c.compoundNameReported,c.nameReported " +
  			" FROM Intervention c " +
              " WHERE c.study.studyAccession = :studyAccession " +           
              " AND c.compoundRole in ('Intervention')"
              ) , 
    */
              
    @NamedQuery(name = "Intervention.getDistinctArmNameForIntervention",
            	query = 
            			" SELECT NEW org.immport.data.shared.model.ArmCohortSortOrder(a.armAccession,SUBSTRING(a.armAccession,4)) " +
            			" FROM Intervention c JOIN c.subject j JOIN j.arm2Subjects b JOIN b.armOrCohort a " +
            		    " WHERE c.study.studyAccession = :studyAccession " +           
            		    " AND  a.study.studyAccession = :studyAccession "  +
            		    " AND c.compoundRole in ('Intervention')" +
                        " GROUP BY a.armAccession " +
                        " ORDER BY SUBSTRING(a.armAccession,4)"
                        ),      
    @NamedQuery(name = "Intervention.getDistinctArmNameForConMed",
                    	query = 
                    			" SELECT NEW org.immport.data.shared.model.ArmCohortSortOrder(a.armAccession,SUBSTRING(a.armAccession,4)) " +
                    			" FROM Intervention c JOIN c.subject j JOIN j.arm2Subjects b JOIN b.armOrCohort a " +
                    		    " WHERE c.study.studyAccession = :studyAccession " +           
                    		    " AND  a.study.studyAccession = :studyAccession "  +
                    		    " AND c.compoundRole in ('Concomitant Medication')" +
                                " GROUP BY a.armAccession " +
                                " ORDER BY SUBSTRING(a.armAccession,4)"
                                ),  
    @NamedQuery(name = "Intervention.getDistinctArmNameForSubstanceUse",
                            	query = 
                            			" SELECT NEW org.immport.data.shared.model.ArmCohortSortOrder(a.armAccession,SUBSTRING(a.armAccession,4)) " +
                            			" FROM Intervention c JOIN c.subject j JOIN j.arm2Subjects b JOIN b.armOrCohort a " +
                            		    " WHERE c.study.studyAccession = :studyAccession " +           
                            		    " AND  a.study.studyAccession = :studyAccession "  +
                            		    " AND c.compoundRole in ('Substance Use')" +
                                        " GROUP BY a.armAccession " +
                                        " ORDER BY SUBSTRING(a.armAccession,4)"
                                        ),                               
           
})
public class Intervention implements java.io.Serializable {

	private String interventionAccession;
	private Workspace workspace;
	private Study study;
	private Subject subject;
	//private LkCompoundRole lkCompoundRole;
	private String compoundRole;
	private String compoundNameReported;
	private Float dose;
	private String doseFreqPerInterval;
	private String doseReported;
	private String doseUnits;
	private Float duration;
	private String durationUnit;
	private Float endDay;
	private String endTime;
	private String formulation;
	private String isOngoing;
	private String namePreferred;
	private String nameReported;
	private String reportedIndication;
	private String routeOfAdminPreferred;
	private String routeOfAdminReported;
	private Float startDay;
	private String startTime;
	private String status;

	public Intervention() {
	}

	public Intervention(String interventionAccession, Workspace workspace,
			Study study, Subject subject, String compoundRole,
			String nameReported) {
		this.interventionAccession = interventionAccession;
		this.workspace = workspace;
		this.study = study;
		this.subject = subject;
		this.compoundRole = compoundRole;
		this.nameReported = nameReported;
	}

	public Intervention(String interventionAccession, Workspace workspace,
			Study study, Subject subject, String compoundRole,
			String compoundNameReported, Float dose,
			String doseFreqPerInterval, String doseReported, String doseUnits,
			Float duration, String durationUnit, Float endDay, String endTime,
			String formulation, String isOngoing, String namePreferred,
			String nameReported, String reportedIndication,
			String routeOfAdminPreferred, String routeOfAdminReported,
			Float startDay, String startTime, String status) {
		this.interventionAccession = interventionAccession;
		this.workspace = workspace;
		this.study = study;
		this.subject = subject;
		this.compoundRole = compoundRole;
		this.compoundNameReported = compoundNameReported;
		this.dose = dose;
		this.doseFreqPerInterval = doseFreqPerInterval;
		this.doseReported = doseReported;
		this.doseUnits = doseUnits;
		this.duration = duration;
		this.durationUnit = durationUnit;
		this.endDay = endDay;
		this.endTime = endTime;
		this.formulation = formulation;
		this.isOngoing = isOngoing;
		this.namePreferred = namePreferred;
		this.nameReported = nameReported;
		this.reportedIndication = reportedIndication;
		this.routeOfAdminPreferred = routeOfAdminPreferred;
		this.routeOfAdminReported = routeOfAdminReported;
		this.startDay = startDay;
		this.startTime = startTime;
		this.status = status;
	}

	@Id
	@Column(name = "intervention_accession", unique = true, nullable = false, length = 15)
	public String getInterventionAccession() {
		return this.interventionAccession;
	}

	public void setInterventionAccession(String interventionAccession) {
		this.interventionAccession = interventionAccession;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workspace_id", nullable = false)
	public Workspace getWorkspace() {
		return this.workspace;
	}

	public void setWorkspace(Workspace workspace) {
		this.workspace = workspace;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "study_accession", nullable = false)
	public Study getStudy() {
		return this.study;
	}

	public void setStudy(Study study) {
		this.study = study;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subject_accession", nullable = false)
	public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	//@ManyToOne(fetch = FetchType.LAZY)
	//@JoinColumn(name = "compound_role", nullable = false)
	@Column(name = "compound_role", length = 250)
	public String getCompoundRole() {
		return this.compoundRole;
	}

	public void setCompoundRole(String compoundRole) {
		this.compoundRole = compoundRole;
	}

	@Column(name = "compound_name_reported", length = 250)
	public String getCompoundNameReported() {
		return this.compoundNameReported;
	}

	public void setCompoundNameReported(String compoundNameReported) {
		this.compoundNameReported = compoundNameReported;
	}

	@Column(name = "dose", precision = 12, scale = 0)
	public Float getDose() {
		return this.dose;
	}

	public void setDose(Float dose) {
		this.dose = dose;
	}

	@Column(name = "dose_freq_per_interval", length = 40)
	public String getDoseFreqPerInterval() {
		return this.doseFreqPerInterval;
	}

	public void setDoseFreqPerInterval(String doseFreqPerInterval) {
		this.doseFreqPerInterval = doseFreqPerInterval;
	}

	@Column(name = "dose_reported", length = 150)
	public String getDoseReported() {
		return this.doseReported;
	}

	public void setDoseReported(String doseReported) {
		this.doseReported = doseReported;
	}

	@Column(name = "dose_units", length = 40)
	public String getDoseUnits() {
		return this.doseUnits;
	}

	public void setDoseUnits(String doseUnits) {
		this.doseUnits = doseUnits;
	}

	@Column(name = "duration", precision = 12, scale = 0)
	public Float getDuration() {
		return this.duration;
	}

	public void setDuration(Float duration) {
		this.duration = duration;
	}

	@Column(name = "duration_unit", length = 10)
	public String getDurationUnit() {
		return this.durationUnit;
	}

	public void setDurationUnit(String durationUnit) {
		this.durationUnit = durationUnit;
	}

	@Column(name = "end_day", precision = 12, scale = 0)
	public Float getEndDay() {
		return this.endDay;
	}

	public void setEndDay(Float endDay) {
		this.endDay = endDay;
	}

	@Column(name = "end_time", length = 40)
	public String getEndTime() {
		return this.endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Column(name = "formulation", length = 125)
	public String getFormulation() {
		return this.formulation;
	}

	public void setFormulation(String formulation) {
		this.formulation = formulation;
	}

	@Column(name = "is_ongoing", length = 40)
	public String getIsOngoing() {
		return this.isOngoing;
	}

	public void setIsOngoing(String isOngoing) {
		this.isOngoing = isOngoing;
	}

	@Column(name = "name_preferred", length = 40)
	public String getNamePreferred() {
		return this.namePreferred;
	}

	public void setNamePreferred(String namePreferred) {
		this.namePreferred = namePreferred;
	}

	@Column(name = "name_reported", nullable = false, length = 125)
	public String getNameReported() {
		return this.nameReported;
	}

	public void setNameReported(String nameReported) {
		this.nameReported = nameReported;
	}

	@Column(name = "reported_indication")
	public String getReportedIndication() {
		return this.reportedIndication;
	}

	public void setReportedIndication(String reportedIndication) {
		this.reportedIndication = reportedIndication;
	}

	@Column(name = "route_of_admin_preferred", length = 40)
	public String getRouteOfAdminPreferred() {
		return this.routeOfAdminPreferred;
	}

	public void setRouteOfAdminPreferred(String routeOfAdminPreferred) {
		this.routeOfAdminPreferred = routeOfAdminPreferred;
	}

	@Column(name = "route_of_admin_reported", length = 40)
	public String getRouteOfAdminReported() {
		return this.routeOfAdminReported;
	}

	public void setRouteOfAdminReported(String routeOfAdminReported) {
		this.routeOfAdminReported = routeOfAdminReported;
	}

	@Column(name = "start_day", precision = 12, scale = 0)
	public Float getStartDay() {
		return this.startDay;
	}

	public void setStartDay(Float startDay) {
		this.startDay = startDay;
	}

	@Column(name = "start_time", length = 40)
	public String getStartTime() {
		return this.startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@Column(name = "status", length = 40)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}

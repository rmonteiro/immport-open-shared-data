package org.immport.data.shared.model;

// Generated Dec 19, 2016 4:14:57 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * InclusionExclusion generated by hbm2java
 */
@Entity
@Table(name = "inclusion_exclusion")
@NamedQueries({
    @NamedQuery(name = "InclusionExclusion.getByStudy",
                    query = "SELECT i FROM InclusionExclusion i WHERE i.study.studyAccession = :studyAccession ORDER BY i.criterionCategory DESC,i.criterion ASC"),
    @NamedQuery(name = "InclusionExclusion.getCount",
            query = "SELECT count(distinct i.criterionAccession) FROM InclusionExclusion i")
})
public class InclusionExclusion implements java.io.Serializable {

	private String criterionAccession;
	private Workspace workspace;
	private Study study;
	private String criterionCategory;
	private String criterion;

	public InclusionExclusion() {
	}

	public InclusionExclusion(String criterionAccession, Workspace workspace,
			Study study) {
		this.criterionAccession = criterionAccession;
		this.workspace = workspace;
		this.study = study;
	}

	public InclusionExclusion(String criterionAccession, Workspace workspace,
			Study study, String criterionCategory,
			String criterion) {
		this.criterionAccession = criterionAccession;
		this.workspace = workspace;
		this.study = study;
		this.criterionCategory = criterionCategory;
		this.criterion = criterion;
	}

	@Id
	@Column(name = "criterion_accession", unique = true, nullable = false, length = 15)
	public String getCriterionAccession() {
		return this.criterionAccession;
	}

	public void setCriterionAccession(String criterionAccession) {
		this.criterionAccession = criterionAccession;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workspace_id", nullable = false)
	public Workspace getWorkspace() {
		return this.workspace;
	}

	public void setWorkspace(Workspace workspace) {
		this.workspace = workspace;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "study_accession", nullable = false)
	public Study getStudy() {
		return this.study;
	}

	public void setStudy(Study study) {
		this.study = study;
	}

	@Column(name = "criterion_category", length = 40)
	public String getCriterionCategory() {
		return this.criterionCategory;
	}

	public void setCriterionCategory(String criterionCategory) {
		this.criterionCategory = criterionCategory;
	}

	@Column(name = "criterion", length = 750)
	public String getCriterion() {
		return this.criterion;
	}

	public void setCriterion(String criterion) {
		this.criterion = criterion;
	}

}

/**
 * 
 */
package org.immport.data.shared.model;

/**
 * @author fmonteiro
 *
 */
public class ArmCohortSortOrder implements java.io.Serializable, Comparable<ArmCohortSortOrder> {
	private String armAccession;
	private Integer sortOrder;
	private String orderToSort;
	private String name;
	
	public ArmCohortSortOrder(){

    }
	
	 public ArmCohortSortOrder(String armAccession) {           
         this.armAccession = armAccession;           
        
    }
	 
	 public ArmCohortSortOrder(String armAccession,String orderToSort) {           
         this.armAccession = armAccession;     
         this.orderToSort = orderToSort;
         this.sortOrder = Integer.parseInt(orderToSort);
        
    } 

    public ArmCohortSortOrder(String armAccession,Integer sortOrder) {           
            this.armAccession = armAccession;           
            this.sortOrder = sortOrder;
    }
    
    public ArmCohortSortOrder(String armAccession,Integer sortOrder,String name) {           
        this.armAccession = armAccession;           
        this.sortOrder = sortOrder;
        this.name = name;
    }
    
    public ArmCohortSortOrder(String armAccession,String orderToSort,String name) {           
        this.armAccession = armAccession;           
        this.orderToSort = orderToSort;
        this.sortOrder = Integer.parseInt(orderToSort);
        this.name = name;
    }
    
    public void setarmAccession(String armAccession) {
        this.armAccession = armAccession;
    }

    public String getarmAccession() {
        return this.armAccession;
    }
    
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Integer getSortOrder() {
        return this.sortOrder;
    }
	
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

	public String getOrderToSort() {
		return orderToSort;
	}

	public void setOrderToSort(String orderToSort) {
		this.orderToSort = orderToSort;
	}
	
	public int compareTo(ArmCohortSortOrder o)	
    {
		return Integer.compare(this.sortOrder, o.sortOrder);
    }

}

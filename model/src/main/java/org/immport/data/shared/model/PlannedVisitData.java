package org.immport.data.shared.model;

import java.util.ArrayList;
import java.util.List;

import org.immport.data.shared.model.GenericPivotData;

public class PlannedVisitData implements java.io.Serializable {
	private List<PlannedVisit> plannedVisits = new ArrayList<PlannedVisit>(); 
	private List<PlannedVisitBin> plannedVisitBins = new ArrayList<PlannedVisitBin>();
	private List<GenericPivotData> genericPivotDatas = new ArrayList<GenericPivotData>();
	private int errorCode;
	private String errorMessage;
	
	public void setPlannedVisits(List<PlannedVisit> plannedVisits) {
          this.plannedVisits = plannedVisits;
	}
  
	public List<PlannedVisit> getPlannedVisits() {
          return this.plannedVisits;
	}
	  
	public void setPlannedVisitBins(List<PlannedVisitBin> plannedVisitBins) {
          this.plannedVisitBins = plannedVisitBins;
	}
  
	public List<PlannedVisitBin> getPlannedVisitBins() {
          return this.plannedVisitBins;
	}
	
	public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
        return this.errorMessage;
	}
	
	public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
	}

	public int getErrorCode() {
        return this.errorCode;
	}
	  
	
	public void setGenericPivotData(List<GenericPivotData> genericPivotDatas) {
        this.genericPivotDatas = genericPivotDatas;
	}

	public List<GenericPivotData> getGenericPivotData() {
        return this.genericPivotDatas;
	}
	  
}

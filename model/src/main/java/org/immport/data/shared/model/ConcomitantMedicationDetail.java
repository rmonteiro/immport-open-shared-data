package org.immport.data.shared.model;

public class ConcomitantMedicationDetail implements java.io.Serializable {

    private String  name;
    private String armAccession;
    private String studyAccession;
    private String compoundNameReported;
    private String compoundRole;
    private long compReportNo;
    private long subjAccNo;

    public ConcomitantMedicationDetail()
    {

    }

    public ConcomitantMedicationDetail(String name,String armAccession,String studyAccession, String compoundNameReported,
                    String compoundRole,
                    long compReportNo,
                    long subjAccNo
                    )
    {
            this.name = name;
            this.armAccession = armAccession;
            this.studyAccession = studyAccession;
            this.compoundNameReported = compoundNameReported;
            this.compoundRole = compoundRole;
            this.compReportNo = compReportNo;
            this.subjAccNo = subjAccNo;
    }

    public void setName(String name) {
            this.name = name;
    }

    public String getName() {
            return this.name;
    }

    public void setarmAccession(String armAccession) {
            this.armAccession = armAccession;
    }

    public String getarmAccession() {
            return this.armAccession;
    }

    public void setStudyAccession(String studyAccession) {
            this.studyAccession = studyAccession;
    }

    public String getStudyAccession() {
            return this.studyAccession;
    }

    public void setCompoundRole(String compoundRole) {
            this.compoundRole = compoundRole;
    }

    public String getCompoundRole() {
            return this.compoundRole;
    }

    public void setCompoundNameReported(String compoundNameReported) {
            this.compoundNameReported = compoundNameReported;
    }

    public String getCompoundNameReported() {
            return this.compoundNameReported;
    }

    public void setCompReportNo(long compReportNo) {
            this.compReportNo = compReportNo;
    }

    public long getCompReportNo() {
            return this.compReportNo;
    }

    public void setSubjAccNo(long subjAccNo) {
            this.subjAccNo = subjAccNo;
    }

    public long getSubjAccNo() {
            return this.subjAccNo;
    }
}
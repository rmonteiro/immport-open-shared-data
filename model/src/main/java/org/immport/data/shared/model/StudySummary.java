package org.immport.data.shared.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * Study
 */
@Entity
@Table(name = "study")


public class StudySummary implements java.io.Serializable {

	private String studyAccession;
	private String briefDescription;
	private String briefTitle;
	private String studyType;
	private String clinicalTrial;

	private List <String> researchFocus = new ArrayList<String>(0);
	private List <String> subjectSpecies = new ArrayList<String>(0);
	private List <String> biosampleType = new ArrayList<String>(0);
	private List <String> experimentMeasurementTechnique = new ArrayList<String>(0);
	
	private List <StudyPersonnel> personnel = new ArrayList<StudyPersonnel>(0);

	public StudySummary() {
	}

	public StudySummary(String studyAccession) {
		this.studyAccession = studyAccession;
	}

	public StudySummary(String studyAccession, String briefDescription,
			String briefTitle, String studyType, String clinicalTrial) {
		this.studyAccession = studyAccession;
		this.briefDescription = briefDescription;
		this.briefTitle = briefTitle;
		this.studyType = studyType;
		this.clinicalTrial = clinicalTrial;
	}

	@Id
	@Column(name = "study_accession", unique = true, nullable = false, length = 15)
	public String getStudyAccession() {
		return this.studyAccession;
	}

	public void setStudyAccession(String studyAccession) {
		this.studyAccession = studyAccession;
	}

	@Column(name = "brief_description", length = 4000)
	public String getBriefDescription() {
		return this.briefDescription;
	}

	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}

	@Column(name = "brief_title", length = 250)
	public String getBriefTitle() {
		return this.briefTitle;
	}

	public void setBriefTitle(String briefTitle) {
		this.briefTitle = briefTitle;
	}

	@Column(name = "type")
	public String getStudyType() {
		return this.studyType;
	}

	public void setStudyType(String studyType) {
		this.studyType = studyType;
	}

	@Column(name = "clinical_trial")
	public String getClinicalTrial() {
		return this.clinicalTrial;
	}

	public void setClinicalTrial(String clinicalTrial) {
		this.clinicalTrial = clinicalTrial;
	}
	
	@Transient
	public List <String> getResearchFocus() {
		return this.researchFocus;
	}

	public void setResearchFocus(List <String> researchFocus) {
		this.researchFocus = researchFocus;
	}
	
	@Transient
	public List <String> getSubjectSpecies() {
		return this.subjectSpecies;
	}

	public void setSubjectSpecies(List <String> subjectSpecies) {
		this.subjectSpecies = subjectSpecies;
	}
	
	@Transient
	public List <String> getBiosampleType() {
		return this.biosampleType;
	}

	public void setBiosampleType(List <String> biosampleType) {
		this.biosampleType = biosampleType;
	}
	
	@Transient
	public List <String> getExperimentMeasurementTechnique() {
		return this.experimentMeasurementTechnique;
	}

	public void setExperimentMeasurementTechnique(List <String> experimentMeasurementTechnique) {
		this.experimentMeasurementTechnique = experimentMeasurementTechnique;
	}

	@Transient
	public List<StudyPersonnel> getPersonnel() {
		return personnel;
	}

	public void setPersonnel(List<StudyPersonnel> personnel) {
		this.personnel = personnel;
	}
}

package org.immport.data.shared.model;

// Generated Dec 19, 2016 4:14:57 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * LkLabTestName generated by hbm2java
 */
@Entity
@Table(name = "lk_lab_test_name")
public class LkLabTestName implements java.io.Serializable {

	private String name;
	private String cdiscLabTestCode;
	private String description;
	private String labTestPanelName;
	private String link;
	private Set<LabTest> labTests = new HashSet<LabTest>(0);

	public LkLabTestName() {
	}

	public LkLabTestName(String name) {
		this.name = name;
	}

	public LkLabTestName(String name, String cdiscLabTestCode,
			String description, String labTestPanelName, String link,
			Set<LabTest> labTests) {
		this.name = name;
		this.cdiscLabTestCode = cdiscLabTestCode;
		this.description = description;
		this.labTestPanelName = labTestPanelName;
		this.link = link;
		this.labTests = labTests;
	}

	@Id
	@Column(name = "name", unique = true, nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "cdisc_lab_test_code", length = 50)
	public String getCdiscLabTestCode() {
		return this.cdiscLabTestCode;
	}

	public void setCdiscLabTestCode(String cdiscLabTestCode) {
		this.cdiscLabTestCode = cdiscLabTestCode;
	}

	@Column(name = "description", length = 1000)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "lab_test_panel_name", length = 50)
	public String getLabTestPanelName() {
		return this.labTestPanelName;
	}

	public void setLabTestPanelName(String labTestPanelName) {
		this.labTestPanelName = labTestPanelName;
	}

	@Column(name = "link", length = 2000)
	public String getLink() {
		return this.link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "lkLabTestName")
	public Set<LabTest> getLabTests() {
		return this.labTests;
	}

	public void setLabTests(Set<LabTest> labTests) {
		this.labTests = labTests;
	}

}

package org.immport.data.shared.model;

public class StudyIntervention implements java.io.Serializable {
	
	
	private String studyAccession;
    private String armAccession;    
    private String compoundRole;
    private String compoundNameReported;  
    private String mergeNameReported;  
    private long subjAccNo;
    
    public StudyIntervention() {

    }

    public StudyIntervention(String studyAccession,
    		                 String armAccession,
    		                 String compoundRole,
    		                 String compoundNameReported,
    		                 String mergeNameReported,                   
    		                 long subjAccNo
                    		)
    {
    	    this.studyAccession = studyAccession;
            this.armAccession = armAccession;            
            this.compoundRole = compoundRole;
            this.compoundNameReported = compoundNameReported; 
            this.mergeNameReported = mergeNameReported;
            this.subjAccNo = subjAccNo;
    }

   

    public void setArmAccession(String armAccession) {
            this.armAccession = armAccession;
    }

    public String getArmAccession() {
            return this.armAccession;
    }

    public void setStudyAccession(String studyAccession) {
            this.studyAccession = studyAccession;
    }

    public String getStudyAccession() {
            return this.studyAccession;
    }

    public void setCompoundRole(String compoundRole) {
            this.compoundRole = compoundRole;
    }

    public String getCompoundRole() {
            return this.compoundRole;
    }

    public void setCompoundNameReported(String compoundNameReported) {
            this.compoundNameReported = compoundNameReported;
    }

    public String getCompoundNameReported() {
            return this.compoundNameReported;
    }

    public void setMergeNameReported(String mergeNameReported) {
        this.mergeNameReported = mergeNameReported;
    }

	public String getMergeNameReported() {
	        return this.mergeNameReported;
	}
    
    
   public void setSubjAccNo(long subjAccNo) {
            this.subjAccNo = subjAccNo;
    }

    public long getSubjAccNo() {
            return this.subjAccNo;
    }

}


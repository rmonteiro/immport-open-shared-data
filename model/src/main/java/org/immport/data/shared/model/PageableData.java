package org.immport.data.shared.model;

import java.util.ArrayList;
import java.util.List;

public class PageableData {
    private long totalCount;
    private List data = new ArrayList();
    
    public PageableData() {
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }
}
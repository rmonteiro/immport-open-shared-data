package org.immport.data.shared.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.immport.data.shared.model.GenericPivotData;

public class AssessmentStudyTimeCollected extends GenericPivotData implements java.io.Serializable {

	  
	   // private String panelNameReported;
	   // private String componentNameReported;   
	   // private String verbatimQuestion; 
	    

	    public AssessmentStudyTimeCollected() {
	    }

	    public AssessmentStudyTimeCollected(String studyAccession,String panelNameReported,
	    		BigDecimal studyTimeCollected,long countOfBioSamples) {
	           
	          //  this.panelNameReported = panelNameReported;
	          //  this.componentNameReported = componentNameReported;
	          //  this.verbatimQuestion = verbatimQuestion;     
	            this.setStudyAccession(studyAccession);
	            List<String> objBaseData = new ArrayList<String>();
		        List<String> objBaseNames = new ArrayList<String>();
		        List<String> objBaseNamesSort = new ArrayList<String>();
				List<String> baseNamesSortParameter = new ArrayList<String>();
				
		        objBaseNames.add("Assessment Panel Name");
		        objBaseNames.add("Study Day");
		        objBaseNames.add("Number of Subjects");
		        
		        objBaseNamesSort.add("true");
		        objBaseNamesSort.add("true");
		        objBaseNamesSort.add("false");
		        
		        baseNamesSortParameter.add("panelNameReported");
		        baseNamesSortParameter.add("studyDay");
		        
		        objBaseData.add(panelNameReported);
		        objBaseData.add(String.valueOf(studyTimeCollected.doubleValue()));
		        objBaseData.add(String.valueOf(countOfBioSamples));
		        
		        this.setBaseData(objBaseData);
		        this.setBaseNames(objBaseNames);
		        this.setBaseNamesSort(objBaseNamesSort);
		        this.setBaseNamesSortParameter(baseNamesSortParameter);
		        
	            
	    }
	    
	    
	    
	   /* public void setPanelNameReported(String panelNameReported) {
	        this.panelNameReported = panelNameReported;
	    }

	    public String getPanelNameReported() {
	        return this.panelNameReported;
	    }

	    
	    public void setComponentNameReported(String componentNameReported) {
	        this.componentNameReported = componentNameReported;
	    }

	    public String getComponentNameReported() {
	        return this.componentNameReported;
	    }
	    
	    public void setVerbatimQuestion(String verbatimQuestion) {
	        this.verbatimQuestion = verbatimQuestion;
	    }

	    public String getVerbatimQuestion() {
	        return this.verbatimQuestion;
	    }*/
	    
	}


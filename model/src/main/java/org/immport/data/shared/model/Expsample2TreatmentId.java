package org.immport.data.shared.model;

// Generated Dec 19, 2016 4:26:25 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Expsample2TreatmentId generated by hbm2java
 */
@Embeddable
public class Expsample2TreatmentId implements java.io.Serializable {

	private String expsampleAccession;
	private String treatmentAccession;

	public Expsample2TreatmentId() {
	}

	public Expsample2TreatmentId(String expsampleAccession,
			String treatmentAccession) {
		this.expsampleAccession = expsampleAccession;
		this.treatmentAccession = treatmentAccession;
	}

	@Column(name = "expsample_accession", nullable = false, length = 15)
	public String getExpsampleAccession() {
		return this.expsampleAccession;
	}

	public void setExpsampleAccession(String expsampleAccession) {
		this.expsampleAccession = expsampleAccession;
	}

	@Column(name = "treatment_accession", nullable = false, length = 15)
	public String getTreatmentAccession() {
		return this.treatmentAccession;
	}

	public void setTreatmentAccession(String treatmentAccession) {
		this.treatmentAccession = treatmentAccession;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof Expsample2TreatmentId))
			return false;
		Expsample2TreatmentId castOther = (Expsample2TreatmentId) other;

		return ((this.getExpsampleAccession() == castOther
				.getExpsampleAccession()) || (this.getExpsampleAccession() != null
				&& castOther.getExpsampleAccession() != null && this
				.getExpsampleAccession().equals(
						castOther.getExpsampleAccession())))
				&& ((this.getTreatmentAccession() == castOther
						.getTreatmentAccession()) || (this
						.getTreatmentAccession() != null
						&& castOther.getTreatmentAccession() != null && this
						.getTreatmentAccession().equals(
								castOther.getTreatmentAccession())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getExpsampleAccession() == null ? 0 : this
						.getExpsampleAccession().hashCode());
		result = 37
				* result
				+ (getTreatmentAccession() == null ? 0 : this
						.getTreatmentAccession().hashCode());
		return result;
	}

}

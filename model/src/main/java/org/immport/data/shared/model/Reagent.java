package org.immport.data.shared.model;

// Generated Dec 19, 2016 4:14:57 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Reagent generated by hbm2java
 */
@Entity
@Table(name = "reagent")
@NamedQueries({
	@NamedQuery(name = "Reagent.getByExperiment",
    query = "SELECT DISTINCT r FROM Reagent r join r.expsamples e" +
            " WHERE r.isSet != 'Y' AND e.experiment.experimentAccession = :experimentAccession " +
    		" ORDER BY e.experiment.experimentAccession ")
})
public class Reagent implements java.io.Serializable {

	private String reagentAccession;
	//private Workspace workspace;
	private String analytePreferred;
	private String analyteReported;
	private String antibodyRegistryId;
	private String catalogNumber;
	private String cloneName;
	private String contact;
	private String description;
	private String isSet;
	private String lotNumber;
	private String manufacturer;
	private String name;
	private String reporterName;
	private String type;
	private String weblink;
	private Set<Expsample> expsamples = new HashSet<Expsample>(0);
	private Set<ReagentSet2Reagent> reagentSet2ReagentsForReagentSetAccession = new HashSet<ReagentSet2Reagent>(
			0);
	private Set<ReagentSet2Reagent> reagentSet2ReagentsForReagentAccession = new HashSet<ReagentSet2Reagent>(
			0);

	public Reagent() {
	}

	/*
	public Reagent(String reagentAccession, Workspace workspace) {
		this.reagentAccession = reagentAccession;
		this.workspace = workspace;
	}
	*/
	public Reagent(String reagentAccession) {
		this.reagentAccession = reagentAccession;
	}

	public Reagent(String reagentAccession, //Workspace workspace,
			String analytePreferred, String analyteReported,
			String antibodyRegistryId, String catalogNumber, String cloneName,
			String contact, String description, String isSet, String lotNumber,
			String manufacturer, String name, String reporterName, String type,
			String weblink, Set<Expsample> expsamples,
			Set<ReagentSet2Reagent> reagentSet2ReagentsForReagentSetAccession,
			Set<ReagentSet2Reagent> reagentSet2ReagentsForReagentAccession) {
		this.reagentAccession = reagentAccession;
		//this.workspace = workspace;
		this.analytePreferred = analytePreferred;
		this.analyteReported = analyteReported;
		this.antibodyRegistryId = antibodyRegistryId;
		this.catalogNumber = catalogNumber;
		this.cloneName = cloneName;
		this.contact = contact;
		this.description = description;
		this.isSet = isSet;
		this.lotNumber = lotNumber;
		this.manufacturer = manufacturer;
		this.name = name;
		this.reporterName = reporterName;
		this.type = type;
		this.weblink = weblink;
		this.expsamples = expsamples;
		this.reagentSet2ReagentsForReagentSetAccession = reagentSet2ReagentsForReagentSetAccession;
		this.reagentSet2ReagentsForReagentAccession = reagentSet2ReagentsForReagentAccession;
	}

	@Id
	@Column(name = "reagent_accession", unique = true, nullable = false, length = 15)
	public String getReagentAccession() {
		return this.reagentAccession;
	}

	public void setReagentAccession(String reagentAccession) {
		this.reagentAccession = reagentAccession;
	}

	/*
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workspace_id", nullable = false)
	public Workspace getWorkspace() {
		return this.workspace;
	}

	public void setWorkspace(Workspace workspace) {
		this.workspace = workspace;
	}
	*/

	@Column(name = "analyte_preferred", length = 200)
	public String getAnalytePreferred() {
		return this.analytePreferred;
	}

	public void setAnalytePreferred(String analytePreferred) {
		this.analytePreferred = analytePreferred;
	}

	@Column(name = "analyte_reported", length = 200)
	public String getAnalyteReported() {
		return this.analyteReported;
	}

	public void setAnalyteReported(String analyteReported) {
		this.analyteReported = analyteReported;
	}

	@Column(name = "antibody_registry_id", length = 250)
	public String getAntibodyRegistryId() {
		return this.antibodyRegistryId;
	}

	public void setAntibodyRegistryId(String antibodyRegistryId) {
		this.antibodyRegistryId = antibodyRegistryId;
	}

	@Column(name = "catalog_number", length = 250)
	public String getCatalogNumber() {
		return this.catalogNumber;
	}

	public void setCatalogNumber(String catalogNumber) {
		this.catalogNumber = catalogNumber;
	}

	@Column(name = "clone_name", length = 200)
	public String getCloneName() {
		return this.cloneName;
	}

	public void setCloneName(String cloneName) {
		this.cloneName = cloneName;
	}

	@Column(name = "contact", length = 1000)
	public String getContact() {
		return this.contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	@Column(name = "description", length = 4000)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "is_set", length = 1)
	public String getIsSet() {
		return this.isSet;
	}

	public void setIsSet(String isSet) {
		this.isSet = isSet;
	}

	@Column(name = "lot_number", length = 250)
	public String getLotNumber() {
		return this.lotNumber;
	}

	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}

	@Column(name = "manufacturer", length = 100)
	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	@Column(name = "name", length = 200)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "reporter_name", length = 200)
	public String getReporterName() {
		return this.reporterName;
	}

	public void setReporterName(String reporterName) {
		this.reporterName = reporterName;
	}

	@Column(name = "type", length = 50)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "weblink", length = 250)
	public String getWeblink() {
		return this.weblink;
	}

	public void setWeblink(String weblink) {
		this.weblink = weblink;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "reagents")
	public Set<Expsample> getExpsamples() {
		return this.expsamples;
	}

	public void setExpsamples(Set<Expsample> expsamples) {
		this.expsamples = expsamples;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "reagentByReagentSetAccession")
	public Set<ReagentSet2Reagent> getReagentSet2ReagentsForReagentSetAccession() {
		return this.reagentSet2ReagentsForReagentSetAccession;
	}

	public void setReagentSet2ReagentsForReagentSetAccession(
			Set<ReagentSet2Reagent> reagentSet2ReagentsForReagentSetAccession) {
		this.reagentSet2ReagentsForReagentSetAccession = reagentSet2ReagentsForReagentSetAccession;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "reagentByReagentAccession")
	public Set<ReagentSet2Reagent> getReagentSet2ReagentsForReagentAccession() {
		return this.reagentSet2ReagentsForReagentAccession;
	}

	public void setReagentSet2ReagentsForReagentAccession(
			Set<ReagentSet2Reagent> reagentSet2ReagentsForReagentAccession) {
		this.reagentSet2ReagentsForReagentAccession = reagentSet2ReagentsForReagentAccession;
	}

}

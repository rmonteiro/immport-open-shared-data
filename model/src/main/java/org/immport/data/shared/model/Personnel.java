package org.immport.data.shared.model;

// Generated Dec 19, 2016 4:14:57 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Personnel generated by hbm2java
 */
@Entity
@Table(name = "personnel")
public class Personnel implements java.io.Serializable {

	private int personnelId;
	private String email;
	private String firstName;
	private String lastName;
	private String organization;
	private Set<ContractGrant2Personnel> contractGrant2Personnels = new HashSet<ContractGrant2Personnel>(
			0);
	private Set<Program2Personnel> program2Personnels = new HashSet<Program2Personnel>(
			0);

	public Personnel() {
	}

	public Personnel(int personnelId, String firstName, String lastName,
			String organization) {
		this.personnelId = personnelId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.organization = organization;
	}

	public Personnel(int personnelId, String email, String firstName,
			String lastName, String organization,
			Set<ContractGrant2Personnel> contractGrant2Personnels,
			Set<Program2Personnel> program2Personnels) {
		this.personnelId = personnelId;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.organization = organization;
		this.contractGrant2Personnels = contractGrant2Personnels;
		this.program2Personnels = program2Personnels;
	}

	@Id
	@Column(name = "personnel_id", unique = true, nullable = false)
	public int getPersonnelId() {
		return this.personnelId;
	}

	public void setPersonnelId(int personnelId) {
		this.personnelId = personnelId;
	}

	@Column(name = "email", length = 100)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "first_name", nullable = false, length = 50)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name", nullable = false, length = 50)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "organization", nullable = false, length = 125)
	public String getOrganization() {
		return this.organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "personnel")
	public Set<ContractGrant2Personnel> getContractGrant2Personnels() {
		return this.contractGrant2Personnels;
	}

	public void setContractGrant2Personnels(
			Set<ContractGrant2Personnel> contractGrant2Personnels) {
		this.contractGrant2Personnels = contractGrant2Personnels;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "personnel")
	public Set<Program2Personnel> getProgram2Personnels() {
		return this.program2Personnels;
	}

	public void setProgram2Personnels(Set<Program2Personnel> program2Personnels) {
		this.program2Personnels = program2Personnels;
	}

}

/**
 * 
 */
package org.immport.data.shared.model;

/**
 * @author fmonteiro
 *
 */
public class MechanisticAssay implements java.io.Serializable  {
	private String experimentAccession;
	private String title;
	private String purpose;
	private String measurementTechnique;
	private String countExpSamples;
	
	public MechanisticAssay(String experimentAccession,String title,
		     String purpose,String measurementTechnique,String countExpSamples) {
		this.experimentAccession = experimentAccession;
		this.title = title;
		this.purpose = purpose;
		this.measurementTechnique = measurementTechnique;
		this.countExpSamples = countExpSamples;		
	}
	
	public void setExperimentAccession(String experimentAccession) {
        this.experimentAccession = experimentAccession;
    }

    public String getExperimentAccession() {
        return this.experimentAccession;
    }
    
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }
    
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getPurpose() {
        return this.purpose;
    }
    
    public void setMeasurementTechnique(String measurementTechnique) {
        this.measurementTechnique = measurementTechnique;
    }

    public String getMeasurementTechnique() {
        return this.measurementTechnique;
    }
    
    public void setCountExpSamples(String countExpSamples) {
        this.countExpSamples = countExpSamples;
    }

    public String getCountExpSamples() {
        return this.countExpSamples;
    }

}

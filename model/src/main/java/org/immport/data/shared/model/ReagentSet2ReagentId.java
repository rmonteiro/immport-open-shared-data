package org.immport.data.shared.model;

// Generated Dec 19, 2016 4:14:57 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ReagentSet2ReagentId generated by hbm2java
 */
@Embeddable
public class ReagentSet2ReagentId implements java.io.Serializable {

	private String reagentSetAccession;
	private String reagentAccession;

	public ReagentSet2ReagentId() {
	}

	public ReagentSet2ReagentId(String reagentSetAccession,
			String reagentAccession) {
		this.reagentSetAccession = reagentSetAccession;
		this.reagentAccession = reagentAccession;
	}

	@Column(name = "reagent_set_accession", nullable = false, length = 15)
	public String getReagentSetAccession() {
		return this.reagentSetAccession;
	}

	public void setReagentSetAccession(String reagentSetAccession) {
		this.reagentSetAccession = reagentSetAccession;
	}

	@Column(name = "reagent_accession", nullable = false, length = 15)
	public String getReagentAccession() {
		return this.reagentAccession;
	}

	public void setReagentAccession(String reagentAccession) {
		this.reagentAccession = reagentAccession;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ReagentSet2ReagentId))
			return false;
		ReagentSet2ReagentId castOther = (ReagentSet2ReagentId) other;

		return ((this.getReagentSetAccession() == castOther
				.getReagentSetAccession()) || (this.getReagentSetAccession() != null
				&& castOther.getReagentSetAccession() != null && this
				.getReagentSetAccession().equals(
						castOther.getReagentSetAccession())))
				&& ((this.getReagentAccession() == castOther
						.getReagentAccession()) || (this.getReagentAccession() != null
						&& castOther.getReagentAccession() != null && this
						.getReagentAccession().equals(
								castOther.getReagentAccession())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getReagentSetAccession() == null ? 0 : this
						.getReagentSetAccession().hashCode());
		result = 37
				* result
				+ (getReagentAccession() == null ? 0 : this
						.getReagentAccession().hashCode());
		return result;
	}

}

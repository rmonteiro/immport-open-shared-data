package org.immport.data.shared.model;

public class DemograhicSummary implements java.io.Serializable {
	private String statistic;
	private String armAccession;
	private String unit;
	private Float data;
	private double avgData;
	private long subjAccNo;
	private String displayData;

	
	public DemograhicSummary() {
    }

    public DemograhicSummary(String statistic,String armAccession,String unit,                 
                    Float data) {  
    		this.statistic = statistic;
            this.armAccession = armAccession;
            this.unit = unit;
            this.data = data;           
    }
    
    public DemograhicSummary(String statistic,String armAccession,String unit,                 
    		double avgData) {  
	this.statistic = statistic;
    this.armAccession = armAccession;
    this.unit = unit;
    this.avgData = avgData; 
    this.data = (float)avgData;
    }
    
    public DemograhicSummary(String statistic,String armAccession,String unit,                 
    		long subjAccNo) {  
	this.statistic = statistic;
    this.armAccession = armAccession;
    this.unit = unit;
    this.subjAccNo =  subjAccNo;
    
    }

    public void setStatistic(String statistic) {
            this.statistic = statistic;
    }
    
    public String getStatistic() {
            return this.statistic;
    }

    public void setArmAccession(String armAccession) {
            this.armAccession = armAccession;
    }

    public String getArmAccession() {
            return this.armAccession;
    }

    public void setUnit(String unit) {
            this.unit = unit;
    }

    public String getUnit() {
            return this.unit;
    }
	
    public void setData(Float data) {
        this.data = data;
    }

    public Float getData() {
        return this.data;
    }
	
    public void setAvgData(double avgData) {
        this.avgData = avgData;
        this.data = (float)avgData;
    }

    public double getAvgData() {
        return this.avgData;
    }
    

    public void setSubjAccNo(long subjAccNo) {
        this.subjAccNo = subjAccNo;        
    }

    public long getSubjAccNo() {
        return this.subjAccNo;
    }
	

    public void setDisplayData(String displayData) {
        this.displayData = displayData;      
    }

    public String getDisplayData() {
        return this.displayData;
    }
    
}

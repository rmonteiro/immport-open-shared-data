package org.immport.data.shared.model;

// Generated Dec 19, 2016 4:14:57 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * LkPersonnelRole generated by hbm2java
 */
@Entity
@Table(name = "lk_personnel_role")
public class LkPersonnelRole implements java.io.Serializable {

	private String name;
	private String description;
	private String link;
	//private Set<StudyPersonnel> studyPersonnels = new HashSet<StudyPersonnel>(0);

	public LkPersonnelRole() {
	}

	public LkPersonnelRole(String name) {
		this.name = name;
	}

	public LkPersonnelRole(String name, String description, String link) {
		this.name = name;
		this.description = description;
		this.link = link;
		//this.studyPersonnels = studyPersonnels;
	}

	@Id
	@Column(name = "name", unique = true, nullable = false, length = 40)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 1000)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "link", length = 2000)
	public String getLink() {
		return this.link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	/*
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "lkPersonnelRole")
	public Set<StudyPersonnel> getStudyPersonnels() {
		return this.studyPersonnels;
	}

	public void setStudyPersonnels(Set<StudyPersonnel> studyPersonnels) {
		this.studyPersonnels = studyPersonnels;
	}
	*/

}

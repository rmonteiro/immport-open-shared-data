package org.immport.data.shared.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
* SubjectDemographic
*/
@Entity
public class SubjectDemographic implements java.io.Serializable {

	@Id
	private String subjectAccession;
	private String ageEvent;
	private String ageEventSpecify;
	private BigDecimal ageReported;
	private String ageUnit;
	private String ethnicity;
	private String gender;
	private String phenotype;
	private String populationName;
	private String race;
	private String raceSpecify;
	private String species;
	private String strain;
	private String taxonomyId;
	private String armAccession;
	private String armName;
	
	public SubjectDemographic() {};

	public SubjectDemographic(String subjectAccession,
			String ageEvent,
			String ageEventSpecify,
			BigDecimal ageReported,
			String ageUnit,
			String ethnicity,
			String gender,
			String phenotype,
			String populationName,
			String race,
			String raceSpecify,
			String species,
			String strain,
			String taxonomyId,
			String armAccession,
			String armName
			) {
		this.subjectAccession = subjectAccession;
		this.ageEvent = ageEvent;
		this.ageEventSpecify = ageEventSpecify;
		this.ageReported = ageReported;
		this.ageUnit = ageUnit;
		this.ethnicity = ethnicity;
		this.gender = gender;
		this.phenotype = phenotype;
		this.populationName = populationName;
		this.race = race;
		this.raceSpecify = raceSpecify;
		this.species = species;
		this.strain = strain;
		this.taxonomyId = taxonomyId;
		this.armAccession = armAccession;
		this.armName = armName;
	}

	public String getSubjectAccession() {
		return subjectAccession;
	}

	public void setSubjectAccession(String subjectAccession) {
		this.subjectAccession = subjectAccession;
	}

	public String getAgeEvent() {
		return ageEvent;
	}

	public void setAgeEvent(String ageEvent) {
		this.ageEvent = ageEvent;
	}

	public String getAgeEventSpecify() {
		return ageEventSpecify;
	}

	public void setAgeEventSpecify(String ageEventSpecify) {
		this.ageEventSpecify = ageEventSpecify;
	}

	public BigDecimal getAgeReported() {
		return ageReported;
	}

	public void setAgeReported(BigDecimal ageReported) {
		this.ageReported = ageReported;
	}

	public String getAgeUnit() {
		return ageUnit;
	}

	public void setAgeUnit(String ageUnit) {
		this.ageUnit = ageUnit;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhenotype() {
		return phenotype;
	}

	public void setPhenotype(String phenotype) {
		this.phenotype = phenotype;
	}

	public String getPopulationName() {
		return populationName;
	}

	public void setPopulationName(String populationName) {
		this.populationName = populationName;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getRaceSpecify() {
		return raceSpecify;
	}

	public void setRaceSpecify(String raceSpecify) {
		this.raceSpecify = raceSpecify;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getStrain() {
		return strain;
	}

	public void setStrain(String strain) {
		this.strain = strain;
	}

	public String getTaxonomyId() {
		return taxonomyId;
	}

	public void setTaxonomyId(String taxonomyId) {
		this.taxonomyId = taxonomyId;
	}

	public String getArmAccession() {
		return armAccession;
	}

	public void setArmAccession(String armAccession) {
		this.armAccession = armAccession;
	}

	public String getArmName() {
		return armName;
	}

	public void setArmName(String armName) {
		this.armName = armName;
	}

}

/**
 * 
 */
package org.immport.data.shared.model;

/**
 * @author fmonteiro
 *
 */
public class AdverseEventDetail {
	
	
	private String name;
    private String armAccession;
    private String studyAccession;
    private String nameReported;
    private String severityReported;  
    private long subjAccNo;
    
    public AdverseEventDetail() {

    }

    public AdverseEventDetail(String name,String armAccession,String studyAccession, String nameReported,
                    String severityReported,                   
                    long subjAccNo
                    )
    {
            this.name = name;
            this.armAccession = armAccession;
            this.studyAccession = studyAccession;
            this.nameReported = nameReported;
            this.severityReported = severityReported;          
            this.subjAccNo = subjAccNo;
    }

    public void setName(String name) {
            this.name = name;
    }

    public String getName() {
            return this.name;
    }

    public void setarmAccession(String armAccession) {
            this.armAccession = armAccession;
    }

    public String getarmAccession() {
            return this.armAccession;
    }

    public void setStudyAccession(String studyAccession) {
            this.studyAccession = studyAccession;
    }

    public String getStudyAccession() {
            return this.studyAccession;
    }

    public void setNameReported(String nameReported) {
            this.nameReported = nameReported;
    }

    public String getNameReported() {
            return this.nameReported;
    }

    public void setSeverityReported(String severityReported) {
            this.severityReported = severityReported;
    }

    public String getSeverityReported() {
            return this.severityReported;
    }

   public void setSubjAccNo(long subjAccNo) {
            this.subjAccNo = subjAccNo;
    }

    public long getSubjAccNo() {
            return this.subjAccNo;
    }

}

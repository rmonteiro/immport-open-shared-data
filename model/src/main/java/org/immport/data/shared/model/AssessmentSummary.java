/**
 * 
 */
package org.immport.data.shared.model;

/**
 * @author fmonteiro
 *
 */
public class AssessmentSummary implements java.io.Serializable {

    private String  panelNameReported;
    private String armAccession;
    private String studyAccession; 
    private long subjAccNo;
    private long assessmentNo;
    

    public AssessmentSummary() {
    }

    public AssessmentSummary(String panelNameReported,String armAccession,String studyAccession,                 
                    long subjAccNo,
                    long assessmentNo) {
            this.panelNameReported = panelNameReported;
            this.armAccession = armAccession;
            this.studyAccession = studyAccession;
            this.subjAccNo = subjAccNo;
            this.assessmentNo = assessmentNo;
            
    }

    public void setPanelNameReported(String panelNameReported) {
            this.panelNameReported = panelNameReported;
    }
    
    public String getPanelNameReported() {
            return this.panelNameReported;
    }

    public void setArmAccession(String armAccession) {
            this.armAccession = armAccession;
    }

    public String getArmAccession() {
            return this.armAccession;
    }

    public void setStudyAccession(String studyAccession) {
            this.studyAccession = studyAccession;
    }

    public String getStudyAccession() {
            return this.studyAccession;
    }

   

    public void setAssessmentNo(long assessmentNo) {
            this.assessmentNo = assessmentNo;
    }
    
    public long getAssessmentNo() {
            return this.assessmentNo;
    }

    public void setSubjAccNo(long subjAccNo) {
            this.subjAccNo = subjAccNo;
    }

    public long getSubjAccNo() {
            return this.subjAccNo;
    }

}
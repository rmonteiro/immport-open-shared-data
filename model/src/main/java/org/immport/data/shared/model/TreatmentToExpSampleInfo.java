package org.immport.data.shared.model;

public class TreatmentToExpSampleInfo implements java.io.Serializable {
	
	private String studyAccession;
	private String experimentAccession;
	private String experimentTitle;
	private String treatmentName;
	private String expsampleAccessionCount;
	
	public TreatmentToExpSampleInfo() {
    }

    public TreatmentToExpSampleInfo
    		(String studyAccession,
    		 String experimentAccession,
    		 String experimentTitle,
    		 String treatmentName,
    		 Long expsampleAccessionCount ) {
    	this.studyAccession = studyAccession;
    	this.experimentAccession = experimentAccession;
    	this.experimentTitle = experimentTitle;
    	this.treatmentName = treatmentName;
    	this.expsampleAccessionCount = String.valueOf(expsampleAccessionCount);
    }
    
    public void setStudyAccession(String studyAccession) {
        this.studyAccession = studyAccession;
    }

    public String getStudyAccessionn() {
        return this.studyAccession;
    }

       
    public void setExperimentAccession(String experimentAccession) {
        this.experimentAccession = experimentAccession;
    }

    public String getExperimentAccession() {
        return this.experimentAccession;
    }
    
    
    public void setExperimentTitle(String experimentTitle) {
        this.experimentTitle = experimentTitle;
    }

    public String getExperimentTitle() {
        return this.experimentTitle;
    }
    
    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    public String getTreatmentName() {
        return this.treatmentName;
    }

    
    public void setExpsampleAccessionCount(String expsampleAccessionCount) {
        this.expsampleAccessionCount = expsampleAccessionCount;
    }

    public String getExpsampleAccessionCount() {
        return this.expsampleAccessionCount;
    }
}

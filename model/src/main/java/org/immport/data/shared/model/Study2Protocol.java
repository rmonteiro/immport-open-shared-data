package org.immport.data.shared.model;

// Generated Dec 19, 2016 4:26:25 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Study2Protocol generated by hbm2java
 */
@Entity
@Table(name = "study_2_protocol")
public class Study2Protocol implements java.io.Serializable {

	private Study2ProtocolId id;

	public Study2Protocol() {
	}

	public Study2Protocol(Study2ProtocolId id) {
		this.id = id;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "studyAccession", column = @Column(name = "study_accession", nullable = false, length = 15)),
			@AttributeOverride(name = "protocolAccession", column = @Column(name = "protocol_accession", nullable = false, length = 15)) })
	public Study2ProtocolId getId() {
		return this.id;
	}

	public void setId(Study2ProtocolId id) {
		this.id = id;
	}

}

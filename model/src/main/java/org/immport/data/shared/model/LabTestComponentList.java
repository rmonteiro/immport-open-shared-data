/**
 * 
 */
package org.immport.data.shared.model;

import java.util.ArrayList;
import java.util.List;

import org.immport.data.shared.model.GenericPivotData;

/**
 * @author fmonteiro
 *
 */
public class LabTestComponentList extends GenericPivotData implements java.io.Serializable {

  
   // private String panelNameReported;
  //  private String testNameReported;   
   

    public LabTestComponentList() {
    }

    public LabTestComponentList(String studyAccession,String panelNameReported,
    					     String testNameReported) {
            super(studyAccession);
          //  this.panelNameReported = panelNameReported;
          //  this.testNameReported = testNameReported;
           
            
            
            List<String> objBaseData = new ArrayList<String>();
	        List<String> objBaseNames = new ArrayList<String>();
	        List<String> objBaseNamesSort = new ArrayList<String>();
			List<String> baseNamesSortParameter = new ArrayList<String>();
			
	        objBaseNames.add("Lab Test Panel");
	        objBaseNames.add("Lab Test");
	        
	        objBaseNamesSort.add("true");
	        objBaseNamesSort.add("true");
	        
	        baseNamesSortParameter.add("panelNameReported");
	        baseNamesSortParameter.add("nameReported");
	        
	        objBaseData.add(panelNameReported);
	        objBaseData.add(testNameReported);
	        
	        this.setBaseData(objBaseData);
	        this.setBaseNames(objBaseNames);
	        this.setBaseNamesSort(objBaseNamesSort);
	        this.setBaseNamesSortParameter(baseNamesSortParameter);
            
    }
    
    
    
    /*public void setPanelNameReported(String panelNameReported) {
        this.panelNameReported = panelNameReported;
    }

    public String getPanelNameReported() {
        return this.panelNameReported;
    }

    
    public void setTestNameReported(String testNameReported) {
        this.testNameReported = testNameReported;
    }

    public String getTestNameReported() {
        return this.testNameReported;
    }*/
    
    
    
}


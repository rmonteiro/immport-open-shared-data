package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.DemograhicSummary;
import org.immport.data.shared.model.Subject;
import org.immport.data.shared.model.SubjectDemographic;
import org.immport.data.shared.repository.SubjectRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the Subject Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class SubjectRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SubjectRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The Subject repository. */
    @Autowired
    SubjectRepository subjectRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the Subject object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("subject");
        log.info("id = " + id);
        Subject subject = subjectRepository.findById(id);
        assertNotNull("The Subject object is null.", subject);
    }

    /**
     * Test getting a PageableData of Subject objects
     */
    @Test
    public void getPageableData() {
        PageableData page = subjectRepository.getPageableData(0,10,"s.subjectAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The Subject count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of Subject objects by study accession
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <Subject> results = subjectRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The Subject count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }
    
    /**
     * Test getting a List of SubjectDemographic objects by study accession
     */
    @Test
    public void getSubjectDemographicsByStudy() {
        String id = resources.getString("studyAccession");
        List <SubjectDemographic> results = subjectRepository.getSubjectDemographicsByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The Subject count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }
    
    @Test
    public void getDemograhicSummaryByStudy() {
    	 String id = resources.getString("studyAccession");    	
    	 List<DemograhicSummary>  resultObjects = subjectRepository.getDemograhicSummaryByStudy(id);
    	 
    	 assertNotNull("The results is null.", resultObjects);
    	 assertTrue("The Result count is greater than zero.", resultObjects.size() > 0);
    	 log.info("Count: " + resultObjects.size());
    	 
    	
    	//display
    	 for (int cnt=0;cnt<resultObjects.size(); cnt++) { 
    		 DemograhicSummary obj = (DemograhicSummary)resultObjects.get(cnt);
    		 if (obj.getArmAccession() == null)
    		 {
    			 log.info(obj.getArmAccession() );
    		 }
    		 else
    		 {
    			 log.info(obj.getStatistic() + " , " 
    					 + obj.getArmAccession() + " , " + String.valueOf(obj.getUnit()) 
    					 + " , " + obj.getDisplayData());
    		 }
    	 }
    	
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

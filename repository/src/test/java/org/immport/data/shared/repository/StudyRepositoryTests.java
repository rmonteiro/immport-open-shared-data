package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Study;
import org.immport.data.shared.model.StudySummary;
import org.immport.data.shared.repository.StudyRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the Study Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class StudyRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The Study repository. */
    @Autowired
    StudyRepository studyRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the Study object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("study");
        log.info("id = " + id);
        Study study = studyRepository.findById(id);
        assertNotNull("The Study object is null.", study);
    }

    /**
     * Test getting a PageableData of Study objects
     */
    @Test
    public void getPageableData() {
        PageableData page = studyRepository.getPageableData(0,10,"s.studyAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The Study count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }
    
    /**
     * Tests getting the StudySummary object by id.
     */
    @Test
    public void getStudySummary() {
        String id = resources.getString("study");
        log.info("id = " + id);
        StudySummary study = studyRepository.getStudySummary(id);
        assertNotNull("The StudySummary object is null.", study);
    }

    /**
     * Test getting a List of Study objects
     */
    @Test
    public void getStudySummaryAll() {
        List <StudySummary> studies = studyRepository.getStudySummaryAll();
        assertNotNull("The studies object is null.", studies);
        assertTrue("The Study count is greater than zero.", studies.size() > 0);
        log.info("Count: " + studies.size());
    }
    
    /**
     * Tests getting the ResearchFocus entries by Study Accession.
     */
    @Test
    public void getResearchFocus() {
        String id = resources.getString("study");
        log.info("id = " + id);
        List <String> researchFocus = studyRepository.getResearchFocus(id);
        assertNotNull("The ResearchFocus object is null.", researchFocus);
        for (String focus: researchFocus) {
        	log.info("ResearchFocus: " + focus);
        }
    }

    /**
     * Tests getting the Species entries by Study Accession.
     */
    @Test
    public void getSubjectSpecies() {
        String id = resources.getString("study");
        log.info("id = " + id);
        List <String> subjectSpecies = studyRepository.getSubjectSpecies(id);
        assertNotNull("The subjectSpecies object is null.", subjectSpecies);
        for (String species: subjectSpecies) {
        	log.info("Species: " + species);
        }
    }
    /**
     * Tests getting the BiosampleType entries by Study Accession.
     */
    @Test
    public void getBiosampleType() {
        String id = resources.getString("study");
        log.info("id = " + id);
        List <String> biosampleType = studyRepository.getBiosampleType(id);
        assertNotNull("The Biosample object is null.", biosampleType);
        for (String type: biosampleType) {
        	log.info("BiosampleType: " + biosampleType);
        }
    }
    /**
     * Tests getting the ExperimentMeasurementTechnique entries by Study Accession.
     */
    @Test
    public void getExperimentMeasurementTechnique() {
        String id = resources.getString("study");
        log.info("id = " + id);
        List <String> techniques = studyRepository.getExperimentMeasurementTechnique(id);
        assertNotNull("The ExperimentMeasurementTechnique object is null.", techniques);
        for (String technique: techniques) {
        	log.info("ExperimentMeasurmentTechnique: " + technique);
        }
    }
    /**
     * Test getting a ContactGrant
     */
    @Test
    public void getContactGrant()
    {
    	  String id = resources.getString("study");
          log.info("id = " + id);
          String contactGrant = studyRepository.getContactGrant(id);
          assertNotNull("The contactGrant object is null.", contactGrant);
          
          log.info("ContractGrant Title: " + contactGrant);
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

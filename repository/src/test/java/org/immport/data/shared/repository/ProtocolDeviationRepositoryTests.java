package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.ProtocolDeviation;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.ProtocolDeviationRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the ProtocolDeviation Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class ProtocolDeviationRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ProtocolDeviationRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The ProtocolDeviation repository. */
    @Autowired
    ProtocolDeviationRepository protocolDeviationRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the ProtocolDeviation object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("protocolDeviation");
        log.info("id = " + id);
        ProtocolDeviation protocolDeviation = protocolDeviationRepository.findById(id);
        assertNotNull("The ProtocolDeviation object is null.", protocolDeviation);
    }

    /**
     * Test getting a PageableData of ProtocolDeviation objects
     */
    @Test
    public void getPageableData() {
        PageableData page = protocolDeviationRepository.getPageableData(0,10,"p.protocolDeviationAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The ProtocolDeviation count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of ProtocolDeviation objects by study accession
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("protocolDeviationStudy");
        List <ProtocolDeviation> results = protocolDeviationRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The ProtocolDeviation count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.AssessmentPanel;
import org.immport.data.shared.model.AssessmentComponentList;
import org.immport.data.shared.model.AssessmentSummary;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.BiosampleStudyTime;
import org.immport.data.shared.repository.AssessmentPanelRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the Assessment Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})

public class AssessmentRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(AssessmentRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The Assessment repository. */
    @Autowired
    AssessmentPanelRepository assessmentRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the Assessment object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("assessment");
        log.info("id = " + id);
        AssessmentPanel assessment = assessmentRepository.findById(id);
        assertNotNull("The Assessment object is null.", assessment);
    }

    /**
     * Test getting a PageableData of Assessment objects
     */
    @Test
    public void getPageableData() {
        PageableData page = assessmentRepository.getPageableData(0,10,"a.assessmentPanelAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The Assessment count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }
    /**
     * Test getting a List of Assessment objects by study accession
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <AssessmentPanel> results = assessmentRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The Assessment count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }
    
    /**
     * Test getting a Adverse Event Summary that are linked to
     * a study.
     */
    @Test
    public void getAssessmentSummaryByStudy() {
    	 String id = resources.getString("studyAccession");    	
    	 PageableData page = assessmentRepository.getAssessmentPanelSummaryByStudy(0, 10,
		            "nameReported", "DESC", id);
    	 
    	// PageableData page1 = assessmentRepository.getAssessmentSummaryByStudy(0, 10,
		 //           "", "", id);
    	
    	 
    	 assertNotNull("The page object is null.", page);        
         log.info("page Count: " + page.getTotalCount());
         
         //display  
         List<AssessmentSummary> objList = (List<AssessmentSummary>)page.getData();         
         if (objList != null){
	         for (int cnt=0;cnt<objList.size(); cnt++){	        	 
	        	 AssessmentSummary obj = (AssessmentSummary)objList.get(cnt);
	    		 log.info("page --" + obj.getPanelNameReported() + " , " 
	    		 + obj.getArmAccession() + " , " + obj.getSubjAccNo() + " , " + obj.getAssessmentNo() );
	    	 }
         }
         else {
        	 log.info("Data Empty: ");         
         }   
         
        /* List<AssessmentSummary> objList1 = (List<AssessmentSummary>)page1.getData();         
         if (objList1 != null){
	         for (int cnt=0;cnt<objList1.size(); cnt++){	        	 
	        	 AssessmentSummary obj = (AssessmentSummary)objList1.get(cnt);
	    		 log.info("page1 --" + obj.getPanelNameReported() + " , " 
	    		 + obj.getArmAccession() + " , " + obj.getSubjAccNo() + " , " + obj.getAssessmentNo() );
	    	 }
         }
         else {
        	 log.info("Data Empty: ");         
         }   
         */
         
    }
    
    
    /**
     * Returns a List of Assessment Component that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Component)
     */
    @Test
    public void getAssessmentComponentListByStudy() {
    	String id = resources.getString("studyAccession");    	
    	PageableData page = assessmentRepository.getAssessmentComponentListByStudy(0, 10,
		            "nameReported", "DESC", id);
   	 
   	 	assertNotNull("The page object is null.", page);        
        log.info("Count: " + page.getTotalCount());
        
        //display  
        List<AssessmentComponentList> objList = (List<AssessmentComponentList>)page.getData();   
       
        
        if (objList != null){        	
        	
	         for (int cnt=0;cnt<objList.size(); cnt++){	        	 
	        	 AssessmentComponentList obj = (AssessmentComponentList)objList.get(cnt);
	        	 
	        	 String baseValues = "";
	             List<String> objBaseData= obj.getBaseData();
	        	 StringBuffer strBuffer = new StringBuffer();
	        	 for (int j=0;j < objBaseData.size();j++)
	        	 {
	        		 strBuffer.append(" , ").append(objBaseData.get(j));
	        		 //baseValues = baseValues + " , " + objBaseData.get(j);
	        	 }
	        	 
	    		 log.info(strBuffer.toString());
	    	 }
        }
        else {
       	 log.info("Data Empty: ");         
        }
    }
    
    
    @Test
    public void getStudyTimeCollectedByAssessments() {
    	  String id = resources.getString("studyAccession");
    	  List<BiosampleStudyTime> results = 
    			  assessmentRepository.getStudyTimeCollectedByAssessments("SDY1");
    	  
    	  assertNotNull("The results is null.", results);
          assertTrue("The Biosample count is greater than zero.", results.size() > 0);
          log.info("Count: " + results.size());
          
          /*for (int j=0;j<results.size();j++){
        	  BiosampleStudyTime biosampleStudyTime=  results.get(j);
        	  
        	  log.info(biosampleStudyTime.getExperimentAccessionOrPanelName()
        			  + " - " + biosampleStudyTime.getStudyTimeCollected() + " - " + biosampleStudyTime.getCountOfBioSamples());
          }*/
    }
    

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

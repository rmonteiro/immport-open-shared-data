package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.StudyLink;
import org.immport.data.shared.repository.StudyLinkRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the StudyLink Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class StudyLinkRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyLinkRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The StudyLink repository. */
    @Autowired
    StudyLinkRepository studyLinkRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the StudyLink object by id.
     */
    @Test
    public void findById() {
        Integer id = Integer.parseInt(resources.getString("studyLink"));
        log.info("id = " + id);
        StudyLink studyLink = studyLinkRepository.findById(id);
        assertNotNull("The StudyLink object is null.", studyLink);
    }

    /**
     * Test getting a PageableData of StudyLink objects
     */
    @Test
    public void getPageableData() {
        PageableData page = studyLinkRepository.getPageableData(0,10,"s.studyLinkId","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The StudyLink count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of StudyLink objects by study accession
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <StudyLink> results = studyLinkRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The StudyLink count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.ArmOrCohort;
import org.immport.data.shared.repository.ArmOrCohortRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the ArmOrCohort Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class ArmOrCohortRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ArmOrCohortRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The ArmOrCohort repository. */
    @Autowired
    ArmOrCohortRepository armOrCohortRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the ArmOrCohort object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("armOrCohort");
        log.info("id = " + id);
        ArmOrCohort armOrCohort = armOrCohortRepository.findById(id);
        assertNotNull("The ArmOrCohort object is null.", armOrCohort);
    }

    /**
     * Test getting a PageableData of ArmOrCohort objects
     */
    @Test
    public void getPageableData() {
        PageableData page = armOrCohortRepository.getPageableData(0,10,"a.armAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The ArmOrCohort count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of ArmOrCohort objects by study accession
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <ArmOrCohort> results = armOrCohortRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The ArmOrCohort count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }
    
    /**
     * Returns a List of ArmCohortSortOrder by sort order that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmOrCohort>
     */   
    @Test
    public  void getArmOrderAndDescByStudy() {
    	String id = resources.getString("studyAccession");
    	List<ArmCohortSortOrder> results = armOrCohortRepository.getArmOrderAndDescByStudy(id);
    	assertNotNull("The results is null.", results);
        assertTrue("The ArmOrCohort count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Biosample;

import org.immport.data.shared.model.TreatmentToExpSampleInfo;
import org.immport.data.shared.repository.TreatmentRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * This class tests the Protocol Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class TreatmentRepositoryTests {
	
	 /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(TreatmentRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The Biosample repository. */
    @Autowired
    TreatmentRepository treatmentRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the Biosample object by id.
     */
    @Test
    public void getTreatmentToExpSample() {
        String id = resources.getString("studyAccession");
        log.info("id = " + id);
        List<TreatmentToExpSampleInfo> lstTreatmentToExpSample= treatmentRepository.getTreatmentToExpSample("SDY201");
        assertNotNull("The Biosample object is null.", lstTreatmentToExpSample);
        
       
        
        if (lstTreatmentToExpSample != null)
        {
        	 log.info("Count of TreatmentToExpSample :" + lstTreatmentToExpSample.size());
        	for (TreatmentToExpSampleInfo obj: lstTreatmentToExpSample) {       		
        		
        	
        		log.info(obj.getExperimentAccession() + " , " + obj.getExperimentTitle() + " , " + obj.getTreatmentName() + ", " + obj.getExpsampleAccessionCount());
        	}
        }
    }

    


    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }

}

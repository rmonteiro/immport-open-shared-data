package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.SubjectMeasureResult;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.SubjectMeasureResultRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the SubjectMeasureResult Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class SubjectMeasureResultRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SubjectMeasureResultRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The SubjectMeasureResult repository. */
    @Autowired
    SubjectMeasureResultRepository subjectMeasureResultRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the SubjectMeasureResult object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("subjectMeasureResult");
        log.info("id = " + id);
        SubjectMeasureResult subjectMeasureResult = subjectMeasureResultRepository.findById(id);
        assertNotNull("The SubjectMeasureResult object is null.", subjectMeasureResult);
    }

    /**
     * Test getting a PageableData of SubjectMeasureResult objects
     */
    @Test
    public void getPageableData() {
        PageableData page = subjectMeasureResultRepository.getPageableData(0,10,"s.subjectMeasureResAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The SubjectMeasureResult count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of SubjectMeasureResult objects by study accession
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <SubjectMeasureResult> results = subjectMeasureResultRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The SubjectMeasureResult count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

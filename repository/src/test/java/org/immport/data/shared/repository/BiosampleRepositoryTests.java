package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Biosample;
import org.immport.data.shared.model.BiosampleStudyTime;
import org.immport.data.shared.repository.BiosampleRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the Biosample Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class BiosampleRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(BiosampleRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The Biosample repository. */
    @Autowired
    BiosampleRepository biosampleRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the Biosample object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("biosample");
        log.info("id = " + id);
        Biosample biosample = biosampleRepository.findById(id);
        assertNotNull("The Biosample object is null.", biosample);
    }

    /**
     * Test getting a PageableData of Biosample objects
     */
    @Test
    public void getPageableData() {
        PageableData page = biosampleRepository.getPageableData(0,10,"b.biosampleAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The Biosample count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of Biosample objects by study accession
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <Biosample> results = biosampleRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The Biosample count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }
    
    @Test
    public void getStudyTimeCollectedByExperiment() {
    	  String id = resources.getString("studyAccession");
    	  List<BiosampleStudyTime> results = 
    			  	biosampleRepository.getStudyTimeCollectedByExperiment("SDY33");
    	  
    	  assertNotNull("The results is null.", results);
          assertTrue("The Biosample count is greater than zero.", results.size() > 0);
          log.info("Count: " + results.size());
          
          for (int j=0;j<results.size();j++){
        	  BiosampleStudyTime biosampleStudyTime=  results.get(j);
        	  
        	  log.info(biosampleStudyTime.getExperimentAccessionOrPanelName()
        			  + " - " + biosampleStudyTime.getStudyTimeCollected() + " - " + biosampleStudyTime.getCountOfBioSamples());
          }
    }
    
    
    @Test
    public void getStudyTimeCollectedByLabTests() {
    	  String id = resources.getString("studyAccession");
    	  List<BiosampleStudyTime> results = 
    			  	biosampleRepository.getStudyTimeCollectedByLabTests("SDY10");
    	  
    	  assertNotNull("The results is null.", results);
          assertTrue("The Biosample count is greater than zero.", results.size() > 0);
          log.info("Count: " + results.size());
          
          for (int j=0;j<results.size();j++){
        	  BiosampleStudyTime biosampleStudyTime=  results.get(j);
        	  
        	  log.info(biosampleStudyTime.getExperimentAccessionOrPanelName()
        			  + " - " + biosampleStudyTime.getStudyTimeCollected() + " - " + biosampleStudyTime.getCountOfBioSamples());
          }
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

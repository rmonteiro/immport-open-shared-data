package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.ConcomitantMedicationDetail;
import org.immport.data.shared.model.ConcomitantMedicationSummary;
import org.immport.data.shared.model.StudyIntervention;
import org.immport.data.shared.model.Intervention;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.InterventionRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the SubstanceMerge Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class InterventionRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(InterventionRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The SubstanceMerge repository. */
    @Autowired
    InterventionRepository interventionRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the SubstanceMerge object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("substanceMerge");
        log.info("id = " + id);
        Intervention substanceMerge = interventionRepository.findById(id);
        assertNotNull("The SubstanceMerge object is null.", substanceMerge);
    }

    /**
     * Test getting a PageableData of SubstanceMerge objects
     */
    @Test
    public void getPageableData() {
        PageableData page = interventionRepository.getPageableData(0,10,"s.interventionAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The Intervention count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of Concomitant Medication Summary data that are linked to
     * a study.
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <Intervention> results = interventionRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The SubstanceMerge count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }
    
    /**
     * Test getting a Concomitant Medication Summary that are linked to
     * a study.
     */
    @Test
    public void getConcomitantMedicationByStudy() {
    	 String id = resources.getString("studyAccession");    	
    	 List<ConcomitantMedicationSummary>  resultObjects = interventionRepository.getConcomitantMedicationByStudy(id);
    	 
    	 assertNotNull("The results is null.", resultObjects);
    	 assertTrue("The ConcomitantMedication Result count is greater than zero.", resultObjects.size() > 0);
    	 log.info("Count: " + resultObjects.size());
    	 
    	 //display
    	 for (int cnt=0;cnt<resultObjects.size(); cnt++) { 
    		 ConcomitantMedicationSummary obj = (ConcomitantMedicationSummary)resultObjects.get(cnt);
    		 log.info(obj.getName() + " , " + obj.getarmAccession() + " , " 
    		 + obj.getCompoundRole() + " , " + obj.getCompReportNo() + " , " + obj.getSubjAccNo());
    	 }
    	
    }
    
    /**
     * Test getting a Substance Use Summary that are linked to
     * a study.
     */
    @Test
    public void getSubstanceUseSummaryByStudy() {
    	 String id = resources.getString("studyAccession");    	
    	 List<ConcomitantMedicationSummary>  resultObjects = interventionRepository.getSubstanceUseSummaryByStudy(id);
    	 
    	 assertNotNull("The results is null.", resultObjects);
    	 if (resultObjects != null) {    	
    	 log.info("Count: " + resultObjects.size());
    	 
    	 //display
    	 for (int cnt=0;cnt<resultObjects.size(); cnt++) { 
    		 ConcomitantMedicationSummary obj = (ConcomitantMedicationSummary)resultObjects.get(cnt);
    		 log.info(obj.getName() + " , " + obj.getarmAccession() + " , " 
    		 + obj.getCompoundRole() + " , " + obj.getCompReportNo() + " , " + obj.getSubjAccNo());
    	 }
    	 }
    	
    }
    
    /**
     * Test getting a Pageable data of Concomitant Medication Detail that are linked to
     * a study.
     */
    @Test
    public void getConcomitantMedDetailByStudy() {
    	 String id = resources.getString("studyAccession");    	
    	 PageableData page = 
    			 interventionRepository.getConcomitantMedDetailByStudy(0, 10,
    			            "compoundNameReported", "DESC", id);
    	 
    	 assertNotNull("The page object is null.", page);        
         log.info("Count: " + page.getTotalCount());
         
         //display
         
         List<ConcomitantMedicationDetail> objList = (List<ConcomitantMedicationDetail>)page.getData();         
         if (objList != null){
	         for (int cnt=0;cnt<objList.size(); cnt++){
	        	 
	        	 ConcomitantMedicationDetail obj = (ConcomitantMedicationDetail)objList.get(cnt);
	        	 log.info(obj.getName() + " , " + obj.getarmAccession() + " , " + obj.getCompoundNameReported() + " , " 
	            		 + obj.getCompoundRole() + " , " + obj.getCompReportNo() + " , " + obj.getSubjAccNo());
	    	 }
         }
         else {
        	 log.info("Data Empty: ");         
         }
    	
    }
    
    /**
     * Test getting a Pageable data of Substance Use Detail that are linked to
     * a study.
     */
    @Test
    public void getSubstanceUseDetailByStudy() {
    	 String id = resources.getString("studyAccession");    	
    	 PageableData page = 
    			 interventionRepository.getSubstanceUseDetailByStudy(0, 10,
    			            "compoundNameReported", "",  id);
    	 
    	 assertNotNull("The page object is null.", page);        
         log.info("Count: " + page.getTotalCount());
         
         //display
         
         List<ConcomitantMedicationDetail> objList = (List<ConcomitantMedicationDetail>)page.getData();         
         if (objList != null){
	         for (int cnt=0;cnt<objList.size(); cnt++){
	        	 
	        	 ConcomitantMedicationDetail obj = (ConcomitantMedicationDetail)objList.get(cnt);
	        	 log.info(obj.getName() + " , " + obj.getarmAccession() + " , " + obj.getCompoundNameReported() + " , " 
	            		 + obj.getCompoundRole() + " , " + obj.getCompReportNo() + " , " + obj.getSubjAccNo());
	    	 }
         }
         else {
        	 log.info("Data Empty: ");         
         }
    	
    }
    
    
    /**
     * Returns a Pageable data of Study Intervention Detail that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return PageableData (Study Intervention Detail data)
     */
    @Test
    public void getStudyInterventionDetailByStudy() {
    	 String id = resources.getString("studyAccession");    	
    	 PageableData page = 
    			 interventionRepository.getStudyInterventionDetailByStudy(0, 10,
    			            "compoundNameReported", "DESC", id);
    	 assertNotNull("The page object is null.", page);        
         log.info("Study Intervention Count: " + page.getTotalCount());
         
         
         
         //display  
         List<StudyIntervention> objList = (List<StudyIntervention>)page.getData();         
         if (objList != null){
	         for (int cnt=0;cnt<objList.size(); cnt++){
	        	 
	        	 StudyIntervention obj = (StudyIntervention)objList.get(cnt);
	        	 log.info(obj.getArmAccession() + " , " + obj.getCompoundRole() + " , "  + obj.getCompoundNameReported() + " , " 
	            		 + obj.getMergeNameReported() + " , " + obj.getSubjAccNo());
	    	 }
         }
         else {
        	 log.info("Data Empty: ");         
         }
    }
    

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

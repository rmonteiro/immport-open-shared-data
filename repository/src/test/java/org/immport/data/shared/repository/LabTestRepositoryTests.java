package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;





import org.immport.data.shared.model.LabTestPanel;
import org.immport.data.shared.model.LabTestComponentList;
import org.immport.data.shared.model.LabTestPanelSummary;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.LabTestPanelRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the LabTest Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class LabTestRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(LabTestRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The LabTest repository. */
    @Autowired
    LabTestPanelRepository labTestRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the LabTest object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("labTestPanel");
        log.info("id = " + id);
        LabTestPanel labTest = labTestRepository.findById(id);
        assertNotNull("The LabTest object is null.", labTest);
    }

    /**
     * Test getting a PageableData of LabTest objects
     */
    @Test
    public void getPageableData() {
        PageableData page = labTestRepository.getPageableData(0,10,"l.labTestPanelAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The LabTestPanel count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of LabTest objects by study accession
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <LabTestPanel> results = labTestRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The LabTest count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }
    
    @Test
    public void getLabTestPanelSummaryByStudy() {
    	 String id = resources.getString("studyAccession");    	
    	 List<LabTestPanelSummary>  resultObjects = labTestRepository.getLabTestPanelSummaryByStudy(id);
    	 
    	 assertNotNull("The results is null.", resultObjects);
    	 assertTrue("The ConcomitantMedication Result count is greater than zero.", resultObjects.size() > 0);
    	 log.info("Count: " + resultObjects.size());
    	 
    	
    	//display
    	 for (int cnt=0;cnt<resultObjects.size(); cnt++) { 
    		 LabTestPanelSummary obj = (LabTestPanelSummary)resultObjects.get(cnt);
    		 
    		 log.info(obj.getPanelNameReported() + " , " 
    		 + obj.getArmAccession() + " , " + obj.getBioSampleNo() + " , " + obj.getLabTestPanelNo() + " , " + obj.getSubjAccNo());
    		 
    	 }
    }
    
    
    @Test
    public void getLabTestComponentListByStudy() {
    	String id = resources.getString("studyAccession");    	
    	PageableData page = labTestRepository.getLabTestComponentListByStudy(0, 22,
		            "nameReported", "", id);
   	 
   	 	assertNotNull("The page object is null.", page);        
        log.info("Count: " + page.getTotalCount());
        
        //display  
        List<LabTestComponentList> objList = (List<LabTestComponentList>)page.getData();         
        if (objList != null){
	         for (int cnt=0;cnt<objList.size(); cnt++){	        	 
	        	 LabTestComponentList obj = (LabTestComponentList)objList.get(cnt);
	        	 
	        	 String baseValues = "";
	        	 List<String> objBaseData= obj.getBaseData();
	        	 
	        	 
	        	 StringBuffer strBuffer = new StringBuffer();
	        	 
	        	 for (int j=0;j < objBaseData.size();j++)
	        	 {
	        		 strBuffer.append(" , ").append( objBaseData.get(j));
	        		 //baseValues = baseValues + " , " + objBaseData.get(j);
	        	 }
	        	 
	    		 log.info(strBuffer.toString());
	    	 }
        }
        else {
       	 log.info("Data Empty: ");         
        }
    	
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

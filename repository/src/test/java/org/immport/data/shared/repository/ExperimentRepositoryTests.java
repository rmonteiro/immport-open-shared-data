package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Experiment;
import org.immport.data.shared.repository.ExperimentRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the Experiment Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class ExperimentRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ExperimentRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The Experiment repository. */
    @Autowired
    ExperimentRepository experimentRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the Experiment object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("experiment");
        log.info("id = " + id);
        Experiment experiment = experimentRepository.findById(id);
        assertNotNull("The Experiment object is null.", experiment);
    }

    /**
     * Test getting a PageableData of Experiment objects
     */
    @Test
    public void getPageableData() {
        PageableData page = experimentRepository.getPageableData(0,10,"e.experimentAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The Experiment count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of Experiment objects by study accession
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <Experiment> results = experimentRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The Experiment count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
        for (int i=0;i<results.size();i++)
        {
        	Experiment exp = results.get(i);
        	log.info("Row " + i + " - "  + exp.getExperimentAccession() + " - "
        			+ exp.getName() + " - " + exp.getPurpose()
        			+ " - " + exp.getMeasurementTechnique() );
        	
        }
        
    }
    
    /**
     * Tests getting the Experiment object by id, plus other objects.
     */
    @Test
    public void getExperimentDetail() {
        String id = resources.getString("experiment");
        log.info("id = " + id);
        Experiment experiment = experimentRepository.getExperimentDetail(id);
        assertNotNull("The Experiment object is null.", experiment);
        log.info("Number of Protocols: " + experiment.getProtocols().size());
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.StudyPersonnel;
import org.immport.data.shared.repository.StudyPersonnelRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the StudyPersonnel Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class StudyPersonnelRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyPersonnelRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The StudyPersonnel repository. */
    @Autowired
    StudyPersonnelRepository studyPersonnelRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the StudyPersonnel object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("studyPersonnel");
        log.info("id = " + id);
        StudyPersonnel studyPersonnel = studyPersonnelRepository.findById(id);
        assertNotNull("The StudyPersonnel object is null.", studyPersonnel);
    }

    /**
     * Test getting a PageableData of StudyPersonnel objects
     */
    @Test
    public void getPageableData() {
        PageableData page = studyPersonnelRepository.getPageableData(0,10,"s.personAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The StudyPersonnel count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of StudyPersonnel objects by study accession
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <StudyPersonnel> results = studyPersonnelRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The StudyPersonnel count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

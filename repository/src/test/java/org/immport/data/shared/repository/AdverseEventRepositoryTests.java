package org.immport.data.shared.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.AdverseEvent;
import org.immport.data.shared.model.AdverseEventDetail;
import org.immport.data.shared.model.AdverseEventSummary;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.AdverseEventRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the AdverseEvent Repository.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-repositories.xml"
})
public class AdverseEventRepositoryTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(AdverseEventRepositoryTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The AdverseEvent repository. */
    @Autowired
    AdverseEventRepository adverseEventRepository;


    /**
     * Set up the tests.
     * 
     * @throws Exception the exception
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the AdverseEvent object by id.
     */
    @Test
    public void findById() {
        String id = resources.getString("adverseEvent");
        log.info("id = " + id);
        AdverseEvent adverseEvent = adverseEventRepository.findById(id);
        assertNotNull("The AdverseEvent object is null.", adverseEvent);
    }

    /**
     * Test getting a PageableData of AdverseEvent objects
     */
    @Test
    public void getPageableData() {
        PageableData page = adverseEventRepository.getPageableData(0,10,"a.adverseEventAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The AdverseEvent count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of AdverseEvent objects by study accession
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <AdverseEvent> results = adverseEventRepository.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The AdverseEvent count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }
    
    
    /**
     * Test getting a Adverse Event Summary that are linked to
     * a study.
     */
    @Test
    public void getAdverseEventSummaryByStudy() {
    	 String id = resources.getString("studyAccession");    	
    	 List<AdverseEventSummary>  resultObjects = adverseEventRepository.getAdverseEventSummaryByStudy(id);
    	 
    	 assertNotNull("The results is null.", resultObjects);
    	 assertTrue("The ConcomitantMedication Result count is greater than zero.", resultObjects.size() > 0);
    	 log.info("Count: " + resultObjects.size());
    	 
    	
    	//display
    	 for (int cnt=0;cnt<resultObjects.size(); cnt++) { 
    		 AdverseEventSummary obj = (AdverseEventSummary)resultObjects.get(cnt);
    		 if (obj.getSortOrder() == null)
    		 {
    			 log.info(obj.getDataType() );
    		 }
    		 else
    		 {
    		 log.info(obj.getDataType() + " , " 
    		 + obj.getArmAccession() + " , " + String.valueOf(obj.getSortOrder().intValue()) + " , " + obj.getDataCount() );
    		 }
    	 }
    	
    }
    
    
    
    /**
     * Test getting a Pageable data of Concomitant Medication Detail that are linked to
     * a study.
     */
    @Test
    public void getAdverseEventDetailByStudy() {
    	 String id = resources.getString("studyAccession");    	
    	 PageableData page = 
    			 adverseEventRepository.getAdverseEventDetailByStudy(0, 10,
    					 "nameReported", "DESC", id);
    	 
    	 assertNotNull("The page object is null.", page);        
         log.info("Count: " + page.getTotalCount());
         
         //display  
         List<AdverseEventDetail> objList = (List<AdverseEventDetail>)page.getData();         
         if (objList != null){
	         for (int cnt=0;cnt<objList.size(); cnt++){
	        	 
	        	 AdverseEventDetail obj = (AdverseEventDetail)objList.get(cnt);
	        	 log.info( obj.getarmAccession() + " , " + obj.getNameReported() + " , " 
	            		 + obj.getSeverityReported() + " , " + obj.getSubjAccNo());
	    	 }
         }
         else {
        	 log.info("Data Empty: ");         
         }
    	
    }
    

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() {
        // empty
    }
}

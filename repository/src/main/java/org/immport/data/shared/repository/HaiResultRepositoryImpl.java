package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.HaiResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the HaiResult object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class HaiResultRepositoryImpl implements HaiResultRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(HaiResultRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns a List of HaiResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <HaiResult>
     */
    public List<HaiResult> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <HaiResult> query = em.createNamedQuery(
            "HaiResult.getByStudy", HaiResult.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}
package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.AdverseEvent;
import org.immport.data.shared.model.AdverseEventSummary;
import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.PageableData;

/**
 * Repository interface for the AdverseEvent object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface AdverseEventRepository {
    /**
     * Returns an AdverseEvent object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the AdverseEvent object
     */
    public AdverseEvent findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of AdverseEvent objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <AdverseEvent>
     */
    public List<AdverseEvent> getByStudy(String studyAccession);
    
    
    /**
     * Returns a List of Adverse Event Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Adverse Event Summary data)
     */
    public List<AdverseEventSummary> getAdverseEventSummaryByStudy(String studyAccession);
    
    
    /**
     * Returns a List of Adverse Event Detail data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Adverse Event Deatil data)
     */
    public PageableData getAdverseEventDetailByStudy(Integer start, Integer limit,
            String sort, String dir, String studyAccession);
    
    /**
     * Returns a List of Arm Cohort Names that are linked to the Adeverse Events for this
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmCohortSortOrder>
     */
    public List<ArmCohortSortOrder> getDistinctArmNameForAdverseEvent(String studyAccession);

}

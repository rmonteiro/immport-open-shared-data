package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.StudyFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the StudyFile object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class StudyFileRepositoryImpl implements StudyFileRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyFileRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an StudyFile object by querying on the primary key.
     *
     * @param id primary key of the StudyFile object
     * @return the StudyFile object
     */
    public StudyFile findById(String id) {
        log.debug("getting StudyFile instance with id: " + id);
        StudyFile instance = em.find(StudyFile.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("StudyFile.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("StudyFile Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT s FROM StudyFile s ORDER BY " + sort + " " + dir;
        TypedQuery<StudyFile> query = em.createQuery(queryString,StudyFile.class)
            .setFirstResult(start).setMaxResults(limit);
        List <StudyFile> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of StudyFile objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyFile>
     */
    public List<StudyFile> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <StudyFile> query = em.createNamedQuery(
            "StudyFile.getByStudy", StudyFile.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

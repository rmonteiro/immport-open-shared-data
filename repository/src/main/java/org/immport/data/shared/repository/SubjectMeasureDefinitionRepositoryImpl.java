package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.SubjectMeasureDefinition;
import org.immport.data.shared.model.PageableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the SubjectMeasureDefinition object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class SubjectMeasureDefinitionRepositoryImpl implements SubjectMeasureDefinitionRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SubjectMeasureDefinitionRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an SubjectMeasureDefinition object by querying on the primary key.
     *
     * @param id primary key of the SubjectMeasureDefinition object
     * @return the SubjectMeasureDefinition object
     */
    public SubjectMeasureDefinition findById(String id) {
        log.debug("getting SubjectMeasureDefinition instance with id: " + id);
        SubjectMeasureDefinition instance = em.find(SubjectMeasureDefinition.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("SubjectMeasureDefinition.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("SubjectMeasureDefinition Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT s FROM SubjectMeasureDefinition s ORDER BY " + sort + " " + dir;
        TypedQuery<SubjectMeasureDefinition> query = em.createQuery(queryString,SubjectMeasureDefinition.class)
            .setFirstResult(start).setMaxResults(limit);
        List <SubjectMeasureDefinition> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of SubjectMeasureDefinition objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <SubjectMeasureDefinition>
     */
    public List<SubjectMeasureDefinition> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <SubjectMeasureDefinition> query = em.createNamedQuery(
            "SubjectMeasureDefinition.getByStudy", SubjectMeasureDefinition.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PlannedVisit;
import org.immport.data.shared.model.PageableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the PlannedVisit object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class PlannedVisitRepositoryImpl implements PlannedVisitRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(PlannedVisitRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an PlannedVisit object by querying on the primary key.
     *
     * @param id primary key of the PlannedVisit object
     * @return the PlannedVisit object
     */
    public PlannedVisit findById(String id) {
        log.debug("getting PlannedVisit instance with id: " + id);
        PlannedVisit instance = em.find(PlannedVisit.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("PlannedVisit.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("PlannedVisit Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT p FROM PlannedVisit p ORDER BY " + sort + " " + dir;
        TypedQuery<PlannedVisit> query = em.createQuery(queryString,PlannedVisit.class)
            .setFirstResult(start).setMaxResults(limit);
        List <PlannedVisit> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of PlannedVisit objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <PlannedVisit>
     */
    public List<PlannedVisit> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <PlannedVisit> query = em.createNamedQuery(
            "PlannedVisit.getByStudy", PlannedVisit.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
    
    /**
     * Returns a List of Planned Visita for
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List 
     */
    public List<PlannedVisit> getPlannedVisits(String studyAccession)
    {
    	log.debug("StudyAccession = " + studyAccession);
        TypedQuery <PlannedVisit> query = em.createNamedQuery(
            "PlannedVisit.getPlannedVisits", PlannedVisit.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

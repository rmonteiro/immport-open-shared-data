package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.SubjectMeasureResult;
import org.immport.data.shared.model.PageableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the SubjectMeasureResult object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class SubjectMeasureResultRepositoryImpl implements SubjectMeasureResultRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SubjectMeasureResultRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an SubjectMeasureResult object by querying on the primary key.
     *
     * @param id primary key of the SubjectMeasureResult object
     * @return the SubjectMeasureResult object
     */
    public SubjectMeasureResult findById(String id) {
        log.debug("getting SubjectMeasureResult instance with id: " + id);
        SubjectMeasureResult instance = em.find(SubjectMeasureResult.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("SubjectMeasureResult.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("SubjectMeasureResult Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT s FROM SubjectMeasureResult s ORDER BY " + sort + " " + dir;
        TypedQuery<SubjectMeasureResult> query = em.createQuery(queryString,SubjectMeasureResult.class)
            .setFirstResult(start).setMaxResults(limit);
        List <SubjectMeasureResult> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of SubjectMeasureResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <SubjectMeasureResult>
     */
    public List<SubjectMeasureResult> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <SubjectMeasureResult> query = em.createNamedQuery(
            "SubjectMeasureResult.getByStudy", SubjectMeasureResult.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

package org.immport.data.shared.repository;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.LabTestPanel;
import org.immport.data.shared.model.LabTestComponentList;
import org.immport.data.shared.model.LabTestPanelSummary;
import org.immport.data.shared.model.PageableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the LabTest object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class LabTestPanelRepositoryImpl implements LabTestPanelRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(LabTestPanelRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

   // private HashMap<String,Long> hashTotalCntLabTestCompList = new HashMap<String,Long>();
    
    
    
    private static final String LAB_TEST_COMPONENT_QUERY = " SELECT  NEW org.immport.data.shared.model.LabTestComponentList(a.study.studyAccession,a.nameReported, b.nameReported) " +
    												  " FROM LabTestPanel a join a.labTests b " +
    												  " WHERE a.study.studyAccession = :studyAccession " +
    												  " GROUP BY a.study.studyAccession,a.nameReported, b.nameReported"; 
    		
    /**
     * Returns an LabTestPanel object by querying on the primary key.
     *
     * @param id primary key of the LabTestPanel object
     * @return the LabTestPanel object
     */
    public LabTestPanel findById(String id) {
        log.debug("getting LabTestPanel instance with id: " + id);
        LabTestPanel instance = em.find(LabTestPanel.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("LabTestPanel.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("LabTestPanel Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT l FROM LabTestPanel l ORDER BY " + sort + " " + dir;
        TypedQuery<LabTestPanel> query = em.createQuery(queryString,LabTestPanel.class)
            .setFirstResult(start).setMaxResults(limit);
        List <LabTestPanel> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of LabTestPanel objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <LabTestPanel>
     */
    public List<LabTestPanel> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <LabTestPanel> query = em.createNamedQuery(
            "LabTestPanel.getByStudy", LabTestPanel.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
    
    
    /**
     * Returns a List of Lab Test Panel Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Summary data)
     */
    public List<LabTestPanelSummary> getLabTestPanelSummaryByStudy(String studyAccession)
    {
    	 Query query = em.createNamedQuery(
 	            "LabTestPanel.getLabTestPanelSummary");
 	     query.setParameter("studyAccession", studyAccession);
 	        return (List<LabTestPanelSummary>)query.getResultList();
    }

    
    /**
     * Returns a List of Arm Cohort Names that are linked to the Lab Test Panel Events for this
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmCohortSortOrder>
     */
    public List<ArmCohortSortOrder> getDistinctArmNameForLabTestPanel(String studyAccession)
    {
    	log.debug("StudyAccession = " + studyAccession);
        Query query = em.createNamedQuery(
            "LabTestPanel.getDistinctArmNameForLabTestPanel");
        query.setParameter("studyAccession", studyAccession);
        
        List<ArmCohortSortOrder> armCohortSortOrders = (List<ArmCohortSortOrder>)query.getResultList();
        Collections.sort(armCohortSortOrders);
        
        return armCohortSortOrders;
    }
    
    
    /**
     * Returns a List of Lab Test Components Data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Component)
     */
    public PageableData getLabTestComponentListByStudy(Integer start, Integer limit,    		
            String sort, String dir,String studyAccession)
    {
    	 String sqlAssessmentCompList = "";
    	 Query queryDetail = null;
    	 PageableData page = new PageableData();
    	 
    	 long iTotalCount = 0;
    	 //Long savedTotalCount = (Long)hashTotalCntLabTestCompList.get(studyAccession);
         
        // if (savedTotalCount != null) {
       	//  iTotalCount = savedTotalCount.longValue();
        // }
       //  else {        	 	 
        	 queryDetail = em.createNamedQuery("LabTestPanel.getLabTestListCount");
             queryDetail.setParameter("studyAccession", studyAccession);            
            // Long totalCount = (Long)queryDetail.getSingleResult();
             List objList = queryDetail.getResultList();
             //iTotalCount = totalCount;
             iTotalCount = objList.size();
 	         //hashTotalCntAssessCompList.put(studyAccession,totalCount);
           //  hashTotalCntLabTestCompList.put(studyAccession,new Long(iTotalCount));		 
        // }
         page.setTotalCount(iTotalCount);
         
    	 sqlAssessmentCompList= LAB_TEST_COMPONENT_QUERY + " ORDER BY a." +  sort + " " + dir;   	 
         queryDetail = em.createQuery(sqlAssessmentCompList);
         queryDetail.setParameter("studyAccession", studyAccession);
         queryDetail.setFirstResult(start);
         queryDetail.setMaxResults(limit);
         List<LabTestComponentList> objLabTestComponentList = (List<LabTestComponentList>)queryDetail.getResultList();
         page.setData(objLabTestComponentList);
    	 
    	 return page;
    }
	
    
}

package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.StudyPersonnel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the StudyPersonnel object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class StudyPersonnelRepositoryImpl implements StudyPersonnelRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyPersonnelRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an StudyPersonnel object by querying on the primary key.
     *
     * @param id primary key of the StudyPersonnel object
     * @return the StudyPersonnel object
     */
    public StudyPersonnel findById(String id) {
        log.debug("getting StudyPersonnel instance with id: " + id);
        StudyPersonnel instance = em.find(StudyPersonnel.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("StudyPersonnel.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("StudyPersonnel Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT s FROM StudyPersonnel s ORDER BY " + sort + " " + dir;
        TypedQuery<StudyPersonnel> query = em.createQuery(queryString,StudyPersonnel.class)
            .setFirstResult(start).setMaxResults(limit);
        List <StudyPersonnel> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of StudyPersonnel objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyPersonnel>
     */
    public List<StudyPersonnel> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <StudyPersonnel> query = em.createNamedQuery(
            "StudyPersonnel.getByStudy", StudyPersonnel.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

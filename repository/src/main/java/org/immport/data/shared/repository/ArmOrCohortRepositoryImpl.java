package org.immport.data.shared.repository;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.ArmOrCohort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the ArmOrCohort object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class ArmOrCohortRepositoryImpl implements ArmOrCohortRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ArmOrCohortRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an ArmOrCohort object by querying on the primary key.
     *
     * @param id primary key of the ArmOrCohort object
     * @return the ArmOrCohort object
     */
    public ArmOrCohort findById(String id) {
        log.debug("getting ArmOrCohort instance with id: " + id);
        ArmOrCohort instance = em.find(ArmOrCohort.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("ArmOrCohort.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("ArmOrCohort Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT a FROM ArmOrCohort a ORDER BY " + sort + " " + dir;
        TypedQuery<ArmOrCohort> query = em.createQuery(queryString,ArmOrCohort.class)
            .setFirstResult(start).setMaxResults(limit);
        List <ArmOrCohort> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of ArmOrCohort objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmOrCohort>
     */
    public List<ArmOrCohort> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <ArmOrCohort> query = em.createNamedQuery(
            "ArmOrCohort.getByStudy", ArmOrCohort.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
    
    /**
     * Returns a List of Armaccession by sort order that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmOrCohort>
     */
    public List<String> getArmAccessionByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        Query query = em.createNamedQuery(
            "ArmOrCohort.getArmAccessionByStudy");
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }    
    
    /**
     * Returns a List of ArmCohortSortOrder by sort order that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmOrCohort>
     */    
    public List<ArmCohortSortOrder> getArmOrderAndDescByStudy (String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        Query query = em.createNamedQuery(
            "ArmOrCohort.getArmOrderAndDescByStudy");
        query.setParameter("studyAccession", studyAccession);
        
        List<ArmCohortSortOrder> armCohortSortOrders = (List<ArmCohortSortOrder>)query.getResultList();
        Collections.sort(armCohortSortOrders);
        
        
        return armCohortSortOrders;
    }    
    
    
}

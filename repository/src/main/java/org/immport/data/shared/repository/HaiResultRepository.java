package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.HaiResult;

/**
 * Repository interface for the HaiResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface HaiResultRepository {

    /**
     * Returns a List of HaiResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <HaiResult>
     */
    public List<HaiResult> getByStudy(String studyAccession);
}

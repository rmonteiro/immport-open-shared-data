package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.StudyLink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the StudyLink object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class StudyLinkRepositoryImpl implements StudyLinkRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyLinkRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an StudyLink object by querying on the primary key.
     *
     * @param id primary key of the StudyLink object
     * @return the StudyLink object
     */
    public StudyLink findById(Integer id) {
        log.debug("getting StudyLink instance with id: " + id);
        StudyLink instance = em.find(StudyLink.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("StudyLink.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("StudyLink Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT s FROM StudyLink s ORDER BY " + sort + " " + dir;
        TypedQuery<StudyLink> query = em.createQuery(queryString,StudyLink.class)
            .setFirstResult(start).setMaxResults(limit);
        List <StudyLink> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of StudyLink objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyLink>
     */
    public List<StudyLink> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <StudyLink> query = em.createNamedQuery(
            "StudyLink.getByStudy", StudyLink.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

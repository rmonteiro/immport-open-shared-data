package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.FcsAnalyzedResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the FcsAnalyzedResult object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class FcsAnalyzedResultRepositoryImpl implements FcsAnalyzedResultRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(FcsAnalyzedResultRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns a List of FcsAnalyzedResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <FcsAnalyzedResult>
     */
    public List<FcsAnalyzedResult> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <FcsAnalyzedResult> query = em.createNamedQuery(
            "FcsAnalyzedResult.getByStudy", FcsAnalyzedResult.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}
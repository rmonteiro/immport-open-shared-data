package org.immport.data.shared.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.ConcomitantMedicationDetail;
import org.immport.data.shared.model.ConcomitantMedicationSummary;
import org.immport.data.shared.model.StudyIntervention;
import org.immport.data.shared.model.Intervention;
import org.immport.data.shared.model.PageableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the Intervention object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class InterventionRepositoryImpl implements InterventionRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(InterventionRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;
    
    
   
    
    
    //Study Intervention
    private Map<String,Long> hashTotalInterventionCntDtl = new HashMap<String,Long>();
    
    private static final String CONCOMITANT_MED_DETAIL_NAME =
            "SELECT s.compoundNameReported " +                                
            "  FROM Intervention s JOIN s.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
            " WHERE s.compoundRole in ('Concomitant Medication') " +
            "   AND m.study.studyAccession = :studyAccession " +
            "   AND s.study.studyAccession = :studyAccession " +
            " GROUP BY s.compoundNameReported , s.compoundRole ";  
    
    
    private static final String CONCOMITANT_MED_DETAIL_QUERY =  "SELECT NEW org.immport.data.shared.model.ConcomitantMedicationDetail(m.name, m.armAccession, m.study.studyAccession, s.compoundNameReported, s.compoundRole " +
            " , COUNT(DISTINCT s.compoundNameReported) as compReportNo, COUNT(DISTINCT s.subject.subjectAccession) as subjAccNo) " +
            " FROM Intervention s JOIN s.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
            "WHERE s.compoundRole in  ('Concomitant Medication')   AND m.study.studyAccession = :studyAccession " +
            " and s.study.studyAccession = :studyAccession " +
            " and s.compoundNameReported IN :compoundName " +
            " GROUP BY m.name, m.armAccession, m.study.studyAccession, s.compoundNameReported, s.compoundRole " ;
           // " ORDER BY s.compoundNameReported,m.sortOrder";
    
    private static final String SUBSTANCE_USE_DETAIL_NAME =
            "SELECT s.compoundNameReported " +                                
            "  FROM Intervention s JOIN s.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
            " WHERE s.compoundRole in ('Substance Use') " +
            "   AND m.study.studyAccession = :studyAccession " +
            "   AND s.study.studyAccession = :studyAccession " +
            " GROUP BY s.compoundNameReported , s.compoundRole ";   
    
    private static final String SUBSTANCE_USE_DETAIL_QUERY =  "SELECT NEW org.immport.data.shared.model.ConcomitantMedicationDetail(m.name, m.armAccession, m.study.studyAccession, s.compoundNameReported, s.compoundRole " +
            " , COUNT(DISTINCT s.compoundNameReported) as compReportNo, COUNT(DISTINCT s.subject.subjectAccession) as subjAccNo) " +
            " FROM Intervention s JOIN s.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
            "WHERE s.compoundRole in  ('Substance Use')   AND m.study.studyAccession = :studyAccession " +
            " and s.study.studyAccession = :studyAccession " +
            " and s.compoundNameReported IN :compoundName " +
            " GROUP BY m.name, m.armAccession, m.study.studyAccession, s.compoundNameReported, s.compoundRole ";
            
    
    
    private static final String INTERVENTION_DETAIL_NAME =	
    		" SELECT  CONCAT(c.compoundNameReported,c.nameReported) " +    			
    	  			" FROM Intervention c JOIN c.subject j JOIN j.arm2Subjects b JOIN b.armOrCohort a " +
    	              " WHERE c.study.studyAccession = :studyAccession " +           
    	              " AND  a.study.studyAccession = :studyAccession "  +
    	              " AND c.compoundRole in ('Intervention')" +
    	              " GROUP BY c.compoundRole,c.compoundNameReported,c.nameReported";  
    
    
    private static final String INTERVENTION_DETAIL_QUERY =
    				" SELECT  NEW org.immport.data.shared.model.StudyIntervention(a.study.studyAccession,a.armAccession,c.compoundRole,c.compoundNameReported,c.nameReported,count( distinct c.subject.subjectAccession)) " +
    						" FROM Intervention c JOIN c.subject j JOIN j.arm2Subjects b JOIN b.armOrCohort a " +
    	    	              " WHERE c.study.studyAccession = :studyAccession " +           
    	    	              " AND  a.study.studyAccession = :studyAccession "  +
    	    	              " AND c.compoundRole in ('Intervention')" +
    	    	              " AND CONCAT(c.compoundNameReported,c.nameReported) IN :nameReport " +
    	    	              " GROUP BY a.study.studyAccession,a.armAccession,c.compoundRole,c.compoundNameReported,c.nameReported";  

    /**
     * Returns an Intervention object by querying on the primary key.
     *
     * @param id primary key of the Intervention object
     * @return the Intervention object
     */
    public Intervention findById(String id) {
        log.debug("getting Intervention instance with id: " + id);
        Intervention instance = em.find(Intervention.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("Intervention.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("Intervention Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT s FROM Intervention s ORDER BY " + sort + " " + dir;
        TypedQuery<Intervention> query = em.createQuery(queryString,Intervention.class)
            .setFirstResult(start).setMaxResults(limit);
        List <Intervention> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of Intervention objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Intervention>
     */
    public List<Intervention> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <Intervention> query = em.createNamedQuery(
            "Intervention.getByStudy", Intervention.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
    
    /**
     * Returns a List of Concomitant Medication Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Concomitant Medication Summary data)
     */
    public List<ConcomitantMedicationSummary> getConcomitantMedicationByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        List<ConcomitantMedicationSummary> objAlSum = null;        
      //  objAlSum = ( List<ConcomitantMedicationSummary>)hashSummary.get(studyAccession);
      //  if (objAlSum == null) {
        Query query = em.createNamedQuery("Intervention.getConcomitantMedication");
        query.setParameter("studyAccession", studyAccession);
        objAlSum = (List<ConcomitantMedicationSummary>)query.getResultList();
	   //     hashSummary.put(studyAccession, objAlSum);
     //   }
        
        return objAlSum;
        	
    }
    
    
    /**
     * Returns a List of Substance Use Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Substance Use Summary data)
     */
    public List<ConcomitantMedicationSummary> getSubstanceUseSummaryByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        List<ConcomitantMedicationSummary> objAlSum = null;        
      //  objAlSum = ( List<ConcomitantMedicationSummary>)hashSummary.get(studyAccession);
      //  if (objAlSum == null) {
        Query query = em.createNamedQuery("Intervention.getSubstanceUse");
        query.setParameter("studyAccession", studyAccession);
        objAlSum = (List<ConcomitantMedicationSummary>)query.getResultList();
	   //     hashSummary.put(studyAccession, objAlSum);
     //   }
        
        return objAlSum;
        	
    }
    
    
    
    
    /**
     * Returns a Pageable data of Concomitant Medication Detail that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return PageableData (Concomitant Medication Detail data)
     */
    public PageableData getConcomitantMedDetailByStudy(Integer start, Integer limit,
            String sort, String dir, String studyAccession)
    {
          PageableData page = new PageableData();
          
          long iTotalCount = 0;
         
          
          
          Query query = em.createNamedQuery(
                    "Intervention.getConcoMedicationDetailCompNameCnt");
          query.setParameter("studyAccession", studyAccession);
          List<Long> totalCountList = (List<Long>) query.getResultList();

       
          for (int i =0;i < totalCountList.size();i++) {
                   iTotalCount = iTotalCount + totalCountList.get(i);
          }
	       
          
	      page.setTotalCount(iTotalCount);
	      if (iTotalCount > 0) {
	          String sqlConMedDetName = CONCOMITANT_MED_DETAIL_NAME + " ORDER BY s." +  sort + " " + dir;
	          Query queryDetail = em.createQuery(sqlConMedDetName);
	          queryDetail.setParameter("studyAccession", studyAccession);
	          queryDetail.setFirstResult(start);
	          queryDetail.setMaxResults(limit);
	          List<String> onjComNameLst = (List<String>)queryDetail.getResultList();
	
	          String sqlConMedDetQuery = CONCOMITANT_MED_DETAIL_QUERY + " ORDER BY s." +  sort + " " + dir;
	          //Query queryDataDetail = em.createNamedQuery(
	          //          "Intervention.getConcomitantMedicationDetail");
	          Query queryDataDetail = em.createQuery(sqlConMedDetQuery);
	          queryDataDetail.setParameter("studyAccession", studyAccession);
	          queryDataDetail.setParameter("compoundName", onjComNameLst);
	          List<ConcomitantMedicationDetail> objData = (List<ConcomitantMedicationDetail>)queryDataDetail.getResultList();
	
	          page.setData(objData);
          }

          return page;
    }  
    
    
    /**
     * Returns a Pageable data of Concomitant Medication Detail that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return PageableData (Concomitant Medication Detail data)
     */
    public PageableData getSubstanceUseDetailByStudy(Integer start, Integer limit,
            String sort, String dir, String studyAccession)
    {
          PageableData page = new PageableData();
          
          long iTotalCount = 0;
          Query query = em.createNamedQuery(
	                    "Intervention.getSubstanceUseDetailCompNameCnt");
	      query.setParameter("studyAccession", studyAccession);
	      List<Long> totalCountList = (List<Long>) query.getResultList();
	
	       
	      for (int i =0;i < totalCountList.size();i++) {
	           iTotalCount = iTotalCount + totalCountList.get(i);
	      }
	      
	      page.setTotalCount(iTotalCount);
	      
	      if (iTotalCount > 0) {
	          String sqlConMedDetName = SUBSTANCE_USE_DETAIL_NAME + " ORDER BY s." +  sort + " " + dir;
	          Query queryDetail = em.createQuery(sqlConMedDetName);
	          queryDetail.setParameter("studyAccession", studyAccession);
	          queryDetail.setFirstResult(start);
	          queryDetail.setMaxResults(limit);
	          List<String> onjComNameLst = (List<String>)queryDetail.getResultList();
	
	          
	          String sqlConMedDetQuery = SUBSTANCE_USE_DETAIL_QUERY + " ORDER BY s." +  sort + " " + dir;
	          Query queryDataDetail = em.createQuery(sqlConMedDetQuery);
	          //Query queryDataDetail = em.createNamedQuery(
	           //         "Intervention.getSubstanceUseDetail");
	          queryDataDetail.setParameter("studyAccession", studyAccession);
	          queryDataDetail.setParameter("compoundName", onjComNameLst);
	          List<ConcomitantMedicationDetail> objData = (List<ConcomitantMedicationDetail>)queryDataDetail.getResultList();
	
	          page.setData(objData);
          }

          return page;
    }  
    
    
    
    /**
     * Returns a Pageable data of Study Intervention Detail that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return PageableData (Study Intervention Detail data)
     */
    public PageableData getStudyInterventionDetailByStudy(Integer start, Integer limit,
            String sort, String dir, String studyAccession)
    {
          PageableData page = new PageableData();
          long iTotalCount = 0;
          Long savedTotalCount = (Long)hashTotalInterventionCntDtl.get(studyAccession);
          
          if (savedTotalCount != null)
          {
        	  iTotalCount = savedTotalCount.longValue();
          }
          else
          {
	          Query query = em.createNamedQuery(
	                    "Intervention.getStudyInterventionDetailCount");
	          query.setParameter("studyAccession", studyAccession);
	          List totalCountList =  query.getResultList();
	          iTotalCount = totalCountList.size();
	          hashTotalInterventionCntDtl.put(studyAccession,Long.valueOf(iTotalCount));
          }
          
       
          page.setTotalCount(iTotalCount);
          
          if (iTotalCount > 0) {
        	  String sqlConMedDetName = "";
        	  if (!sort.equalsIgnoreCase(""))
        	  {
        		  if (sort.equalsIgnoreCase("nameReported"))
        		  {
        			//  if (dir.equalsIgnoreCase("ASC"))
	        		 // {
        				  sqlConMedDetName= INTERVENTION_DETAIL_NAME + " ORDER BY c." +  sort + " " + dir +  ",c.compoundNameReported ASC";
	        		 // }
        			 // else
        			 // {
        			//	  sqlConMedDetName= INTERVENTION_DETAIL_NAME + " ORDER BY c." +  sort + " " + dir;
        			 // }
        		  }
        		  else if (sort.equalsIgnoreCase("compoundNameReported"))
        		  {
        			  sqlConMedDetName= INTERVENTION_DETAIL_NAME + " ORDER BY c." +  sort + " " + dir +  ",c.nameReported ASC";
        		  }
        	  }        	  
        	  else
        	  {
        		  sqlConMedDetName= INTERVENTION_DETAIL_NAME + " ORDER BY c.nameReported,c.compoundNameReported " + dir; 
        	  }
	          Query queryDetail = em.createQuery(sqlConMedDetName);
	          queryDetail.setParameter("studyAccession", studyAccession);
	          queryDetail.setFirstResult(start);
	          queryDetail.setMaxResults(limit);
	          List<String> onjStudyInterventionNameLst = (List<String>)queryDetail.getResultList();
	          
	          if (!sort.equalsIgnoreCase(""))
        	  {
	        	  if (sort.equalsIgnoreCase("nameReported"))
	        	  {
	        		//  if (dir.equalsIgnoreCase("ASC"))
	        		//  {
	        			  sqlConMedDetName= INTERVENTION_DETAIL_QUERY + " ORDER BY c." +  sort + " " + dir +  ",c.compoundNameReported  ASC";
	        		//  }
	        		//  else
	        		//  {
	        		//	  sqlConMedDetName= INTERVENTION_DETAIL_QUERY + " ORDER BY c." +  sort + "  " + dir;
	        		//  }
	        	  }
	        	  else if (sort.equalsIgnoreCase("compoundNameReported"))
	        	  {
	        		  sqlConMedDetName= INTERVENTION_DETAIL_QUERY + " ORDER BY c." +  sort + " " + dir +  ",c.nameReported ASC"; 
	        	  }
        	  }        	  
        	  else
        	  {
        		  sqlConMedDetName= INTERVENTION_DETAIL_QUERY + " ORDER BY c.nameReported,c.compoundNameReported " + dir; 
        	  }
	        
	          Query queryDataDetail = em.createQuery(sqlConMedDetName);
	          queryDataDetail.setParameter("studyAccession", studyAccession);
	          queryDataDetail.setParameter("nameReport", onjStudyInterventionNameLst);
	          List<StudyIntervention> objData = ( List<StudyIntervention>)queryDataDetail.getResultList();	
	          page.setData(objData);	          
          }
          
          
	     
          return page;
    }   
    
    /**
     * Returns a List of Arm Cohort Names that are linked to the Interventions for this
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmCohortSortOrder>
     */
    public List<ArmCohortSortOrder> getDistinctArmNameForCompoundRole(String studyAccession,String type)
    {
    	 log.debug("StudyAccession = " + studyAccession);
    	 Query query = null;
    	 if (type.equalsIgnoreCase("Intervention")) {
    		  query = em.createNamedQuery(
    	             "Intervention.getDistinctArmNameForIntervention");
    		 
    	 } else if (type.equalsIgnoreCase("ConcomitantMedication")) {
    		  query = em.createNamedQuery(
     	             "Intervention.getDistinctArmNameForConMed");
    	 } else if (type.equalsIgnoreCase("SubstanceUse")) {
    		  query = em.createNamedQuery(
     	             "Intervention.getDistinctArmNameForSubstanceUse");
    	 }
    	
    	 List<ArmCohortSortOrder> objArmCohortSortOrder = new ArrayList<ArmCohortSortOrder>();
         if (query != null)
         {
        	 query.setParameter("studyAccession", studyAccession);
        	 objArmCohortSortOrder = (List<ArmCohortSortOrder>)query.getResultList();
         }
         
         Collections.sort(objArmCohortSortOrder);
         
         return objArmCohortSortOrder;
    }
    
    
    
}

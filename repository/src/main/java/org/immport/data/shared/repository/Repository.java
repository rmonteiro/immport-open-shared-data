package org.immport.data.shared.repository;

import org.hibernate.SessionFactory;

public interface Repository {
    public void setSessionFactory(SessionFactory sessionFactory);
}

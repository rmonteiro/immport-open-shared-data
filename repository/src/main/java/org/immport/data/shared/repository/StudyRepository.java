package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Study;
import org.immport.data.shared.model.StudyImage;
import org.immport.data.shared.model.StudySummary;

/**
 * Repository interface for the Study object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface StudyRepository {
    /**
     * Returns an Study object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the Study object
     */
    public Study findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    public StudyImage getStudyImage(String studyAccession);
    
    
    /**
     * Returns an contract grant for the study accession.
     *
     * @param studyAccession primary key of the object
     * @return the ContractGrant object
     */
    public String getContactGrant(String studyAccession);
    
    /**
     * Returns a Study Summary for the study accession.
     *
     * @param studyAccession primary key of the object
     * @return the StudySummary object
     */
    public StudySummary getStudySummary(String studyAccession);
    
    /**
     * Returns List of StudySummary objects.
     *
     * @return the List of StudySummary objects
     */
    public List <StudySummary> getStudySummaryAll();
    
    /**
     * Returns List of Strings representing ResearchFocus.
     *
     * @return the List of Strings representing ResearchFocus
     */
    public List <String> getResearchFocus(String studyAccession);
    
    /**
     * Returns List of Strings representing Species.
     *
     * @return the List of Strings representing Species
     */
    public List <String> getSubjectSpecies(String studyAccession);

    /**
     * Returns List of Strings representing BiosampleType.
     *
     * @return the List of Strings representing ResearchFocus
     */
    public List <String> getBiosampleType(String studyAccession);

    /**
     * Returns List of Strings representing ExperimentMeasurementTechnique.
     *
     * @return the List of Strings representing ExperimentMeasurementTechnique
     */
    public List <String> getExperimentMeasurementTechnique(String studyAccession);

}
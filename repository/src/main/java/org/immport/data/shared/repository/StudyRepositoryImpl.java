package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Study;
import org.immport.data.shared.model.StudyImage;
import org.immport.data.shared.model.StudySummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the Study object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class StudyRepositoryImpl implements StudyRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an Study object by querying on the primary key.
     *
     * @param id primary key of the Study object
     * @return the Study object
     */
    public Study findById(String id) {
        log.debug("getting Study instance with id: " + id);
        Study instance = em.find(Study.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns an Study object by querying on the primary key.
     *
     * @param id primary key of the Study object
     * @return the Study object
     */
    public StudySummary getStudySummary(String id) {
        log.debug("getting StudySummary instance with id: " + id);
        StudySummary instance = em.find(StudySummary.class, id);
        log.debug("Find successful");
        return instance;
    }
    
    /**
     * Returns a List of StudySummary objects
     *
     * @return List <StudySummary> objects
     */
    public List getStudySummaryAll() {
        String queryString = "SELECT s FROM StudySummary s ";
        TypedQuery<StudySummary> query = em.createQuery(queryString,StudySummary.class);
        List <StudySummary> results = query.getResultList();

        return results;
    }
    
    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("Study.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("Study Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT s FROM Study s ORDER BY " + sort + " " + dir;
        TypedQuery<Study> query = em.createQuery(queryString,Study.class)
            .setFirstResult(start).setMaxResults(limit);
        List <Study> results = query.getResultList();
        page.setData(results);

        return page;
    }
    
    public StudyImage getStudyImage(String studyAccession) {
        Query query = em.createQuery("from StudyImage studyImage where studyImage.study.studyAccession = :studyAccession");
        query.setParameter("studyAccession", studyAccession);
        StudyImage studyImage = (StudyImage) query.getSingleResult();
        return studyImage;
    }

    
    /**
     * Returns an contract grant for the study accession.
     *
     * @param studyAccession primary key of the object
     * @return the ContractGrant object
     */
    public String getContactGrant(String studyAccession)
    {
    	String contractGrant = "";
    	Query query = 
    			em.createNamedQuery("Study.getContractGrantName");
    	  query.setParameter("studyAccession", studyAccession);
    	List lstContractGrant = query.getResultList();   
    	if (lstContractGrant != null && lstContractGrant.size() > 0)
    	{
    		contractGrant = (String)lstContractGrant.get(0);
    	}
    	return contractGrant;
    }
    
    /**
     * Returns List of Strings representing ResearchFocus.
     *
     * @return the List of Strings representing ResearchFocus
     */
    public List <String> getResearchFocus(String studyAccession)
    {
    	String queryString = "SELECT "
                + "       s1.research_focus as researchFocus "
                + "  FROM study_categorization s1 "
                + " WHERE s1.study_accession = :studyAccession ";

    	
    	Query query = em.createNativeQuery(queryString);
    	query.setParameter("studyAccession",studyAccession);
    	List <String> results = query.getResultList();
    	return results;
    }
    
    /**
     * Returns List of Strings representing Species.
     *
     * @return the List of Strings representing Species
     */
    public List <String> getSubjectSpecies(String studyAccession)
    {
    	String queryString = "SELECT "
                + "       distinct s1.species "
                + "  FROM subject s1, "
                + "       arm_2_subject a1, "
                + "       arm_or_cohort a2 "
                + " WHERE s1.subject_accession = a1.subject_accession "
                + "   AND a1.arm_accession = a2.arm_accession "
                + "   AND a2.study_accession = :studyAccession ";

    	
    	Query query = em.createNativeQuery(queryString);
    	query.setParameter("studyAccession",studyAccession);
    	List <String> results = query.getResultList();
    	return results;
    }
    
    /**
     * Returns List of Strings representing BiosampleType.
     *
     * @return the List of Strings representing BiosampleType
     */
    public List <String> getBiosampleType(String studyAccession)
    {
    	String queryString = "SELECT "
                + "       distinct b1.type "
                + "  FROM biosample b1 "
                + " WHERE b1.study_accession = :studyAccession ";

    	
    	Query query = em.createNativeQuery(queryString);
    	query.setParameter("studyAccession",studyAccession);
    	List <String> results = query.getResultList();
    	return results;
    }
    
    /**
     * Returns List of Strings representing ResearchFocus.
     *
     * @return the List of Strings representing ResearchFocus
     */
    public List <String> getExperimentMeasurementTechnique(String studyAccession)
    {
    	/*String queryString = "SELECT "
    		    + "       distinct e1.measurement_technique "
                + "  FROM experiment e1, "
    		    + "       expsample  b3, "
                + "       expsample_2_biosample b1, "
                + "       biosample b2 "
                + " WHERE e1.experiment_accession = b3.experiment_accession "
                + "   AND b3.expsample_accession = b1.expsample_accession "
                + "   AND b1.biosample_accession = b2.biosample_accession "
                + "   AND b2.study_accession = :studyAccession ";*/
    	String queryString = "SELECT "
    		    + "       distinct e1.measurement_technique "
                + "  FROM experiment e1 "    		 
                + " WHERE "
                + "    e1.study_accession = :studyAccession ";
    			
    	Query query = em.createNativeQuery(queryString);
    	query.setParameter("studyAccession",studyAccession);
    	List <String> results = query.getResultList();
    	return results;
    }
}

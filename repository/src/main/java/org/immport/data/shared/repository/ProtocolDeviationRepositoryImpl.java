package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.ProtocolDeviation;
import org.immport.data.shared.model.PageableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the ProtocolDeviation object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class ProtocolDeviationRepositoryImpl implements ProtocolDeviationRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ProtocolDeviationRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an ProtocolDeviation object by querying on the primary key.
     *
     * @param id primary key of the ProtocolDeviation object
     * @return the ProtocolDeviation object
     */
    public ProtocolDeviation findById(String id) {
        log.debug("getting ProtocolDeviation instance with id: " + id);
        ProtocolDeviation instance = em.find(ProtocolDeviation.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("ProtocolDeviation.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("ProtocolDeviation Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT p FROM ProtocolDeviation p ORDER BY " + sort + " " + dir;
        TypedQuery<ProtocolDeviation> query = em.createQuery(queryString,ProtocolDeviation.class)
            .setFirstResult(start).setMaxResults(limit);
        List <ProtocolDeviation> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of ProtocolDeviation objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ProtocolDeviation>
     */
    public List<ProtocolDeviation> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <ProtocolDeviation> query = em.createNamedQuery(
            "ProtocolDeviation.getByStudy", ProtocolDeviation.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.HlaTypingResult;

/**
 * Repository interface for the HlaTypingResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface HlaTypingResultRepository {

    /**
     * Returns a List of HlaTypingResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <HlaTypingResult>
     */
    public List<HlaTypingResult> getByStudy(String studyAccession);
}

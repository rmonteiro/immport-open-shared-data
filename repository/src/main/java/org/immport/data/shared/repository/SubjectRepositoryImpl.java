package org.immport.data.shared.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.DemograhicSummary;
import org.immport.data.shared.model.Subject;
import org.immport.data.shared.model.SubjectDemographic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the Subject object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class SubjectRepositoryImpl implements SubjectRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SubjectRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an Subject object by querying on the primary key.
     *
     * @param id primary key of the Subject object
     * @return the Subject object
     */
    public Subject findById(String id) {
        log.debug("getting Subject instance with id: " + id);
        Subject instance = em.find(Subject.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("Subject.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("Subject Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT s FROM Subject s ORDER BY " + sort + " " + dir;
        TypedQuery<Subject> query = em.createQuery(queryString,Subject.class)
            .setFirstResult(start).setMaxResults(limit);
        List <Subject> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of Subject objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Subject>
     */
    public List<Subject> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <Subject> query = em.createNamedQuery(
            "Subject.getByStudy", Subject.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
    
    
    /**
     * Returns a List of Demographic Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Demographic Summary data data)
     */
    public List<DemograhicSummary> getDemograhicSummaryByStudy(String studyAccession) {
    	ArrayList<DemograhicSummary> objAllDemographic = new ArrayList<DemograhicSummary>();
    	
    	//Min Age
	    Query query = em.createNamedQuery("Subject.getDemographicMinAge");
        query.setParameter("studyAccession", studyAccession);
        List<DemograhicSummary> objList = query.getResultList(); 
        for (int i=0;i<objList.size();i++) {
        	DemograhicSummary obj = (DemograhicSummary)objList.get(i);
        	Float fltValue = obj.getData();
        	int iValue = fltValue.intValue();
        	obj.setDisplayData(String.valueOf(iValue));
        }
        objAllDemographic.addAll(objList);
      
	    //Mean age 
        query = em.createNamedQuery("Subject.getDemographicMeanAge");
        query.setParameter("studyAccession", studyAccession);
        objList = query.getResultList();
    	for (int i=0;i<objList.size();i++) {
    		DemograhicSummary obj = (DemograhicSummary)objList.get(i);        		
    		double dbValue = roundDouble(obj.getAvgData(),3);
    		obj.setDisplayData(String.valueOf(dbValue));
    	}
    	objAllDemographic.addAll(objList);
      
        //Max age
        query = em.createNamedQuery("Subject.getDemographicMaxAge");
        query.setParameter("studyAccession", studyAccession);
        objList = query.getResultList();
        for (int i=0;i<objList.size();i++) {
    		DemograhicSummary obj = (DemograhicSummary)objList.get(i);        		
    		Float fltValue = obj.getData();
    		int iValue = fltValue.intValue();
    		obj.setDisplayData(String.valueOf(iValue));
        }
        objAllDemographic.addAll(objList);
    
        //All Counts
        query = em.createNamedQuery("Subject.getDemographicAll");
        query.setParameter("studyAccession", studyAccession);
        objList = query.getResultList();
        HashMap hashAll = new HashMap();
        for (int i=0;i<objList.size();i++) {
        	DemograhicSummary obj = (DemograhicSummary)objList.get(i);   
        	hashAll.put(obj.getArmAccession(), obj.getSubjAccNo());
        }
        
        //Male Count and Percent 
        ArrayList<DemograhicSummary> objMalePercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicGenderMale",
        				"Gender - Male (Percent of Arm)",objAllDemographic, hashAll);
        
        
      /*  query = em.createNamedQuery("Subject.getDemographicGenderMale");
        query.setParameter("studyAccession", studyAccession);
        objList = query.getResultList();
        ArrayList<DemograhicSummary> objMalePercent = new ArrayList<DemograhicSummary>();        
        for (int i=0;i<objList.size();i++) {
        		DemograhicSummary obj = (DemograhicSummary)objList.get(i);        		
        		long iValue = obj.getSubjAccNo();        		
        		obj.setDisplayData(String.valueOf(iValue));
        		
        		DemograhicSummary objDemo = new DemograhicSummary();        		
        		String sArm = obj.getArmAccession();
        		
        		long countAllGender = (long)hashAllGender.get(sArm);
        		
        		double dbPercent = ((double)iValue/(double)countAllGender) * 100;
        		//double dbPercentRound = roundDouble(dbPercent,3);
        		
        		String sdisplaydata = String.format("%.2f", dbPercent) + "%";
        		
        		objDemo.setArmAccession(sArm);
        		objDemo.setUnit("%");
        		objDemo.setStatistic("Gender Male (Percent of Arm)");
        		objDemo.setDisplayData(String.valueOf(sdisplaydata));
        		objMalePercent.add(objDemo);
        		
       	}
       	objAllDemographic.addAll(objList);*/
      
       	//Female Count and Percent
        ArrayList<DemograhicSummary> objFemalePercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicGenderFemale",
        				"Gender - Female (Percent of Arm)",objAllDemographic, hashAll);
        
             	
        //Other Unknown and Percents
        ArrayList<DemograhicSummary> objOtherUnknownPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicGenderOtherUnknown",
        				"Gender - Other/Unknown (Percent of Arm)",objAllDemographic, hashAll);
             
        	
        //Percent Male (Count)	
        objAllDemographic.addAll(objMalePercent);
        
        //Percent FeMale (Count)	
        objAllDemographic.addAll(objFemalePercent);
        
        //Percent Other Unknown (Count)
        objAllDemographic.addAll(objOtherUnknownPercent);
        
        //Race - White (Count) and Percent
        ArrayList<DemograhicSummary> objRaceWhitePercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicRaceWhite",
        				"Race - White (Percent of Arm)",objAllDemographic, hashAll);
        
        //Race - Black (Count)
        ArrayList<DemograhicSummary> objRaceBlackPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicRaceBlack",
        				"Race - Black (Percent of Arm)",objAllDemographic, hashAll);
        
                
        //Race - Black or African American (Count)
        ArrayList<DemograhicSummary> objRaceBlackOrAfricanAmericanPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicRaceBlackOrAfricanAmerican",
        				"Race - Black or African American (Percent of Arm)",objAllDemographic, hashAll);
                
        
        //Race - Asian (Count) and Percent
        ArrayList<DemograhicSummary> objRaceAsianPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicRaceAsian",
        				"Race - Asian (Percent of Arm)",objAllDemographic, hashAll);
        
       //Race - American Indian or Alaskan Native (Count) and Percent
        ArrayList<DemograhicSummary> objRaceAmericanIndianAlaskanNativePercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicRaceAmericanIndianOrAlaskanNative",
        				"Race - American Indian or Alaskan Native (Percent of Arm)",objAllDemographic, hashAll); 
        
        //Race - More than one Race (Count) and Percent
        ArrayList<DemograhicSummary> objRaceMoreThanOneRacePercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicRaceMoreThanOneRace",
        				"Race - More than one Race (Percent of Arm)",objAllDemographic, hashAll); 
        
       //Race - Other (Count) and Percent
        ArrayList<DemograhicSummary> objRaceOtherPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicRaceOther",
        				"Race - Other (Percent of Arm)",objAllDemographic, hashAll); 
        
        //Race - Unknown (Count) and Percent
        ArrayList<DemograhicSummary> objRaceUnknownPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicRaceUnknown",
        				"Race - Unknown (Percent of Arm)",objAllDemographic, hashAll); 
        
        //Race - NULL (Count) and Percent
        ArrayList<DemograhicSummary> objRaceNULLPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicRaceNULL",
        				"Race - NULL Value (Percent of Arm)",objAllDemographic, hashAll); 
        
        
        //Race - Other Freetext (Count)        
        ArrayList<DemograhicSummary> objRaceOtherFreetextPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicRaceOtherFreetext",
        				"Race - Other Freetext (Percent of Arm)",objAllDemographic, hashAll); 
        
        objAllDemographic.addAll(objRaceWhitePercent);
        objAllDemographic.addAll(objRaceBlackPercent);
        objAllDemographic.addAll(objRaceBlackOrAfricanAmericanPercent);
        objAllDemographic.addAll(objRaceAsianPercent);
        objAllDemographic.addAll(objRaceAmericanIndianAlaskanNativePercent);
        objAllDemographic.addAll(objRaceMoreThanOneRacePercent);
        objAllDemographic.addAll(objRaceOtherPercent);
        objAllDemographic.addAll(objRaceUnknownPercent);
        objAllDemographic.addAll(objRaceNULLPercent);
        objAllDemographic.addAll(objRaceOtherFreetextPercent);
        
        
        //Ethnicity - Not Hispanic or Latino (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityNotHispanicOrLatinoPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityNotHispanicOrLatino",
        				"Ethnicity - Not Hispanic or Latino (Percent of Arm)",objAllDemographic, hashAll); 
        
      //Ethnicity - Hispanic or Latino (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityHispanicOrLatinoPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityHispanicOrLatino",
        				"Ethnicity - Hispanic or Latino (Percent of Arm)",objAllDemographic, hashAll); 
        
        //Ethnicity - White (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityWhitePercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityWhite",
        				"Ethnicity - White (Percent of Arm)",objAllDemographic, hashAll);   
        
        //Ethnicity - Black (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityBlackPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityBlack",
        				"Ethnicity - Black (Percent of Arm)",objAllDemographic, hashAll);   
        
        
      //Ethnicity - Hispanic (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityHispanicPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityHispanic",
        				"Ethnicity - Hispanic (Percent of Arm)",objAllDemographic, hashAll);  
        
      //Ethnicity - Non-Hispanic (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityNonHispanicPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityNonHispanic",
        				"Ethnicity - Non-Hispanic (Percent of Arm)",objAllDemographic, hashAll);   
        
      //Ethnicity - Asian (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityAsianPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityAsian",
        				"Ethnicity - Asian (Percent of Arm)",objAllDemographic, hashAll); 
        
        //Ethnicity - Native Indians (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityNativeIndiansPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityNativeIndians",
        				"Ethnicity - Native Indians (Percent of Arm)",objAllDemographic, hashAll);   
        
      //Ethnicity - Other (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityOtherPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityOther",
        				"Ethnicity - Other (Percent of Arm)",objAllDemographic, hashAll);  
        
      //Ethnicity - Unknown (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityUnknownPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityUnknown",
        				"Ethnicity - Unknown (Percent of Arm)",objAllDemographic, hashAll);    
        
      //Ethnicity - NULL (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityNULLPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityNULL",
        				"Ethnicity - NULL Value (Percent of Arm)",objAllDemographic, hashAll); 
        
        
        //Ethnicity - Other Freetext (Count) and Percent      
        ArrayList<DemograhicSummary> objEthnicityOtherFreetextPercent = (ArrayList<DemograhicSummary>)
        		getCountAndPercent(studyAccession,"Subject.getDemographicEthnicityOtherFreetext",
        				"Ethnicity - Other Freetext (Percent of Arm)",objAllDemographic, hashAll); 
        
        objAllDemographic.addAll(objEthnicityNotHispanicOrLatinoPercent);
        objAllDemographic.addAll(objEthnicityHispanicOrLatinoPercent);
        objAllDemographic.addAll(objEthnicityWhitePercent);
        objAllDemographic.addAll(objEthnicityBlackPercent);
        objAllDemographic.addAll(objEthnicityHispanicPercent);
        objAllDemographic.addAll(objEthnicityNonHispanicPercent);
        objAllDemographic.addAll(objEthnicityAsianPercent);
        objAllDemographic.addAll(objEthnicityNativeIndiansPercent);
        objAllDemographic.addAll(objEthnicityOtherPercent);
        objAllDemographic.addAll(objEthnicityUnknownPercent);
        objAllDemographic.addAll(objEthnicityNULLPercent);
        objAllDemographic.addAll(objEthnicityOtherFreetextPercent);
        
                
	    return objAllDemographic;
    }
    
    
    
   private double roundDouble(double d, int places) {
        return Math.round(d * Math.pow(10, (double) places)) / Math.pow(10, (double)places);
    }
   
   
   
   private List<DemograhicSummary> getCountAndPercent(String studyAccession,String queryName,
		   String statistic,List<DemograhicSummary> allList, Map allArms)
   {
	   Query query = em.createNamedQuery(queryName);
       query.setParameter("studyAccession", studyAccession);
       List<DemograhicSummary> objList = query.getResultList(); 
       
       ArrayList<DemograhicSummary> objPercent = new ArrayList<DemograhicSummary>();        
   		for (int i=0;i<objList.size();i++) {
	   		DemograhicSummary obj = (DemograhicSummary)objList.get(i);        		
	   		long iValue = obj.getSubjAccNo();        		
	   		obj.setDisplayData(String.valueOf(iValue));
	   		
	   		DemograhicSummary objDemo = new DemograhicSummary();        		
	   		String sArm = obj.getArmAccession();
	   		
	   		long countAllGender = (long)allArms.get(sArm);
	   		
	   		double dbPercent = ((double)iValue/(double)countAllGender) * 100;
	   		//double dbPercentRound = roundDouble(dbPercent,3);
	   		
	   		String sdisplaydata = String.format("%.2f", dbPercent) + "%";
	   		
	   		objDemo.setArmAccession(sArm);
	   		objDemo.setUnit("%");
	   		objDemo.setStatistic(statistic);
	   		objDemo.setDisplayData(String.valueOf(sdisplaydata));
	   		objPercent.add(objDemo);    		
   		}
   		
   		allList.addAll(objList);
   		
   		return objPercent;
   }
   
   
   

   /**
    * Returns a List of Arm Cohort Names that are linked to the Demograhics for this
    * a study.
    *
    * @param studyAccession accession for the study
    * @return List <ArmCohortSortOrder>
    */
   public List<ArmCohortSortOrder> getDistinctArmNameForDemograhics(String studyAccession) {
	   log.debug("StudyAccession = " + studyAccession);
       Query query = em.createNamedQuery(
           "Subject.getDistinctArmNameForDemographics");
       query.setParameter("studyAccession", studyAccession);
       
       List<ArmCohortSortOrder> armCohortSortOrders = (List<ArmCohortSortOrder>)query.getResultList();
       Collections.sort(armCohortSortOrders);
       
       return armCohortSortOrders;
   }
    
   /**
    * Returns a List of Arm Cohort Names that are linked to the Demograhics for this
    * a study.
    *
    * @param studyAccession accession for the study
    * @return List <ArmCohortSortOrder>
    */
   public List<SubjectDemographic> getSubjectDemographicsByStudy(String studyAccession) {
	   log.debug("StudyAccession = " + studyAccession);
	   
	   String queryString = 
		        "SELECT distinct " +
	                "       s1.subject_accession as subjectAccession, " +
	                "       a1.age_event as ageEvent, " +
	                "       a1.age_event_specify as ageEventSpecify, " +
	                "       a1.min_subject_age as ageReported, " +
	                "       a1.age_unit as ageUnit, " +
	                "       s1.ethnicity as ethnicity, " +
	                "       s1.gender as gender, " +
	                "       null as phenotype, " +
	                "       s1.ancestral_population as populationName, " +
	                "       s1.race as race, " +
	                "       s1.race_specify as raceSpecify, " +
	                "       s1.species as species, " +
	                "       s1.strain as strain, " +
	                "       s2.taxonomy_id as taxonomyId, " +
	                "       a2.arm_accession as armAccession, " +
	                "       a2.name as armName " +
	                "  FROM subject s1, " +
	                "       lk_species s2, " +
	                "       arm_2_subject a1, " +
	                "       arm_or_cohort a2 " +
	                " WHERE s1.subject_accession = a1.subject_accession " +
	                "   AND s1.species = s2.name " + 
	                "   AND a1.arm_accession = a2.arm_accession " +
	                "   AND a2.study_accession = :studyAccession " +
	                "ORDER BY s1.subject_accession ";
	   
       Query query = em.createNativeQuery(queryString,SubjectDemographic.class);
       query.setParameter("studyAccession", studyAccession);
       List <SubjectDemographic> results = query.getResultList();
       
       return results;
   }
}

package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.TreatmentToExpSampleInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;


@Repository
public class TreatmentRepositoryImpl implements TreatmentRepository {
	
	 /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(TreatmentRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;
    
    
    /**
     * Returns a List of Arm Cohort Names that are linked to the Adeverse Events for this
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmCohortSortOrder>
     */
    public List<TreatmentToExpSampleInfo> getTreatmentToExpSample(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        Query query = em.createNamedQuery(
            "Treatment.getTreatmentToExpSample");
        query.setParameter("studyAccession", studyAccession);
        return (List<TreatmentToExpSampleInfo>)query.getResultList();
    }

}

package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PcrResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the PcrResult object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class PcrResultRepositoryImpl implements PcrResultRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(PcrResultRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns a List of PcrResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <PcrResult>
     */
    public List<PcrResult> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <PcrResult> query = em.createNamedQuery(
            "PcrResult.getByStudy", PcrResult.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}
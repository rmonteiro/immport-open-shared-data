package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Study2Panel;
import org.immport.data.shared.model.StudyFile;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class Study2PanelRepositoryImpl implements Study2PanelRepository {
	
	 /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(Study2PanelRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;
	
	 /**
     * Returns a List of StudyFile objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyFile>
     */
    public List<Study2Panel> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <Study2Panel> query = em.createNamedQuery(
            "Study2Panel.getByStudy", Study2Panel.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }

}

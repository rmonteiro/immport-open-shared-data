package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.StudyPubmed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the StudyPubmed object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class StudyPubmedRepositoryImpl implements StudyPubmedRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyPubmedRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;


    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("StudyPubmed.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("StudyPubmed Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT s FROM StudyPubmed s ORDER BY " + sort + " " + dir;
        TypedQuery<StudyPubmed> query = em.createQuery(queryString,StudyPubmed.class)
            .setFirstResult(start).setMaxResults(limit);
        List <StudyPubmed> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of StudyPubmed objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyPubmed>
     */
    public List<StudyPubmed> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <StudyPubmed> query = em.createNamedQuery(
            "StudyPubmed.getByStudy", StudyPubmed.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}
package org.immport.data.shared.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.SessionFactory;


public class RepositoryImpl implements Repository {

    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(RepositoryImpl.class);
    
    protected SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        log.debug("setting session factory to " + sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}

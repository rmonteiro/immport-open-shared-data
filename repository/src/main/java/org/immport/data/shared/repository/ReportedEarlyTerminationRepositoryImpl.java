package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.ReportedEarlyTermination;
import org.immport.data.shared.model.PageableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the ReportedEarlyTermination object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class ReportedEarlyTerminationRepositoryImpl implements ReportedEarlyTerminationRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ReportedEarlyTerminationRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an ReportedEarlyTermination object by querying on the primary key.
     *
     * @param id primary key of the ReportedEarlyTermination object
     * @return the ReportedEarlyTermination object
     */
    public ReportedEarlyTermination findById(String id) {
        log.debug("getting ReportedEarlyTermination instance with id: " + id);
        ReportedEarlyTermination instance = em.find(ReportedEarlyTermination.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("ReportedEarlyTermination.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("ReportedEarlyTermination Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT r FROM ReportedEarlyTermination r ORDER BY " + sort + " " + dir;
        TypedQuery<ReportedEarlyTermination> query = em.createQuery(queryString,ReportedEarlyTermination.class)
            .setFirstResult(start).setMaxResults(limit);
        List <ReportedEarlyTermination> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of ReportedEarlyTermination objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ReportedEarlyTermination>
     */
    public List<ReportedEarlyTermination> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <ReportedEarlyTermination> query = em.createNamedQuery(
            "ReportedEarlyTermination.getByStudy", ReportedEarlyTermination.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

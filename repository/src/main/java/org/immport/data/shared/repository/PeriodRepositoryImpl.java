package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.Period;
import org.immport.data.shared.model.PageableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the Period object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class PeriodRepositoryImpl implements PeriodRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(PeriodRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an Period object by querying on the primary key.
     *
     * @param id primary key of the Period object
     * @return the Period object
     */
    public Period findById(String id) {
        log.debug("getting Period instance with id: " + id);
        Period instance = em.find(Period.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("Period.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("Period Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT p FROM Period p ORDER BY " + sort + " " + dir;
        TypedQuery<Period> query = em.createQuery(queryString,Period.class)
            .setFirstResult(start).setMaxResults(limit);
        List <Period> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of Period objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Period>
     */
    public List<Period> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <Period> query = em.createNamedQuery(
            "Period.getByStudy", Period.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

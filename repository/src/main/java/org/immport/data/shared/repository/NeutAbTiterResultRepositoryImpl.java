package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.NeutAbTiterResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the NeutAbTiterResult object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class NeutAbTiterResultRepositoryImpl implements NeutAbTiterResultRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(NeutAbTiterResultRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns a List of NeutAbTiterResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <NeutAbTiterResult>
     */
    public List<NeutAbTiterResult> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <NeutAbTiterResult> query = em.createNamedQuery(
            "NeutAbTiterResult.getByStudy", NeutAbTiterResult.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}
package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.MbaaResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the MbaaResult object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class MbaaResultRepositoryImpl implements MbaaResultRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(MbaaResultRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns a List of MbaaResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <MbaaResult>
     */
    public List<MbaaResult> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <MbaaResult> query = em.createNamedQuery(
            "MbaaResult.getByStudy", MbaaResult.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}
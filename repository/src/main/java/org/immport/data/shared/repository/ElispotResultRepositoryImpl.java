package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.ElispotResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the ElispotResult object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class ElispotResultRepositoryImpl implements ElispotResultRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ElispotResultRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns a List of ElispotResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ElispotResult>
     */
    public List<ElispotResult> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <ElispotResult> query = em.createNamedQuery(
            "ElispotResult.getByStudy", ElispotResult.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}
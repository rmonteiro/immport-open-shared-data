package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Biosample;
import org.immport.data.shared.model.BiosampleStudyTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the Biosample object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class BiosampleRepositoryImpl implements BiosampleRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(BiosampleRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an Biosample object by querying on the primary key.
     *
     * @param id primary key of the Biosample object
     * @return the Biosample object
     */
    public Biosample findById(String id) {
        log.debug("getting Biosample instance with id: " + id);
        Biosample instance = em.find(Biosample.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("Biosample.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("Biosample Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT b FROM Biosample b ORDER BY " + sort + " " + dir;
        TypedQuery<Biosample> query = em.createQuery(queryString,Biosample.class)
            .setFirstResult(start).setMaxResults(limit);
        List <Biosample> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of Biosample objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Biosample>
     */
    public List<Biosample> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <Biosample> query = em.createNamedQuery(
            "Biosample.getByStudy", Biosample.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
    
    /**
     * Returns a List of BiosampleStudyTime objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <BiosampleStudyTime>
     */
    public List<BiosampleStudyTime> getStudyTimeCollectedByExperiment(String studyAccession)
    {
    	   // Query query = em.createNamedQuery("Biosample.getStudyTimeCollectedExperiment");
    	   Query query = em.createNamedQuery("Biosample.getStudyTimeCollectedExperimentPlannedVisit");
           query.setParameter("studyAccession", studyAccession);
           List<BiosampleStudyTime> lstBiosampleStudyTime = query.getResultList();
           return lstBiosampleStudyTime;
    }
    
    /**
     * Returns a List of BiosampleStudyTime objects that are linked to
     * a study via Lab Tests
     *
     * @param studyAccession accession for the study
     * @return List <BiosampleStudyTime>
     */
    public List<BiosampleStudyTime> getStudyTimeCollectedByLabTests(String studyAccession)
    {
    	  //Query query = em.createNamedQuery("Biosample.getStudyTimeCollectedLabTest");
    	 Query query = em.createNamedQuery("Biosample.getStudyTimeCollectedLabTestPlannedVisit");
         query.setParameter("studyAccession", studyAccession);
         List<BiosampleStudyTime> lstBiosampleStudyTime = query.getResultList();
         return lstBiosampleStudyTime;
    }
    
}

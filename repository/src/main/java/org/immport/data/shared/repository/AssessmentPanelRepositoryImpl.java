package org.immport.data.shared.repository;


import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.AssessmentPanel;
import org.immport.data.shared.model.AssessmentComponentList;
import org.immport.data.shared.model.AssessmentStudyTimeCollected;
import org.immport.data.shared.model.AssessmentSummary;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.BiosampleStudyTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the Assessment object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class AssessmentPanelRepositoryImpl implements AssessmentPanelRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(AssessmentPanelRepositoryImpl.class);
    
    private static final String ASSESSMENT_COMPONENT_QUERY = 
    		" SELECT  NEW org.immport.data.shared.model.AssessmentComponentList(a.study.studyAccession,a.nameReported, " +
    		" j.nameReported " +
    		") " +
    		" FROM AssessmentPanel a JOIN a.assessmentComponents j " +
            " WHERE a.study.studyAccession = :studyAccession " + 
            " GROUP BY a.study.studyAccession,a.nameReported, j.nameReported, j.verbatimQuestion ";

     private static final String ASSESSMENT_STUDY_TIME_QUERY =  
            " SELECT NEW org.immport.data.shared.model.AssessmentStudyTimeCollected(a.study.studyAccession,a.nameReported, j.studyDay, " +
            		"count(distinct j.subject.subjectAccession)) " +
            		" FROM AssessmentPanel a JOIN a.assessmentComponents j " +
                    " WHERE a.study.studyAccession = :studyAccession " +
                    " and j.studyDay != null " +
					" group by a.nameReported, j.studyDay";
            
    private static final String ASSESSMENT_SUMMARY_NAME = " SELECT  DISTINCT a.nameReported " +
            " FROM AssessmentPanel a JOIN a.assessmentComponents n JOIN n.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
            " WHERE m.study.studyAccession = :studyAccession " +
            " AND a.study.studyAccession = :studyAccession ";                           
            //" ORDER BY a.panelNameReported "
    
    private static final String ASSESSMENT_SUMMARY_QUERY = " SELECT  NEW org.immport.data.shared.model.AssessmentSummary(a.nameReported, m.armAccession, m.study.studyAccession,  " +
            " COUNT(DISTINCT j.subjectAccession) as subjAccNo,COUNT(n.assessmentComponentAccession) as assessmentNo) " +
            " FROM AssessmentPanel a JOIN a.assessmentComponents n JOIN n.subject j JOIN j.arm2Subjects k JOIN k.armOrCohort m " +
            " WHERE m.study.studyAccession = :studyAccession " +
            " AND a.study.studyAccession = :studyAccession " +
            " AND a.nameReported IN :panelNames " +
            " GROUP BY a.nameReported,m.armAccession ";
           // " ORDER BY a.panelNameReported,m.sortOrder ";
    
    @PersistenceContext
    private EntityManager em;
    
    private Map<String,Long> hashTotalCntDetail = new HashMap<String,Long>();
    private Map<String,Long> hashTotalCntAssessCompList = new HashMap<String,Long>();

    private Map<String,Long> hashTotalCntStudyTimeList = new HashMap<String,Long>();	
  //  private HashMap<String, List<AssessmentSummary>> hashAssessmentSummary = new HashMap<String, List<AssessmentSummary>>();
    
    /**
     * Returns an Assessment object by querying on the primary key.
     *
     * @param id primary key of the Assessment object
     * @return the Assessment object
     */
    public AssessmentPanel findById(String id) {
        log.debug("getting AssessmentPanel instance with id: " + id);
        AssessmentPanel instance = em.find(AssessmentPanel.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("AssessmentPanel.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("AssessmentPanel Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT a FROM AssessmentPanel a ORDER BY " + sort + " " + dir;
        TypedQuery<AssessmentPanel> query = em.createQuery(queryString,AssessmentPanel.class)
            .setFirstResult(start).setMaxResults(limit);
        List <AssessmentPanel> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of Assessment objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Assessment>
     */
    public List<AssessmentPanel> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <AssessmentPanel> query = em.createNamedQuery(
            "AssessmentPanel.getByStudy", AssessmentPanel.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
    
    
    /**
     * Returns a List of Assessment Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Summary data)
     */
    public PageableData getAssessmentPanelSummaryByStudy(Integer start, Integer limit,    		
            String sort, String dir,String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        PageableData page = new PageableData();
        //System.out.println("start:" + start + " limit:" + limit);
        
        List<AssessmentSummary> objAllAssessmentSummarySum = null;
        
        
        long iTotalCount = 0;
        Long savedTotalCount = (Long)hashTotalCntDetail.get(studyAccession);
        
        if (savedTotalCount != null) {
      	  iTotalCount = savedTotalCount.longValue();
        }
        else {
	          Query query = em.createNamedQuery(
	                    "AssessmentPanel.getAssessmentSummaryCount");
	          query.setParameter("studyAccession", studyAccession);
	          Long totalCount =  (Long)query.getSingleResult();
	          iTotalCount = totalCount * 2;
	          hashTotalCntDetail.put(studyAccession,Long.valueOf(iTotalCount));
        }
        Integer startTemp = 0;
        Integer limitTemp = 0;
        if (start != 0) {
        	startTemp = start/2;        		
   	 	}
        limitTemp = limit/2;
        
        page.setTotalCount(iTotalCount);
        
      //  objAllAssessmentSummarySum = ( List<AssessmentSummary>)hashAssessmentSummary.get(studyAccession + "-" + start);
        
     //   if (objAllAssessmentSummarySum == null)	{
        	 //ArrayList<AssessmentSummary> objAllAssessmentSummaryTemp = new ArrayList<AssessmentSummary>();
        	// Query query = em.createNamedQuery(
	        //            "Assessment.getAssessmentPanelNameSummary");
        	 
        	 String sqlAssessmentSummaryList= ASSESSMENT_SUMMARY_NAME + " ORDER BY a." +  sort + " " + dir;   	 
        	 Query query = em.createQuery(sqlAssessmentSummaryList);
        	
        	 query.setFirstResult(startTemp);
        	 query.setMaxResults(limitTemp);
        	 query.setParameter("studyAccession", studyAccession);
	         List<String> onjAssessmentPanelNameLst = (List<String>)query.getResultList();
        	
	         String sqlAssessmentSummaryQuery= ASSESSMENT_SUMMARY_QUERY + " ORDER BY a." +  sort + " " + dir;   	 
        	 query = em.createQuery(sqlAssessmentSummaryQuery);
	         
        	//query = em.createNamedQuery("Assessment.getAssessmentSummary");
		    query.setParameter("studyAccession", studyAccession);
		    query.setParameter("panelNames", onjAssessmentPanelNameLst);
		    objAllAssessmentSummarySum = (List<AssessmentSummary>)query.getResultList(); 
		  //  objAllAssessmentSummaryTemp.addAll(objAllAssessmentSummarySum);
		//    hashAssessmentSummary.put(studyAccession + "-" + start, objAllAssessmentSummaryTemp);
       // }
        page.setData(objAllAssessmentSummarySum);
        
        return page;
    }
    
    
    /**
     * Returns a List of Arm Cohort Names that are linked to the Adeverse Events for this
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmCohortSortOrder>
     */
    public List<ArmCohortSortOrder> getDistinctArmNameForAssessmentPanel(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        Query query = em.createNamedQuery(
            "AssessmentPanel.getDistinctArmNameForAssessment");
        query.setParameter("studyAccession", studyAccession);
        
        List<ArmCohortSortOrder> armCohortSortOrders = (List<ArmCohortSortOrder>)query.getResultList();
        Collections.sort(armCohortSortOrders);
        
        return armCohortSortOrders;
    }
    
    
    /**
     * Returns a List of Assessment Component that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Component)
     */
    public PageableData getAssessmentComponentListByStudy(Integer start, Integer limit,    		
            String sort, String dir,String studyAccession) {
    	 PageableData page = new PageableData();
    	 
    	 String sqlAssessmentCompList = "";
    	 Query queryDetail = null;
    	 
    	 long iTotalCount = 0;
    	 Long savedTotalCount = (Long)hashTotalCntAssessCompList.get(studyAccession);
         
         if (savedTotalCount != null) {
       	  iTotalCount = savedTotalCount.longValue();
         }
         else {        	 	 
        	 queryDetail = em.createNamedQuery("AssessmentPanel.getAssessmentCompListCount");
             queryDetail.setParameter("studyAccession", studyAccession);            
            // Long totalCount = (Long)queryDetail.getSingleResult();
             List objList = queryDetail.getResultList();
             //iTotalCount = totalCount;
             iTotalCount = objList.size();
 	         //hashTotalCntAssessCompList.put(studyAccession,totalCount);
             hashTotalCntAssessCompList.put(studyAccession,Long.valueOf(iTotalCount));		 
         }
         page.setTotalCount(iTotalCount);
         
    	 sqlAssessmentCompList= ASSESSMENT_COMPONENT_QUERY + " ORDER BY a." +  sort + " " + dir;   	 
         queryDetail = em.createQuery(sqlAssessmentCompList);
         queryDetail.setParameter("studyAccession", studyAccession);
         queryDetail.setFirstResult(start);
         queryDetail.setMaxResults(limit);
         List<AssessmentComponentList> objAssessmentComponentList = (List<AssessmentComponentList>)queryDetail.getResultList();
         page.setData(objAssessmentComponentList);
    	 
    	 return page;
    	
    }
    
    /**
     * Returns a List of BiosampleStudyTime objects that are linked to
     * a study
     *
     * @param studyAccession accession for the study
     * @return List <BiosampleStudyTime>
     */
    public List<BiosampleStudyTime> getStudyTimeCollectedByAssessments(String studyAccession)
    {
    	log.debug("StudyAccession = " + studyAccession);
        Query query = em.createNamedQuery(
            "AssessmentPanel.getStudyTimeCollectedAssessment");
        query.setParameter("studyAccession", studyAccession);
        return (List<BiosampleStudyTime>)query.getResultList();
    }
    
     /**
     * Returns a List of Assessment Component that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Component)
     */
    public PageableData getStudyTimeCollectedByAssessmentsPageable(Integer start, Integer limit,    		
            String sort, String dir,String studyAccession) {
    	 PageableData page = new PageableData();
    	 
    	 String sqlAssessmentStudyTimeList = "";
    	 Query queryDetail = null;
    	 
    	 long iTotalCount = 0;
    	 Long savedTotalCount = (Long)hashTotalCntStudyTimeList.get(studyAccession);
         
         if (savedTotalCount != null) {
       	   iTotalCount = savedTotalCount.longValue();
         }
         else {        	 	 
        	 queryDetail = em.createNamedQuery("AssessmentPanel.getStudyTimeCollectedAssessmentCount");
             queryDetail.setParameter("studyAccession", studyAccession);            
            // Long totalCount = (Long)queryDetail.getSingleResult();
             List objList = queryDetail.getResultList();
             //iTotalCount = totalCount;
             iTotalCount = objList.size();
 	         //hashTotalCntAssessCompList.put(studyAccession,totalCount);
             hashTotalCntStudyTimeList.put(studyAccession,Long.valueOf(iTotalCount));		 
         }
         page.setTotalCount(iTotalCount);
         
    	 sqlAssessmentStudyTimeList= ASSESSMENT_STUDY_TIME_QUERY + " ORDER BY a." +  sort + " " + dir;   	 
         queryDetail = em.createQuery(sqlAssessmentStudyTimeList);
         queryDetail.setParameter("studyAccession", studyAccession);
         queryDetail.setFirstResult(start);
         queryDetail.setMaxResults(limit);
         List<AssessmentStudyTimeCollected> objAssessmentStudyTimeList = (List<AssessmentStudyTimeCollected>)queryDetail.getResultList();
         page.setData(objAssessmentStudyTimeList);
    	 
    	 return page;
    	
    }
}

package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Protocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the Protocol object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class ProtocolRepositoryImpl implements ProtocolRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ProtocolRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an Protocol object by querying on the primary key.
     *
     * @param id primary key of the Protocol object
     * @return the Protocol object
     */
    public Protocol findById(String id) {
        log.debug("getting Protocol instance with id: " + id);
        Protocol instance = em.find(Protocol.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("Protocol.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("Protocol Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT p FROM Protocol p ORDER BY " + sort + " " + dir;
        TypedQuery<Protocol> query = em.createQuery(queryString,Protocol.class)
            .setFirstResult(start).setMaxResults(limit);
        List <Protocol> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of Protocol objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Protocol>
     */
    public List<Protocol> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <Protocol> query = em.createNamedQuery(
            "Protocol.getByStudy", Protocol.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Expsample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the Expsample object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class ExpsampleRepositoryImpl implements ExpsampleRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ExpsampleRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an Expsample object by querying on the primary key.
     *
     * @param id primary key of the Expsample object
     * @return the Expsample object
     */
    public Expsample findById(String id) {
        log.debug("getting Expsample instance with id: " + id);
        Expsample instance = em.find(Expsample.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("Expsample.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("Expsample Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT e FROM Expsample e ORDER BY " + sort + " " + dir;
        TypedQuery<Expsample> query = em.createQuery(queryString,Expsample.class)
            .setFirstResult(start).setMaxResults(limit);
        List <Expsample> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of Expsample objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Expsample>
     */
    public List<Expsample> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <Expsample> query = em.createNamedQuery(
            "Expsample.getByStudy", Expsample.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
    
    
    /**
     * Returns a count of Expsample that are linked to
     * a experimentAccession.
     *
     * @param studyAccession accession for the study
     * @return List <Expsample>
     */
    public long getCountByExperiment(String experimentAccession) {
        log.debug("experimentAccession = " + experimentAccession);
        Query query = em.createNamedQuery(
            "Expsample.getCountByExperiment");
        query.setParameter("experimentAccession", experimentAccession);
        return (long)query.getSingleResult();
    }
}

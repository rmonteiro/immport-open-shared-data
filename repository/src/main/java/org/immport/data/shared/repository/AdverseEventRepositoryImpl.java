package org.immport.data.shared.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.AdverseEvent;
import org.immport.data.shared.model.AdverseEventDetail;
import org.immport.data.shared.model.AdverseEventSummary;
import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.PageableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the AdverseEvent object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class AdverseEventRepositoryImpl implements AdverseEventRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(AdverseEventRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;
    
    private Map<String,Long> hashTotalCntDetail = new HashMap<String,Long>();
   // private HashMap<String, List<AdverseEventSummary>> hashAdverseEventSummary = new HashMap<String, List<AdverseEventSummary>>();
    
    private static final String ADVERSE_EVENT_DETAIL_NAME =	
    		//" SELECT  CONCAT(c.nameReported,c.severityReported) " +
    		" SELECT  CONCAT(c.nameReported,c.lkAdverseEventSeverity.name) " +
			" FROM AdverseEvent c JOIN c.subject j JOIN j.arm2Subjects b JOIN b.armOrCohort a " +
            " WHERE c.study.studyAccession = :studyAccession " +           
            " AND  a.study.studyAccession = :studyAccession " +
           // " GROUP BY c.nameReported,c.severityReported ";    
           " GROUP BY c.nameReported,c.lkAdverseEventSeverity.name "; 
           
    
    
    private static final String ADVERSE_EVENT_DETAIL_QUERY =
    				//" SELECT  NEW org.immport.data.shared.model.AdverseEventDetail(a.name,a.armAccession,a.study.studyAccession,c.nameReported,c.severityReported,count(c.subject.subjectAccession)) " +
    		" SELECT  NEW org.immport.data.shared.model.AdverseEventDetail(a.name,a.armAccession,a.study.studyAccession,c.nameReported,c.lkAdverseEventSeverity.name,count(c.subject.subjectAccession)) " +
    				" FROM AdverseEvent c JOIN c.subject j JOIN j.arm2Subjects b JOIN b.armOrCohort a " +
    	            " WHERE c.study.studyAccession = :studyAccession " +           
    	            " AND  a.study.studyAccession = :studyAccession " +
    	          //  " AND CONCAT(c.nameReported,c.severityReported) IN :nameReport " +
    	          " AND CONCAT(c.nameReported,c.lkAdverseEventSeverity.name) IN :nameReport " +
    	          //  " GROUP BY a.name,a.armAccession,a.study.studyAccession,c.nameReported,c.severityReported ";
    	          " GROUP BY a.name,a.armAccession,a.study.studyAccession,c.nameReported,c.lkAdverseEventSeverity.name ";
    	            
    	               

    /**
     * Returns an AdverseEvent object by querying on the primary key.
     *
     * @param id primary key of the AdverseEvent object
     * @return the AdverseEvent object
     */
    public AdverseEvent findById(String id) {
        log.debug("getting AdverseEvent instance with id: " + id);
        AdverseEvent instance = em.find(AdverseEvent.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("AdverseEvent.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("AdverseEvent Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT a FROM AdverseEvent a ORDER BY " + sort + " " + dir;
        TypedQuery<AdverseEvent> query = em.createQuery(queryString,AdverseEvent.class)
            .setFirstResult(start).setMaxResults(limit);
        List <AdverseEvent> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of AdverseEvent objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <AdverseEvent>
     */
    public List<AdverseEvent> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <AdverseEvent> query = em.createNamedQuery(
            "AdverseEvent.getByStudy", AdverseEvent.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
    
    
    /**
     * Returns a List of Adverse Event Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Adverse Event Summary data)
     */
    public List<AdverseEventSummary> getAdverseEventSummaryByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        List<AdverseEventSummary> objAllAdverseEvntSum = null;
        
     //   objAllAdverseEvntSum = ( List<AdverseEventSummary>)hashAdverseEventSummary.get(studyAccession);
        
       // if (objAllAdverseEvntSum == null)
     //   {
        
		        objAllAdverseEvntSum = new ArrayList<AdverseEventSummary>();
		        Query query = em.createNamedQuery("AdverseEvent.getAdverseEventSummaryDeath");
		        query.setParameter("studyAccession", studyAccession);
		        List<AdverseEventSummary> objList = query.getResultList(); 
		        
		        if (objList.size() == 0)
		        {	 AdverseEventSummary obj = new AdverseEventSummary();
		        	 obj.setDataType("Grade 5 Death Related to Adverse Events");
		        	 objAllAdverseEvntSum.add(obj);
		        }
		        objAllAdverseEvntSum.addAll(objList);
		        
		        query = em.createNamedQuery("AdverseEvent.getAdverseEventSummaryLifeThreatening");
		        query.setParameter("studyAccession", studyAccession);
		        objList = query.getResultList();        
		
		        if (objList.size() == 0)
		        {	 AdverseEventSummary obj = new AdverseEventSummary();
		        	 obj.setDataType("Grade 4 Life Threatening or Disabling Adverse Events");
		        	 objAllAdverseEvntSum.add(obj);
		        }
		        objAllAdverseEvntSum.addAll(objList);
		        
		        query = em.createNamedQuery("AdverseEvent.getAdverseEventSummaryMild");
		        query.setParameter("studyAccession", studyAccession);
		        objList = query.getResultList();     
		        if (objList.size() == 0)
		        {	 AdverseEventSummary obj = new AdverseEventSummary();
		        	 obj.setDataType("Grade 1 Mild Adverse Events");
		        	 objAllAdverseEvntSum.add(obj);
		        }
		        objAllAdverseEvntSum.addAll(objList);
		        
		        query = em.createNamedQuery("AdverseEvent.getAdverseEventSummaryModerate");
		        query.setParameter("studyAccession", studyAccession);
		        objList = query.getResultList();   
		        if (objList.size() == 0)
		        {	 AdverseEventSummary obj = new AdverseEventSummary();
		        	 obj.setDataType("Grade 2 Moderate Adverse Events");
		        	 objAllAdverseEvntSum.add(obj);
		        }
		        objAllAdverseEvntSum.addAll(objList);
		        
		        query = em.createNamedQuery("AdverseEvent.getAdverseEventSummarySevere");
		        query.setParameter("studyAccession", studyAccession);
		        objList = query.getResultList();      
		        if (objList.size() == 0)
		        {	 AdverseEventSummary obj = new AdverseEventSummary();
		        	 obj.setDataType("Grade 3 Severe Adverse Events");
		        	 objAllAdverseEvntSum.add(obj);
		        }
		        objAllAdverseEvntSum.addAll(objList);     
		        
		        query = em.createNamedQuery("AdverseEvent.getAdverseEventSummarySubect_in_ARM");
		        query.setParameter("studyAccession", studyAccession);
		        objList = query.getResultList();      
		        if (objList.size() == 0)
		        {	 AdverseEventSummary obj = new AdverseEventSummary();
		        	 obj.setDataType("Subjects");
		        	 objAllAdverseEvntSum.add(obj);
		        }
		        objAllAdverseEvntSum.addAll(objList);
		        
		        query = em.createNamedQuery("AdverseEvent.getAdverseEventSummarySubject_w_AE");
		        query.setParameter("studyAccession", studyAccession);
		        objList = query.getResultList();  
		        if (objList.size() == 0)
		        {	 AdverseEventSummary obj = new AdverseEventSummary();
		        	 obj.setDataType("Subjects with Adverse Events");
		        	 objAllAdverseEvntSum.add(obj);
		        }
		        objAllAdverseEvntSum.addAll(objList);        
		        
		        query = em.createNamedQuery("AdverseEvent.getAdverseEventSummaryTotalAdverseEvent");
		        query.setParameter("studyAccession", studyAccession);
		        objList = query.getResultList();    
		        if (objList.size() == 0)
		        {	 AdverseEventSummary obj = new AdverseEventSummary();
		        	 obj.setDataType("Total Adverse Events");
		        	 objAllAdverseEvntSum.add(obj);
		        }
		        objAllAdverseEvntSum.addAll(objList);
		     //   hashAdverseEventSummary.put(studyAccession, objAllAdverseEvntSum);
      //  }
        
        return objAllAdverseEvntSum;
       
    }
    
    
    /**
     * Returns a Pageable data of Adverse Event Detail that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return PageableData (Adverse Event Detail data)
     */
    public PageableData getAdverseEventDetailByStudy(Integer start, Integer limit,
            String sort, String dir, String studyAccession)
    {
          PageableData page = new PageableData();
          long iTotalCount = 0;
          Long savedTotalCount = (Long)hashTotalCntDetail.get(studyAccession);
          
          if (savedTotalCount != null)
          {
        	  iTotalCount = savedTotalCount.longValue();
          }
          else
          {
	          Query query = em.createNamedQuery(
	                    "AdverseEvent.getAdverseEventDetailCount");
	          query.setParameter("studyAccession", studyAccession);
	          List totalCountList =  query.getResultList();
	          iTotalCount = totalCountList.size();
	          hashTotalCntDetail.put(studyAccession,Long.valueOf(iTotalCount));
          }
          
       
          page.setTotalCount(iTotalCount);
          if (iTotalCount > 0) {
        	  String sqlConMedDetName = "";
        	  if (sort.equalsIgnoreCase("lkAdverseEventSeverity.name"))
        	  {
        		  sqlConMedDetName= ADVERSE_EVENT_DETAIL_NAME + " ORDER BY c." +  sort + " " + dir + ",c.nameReported ASC ";
        	  }
        	  else if (sort.equalsIgnoreCase("nameReported"))
        	  {
        		  sqlConMedDetName= ADVERSE_EVENT_DETAIL_NAME + " ORDER BY c." +  sort + " " + dir + ",c.lkAdverseEventSeverity.name ASC" ;
        	  }
        	  else
        	  {
        		  sqlConMedDetName= ADVERSE_EVENT_DETAIL_NAME + " ORDER BY c.lkAdverseEventSeverity.name,c.nameReported " + dir; 
        	  }
	          Query queryDetail = em.createQuery(sqlConMedDetName);
	          queryDetail.setParameter("studyAccession", studyAccession);
	          queryDetail.setFirstResult(start);
	          queryDetail.setMaxResults(limit);
	          List<String> onjAdverseEventNameLst = (List<String>)queryDetail.getResultList();
	          
	          if (sort.equalsIgnoreCase("lkAdverseEventSeverity.name"))
        	  {
        		  sqlConMedDetName= ADVERSE_EVENT_DETAIL_QUERY + " ORDER BY c." +  sort + " " + dir + ",c.nameReported ASC ";
        	  }
        	  else if (sort.equalsIgnoreCase("nameReported"))
        	  {
        		  sqlConMedDetName= ADVERSE_EVENT_DETAIL_QUERY + " ORDER BY c." +  sort + " " + dir + ",c.lkAdverseEventSeverity.name ASC" ;
        	  }
        	  else
        	  {
        		  sqlConMedDetName= ADVERSE_EVENT_DETAIL_QUERY + " ORDER BY c.lkAdverseEventSeverity.name,c.nameReported " + dir; 
        	  }
	        
	          Query queryDataDetail = em.createQuery(sqlConMedDetName);
	          queryDataDetail.setParameter("studyAccession", studyAccession);
	          queryDataDetail.setParameter("nameReport", onjAdverseEventNameLst);
	          List<AdverseEventDetail> objData = ( List<AdverseEventDetail>)queryDataDetail.getResultList();	
	          page.setData(objData);	          
          }
          
          
	     
          return page;
    }   
    
    
    /**
     * Returns a List of Arm Cohort Names that are linked to the Adeverse Events for this
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmCohortSortOrder>
     */
    public List<ArmCohortSortOrder> getDistinctArmNameForAdverseEvent(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        Query query = em.createNamedQuery(
            "AdverseEvent.getDistinctArmNameForAdverseEvent");
        query.setParameter("studyAccession", studyAccession);
        
        List<ArmCohortSortOrder> armCohortSortOrders = (List<ArmCohortSortOrder>)query.getResultList();
        Collections.sort(armCohortSortOrders);
        
        return armCohortSortOrders;
    }
    
    
    
    
    
}

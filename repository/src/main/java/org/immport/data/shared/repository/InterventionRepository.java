package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.ConcomitantMedicationSummary;
import org.immport.data.shared.model.Intervention;
import org.immport.data.shared.model.PageableData;

/**
 * Repository interface for the SubstanceMerge object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface InterventionRepository {
    /**
     * Returns an SubstanceMerge object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the SubstanceMerge object
     */
    public Intervention findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of SubstanceMerge objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <SubstanceMerge>
     */
    public List<Intervention> getByStudy(String studyAccession);
 
    /**
     * Returns a List of Concomitant Medication Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Concomitant Medication Summary data)
     */
    public List<ConcomitantMedicationSummary> getConcomitantMedicationByStudy(String studyAccession);


    /**
     * Returns a List of Concomitant Medication Detail data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Concomitant Medication Deatil data)
     */
    public PageableData getConcomitantMedDetailByStudy(Integer start, Integer limit,
            String sort, String dir, String studyAccession);
    
    
    
    /**
     * Returns a List of Substance Use Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Concomitant Medication Summary data)
     */
    public List<ConcomitantMedicationSummary> getSubstanceUseSummaryByStudy(String studyAccession);


    /**
     * Returns a List of Substance Use Detail data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Concomitant Medication Deatil data)
     */
    public PageableData getSubstanceUseDetailByStudy(Integer start, Integer limit,
            String sort, String dir, String studyAccession);
    
    
    /**
     * Returns a Pageable data of Study Intervention Detail that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return PageableData (Study Intervention Detail data)
     */
    public PageableData getStudyInterventionDetailByStudy(Integer start, Integer limit,
            String sort, String dir, String studyAccession);
    
    /**
     * Returns a List of Arm Cohort Names that are linked to the Interventions for this
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmCohortSortOrder>
     */
    public List<ArmCohortSortOrder> getDistinctArmNameForCompoundRole(String studyAccession,String type);
    
}

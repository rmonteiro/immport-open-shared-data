package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.StudyGlossary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the StudyGlossary object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class StudyGlossaryRepositoryImpl implements StudyGlossaryRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyGlossaryRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;


    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("StudyGlossary.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("StudyGlossary Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT s FROM StudyGlossary s ORDER BY " + sort + " " + dir;
        TypedQuery<StudyGlossary> query = em.createQuery(queryString,StudyGlossary.class)
            .setFirstResult(start).setMaxResults(limit);
        List <StudyGlossary> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of StudyGlossary objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyGlossary>
     */
    public List<StudyGlossary> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <StudyGlossary> query = em.createNamedQuery(
            "StudyGlossary.getByStudy", StudyGlossary.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}
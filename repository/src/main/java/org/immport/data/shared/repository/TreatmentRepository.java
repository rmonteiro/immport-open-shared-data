package org.immport.data.shared.repository;

import java.util.List;


import org.immport.data.shared.model.TreatmentToExpSampleInfo;



public interface TreatmentRepository {
	
	/**
     * Returns a List of TreatmentToExpSample Info are linked to this
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <TreatmentToExpSampleInfo>
     */
    public List<TreatmentToExpSampleInfo> getTreatmentToExpSample(String studyAccession);

}

package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.HlaTypingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the HlaTypingResult object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class HlaTypingResultRepositoryImpl implements HlaTypingResultRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(HlaTypingResultRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns a List of HlaTypingResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <HlaTypingResult>
     */
    public List<HlaTypingResult> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <HlaTypingResult> query = em.createNamedQuery(
            "HlaTypingResult.getByStudy", HlaTypingResult.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}
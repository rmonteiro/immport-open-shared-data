package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.KirTypingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the KirTypingResult object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class KirTypingResultRepositoryImpl implements KirTypingResultRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(KirTypingResultRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns a List of KirTypingResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <KirTypingResult>
     */
    public List<KirTypingResult> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <KirTypingResult> query = em.createNamedQuery(
            "KirTypingResult.getByStudy", KirTypingResult.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}
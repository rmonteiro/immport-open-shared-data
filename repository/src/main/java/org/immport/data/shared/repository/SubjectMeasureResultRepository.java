package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.SubjectMeasureResult;
import org.immport.data.shared.model.PageableData;

/**
 * Repository interface for the SubjectMeasureResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface SubjectMeasureResultRepository {
    /**
     * Returns an SubjectMeasureResult object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the SubjectMeasureResult object
     */
    public SubjectMeasureResult findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of SubjectMeasureResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <SubjectMeasureResult>
     */
    public List<SubjectMeasureResult> getByStudy(String studyAccession);
}

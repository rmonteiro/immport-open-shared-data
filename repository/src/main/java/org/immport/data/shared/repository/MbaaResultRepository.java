package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.MbaaResult;

/**
 * Repository interface for the MbaaResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface MbaaResultRepository {

    /**
     * Returns a List of MbaaResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <MbaaResult>
     */
    public List<MbaaResult> getByStudy(String studyAccession);
}

package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.PcrResult;

/**
 * Repository interface for the PcrResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface PcrResultRepository {
    /**
     * Returns a List of PcrResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <PcrResult>
     */
    public List<PcrResult> getByStudy(String studyAccession);
}

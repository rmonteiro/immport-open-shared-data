package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.StudyPubmed;

/**
 * Repository interface for the StudyPubmed object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface StudyPubmedRepository {

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of StudyPubmed objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyPubmed>
     */
    public List<StudyPubmed> getByStudy(String studyAccession);
}
package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Experiment;
import org.immport.data.shared.model.Reagent;
import org.immport.data.shared.model.Treatment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the Experiment object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class ExperimentRepositoryImpl implements ExperimentRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ExperimentRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;	

    /**
     * Returns an Experiment object by querying on the primary key.
     *
     * @param id primary key of the Experiment object
     * @return the Experiment object
     */
    public Experiment findById(String id) {
        log.debug("getting Experiment instance with id: " + id);
        Experiment instance = em.find(Experiment.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("Experiment.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("Experiment Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT e FROM Experiment e ORDER BY " + sort + " " + dir;
        TypedQuery<Experiment> query = em.createQuery(queryString,Experiment.class)
            .setFirstResult(start).setMaxResults(limit);
        List <Experiment> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of Experiment objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Experiment>
     */
    public List<Experiment> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <Experiment> query = em.createNamedQuery(
            "Experiment.getByStudy", Experiment.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
    
    /**
     * Returns an Experiment object by querying on the primary key.
     *
     * @param id primary key of the Experiment object
     * @return the Experiment object
     */
    public Experiment getExperimentDetail(String id) {
    	Query query = em.createQuery("select e from Experiment e join fetch e.protocols where e.experimentAccession = :id");
    	query.setParameter("id", id);
    	Experiment experiment = null;
    	try {
    	   experiment = (Experiment) query.getSingleResult();
    	} catch (NoResultException e) {
    	   // no result found
    	}
    	
    	TypedQuery <Reagent> rquery = em.createNamedQuery("Reagent.getByExperiment",Reagent.class);
    	rquery.setParameter("experimentAccession",id);
    	List <Reagent> reagents = rquery.getResultList();
    	experiment.setReagents(reagents);
    	
    	TypedQuery <Treatment> tquery = em.createNamedQuery("Treatment.getByExperiment",Treatment.class);
    	tquery.setParameter("experimentAccession",id);
    	List <Treatment> treatments = tquery.getResultList();
    	experiment.setTreatments(treatments);

    	return experiment;
    	/*
        Experiment instance = em.find(Experiment.class, id);
        List <Protocol> protocols = em.findEager(Experiment.class, id).getProtocols();
        instance.setProtocols(protocols);
        return instance;
        */
    }
}

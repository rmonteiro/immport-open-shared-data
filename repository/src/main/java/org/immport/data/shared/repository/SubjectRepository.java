package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.DemograhicSummary;
import org.immport.data.shared.model.Subject;
import org.immport.data.shared.model.SubjectDemographic;

/**
 * Repository interface for the Subject object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface SubjectRepository {
    /**
     * Returns an Subject object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the Subject object
     */
    public Subject findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of Subject objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Subject>
     */
    public List<Subject> getByStudy(String studyAccession);
    
    
    /**
     * Returns a List of Demographic Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Demographic Summary data data)
     */
    public List<DemograhicSummary> getDemograhicSummaryByStudy(String studyAccession);
    
    /**
     * Returns a List of Arm Cohort Names that are linked to the Demograhics for this
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmCohortSortOrder>
     */
    public List<ArmCohortSortOrder> getDistinctArmNameForDemograhics(String studyAccession);
    
    /**
     * Returns a List of SubjectDemographic Information
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <SubjectDemographics>
     */
    public List<SubjectDemographic> getSubjectDemographicsByStudy(String studyAccession);
}

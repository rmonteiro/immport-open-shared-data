package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.ElisaResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the ElisaMbaaResult object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class ElisaResultRepositoryImpl implements ElisaResultRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ElisaResultRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns a List of ElisaMbaaResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ElisaMbaaResult>
     */
    public List<ElisaResult> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <ElisaResult> query = em.createNamedQuery(
            "ElisaResult.getByStudy", ElisaResult.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

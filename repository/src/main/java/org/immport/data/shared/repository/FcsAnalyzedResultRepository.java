package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.FcsAnalyzedResult;

/**
 * Repository interface for the FcsAnalyzedResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface FcsAnalyzedResultRepository {

    /**
     * Returns a List of FcsAnalyzedResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <FcsAnalyzedResult>
     */
    public List<FcsAnalyzedResult> getByStudy(String studyAccession);
}
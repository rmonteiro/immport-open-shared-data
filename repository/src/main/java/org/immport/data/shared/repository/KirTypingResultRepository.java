package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.KirTypingResult;

/**
 * Repository interface for the KirTypingResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface KirTypingResultRepository {

    /**
     * Returns a List of KirTypingResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <KirTypingResult>
     */
    public List<KirTypingResult> getByStudy(String studyAccession);
}

package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.ReferenceRange;
import org.immport.data.shared.model.PageableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the ReferenceRange object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class ReferenceRangeRepositoryImpl implements ReferenceRangeRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ReferenceRangeRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an ReferenceRange object by querying on the primary key.
     *
     * @param id primary key of the ReferenceRange object
     * @return the ReferenceRange object
     */
    public ReferenceRange findById(String id) {
        log.debug("getting ReferenceRange instance with id: " + id);
        ReferenceRange instance = em.find(ReferenceRange.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("ReferenceRange.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("ReferenceRange Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT r FROM ReferenceRange r ORDER BY " + sort + " " + dir;
        TypedQuery<ReferenceRange> query = em.createQuery(queryString,ReferenceRange.class)
            .setFirstResult(start).setMaxResults(limit);
        List <ReferenceRange> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of ReferenceRange objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ReferenceRange>
     */
    public List<ReferenceRange> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <ReferenceRange> query = em.createNamedQuery(
            "ReferenceRange.getByStudy", ReferenceRange.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

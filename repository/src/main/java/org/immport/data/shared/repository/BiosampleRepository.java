package org.immport.data.shared.repository;

import java.util.List;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Biosample;
import org.immport.data.shared.model.BiosampleStudyTime;

/**
 * Repository interface for the Biosample object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface BiosampleRepository {
    /**
     * Returns an Biosample object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the Biosample object
     */
    public Biosample findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of Biosample objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Biosample>
     */
    public List<Biosample> getByStudy(String studyAccession);
    
    /**
     * Returns a List of BiosampleStudyTime objects that are linked to
     * a study via Experiments
     *
     * @param studyAccession accession for the study
     * @return List <BiosampleStudyTime>
     */
    public List<BiosampleStudyTime> getStudyTimeCollectedByExperiment(String studyAccession);
    
    /**
     * Returns a List of BiosampleStudyTime objects that are linked to
     * a study via Lab Tests
     *
     * @param studyAccession accession for the study
     * @return List <BiosampleStudyTime>
     */
    public List<BiosampleStudyTime> getStudyTimeCollectedByLabTests(String studyAccession);
    
}

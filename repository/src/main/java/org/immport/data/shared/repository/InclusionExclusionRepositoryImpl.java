package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.InclusionExclusion;
import org.immport.data.shared.model.PageableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the InclusionExclusion object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class InclusionExclusionRepositoryImpl implements InclusionExclusionRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(InclusionExclusionRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an InclusionExclusion object by querying on the primary key.
     *
     * @param id primary key of the InclusionExclusion object
     * @return the InclusionExclusion object
     */
    public InclusionExclusion findById(String id) {
        log.debug("getting InclusionExclusion instance with id: " + id);
        InclusionExclusion instance = em.find(InclusionExclusion.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("InclusionExclusion.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("InclusionExclusion Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT i FROM InclusionExclusion i ORDER BY " + sort + " " + dir;
        TypedQuery<InclusionExclusion> query = em.createQuery(queryString,InclusionExclusion.class)
            .setFirstResult(start).setMaxResults(limit);
        List <InclusionExclusion> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of InclusionExclusion objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <InclusionExclusion>
     */
    public List<InclusionExclusion> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <InclusionExclusion> query = em.createNamedQuery(
            "InclusionExclusion.getByStudy", InclusionExclusion.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

package org.immport.data.shared.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.StudyImage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository class for the StudyImage object. Contains methods
 * for retrieving information based on different query parameters
 */

@Repository
public class StudyImageRepositoryImpl implements StudyImageRepository {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyImageRepositoryImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    /**
     * Returns an StudyImage object by querying on the primary key.
     *
     * @param id primary key of the StudyImage object
     * @return the StudyImage object
     */
    public StudyImage findById(String id) {
        log.debug("getting StudyImage instance with id: " + id);
        StudyImage instance = em.find(StudyImage.class, id);
        log.debug("Find successful");
        return instance;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir) {
        PageableData page = new PageableData();
        Query countQuery = em.createNamedQuery("StudyImage.getCount");
        Long count = (Long) countQuery.getSingleResult();
        log.debug("StudyImage Count: " + count);
        page.setTotalCount(count);

        String queryString = "SELECT s FROM StudyImage s ORDER BY " + sort + " " + dir;
        TypedQuery<StudyImage> query = em.createQuery(queryString,StudyImage.class)
            .setFirstResult(start).setMaxResults(limit);
        List <StudyImage> results = query.getResultList();
        page.setData(results);

        return page;
    }

    /**
     * Returns a List of StudyImage objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyImage>
     */
    public List<StudyImage> getByStudy(String studyAccession) {
        log.debug("StudyAccession = " + studyAccession);
        TypedQuery <StudyImage> query = em.createNamedQuery(
            "StudyImage.getByStudy", StudyImage.class);
        query.setParameter("studyAccession", studyAccession);
        return query.getResultList();
    }
}

package org.immport.open.web.controller.common;

import java.util.List;
import org.immport.data.shared.model.GenericPivotData;

/**
 * @author fmonteiro
 *
 */
public final class JSONStringBuilderUtilNew {
	
	
	private JSONStringBuilderUtilNew() {
		
	}
	
	/** 
     * Returns a JSON String using the list of objects 
     *
     * @param pDataList list
     * @return JSON String
     */
	public static String buildJSONStringfromList(List pDataList,long pTotalCount)
	{
		return buildJSONStringfromList(pDataList,pTotalCount,true,"");		
	}
	
	
	public static String buildJSONStringfromList(List objList,long totalCount,boolean success, String message)
	{
		StringBuffer resultBuf = new StringBuffer();
		resultBuf.append("{\n");
		resultBuf.append("  \"metaData\": {\n");
		if (success) {
			resultBuf.append("    \"success\": true,\n");
		} else {
			resultBuf.append("    \"success\": false,\n");
		}
		resultBuf.append("    \"message\": \"" + message + "\",\n");
		resultBuf.append("    \"total\": " + totalCount + ",\n");
		resultBuf.append(buildColumns(objList));
		resultBuf.append("  },\n");
		
		resultBuf.append(buildDataStore(objList));
		
		resultBuf.append("}\n");		
		return resultBuf.toString();
	}
	
	
	/** 
     * Returns a JSON String for the Data Store 
     *
     * @param objList list
     * @return JSON String
     */
	private static String buildDataStore(List objList) {
		StringBuffer resultBuf = new StringBuffer();
		resultBuf.append("  \"data\": [\n");
		for (int i = 0; i < objList.size(); i++) {
			Object objData = objList.get(i);
			String objResult = buildDataStoreForObject(objData);
			if (i == (objList.size() -1)) {
				resultBuf.append("    " + objResult + "\n");
			} else {
				resultBuf.append("    " + objResult + ",\n");
			}
		}
		resultBuf.append("  ]\n");
		return resultBuf.toString();
	}
	
	/** 
     * Returns a JSON String for the object passed in 
     *
     * @param Object list
     * @return JSON String
     */
	private static String buildDataStoreForObject(Object objData) {
		StringBuffer resultBuf = new StringBuffer();
		List<String> objList = null;

		resultBuf.append("[");
		int count = 0;
		if (objData instanceof GenericPivotData){
			List<String> objDataLst = ((GenericPivotData)objData).getBaseData();	
			for (int i=0; i<objDataLst.size(); i++) {
				if (i == (objDataLst.size() -1)) {
					//resultBuf.append("\"col" + count++ + "\": \""
					//	+ objDataLst.get(i) + "\"");
					resultBuf.append("\"" + objDataLst.get(i) + "\"");
				} else {
					//resultBuf.append("\"col" + count++ + "\": \""
					//		+ objDataLst.get(i) + "\",");
					resultBuf.append("\"" + objDataLst.get(i) + "\",");
				}
			}
			
			objList = ((GenericPivotData) objData).getArmData();
			if (objList != null) {
				resultBuf.append(",");
			}
		}
			
        if (objList != null) {
			for (int i = 0; i < objList.size(); i++) {
				if (i == (objList.size() - 1)) {
					//resultBuf.append("\"col" + count++ + "\": \""
					//		+ ((objList.get(i) == null) ? "" : objList.get(i))
					//		+ "\"");
					resultBuf.append("\"" + objList.get(i) + "\"");
				} else {
					//resultBuf.append("\"col" + count++ + "\": \""
					//		+ ((objList.get(i) == null) ? "" : objList.get(i))
					//		+ "\",");
					resultBuf.append("\"" + objList.get(i) + "\",");
				}
			}
        }
		resultBuf.append("]");
		return resultBuf.toString();
	}

	
	/** 
     * Returns a JSON String for the Columns 
     *
     * @param Object list
     * @return JSON String
     */
	private static String buildColumns(List objList) {
		StringBuffer resultBuf = new StringBuffer();
		boolean isArmNames = false;
		List<String> objArmData = null;
		List<String> objArmName = null;
		resultBuf.append("    \"columns\" : [\n");

		int count = 0;
		if (objList.size() > 0) {
			Object obj = objList.get(0);
			
			if (obj instanceof GenericPivotData){
				List<String> objDataLst = ((GenericPivotData)obj).getBaseNames();
				List<String> objDataLstSort = ((GenericPivotData)obj).getBaseNamesSort();
				List<String>  objbaseNamesSortParameter = ((GenericPivotData)obj).getBaseNamesSortParameter();
				for (int i=0;i<objDataLst.size();i++) {

					if (i == (objDataLst.size() -1)) {
						resultBuf.append("        { \"title\": \"" + objDataLst.get(i) + "\", \"dataIndex\": "+ count++
									+ " }");
					} else {
						resultBuf.append("        { \"title\": \"" + objDataLst.get(i) + "\", \"dataIndex\": "+ count++
								+ " },\n");
					}
					objArmData = ((GenericPivotData) obj).getArmData();
					objArmName = ((GenericPivotData) obj).getArmNames();
					if (objArmName != null) {
					    isArmNames = true;
					}
			}

			if ((obj instanceof GenericPivotData) &&  isArmNames) {	
				resultBuf.append(",\n");
				resultBuf.append(buildArmColumns(objArmData,objArmName,count));			
				}			
			}
		}
		resultBuf.append("\n    ]\n ");
		return resultBuf.toString();
	}

	
	private static String buildArmColumns(List<String> objArmData,List<String> objArmName,int inCount) {
		StringBuffer resultBuf = new StringBuffer();
		int count = inCount;
		
		if (objArmData != null) {
			for (int i = 0; i < objArmData.size(); i++) {
				if (i == (objArmData.size() - 1)) {
					resultBuf.append("        { \"title\": \"" + objArmName.get(i)
							+ "\",  \"dataIndex\": " + count++ + "}\n");
	
				} else {
					resultBuf.append("        { \"title\": \"" + objArmName.get(i)
							+ "\", \"dataIndex\": " + count++ + " },\n");
				}
			}
		}
		return resultBuf.toString();
	}
	/** 
     * Returns a JSON String for the Fields 
     *
     * @param Object list
     * @return JSON String
     */
	private static String buildFields(List objList) {
		String lFields = null;
		List<String> objArmData = null;
		lFields = "  \"fields\" : [ ";
		int count = 0;

		if (objList.size() > 0) {
			Object obj = objList.get(0);
			
			if (obj instanceof GenericPivotData){
				List<String> objDataLst = ((GenericPivotData)obj).getBaseNames();
				
				for (int i=0;i<objDataLst.size();i++)
				{
					if (i == (objDataLst.size() -1)) {
						lFields = lFields + "{ \"name\": \"col" + count++
							+ "\", \"type\": \"string\" }";
					} else {
						lFields = lFields + "{ \"name\": \"col" + count++
								+ "\", \"type\": \"string\" },";
					}
				}
				objArmData = ((GenericPivotData) obj).getArmData();
				if (objArmData != null) {
					lFields = lFields + ",";
				}
			}
		}
		
		if (objArmData != null) {
			for (int i = 0; i < objArmData.size(); i++) {
				if (i == (objArmData.size() - 1)) {
					lFields = lFields + "{ \"name\": \"col" + count++
							+ "\", \"type\": \"string\" }";

				} else {
					lFields = lFields + "{ \"name\": \"col" + count++
							+ "\", \"type\": \"string\" },";
				}
			}

		}
		lFields = lFields + "] \n ";
		return lFields;
	}
	
	/** 
     * Returns a JSON String for the Total Count 
     *
     * @param long
     * @return JSON String
     */
	private static String buildTotalCount(long pTotalCount)	{
		String lTotalCount = "\"totalCount\":" + String.valueOf(pTotalCount) + ",\n ";
		return lTotalCount;
	}
}

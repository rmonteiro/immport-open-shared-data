package org.immport.open.web.controller.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class ErrorController {
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ErrorController.class);


    @RequestMapping(value="/error", method = RequestMethod.GET)
    public String displayErrorPage(Model model) {
        log.debug("Redirecting to error page...");
        return "error";
    }


    @RequestMapping(value="/throwException", method = RequestMethod.GET)
    public void throwException() {
        log.debug("Throwing runtime exception to test error handling...");
        throw new RuntimeException("Runtime exception thrown to test error handling.");
    }


}

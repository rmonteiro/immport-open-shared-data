package org.immport.open.web.controller.study;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.immport.data.shared.model.StudyFile;
import org.immport.data.shared.service.StudyFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/public/study/studyFile/*")
public class StudyFileController {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyFileController.class);

    @Autowired
    private StudyFileService studyFileService;


    @RequestMapping(value="displayStudyFileHtml/{studyAccession}", method=RequestMethod.GET)
    public String displayProtocolHtml(@PathVariable String studyAccession, Model model) {
        log.info("Study Accession : " + studyAccession);
        List<StudyFile> studyfiles = studyFileService.getByStudy(studyAccession);
        model.addAttribute("studyfiles",studyfiles);
        return "public/study/studyFile/displayStudyFileHtml";
    }
    
    @RequestMapping(value="getStudyFile/{studyFileAccession}",method = RequestMethod.GET)
    public ModelAndView getProtocolFile(@PathVariable String studyFileAccession,
            HttpServletResponse response)
            throws IOException {
    	/*

        StudyFile studyFile = studyFileService.findById(studyFileAccession);
        OutputStream out = response.getOutputStream();
        response.addHeader("Content-Type","application/octet-stream");
        String fileName = StringUtils.replace(studyFile.getFileName(), " ", "_");
        response.setHeader("Content-Disposition", "attachment; filename="
                    + fileName);

        // Tell browser to validate cache
        response.addHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Cache-Control", "max-age=0");
        // Using public somehow cures IE's inability to save attachment
        // (and doesn't seem to hurt other browsers)
        response.setHeader("Pragma", "public");

        InputStream is = new ByteArrayInputStream(studyFile.getStudyFile());
        byte[] buf = new byte[16 * 1024]; // 16k buffer
        int nRead = 0;
        while ((nRead = is.read(buf)) != -1) {
            out.write(buf, 0, nRead);
        }

        //out.write(studyImage.getImage());
        out.flush();
        out.close();
        */
        return null;
    }
    
}

package org.immport.open.web.controller.properties;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;


@Configuration
@Profile("aws")
@PropertySource("classpath:application-aws.properties")
public class PropertySourcesConfigAws {

	 /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(PropertySourcesConfigAws.class);
    
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    	 return new PropertySourcesPlaceholderConfigurer();
    }
    
   

}
package org.immport.open.web.controller.exception;

public class GridException extends RuntimeException{
	public GridException(String issue) 
	{ 
		super(issue); 
	} 
}

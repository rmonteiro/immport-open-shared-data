package org.immport.open.web.controller.common;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.immport.open.web.controller.home.HomeController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class CustomInterceptor implements HandlerInterceptor {
	
	
	 /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(CustomInterceptor.class);
	
	 @Autowired
	 private Environment env;
	    
	 @Autowired
	 ServletContext servletContext;
	    
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
            Object handler) throws Exception {
		
		String registerUrl = env.getProperty("immport.ecosystem.registerUrl");    	
    	log.debug("registerUrl-" + registerUrl);
    	//model.addAttribute("registerUrl",registerUrl);
    	String privateDataUrl = env.getProperty("immport.ecosystem.privateDataUrl");
    	//model.addAttribute("privateDataUrl",privateDataUrl);
    	String sharedDataStudySearchUrl = env.getProperty("immport.ecosystem.sharedDataStudySearchUrl");
    	//model.addAttribute("sharedDataStudySearchUrl",sharedDataStudySearchUrl);
    	String dataAnalysisUrl = env.getProperty("immport.ecosystem.dataAnalysisUrl");
    	//model.addAttribute("dataAnalysisUrl",dataAnalysisUrl);
    	String documentationUrl = env.getProperty("immport.ecosystem.documentationUrl");
    	//model.addAttribute("documentationUrl",documentationUrl);
    	String publicationsUrl = env.getProperty("immport.ecosystem.publicationsUrl");
    	//model.addAttribute("publicationsUrl",publicationsUrl);
    	
    	String dataProvidersUrl = env.getProperty("immport.ecosystem.dataProvidersUrl");
    	//model.addAttribute("dataProvidersUrl",dataProvidersUrl);
    	String dataSummaryUrl = env.getProperty("immport.ecosystem.dataSummaryUrl");
    	//model.addAttribute("dataSummaryUrl",dataSummaryUrl);
    	String citeUrl = env.getProperty("immport.ecosystem.citeUrl");
    	//model.addAttribute("citeUrl",citeUrl);
    	String newsUrl = env.getProperty("immport.ecosystem.newsUrl");
    	//model.addAttribute("newsUrl",newsUrl);
    	String teamUrl = env.getProperty("immport.ecosystem.teamUrl");
    	//model.addAttribute("teamUrl",teamUrl);
    	String aboutUrl = env.getProperty("immport.ecosystem.aboutUrl");
    	//model.addAttribute("aboutUrl",aboutUrl);
    	
    	String dataUploadValidatorDocumentationUrl = env.getProperty("immport.ecosystem.dataUploadValidatorDocumentationUrl");
    	//model.addAttribute("dataUploadValidatorDocumentationUrl",dataUploadValidatorDocumentationUrl);
    	String sharedDataTutorialsUrl = env.getProperty("immport.ecosystem.sharedDataTutorialsUrl");
    	//model.addAttribute("sharedDataTutorialsUrl",sharedDataTutorialsUrl);
    	String sharedDataGeneListsUrl = env.getProperty("immport.ecosystem.sharedDataGeneListsUrl");
    	//model.addAttribute("sharedDataGeneListsUrl",sharedDataGeneListsUrl);
    	String dataAnalysisTutorialsUrl = env.getProperty("immport.ecosystem.dataAnalysisTutorialsUrl");
    	//model.addAttribute("dataAnalysisTutorialsUrl",dataAnalysisTutorialsUrl);

    	String sharedDataUrl = env.getProperty("immport.ecosystem.sharedDataUrl");
    	//model.addAttribute("sharedDataUrl",sharedDataUrl);
    	String cellOntologyUrl = env.getProperty("immport.ecosystem.cellOntologyUrl");
    	//model.addAttribute("cellOntologyUrl",cellOntologyUrl);
    	String cytokineRegistryUrl = env.getProperty("immport.ecosystem.cytokineRegistryUrl");
    	//model.addAttribute("cytokineRegistryUrl",cytokineRegistryUrl);
    	String immuneXpressoUrl = env.getProperty("immport.ecosystem.immuneXpressoUrl");
    	//model.addAttribute("immuneXpressoUrl",immuneXpressoUrl);
    	String dataUploadDocumentationUrl = env.getProperty("immport.ecosystem.dataUploadDocumentationUrl");
    	//model.addAttribute("dataUploadDocumentationUrl",dataUploadDocumentationUrl);
    	String sharedDataOverviewUrl = env.getProperty("immport.ecosystem.sharedDataOverviewUrl");
    	//model.addAttribute("sharedDataOverviewUrl",sharedDataOverviewUrl);
    	String sharedDataNavigationUrl = env.getProperty("immport.ecosystem.sharedDataNavigationUrl");
    	//model.addAttribute("sharedDataNavigationUrl",sharedDataNavigationUrl);
    	String sharedDataRegistrationUrl = env.getProperty("immport.ecosystem.sharedDataRegistrationUrl");
    	//model.addAttribute("sharedDataRegistrationUrl",sharedDataRegistrationUrl);
    	String dataUploadExamplePackagesUrl = env.getProperty("immport.ecosystem.dataUploadExamplePackagesUrl");
    	//model.addAttribute("dataUploadExamplePackagesUrl",dataUploadExamplePackagesUrl);
    	String dataUploadTemplateDescriptionPdfUrl = env.getProperty("immport.ecosystem.dataUploadTemplateDescriptionPdfUrl");
    	//model.addAttribute("dataUploadTemplateDescriptionPdfUrl",dataUploadTemplateDescriptionPdfUrl);
    	String dataUploadUserGuideUrl = env.getProperty("immport.ecosystem.dataUploadUserGuideUrl");
    	//model.addAttribute("dataUploadUserGuideUrl",dataUploadUserGuideUrl);
    	String dataUploadTemplatesUrl = env.getProperty("immport.ecosystem.dataUploadTemplatesUrl");
    	//model.addAttribute("dataUploadTemplatesUrl",dataUploadTemplatesUrl);
    	String dataUploadTemplateDescriptionInteractiveUrl = env.getProperty("immport.ecosystem.dataUploadTemplateDescriptionInteractiveUrl");
    	//model.addAttribute("dataUploadTemplateDescriptionInteractiveUrl",dataUploadTemplateDescriptionInteractiveUrl);
    	String dataUploadTemplateHistoryUrl = env.getProperty("immport.ecosystem.dataUploadTemplateHistoryUrl");
    	//model.addAttribute("dataUploadTemplateHistoryUrl",dataUploadTemplateHistoryUrl);
    	String dataUploadTemplateListUrl = env.getProperty("immport.ecosystem.dataUploadTemplateListUrl");
    	//model.addAttribute("dataUploadTemplateListUrl",dataUploadTemplateListUrl);
    	String dataUploadTemplatesZipUrl = env.getProperty("immport.ecosystem.dataUploadTemplatesZipUrl");
    	//model.addAttribute("dataUploadTemplatesZipUrl",dataUploadTemplatesZipUrl);
    	
    	
    
    	//request.getSession().setAttribute("userAdminPath",userAdminPath);
    	/*request.getSession().setAttribute("registerUrl",registerUrl);
    	request.getSession().setAttribute("privateDataUrl",privateDataUrl);
    	request.getSession().setAttribute("sharedDataStudySearchUrl",sharedDataStudySearchUrl);
    	request.getSession().setAttribute("dataAnalysisUrl",dataAnalysisUrl);
    	request.getSession().setAttribute("documentationUrl",documentationUrl);
    	request.getSession().setAttribute("publicationsUrl",publicationsUrl);
    	
    	request.getSession().setAttribute("dataProvidersUrl",dataProvidersUrl);
    	request.getSession().setAttribute("dataSummaryUrl",dataSummaryUrl);
    	request.getSession().setAttribute("citeUrl",citeUrl);
    	request.getSession().setAttribute("newsUrl",newsUrl);
    	request.getSession().setAttribute("teamUrl",teamUrl);
    	request.getSession().setAttribute("aboutUrl",aboutUrl);
    	
    	request.getSession().setAttribute("dataUploadValidatorDocumentationUrl",dataUploadValidatorDocumentationUrl);
    	
    	request.getSession().setAttribute("sharedDataTutorialsUrl",sharedDataTutorialsUrl);
    	request.getSession().setAttribute("sharedDataGeneListsUrl",sharedDataGeneListsUrl);
    	request.getSession().setAttribute("dataAnalysisTutorialsUrl",dataAnalysisTutorialsUrl);
    	
    	
    	request.getSession().setAttribute("sharedDataUrl",sharedDataUrl);
    	request.getSession().setAttribute("cellOntologyUrl",cellOntologyUrl);
    	request.getSession().setAttribute("cytokineRegistryUrl",cytokineRegistryUrl);
    	request.getSession().setAttribute("immuneXpressoUrl",immuneXpressoUrl);
    	request.getSession().setAttribute("dataUploadDocumentationUrl",dataUploadDocumentationUrl);
    	request.getSession().setAttribute("sharedDataOverviewUrl",sharedDataOverviewUrl);
    	request.getSession().setAttribute("sharedDataNavigationUrl",sharedDataNavigationUrl);
    	request.getSession().setAttribute("sharedDataRegistrationUrl",sharedDataRegistrationUrl);
    	request.getSession().setAttribute("dataUploadExamplePackagesUrl",dataUploadExamplePackagesUrl);
    	request.getSession().setAttribute("dataUploadTemplateDescriptionPdfUrl",dataUploadTemplateDescriptionPdfUrl);
    	request.getSession().setAttribute("dataUploadUserGuideUrl",dataUploadUserGuideUrl);
    	request.getSession().setAttribute("dataUploadTemplatesUrl",dataUploadTemplatesUrl);
    	request.getSession().setAttribute("dataUploadTemplateDescriptionInteractiveUrl",dataUploadTemplateDescriptionInteractiveUrl);
    	request.getSession().setAttribute("dataUploadTemplateHistoryUrl",dataUploadTemplateHistoryUrl);
    	request.getSession().setAttribute("dataUploadTemplateListUrl",dataUploadTemplateListUrl);
    	request.getSession().setAttribute("dataUploadTemplatesZipUrl",dataUploadTemplatesZipUrl);*/
    	
    	
    	
    	servletContext.setAttribute("registerUrl",registerUrl);
    	servletContext.setAttribute("privateDataUrl",privateDataUrl);
    	servletContext.setAttribute("sharedDataStudySearchUrl",sharedDataStudySearchUrl);
    	servletContext.setAttribute("dataAnalysisUrl",dataAnalysisUrl);
    	servletContext.setAttribute("documentationUrl",documentationUrl);
    	servletContext.setAttribute("publicationsUrl",publicationsUrl);
    	
    	servletContext.setAttribute("dataProvidersUrl",dataProvidersUrl);
    	servletContext.setAttribute("dataSummaryUrl",dataSummaryUrl);
    	servletContext.setAttribute("citeUrl",citeUrl);
    	servletContext.setAttribute("newsUrl",newsUrl);
    	servletContext.setAttribute("teamUrl",teamUrl);
    	servletContext.setAttribute("aboutUrl",aboutUrl);
    	
    	servletContext.setAttribute("dataUploadValidatorDocumentationUrl",dataUploadValidatorDocumentationUrl);
    	
    	servletContext.setAttribute("sharedDataTutorialsUrl",sharedDataTutorialsUrl);
    	servletContext.setAttribute("sharedDataGeneListsUrl",sharedDataGeneListsUrl);
    	servletContext.setAttribute("dataAnalysisTutorialsUrl",dataAnalysisTutorialsUrl);
    	
    	
    	servletContext.setAttribute("sharedDataUrl",sharedDataUrl);
    	servletContext.setAttribute("cellOntologyUrl",cellOntologyUrl);
    	servletContext.setAttribute("cytokineRegistryUrl",cytokineRegistryUrl);
    	servletContext.setAttribute("immuneXpressoUrl",immuneXpressoUrl);
    	servletContext.setAttribute("dataUploadDocumentationUrl",dataUploadDocumentationUrl);
    	servletContext.setAttribute("sharedDataOverviewUrl",sharedDataOverviewUrl);
    	servletContext.setAttribute("sharedDataNavigationUrl",sharedDataNavigationUrl);
    	servletContext.setAttribute("sharedDataRegistrationUrl",sharedDataRegistrationUrl);
    	servletContext.setAttribute("dataUploadExamplePackagesUrl",dataUploadExamplePackagesUrl);
    	servletContext.setAttribute("dataUploadTemplateDescriptionPdfUrl",dataUploadTemplateDescriptionPdfUrl);
    	servletContext.setAttribute("dataUploadUserGuideUrl",dataUploadUserGuideUrl);
    	servletContext.setAttribute("dataUploadTemplatesUrl",dataUploadTemplatesUrl);
    	servletContext.setAttribute("dataUploadTemplateDescriptionInteractiveUrl",dataUploadTemplateDescriptionInteractiveUrl);
    	servletContext.setAttribute("dataUploadTemplateHistoryUrl",dataUploadTemplateHistoryUrl);
    	servletContext.setAttribute("dataUploadTemplateListUrl",dataUploadTemplateListUrl);
    	servletContext.setAttribute("dataUploadTemplatesZipUrl",dataUploadTemplatesZipUrl);
		
		return true;
    }
	
	 @Override
	    public void postHandle(HttpServletRequest request,
	            HttpServletResponse response, Object handler,
	            ModelAndView modelAndView) throws Exception {
	       // System.out.println("Inside post handle");
	    }
	 
	    @Override
	    public void afterCompletion(HttpServletRequest request,
	            HttpServletResponse response, Object handler, Exception exception)
	            throws Exception {
	       // System.out.println("Inside after completion");
	    }

}

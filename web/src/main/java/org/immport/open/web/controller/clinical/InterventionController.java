package org.immport.open.web.controller.clinical;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.service.InterventionService;
import org.immport.data.shared.service.ArmOrCohortService;
import org.immport.open.web.controller.common.JSONStringBuilderUtil;
import org.immport.open.web.controller.common.JSONStringBuilderUtilNew;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/public/clinical/intervention/*")
public class InterventionController {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(InterventionController.class);

    @Autowired
    private InterventionService interventionService;

    @Autowired
    private ArmOrCohortService armOrCohortService;
    
        
    @RequestMapping(value = "displayInterventionHtml/{studyAccession}", method = RequestMethod.GET)
    public String displayInterventionHtml(
            @PathVariable String studyAccession, Model model) {
        model.addAttribute("studyAccession", studyAccession);
        
        List<ArmCohortSortOrder> armorcohorts = armOrCohortService.getArmOrderAndDescByStudy(studyAccession);
        model.addAttribute("armorcohorts", armorcohorts);
        
        return "public/clinical/intervention/displayInterventionHtml";
    }
    
    
    @RequestMapping(value = "getInterventionDetailByStudy/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getInterventionDetailByStudy(
                @PathVariable String studyAccession, HttpServletResponse response,
                @RequestParam("start") Integer start,
                @RequestParam("limit") Integer limit,
                @RequestParam("sort") String sort,
                @RequestParam("dir") String dir
            )
            throws IOException {
        PageableData page = interventionService
                .getInterventionDetailByStudy(start, limit, sort,
                		dir, studyAccession);
        
        List<GenericPivotData> objConMedList = page.getData();
        
        
        String sbuffer = JSONStringBuilderUtil.buildJSONStringfromList(objConMedList,page.getTotalCount());
        
        log.debug("Data Sent :" + sbuffer);
        //System.out.println("Data Sent :" + sbuffer);    
        response.getWriter().println(sbuffer);

        return null;
    }

    @RequestMapping(value = "getInterventionDetailByStudyNew/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getInterventionDetailByStudyNew(
                @PathVariable String studyAccession, HttpServletResponse response,
                @RequestParam("start") Integer start,
                @RequestParam("limit") Integer limit,
                @RequestParam("sort") String sort,
                @RequestParam("dir") String dir
            )
            throws IOException {
        PageableData page = interventionService
                .getInterventionDetailByStudy(start, limit, sort,
                		dir, studyAccession);
        
        List<GenericPivotData> objConMedList = page.getData();
        
        
        String sbuffer = JSONStringBuilderUtilNew.buildJSONStringfromList(objConMedList,page.getTotalCount());
        
        log.debug("Data Sent :" + sbuffer);
        //System.out.println("Data Sent :" + sbuffer);    
        response.getWriter().println(sbuffer);

        return null;
    }
}


package org.immport.open.web.controller.home;


import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.immport.data.shared.model.Study;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;

import java.util.Locale;

@Controller
public class HomeController {
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(HomeController.class);
    
    @Autowired
    private MessageSource msgSrc;
    
    @Autowired
    private Environment env;
    
    @Autowired
    ServletContext servletContext;
    
   

    @RequestMapping(value="/public/home/home",method = RequestMethod.GET)
    public String homePage(Model model, HttpServletRequest request) {

    	//log.info());
    	ResourceBundle resources = ResourceBundle.getBundle("application");
    
    	
    	/*String registerUrl = env.getProperty("immport.ecosystem.registerUrl");    	
    	log.info("registerUrl-" + registerUrl);
    	model.addAttribute("registerUrl",registerUrl);
    	String privateDataUrl = env.getProperty("immport.ecosystem.privateDataUrl");
    	model.addAttribute("privateDataUrl",privateDataUrl);
    	String sharedDataStudySearchUrl = env.getProperty("immport.ecosystem.sharedDataStudySearchUrl");
    	model.addAttribute("sharedDataStudySearchUrl",sharedDataStudySearchUrl);
    	String dataAnalysisUrl = env.getProperty("immport.ecosystem.dataAnalysisUrl");
    	model.addAttribute("dataAnalysisUrl",dataAnalysisUrl);
    	String documentationUrl = env.getProperty("immport.ecosystem.documentationUrl");
    	model.addAttribute("documentationUrl",documentationUrl);
    	String publicationsUrl = env.getProperty("immport.ecosystem.publicationsUrl");
    	model.addAttribute("publicationsUrl",publicationsUrl);
    	
    	String dataProvidersUrl = env.getProperty("immport.ecosystem.dataProvidersUrl");
    	model.addAttribute("dataProvidersUrl",dataProvidersUrl);
    	String dataSummaryUrl = env.getProperty("immport.ecosystem.dataSummaryUrl");
    	model.addAttribute("dataSummaryUrl",dataSummaryUrl);
    	String citeUrl = env.getProperty("immport.ecosystem.citeUrl");
    	model.addAttribute("citeUrl",citeUrl);
    	String newsUrl = env.getProperty("immport.ecosystem.newsUrl");
    	model.addAttribute("newsUrl",newsUrl);
    	String teamUrl = env.getProperty("immport.ecosystem.teamUrl");
    	model.addAttribute("teamUrl",teamUrl);
    	String aboutUrl = env.getProperty("immport.ecosystem.aboutUrl");
    	model.addAttribute("aboutUrl",aboutUrl);
    	
    	String dataUploadValidatorDocumentationUrl = env.getProperty("immport.ecosystem.dataUploadValidatorDocumentationUrl");
    	model.addAttribute("dataUploadValidatorDocumentationUrl",dataUploadValidatorDocumentationUrl);
    	String sharedDataTutorialsUrl = env.getProperty("immport.ecosystem.sharedDataTutorialsUrl");
    	model.addAttribute("sharedDataTutorialsUrl",sharedDataTutorialsUrl);
    	String sharedDataGeneListsUrl = env.getProperty("immport.ecosystem.sharedDataGeneListsUrl");
    	model.addAttribute("sharedDataGeneListsUrl",sharedDataGeneListsUrl);
    	String dataAnalysisTutorialsUrl = env.getProperty("immport.ecosystem.dataAnalysisTutorialsUrl");
    	model.addAttribute("dataAnalysisTutorialsUrl",dataAnalysisTutorialsUrl);

    	String sharedDataUrl = env.getProperty("immport.ecosystem.sharedDataUrl");
    	model.addAttribute("sharedDataUrl",sharedDataUrl);
    	String cellOntologyUrl = env.getProperty("immport.ecosystem.cellOntologyUrl");
    	model.addAttribute("cellOntologyUrl",cellOntologyUrl);
    	String cytokineRegistryUrl = env.getProperty("immport.ecosystem.cytokineRegistryUrl");
    	model.addAttribute("cytokineRegistryUrl",cytokineRegistryUrl);
    	String immuneXpressoUrl = env.getProperty("immport.ecosystem.immuneXpressoUrl");
    	model.addAttribute("immuneXpressoUrl",immuneXpressoUrl);
    	String dataUploadDocumentationUrl = env.getProperty("immport.ecosystem.dataUploadDocumentationUrl");
    	model.addAttribute("dataUploadDocumentationUrl",dataUploadDocumentationUrl);
    	String sharedDataOverviewUrl = env.getProperty("immport.ecosystem.sharedDataOverviewUrl");
    	model.addAttribute("sharedDataOverviewUrl",sharedDataOverviewUrl);
    	String sharedDataNavigationUrl = env.getProperty("immport.ecosystem.sharedDataNavigationUrl");
    	model.addAttribute("sharedDataNavigationUrl",sharedDataNavigationUrl);
    	String sharedDataRegistrationUrl = env.getProperty("immport.ecosystem.sharedDataRegistrationUrl");
    	model.addAttribute("sharedDataRegistrationUrl",sharedDataRegistrationUrl);
    	String dataUploadExamplePackagesUrl = env.getProperty("immport.ecosystem.dataUploadExamplePackagesUrl");
    	model.addAttribute("dataUploadExamplePackagesUrl",dataUploadExamplePackagesUrl);
    	String dataUploadTemplateDescriptionPdfUrl = env.getProperty("immport.ecosystem.dataUploadTemplateDescriptionPdfUrl");
    	model.addAttribute("dataUploadTemplateDescriptionPdfUrl",dataUploadTemplateDescriptionPdfUrl);
    	String dataUploadUserGuideUrl = env.getProperty("immport.ecosystem.dataUploadUserGuideUrl");
    	model.addAttribute("dataUploadUserGuideUrl",dataUploadUserGuideUrl);
    	String dataUploadTemplatesUrl = env.getProperty("immport.ecosystem.dataUploadTemplatesUrl");
    	model.addAttribute("dataUploadTemplatesUrl",dataUploadTemplatesUrl);
    	String dataUploadTemplateDescriptionInteractiveUrl = env.getProperty("immport.ecosystem.dataUploadTemplateDescriptionInteractiveUrl");
    	model.addAttribute("dataUploadTemplateDescriptionInteractiveUrl",dataUploadTemplateDescriptionInteractiveUrl);
    	String dataUploadTemplateHistoryUrl = env.getProperty("immport.ecosystem.dataUploadTemplateHistoryUrl");
    	model.addAttribute("dataUploadTemplateHistoryUrl",dataUploadTemplateHistoryUrl);
    	String dataUploadTemplateListUrl = env.getProperty("immport.ecosystem.dataUploadTemplateListUrl");
    	model.addAttribute("dataUploadTemplateListUrl",dataUploadTemplateListUrl);
    	String dataUploadTemplatesZipUrl = env.getProperty("immport.ecosystem.dataUploadTemplatesZipUrl");
    	model.addAttribute("dataUploadTemplatesZipUrl",dataUploadTemplatesZipUrl);*/
    	
    	
    
    	//request.getSession().setAttribute("userAdminPath",userAdminPath);
    	/*request.getSession().setAttribute("registerUrl",registerUrl);
    	request.getSession().setAttribute("privateDataUrl",privateDataUrl);
    	request.getSession().setAttribute("sharedDataStudySearchUrl",sharedDataStudySearchUrl);
    	request.getSession().setAttribute("dataAnalysisUrl",dataAnalysisUrl);
    	request.getSession().setAttribute("documentationUrl",documentationUrl);
    	request.getSession().setAttribute("publicationsUrl",publicationsUrl);
    	
    	request.getSession().setAttribute("dataProvidersUrl",dataProvidersUrl);
    	request.getSession().setAttribute("dataSummaryUrl",dataSummaryUrl);
    	request.getSession().setAttribute("citeUrl",citeUrl);
    	request.getSession().setAttribute("newsUrl",newsUrl);
    	request.getSession().setAttribute("teamUrl",teamUrl);
    	request.getSession().setAttribute("aboutUrl",aboutUrl);
    	
    	request.getSession().setAttribute("dataUploadValidatorDocumentationUrl",dataUploadValidatorDocumentationUrl);
    	
    	request.getSession().setAttribute("sharedDataTutorialsUrl",sharedDataTutorialsUrl);
    	request.getSession().setAttribute("sharedDataGeneListsUrl",sharedDataGeneListsUrl);
    	request.getSession().setAttribute("dataAnalysisTutorialsUrl",dataAnalysisTutorialsUrl);
    	
    	
    	request.getSession().setAttribute("sharedDataUrl",sharedDataUrl);
    	request.getSession().setAttribute("cellOntologyUrl",cellOntologyUrl);
    	request.getSession().setAttribute("cytokineRegistryUrl",cytokineRegistryUrl);
    	request.getSession().setAttribute("immuneXpressoUrl",immuneXpressoUrl);
    	request.getSession().setAttribute("dataUploadDocumentationUrl",dataUploadDocumentationUrl);
    	request.getSession().setAttribute("sharedDataOverviewUrl",sharedDataOverviewUrl);
    	request.getSession().setAttribute("sharedDataNavigationUrl",sharedDataNavigationUrl);
    	request.getSession().setAttribute("sharedDataRegistrationUrl",sharedDataRegistrationUrl);
    	request.getSession().setAttribute("dataUploadExamplePackagesUrl",dataUploadExamplePackagesUrl);
    	request.getSession().setAttribute("dataUploadTemplateDescriptionPdfUrl",dataUploadTemplateDescriptionPdfUrl);
    	request.getSession().setAttribute("dataUploadUserGuideUrl",dataUploadUserGuideUrl);
    	request.getSession().setAttribute("dataUploadTemplatesUrl",dataUploadTemplatesUrl);
    	request.getSession().setAttribute("dataUploadTemplateDescriptionInteractiveUrl",dataUploadTemplateDescriptionInteractiveUrl);
    	request.getSession().setAttribute("dataUploadTemplateHistoryUrl",dataUploadTemplateHistoryUrl);
    	request.getSession().setAttribute("dataUploadTemplateListUrl",dataUploadTemplateListUrl);
    	request.getSession().setAttribute("dataUploadTemplatesZipUrl",dataUploadTemplatesZipUrl);*/
    	
    	
    	
    	/*servletContext.setAttribute("registerUrl",registerUrl);
    	servletContext.setAttribute("privateDataUrl",privateDataUrl);
    	servletContext.setAttribute("sharedDataStudySearchUrl",sharedDataStudySearchUrl);
    	servletContext.setAttribute("dataAnalysisUrl",dataAnalysisUrl);
    	servletContext.setAttribute("documentationUrl",documentationUrl);
    	servletContext.setAttribute("publicationsUrl",publicationsUrl);
    	
    	servletContext.setAttribute("dataProvidersUrl",dataProvidersUrl);
    	servletContext.setAttribute("dataSummaryUrl",dataSummaryUrl);
    	servletContext.setAttribute("citeUrl",citeUrl);
    	servletContext.setAttribute("newsUrl",newsUrl);
    	servletContext.setAttribute("teamUrl",teamUrl);
    	servletContext.setAttribute("aboutUrl",aboutUrl);
    	
    	servletContext.setAttribute("dataUploadValidatorDocumentationUrl",dataUploadValidatorDocumentationUrl);
    	
    	servletContext.setAttribute("sharedDataTutorialsUrl",sharedDataTutorialsUrl);
    	servletContext.setAttribute("sharedDataGeneListsUrl",sharedDataGeneListsUrl);
    	servletContext.setAttribute("dataAnalysisTutorialsUrl",dataAnalysisTutorialsUrl);
    	
    	
    	servletContext.setAttribute("sharedDataUrl",sharedDataUrl);
    	servletContext.setAttribute("cellOntologyUrl",cellOntologyUrl);
    	servletContext.setAttribute("cytokineRegistryUrl",cytokineRegistryUrl);
    	servletContext.setAttribute("immuneXpressoUrl",immuneXpressoUrl);
    	servletContext.setAttribute("dataUploadDocumentationUrl",dataUploadDocumentationUrl);
    	servletContext.setAttribute("sharedDataOverviewUrl",sharedDataOverviewUrl);
    	servletContext.setAttribute("sharedDataNavigationUrl",sharedDataNavigationUrl);
    	servletContext.setAttribute("sharedDataRegistrationUrl",sharedDataRegistrationUrl);
    	servletContext.setAttribute("dataUploadExamplePackagesUrl",dataUploadExamplePackagesUrl);
    	servletContext.setAttribute("dataUploadTemplateDescriptionPdfUrl",dataUploadTemplateDescriptionPdfUrl);
    	servletContext.setAttribute("dataUploadUserGuideUrl",dataUploadUserGuideUrl);
    	servletContext.setAttribute("dataUploadTemplatesUrl",dataUploadTemplatesUrl);
    	servletContext.setAttribute("dataUploadTemplateDescriptionInteractiveUrl",dataUploadTemplateDescriptionInteractiveUrl);
    	servletContext.setAttribute("dataUploadTemplateHistoryUrl",dataUploadTemplateHistoryUrl);
    	servletContext.setAttribute("dataUploadTemplateListUrl",dataUploadTemplateListUrl);
    	servletContext.setAttribute("dataUploadTemplatesZipUrl",dataUploadTemplatesZipUrl);*/
    	
    	
    	
        return "public/home/home";
    }
    
    @RequestMapping(value="/public/home/home/{messageCode}",method = RequestMethod.GET)
    public ModelAndView ecoSystem(@PathVariable("messageCode") String messageCode,Model model,final Locale locale, HttpServletRequest request) { 	

    	ResourceBundle resources = ResourceBundle.getBundle("application");
    	
    	/*String registerUrl = env.getProperty("immport.ecosystem.registerUrl");
    	log.info("registerUrl - " + registerUrl);
    	model.addAttribute("registerUrl",registerUrl);
    	String privateDataUrl = env.getProperty("immport.ecosystem.privateDataUrl");
    	model.addAttribute("privateDataUrl",privateDataUrl);  
    	String sharedDataStudySearchUrl = env.getProperty("immport.ecosystem.sharedDataStudySearchUrl");
    	model.addAttribute("sharedDataStudySearchUrl",sharedDataStudySearchUrl);
    	String dataAnalysisUrl = env.getProperty("immport.ecosystem.dataAnalysisUrl");
    	model.addAttribute("dataAnalysisUrl",dataAnalysisUrl);
    	String documentationUrl = env.getProperty("immport.ecosystem.documentationUrl");
    	model.addAttribute("documentationUrl",documentationUrl);
    	String publicationsUrl = env.getProperty("immport.ecosystem.publicationsUrl");
    	model.addAttribute("publicationsUrl",publicationsUrl);
    	
    	String dataProvidersUrl = env.getProperty("immport.ecosystem.dataProvidersUrl");
    	model.addAttribute("dataProvidersUrl",dataProvidersUrl);
    	String dataSummaryUrl = env.getProperty("immport.ecosystem.dataSummaryUrl");
    	model.addAttribute("dataSummaryUrl",dataSummaryUrl);
    	String citeUrl = env.getProperty("immport.ecosystem.citeUrl");
    	model.addAttribute("citeUrl",citeUrl);
    	String newsUrl = env.getProperty("immport.ecosystem.newsUrl");
    	model.addAttribute("newsUrl",newsUrl);
    	String teamUrl = env.getProperty("immport.ecosystem.teamUrl");
    	model.addAttribute("teamUrl",teamUrl);
    	String aboutUrl = env.getProperty("immport.ecosystem.aboutUrl");
    	model.addAttribute("aboutUrl",aboutUrl);

    	String dataUploadValidatorDocumentationUrl = env.getProperty("immport.ecosystem.dataUploadValidatorDocumentationUrl");
    	model.addAttribute("dataUploadValidatorDocumentationUrl",dataUploadValidatorDocumentationUrl);
    	String sharedDataTutorialsUrl = env.getProperty("immport.ecosystem.sharedDataTutorialsUrl");
    	model.addAttribute("sharedDataTutorialsUrl",sharedDataTutorialsUrl);
    	String sharedDataGeneListsUrl = env.getProperty("immport.ecosystem.sharedDataGeneListsUrl");
    	model.addAttribute("sharedDataGeneListsUrl",sharedDataGeneListsUrl);
    	String dataAnalysisTutorialsUrl = env.getProperty("immport.ecosystem.dataAnalysisTutorialsUrl");
    	model.addAttribute("dataAnalysisTutorialsUrl",dataAnalysisTutorialsUrl);
    	
    	
    	String sharedDataUrl = env.getProperty("immport.ecosystem.sharedDataUrl");
    	model.addAttribute("sharedDataUrl",sharedDataUrl);
    	String cellOntologyUrl = env.getProperty("immport.ecosystem.cellOntologyUrl");
    	model.addAttribute("cellOntologyUrl",cellOntologyUrl);
    	String cytokineRegistryUrl = env.getProperty("immport.ecosystem.cytokineRegistryUrl");
    	model.addAttribute("cytokineRegistryUrl",cytokineRegistryUrl);
    	String immuneXpressoUrl = env.getProperty("immport.ecosystem.immuneXpressoUrl");
    	model.addAttribute("immuneXpressoUrl",immuneXpressoUrl);
    	String dataUploadDocumentationUrl = env.getProperty("immport.ecosystem.dataUploadDocumentationUrl");
    	model.addAttribute("dataUploadDocumentationUrl",dataUploadDocumentationUrl);
    	String sharedDataOverviewUrl = env.getProperty("immport.ecosystem.sharedDataOverviewUrl");
    	model.addAttribute("sharedDataOverviewUrl",sharedDataOverviewUrl);
    	String sharedDataNavigationUrl = env.getProperty("immport.ecosystem.sharedDataNavigationUrl");
    	model.addAttribute("sharedDataNavigationUrl",sharedDataNavigationUrl);
    	String sharedDataRegistrationUrl = env.getProperty("immport.ecosystem.sharedDataRegistrationUrl");
    	model.addAttribute("sharedDataRegistrationUrl",sharedDataRegistrationUrl);
    	String dataUploadExamplePackagesUrl = env.getProperty("immport.ecosystem.dataUploadExamplePackagesUrl");
    	model.addAttribute("dataUploadExamplePackagesUrl",dataUploadExamplePackagesUrl);
    	String dataUploadTemplateDescriptionPdfUrl = env.getProperty("immport.ecosystem.dataUploadTemplateDescriptionPdfUrl");
    	model.addAttribute("dataUploadTemplateDescriptionPdfUrl",dataUploadTemplateDescriptionPdfUrl);
    	String dataUploadUserGuideUrl = env.getProperty("immport.ecosystem.dataUploadUserGuideUrl");
    	model.addAttribute("dataUploadUserGuideUrl",dataUploadUserGuideUrl);
    	String dataUploadTemplatesUrl = env.getProperty("immport.ecosystem.dataUploadTemplatesUrl");
    	model.addAttribute("dataUploadTemplatesUrl",dataUploadTemplatesUrl);
    	String dataUploadTemplateDescriptionInteractiveUrl = env.getProperty("immport.ecosystem.dataUploadTemplateDescriptionInteractiveUrl");
    	model.addAttribute("dataUploadTemplateDescriptionInteractiveUrl",dataUploadTemplateDescriptionInteractiveUrl);
    	String dataUploadTemplateHistoryUrl = env.getProperty("immport.ecosystem.dataUploadTemplateHistoryUrl");
    	model.addAttribute("dataUploadTemplateHistoryUrl",dataUploadTemplateHistoryUrl);
    	String dataUploadTemplateListUrl = env.getProperty("immport.ecosystem.dataUploadTemplateListUrl");
    	model.addAttribute("dataUploadTemplateListUrl",dataUploadTemplateListUrl);
    	String dataUploadTemplatesZipUrl = env.getProperty("immport.ecosystem.dataUploadTemplatesZipUrl");
    	model.addAttribute("dataUploadTemplatesZipUrl",dataUploadTemplatesZipUrl);*/
    	
    	//request.getSession().setAttribute("userAdminPath",userAdminPath);
    	/*request.getSession().setAttribute("registerUrl",registerUrl);
    	request.getSession().setAttribute("privateDataUrl",privateDataUrl);
    	request.getSession().setAttribute("sharedDataStudySearchUrl",sharedDataStudySearchUrl);
    	request.getSession().setAttribute("dataAnalysisUrl",dataAnalysisUrl);
    	request.getSession().setAttribute("documentationUrl",documentationUrl);
    	request.getSession().setAttribute("publicationsUrl",publicationsUrl);
    	
    	request.getSession().setAttribute("dataProvidersUrl",dataProvidersUrl);
    	request.getSession().setAttribute("dataSummaryUrl",dataSummaryUrl);
    	request.getSession().setAttribute("citeUrl",citeUrl);
    	request.getSession().setAttribute("newsUrl",newsUrl);
    	request.getSession().setAttribute("teamUrl",teamUrl);
    	request.getSession().setAttribute("aboutUrl",aboutUrl);
    	
    	request.getSession().setAttribute("dataUploadValidatorDocumentationUrl",dataUploadValidatorDocumentationUrl);
    	request.getSession().setAttribute("sharedDataTutorialsUrl",sharedDataTutorialsUrl);
    	request.getSession().setAttribute("sharedDataGeneListsUrl",sharedDataGeneListsUrl);
    	request.getSession().setAttribute("dataAnalysisTutorialsUrl",dataAnalysisTutorialsUrl);
    	
    	
    	request.getSession().setAttribute("sharedDataUrl",sharedDataUrl);
    	request.getSession().setAttribute("cellOntologyUrl",cellOntologyUrl);
    	request.getSession().setAttribute("cytokineRegistryUrl",cytokineRegistryUrl);
    	request.getSession().setAttribute("immuneXpressoUrl",immuneXpressoUrl);
    	request.getSession().setAttribute("dataUploadDocumentationUrl",dataUploadDocumentationUrl);
    	request.getSession().setAttribute("sharedDataOverviewUrl",sharedDataOverviewUrl);
    	request.getSession().setAttribute("sharedDataNavigationUrl",sharedDataNavigationUrl);
    	request.getSession().setAttribute("sharedDataRegistrationUrl",sharedDataRegistrationUrl);
    	request.getSession().setAttribute("dataUploadExamplePackagesUrl",dataUploadExamplePackagesUrl);
    	request.getSession().setAttribute("dataUploadTemplateDescriptionPdfUrl",dataUploadTemplateDescriptionPdfUrl);
    	request.getSession().setAttribute("dataUploadUserGuideUrl",dataUploadUserGuideUrl);
    	request.getSession().setAttribute("dataUploadTemplatesUrl",dataUploadTemplatesUrl);
    	request.getSession().setAttribute("dataUploadTemplateDescriptionInteractiveUrl",dataUploadTemplateDescriptionInteractiveUrl);
    	request.getSession().setAttribute("dataUploadTemplateHistoryUrl",dataUploadTemplateHistoryUrl);
    	request.getSession().setAttribute("dataUploadTemplateListUrl",dataUploadTemplateListUrl);
    	request.getSession().setAttribute("dataUploadTemplatesZipUrl",dataUploadTemplatesZipUrl);*/
    	
    	/*servletContext.setAttribute("registerUrl",registerUrl);
    	servletContext.setAttribute("privateDataUrl",privateDataUrl);
    	servletContext.setAttribute("sharedDataStudySearchUrl",sharedDataStudySearchUrl);
    	servletContext.setAttribute("dataAnalysisUrl",dataAnalysisUrl);
    	servletContext.setAttribute("documentationUrl",documentationUrl);
    	servletContext.setAttribute("publicationsUrl",publicationsUrl);
    	
    	servletContext.setAttribute("dataProvidersUrl",dataProvidersUrl);
    	servletContext.setAttribute("dataSummaryUrl",dataSummaryUrl);
    	servletContext.setAttribute("citeUrl",citeUrl);
    	servletContext.setAttribute("newsUrl",newsUrl);
    	servletContext.setAttribute("teamUrl",teamUrl);
    	servletContext.setAttribute("aboutUrl",aboutUrl);
    	
    	servletContext.setAttribute("dataUploadValidatorDocumentationUrl",dataUploadValidatorDocumentationUrl);
    	
    	servletContext.setAttribute("sharedDataTutorialsUrl",sharedDataTutorialsUrl);
    	servletContext.setAttribute("sharedDataGeneListsUrl",sharedDataGeneListsUrl);
    	servletContext.setAttribute("dataAnalysisTutorialsUrl",dataAnalysisTutorialsUrl);
    	
    	
    	servletContext.setAttribute("sharedDataUrl",sharedDataUrl);
    	servletContext.setAttribute("cellOntologyUrl",cellOntologyUrl);
    	servletContext.setAttribute("cytokineRegistryUrl",cytokineRegistryUrl);
    	servletContext.setAttribute("immuneXpressoUrl",immuneXpressoUrl);
    	servletContext.setAttribute("dataUploadDocumentationUrl",dataUploadDocumentationUrl);
    	servletContext.setAttribute("sharedDataOverviewUrl",sharedDataOverviewUrl);
    	servletContext.setAttribute("sharedDataNavigationUrl",sharedDataNavigationUrl);
    	servletContext.setAttribute("sharedDataRegistrationUrl",sharedDataRegistrationUrl);
    	servletContext.setAttribute("dataUploadExamplePackagesUrl",dataUploadExamplePackagesUrl);
    	servletContext.setAttribute("dataUploadTemplateDescriptionPdfUrl",dataUploadTemplateDescriptionPdfUrl);
    	servletContext.setAttribute("dataUploadUserGuideUrl",dataUploadUserGuideUrl);
    	servletContext.setAttribute("dataUploadTemplatesUrl",dataUploadTemplatesUrl);
    	servletContext.setAttribute("dataUploadTemplateDescriptionInteractiveUrl",dataUploadTemplateDescriptionInteractiveUrl);
    	servletContext.setAttribute("dataUploadTemplateHistoryUrl",dataUploadTemplateHistoryUrl);
    	servletContext.setAttribute("dataUploadTemplateListUrl",dataUploadTemplateListUrl);
    	servletContext.setAttribute("dataUploadTemplatesZipUrl",dataUploadTemplatesZipUrl);*/
    	
    	
    	ModelAndView modelAndView = new ModelAndView("public/home/home","message", messageCode);
    	return modelAndView;
    }
    
    @RequestMapping(value="/public/home/ecoSystem",method = RequestMethod.GET)
    public ModelAndView ecoSystem(Model model,final Locale locale) {    	
    	
    	ModelAndView modelAndView = new ModelAndView("public/home/ecoSystem","message","");	
        return modelAndView;
    }
    
    @RequestMapping(value="/public/home/about",method = RequestMethod.GET)
    public String about(Model model) {
        return "public/home/about";
    }
    
    @RequestMapping(value="/public/home/agreement",method = RequestMethod.GET)
    public String agreement(Model model) {
        return "public/home/agreement";
    }
    
    @RequestMapping(value="/public/home/accessibility",method = RequestMethod.GET)
    public String accessibility(Model model) {
        return "public/home/accessibility";
    }
    
    @RequestMapping(value="/public/home/cite",method = RequestMethod.GET)
    public String cite(Model model) {
        return "public/home/cite";
    }

    @RequestMapping(value="/public/home/dataSummary",method = RequestMethod.GET)
    public String dataSummary(Model model) {
        return "public/home/dataSummary";
    }
	@RequestMapping(value="/public/home/dataTemplates",method = RequestMethod.GET)
    public String dataTemplates(Model model) {
        return "public/home/dataTemplates";
    }
	@RequestMapping(value="/public/home/templateHistory",method = RequestMethod.GET)
    public String templateHistory(Model model) {
        return "public/home/templateHistory";
    }	

    @RequestMapping(value="/public/home/dataSummaryPopup",method = RequestMethod.GET)
    public String dataSummaryPopup(Model model) {
        return "public/home/dataSummaryPopup";
    }
    
    @RequestMapping(value="/public/home/disclaimer",method = RequestMethod.GET)
    public String disclaimer(Model model) {
        return "public/home/disclaimer";
    }
    
    @RequestMapping(value="/public/home/documentation",method = RequestMethod.GET)
    public String documentation(Model model) {
        return "public/home/documentation";
    }
    
    @RequestMapping(value="/public/home/publications",method = RequestMethod.GET)
    public String publications(Model model) {
        return "public/home/publications";
    }
    
    @RequestMapping(value="/public/home/privacyPolicy",method = RequestMethod.GET)
    public String privacyPolicy(Model model) {
        return "public/home/privacyPolicy";
    }
    
    @RequestMapping(value="/public/home/dataProviders",method = RequestMethod.GET)
    public String dataProviders(Model model) {
        return "public/home/dataProviders";
    }
    
    @RequestMapping(value="/public/home/flowAnalysis",method = RequestMethod.GET)
    public String flowAnalysis(Model model) {
        return "public/home/flowAnalysis";
    }
    
    @RequestMapping(value="/public/home/teamCurrent",method = RequestMethod.GET)
    public String teamCurrent(Model model) {
        return "public/home/teamCurrent";
    }
    
    @RequestMapping(value="/public/home/news",method = RequestMethod.GET)
    public String news(Model model) {
        return "public/home/news";
    }
    
    @RequestMapping(value="/public/home/dataRelease18",method = RequestMethod.GET)
    public String dataRelease18(Model model) {
        return "public/home/dataRelease18";
    }
    
    @RequestMapping(value="/public/home/dataReleaseNoteDetail/{dataRelease}", method=RequestMethod.GET)
    public String displayDataReleaseNoteDetail(@PathVariable String dataRelease, Model model) {
        model.addAttribute("dataRelease",dataRelease);
        return "public/home/dataReleaseNoteDetail";
    }
    
    @RequestMapping(value="/public/home/softwareReleaseNotes",method = RequestMethod.GET)
    public String softwareReleaseNotes(Model model) {
        return "public/home/softwareReleaseNotes";
    }
    
    @RequestMapping(value="/public/home/dataReleaseNotes",method = RequestMethod.GET)
    public String dataReleaseNotes(Model model) {
        return "public/home/dataReleaseNotes";
    }
    
    @RequestMapping(value="/public/home/dataSubmission",method = RequestMethod.GET)
    public String dataSubmission(Model model) {
        return "public/home/dataSubmission";
    }

    @RequestMapping(value="/public/home/sharedStudyDownload",method = RequestMethod.GET)
    public String sharedStudyDownload(Model model) {
    	ResourceBundle resources = ResourceBundle.getBundle("application");
    	String asperaDataBrowserUrl = env.getProperty("immport.databrowser.baseurl") + env.getProperty("immport.databrowser.browsePathPrefix");
    	model.addAttribute("asperaDataBrowserUrl",asperaDataBrowserUrl);
        return "public/home/sharedStudyDownload";
    }
    
    @RequestMapping(value="/public/home/facetSearch",method = RequestMethod.GET)
    public String facetSearch(Model model) {
        return "public/home/facetSearch";
    }
    
    @RequestMapping(value="/public/home/studySearch",method = RequestMethod.GET)
    public String studySearch(Model model,
    		@RequestParam(value="searchTerm", required=false) String searchTerm,
    		@RequestParam(value="clinicalTrial", required=false) String clinicalTrial,
    		@RequestParam(value="researchFocus", required=false) String researchFocus,
    		@RequestParam(value="subjectSpecies", required=false) String subjectSpecies,
    		@RequestParam(value="biosampleType", required=false) String biosampleType,
    		@RequestParam(value="studyType", required=false) String studyType,
    		@RequestParam(value="experimentMeasurementTechnique", required=false) String experimentMeasurementTechnique) {

    	if (searchTerm != null) {
    	    model.addAttribute("searchTerm",searchTerm);
    	} else {
    		model.addAttribute("searchTerm","");
    	}
    	
    	if (clinicalTrial != null) {
    	    model.addAttribute("clinicalTrial",clinicalTrial);
    	} else {
    		model.addAttribute("clinicalTrial","");
    	}
    	
    	if (researchFocus != null) {
    	    model.addAttribute("researchFocus",researchFocus);
    	} else {
    		model.addAttribute("researchFocus","");
    	}
    	
       	if (studyType != null) {
    	    model.addAttribute("studyType",studyType);
    	} else {
    		model.addAttribute("studyType","");
    	}
       	
    	if (subjectSpecies != null) {
    	    model.addAttribute("subjectSpecies",subjectSpecies);
    	} else {
    		model.addAttribute("subjectSpecies","");
    	}
    	
    	if (biosampleType != null) {
    	    model.addAttribute("biosampleType",biosampleType);
    	} else {
    		model.addAttribute("biosampleType","");
    	}
    	
    	if (experimentMeasurementTechnique != null) {
    	    model.addAttribute("experimentMeasurementTechnique",experimentMeasurementTechnique);
    	} else {
    		model.addAttribute("experimentMeasurementTechnique","");
    	}
    	
    	ResourceBundle resources = ResourceBundle.getBundle("application");
    	String asperaDataBrowserUrl = env.getProperty("immport.databrowser.baseurl") + env.getProperty("immport.databrowser.browsePathPrefix");
    	model.addAttribute("asperaDataBrowserUrl",asperaDataBrowserUrl);

        return "public/home/studySearch";
    }
    
    @RequestMapping(value="/public/home/studySearchNew",method = RequestMethod.GET)
    public String studySearchNew(Model model,
    		@RequestParam(value="searchTerm", required=false) String searchTerm,
    		@RequestParam(value="clinicalTrial", required=false) String clinicalTrial,
    		@RequestParam(value="researchFocus", required=false) String researchFocus,
    		@RequestParam(value="subjectSpecies", required=false) String subjectSpecies,
    		@RequestParam(value="biosampleType", required=false) String biosampleType,
    		@RequestParam(value="studyType", required=false) String studyType,
    		@RequestParam(value="experimentMeasurementTechnique", required=false) String experimentMeasurementTechnique) {

    	if (searchTerm != null) {
    	    model.addAttribute("searchTerm",searchTerm);
    	} else {
    		model.addAttribute("searchTerm","");
    	}
    	
    	if (clinicalTrial != null) {
    	    model.addAttribute("clinicalTrial",clinicalTrial);
    	} else {
    		model.addAttribute("clinicalTrial","");
    	}
    	
    	if (researchFocus != null) {
    	    model.addAttribute("researchFocus",researchFocus);
    	} else {
    		model.addAttribute("researchFocus","");
    	}
    	
       	if (studyType != null) {
    	    model.addAttribute("studyType",studyType);
    	} else {
    		model.addAttribute("studyType","");
    	}
       	
    	if (subjectSpecies != null) {
    	    model.addAttribute("subjectSpecies",subjectSpecies);
    	} else {
    		model.addAttribute("subjectSpecies","");
    	}
    	
    	if (biosampleType != null) {
    	    model.addAttribute("biosampleType",biosampleType);
    	} else {
    		model.addAttribute("biosampleType","");
    	}
    	
    	if (experimentMeasurementTechnique != null) {
    	    model.addAttribute("experimentMeasurementTechnique",experimentMeasurementTechnique);
    	} else {
    		model.addAttribute("experimentMeasurementTechnique","");
    	}
    	
        return "public/home/studySearchNew";
    }
    
    @RequestMapping(value="/public/home/releaseNotes",method = RequestMethod.GET)
    public String releaseNotes(Model model) {
        return "public/home/releaseNotes";
    }
    
    @RequestMapping(value="/public/home/immport/open/apidoc",method = RequestMethod.GET)
    public String apidocPageOpen(Model model) {
        return "public/home/immport/open/apidoc";
    }

    @RequestMapping(value="/public/home/registry/cytokine",method = RequestMethod.GET)
    public String getCytokineRegistry(Model model) {
        return "public/home/registry/cytokine";
    }
}


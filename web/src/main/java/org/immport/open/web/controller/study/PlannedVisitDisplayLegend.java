package org.immport.open.web.controller.study;

public class PlannedVisitDisplayLegend implements java.io.Serializable {
	private String plannedVisitAccession;
	private String visitName;
	private String originalBinRange;
	private String calculatedBinRange;
	
	public void PlannedVisitDisplayLegend(String plannedVisitAccession,
										  String visitName,
										  String originalBinRange,
										  String calculatedBinRange) {
		this.plannedVisitAccession = plannedVisitAccession;
		this.visitName = visitName;
		this.originalBinRange = originalBinRange;
		this.calculatedBinRange =  calculatedBinRange;
	}
	
	public void setPlannedVisitAccession(String plannedVisitAccession) {
        this.plannedVisitAccession = plannedVisitAccession;
	}

	public String getPlannedVisitAccession() {
        return this.plannedVisitAccession;
	}
	
	
	public void setVisitName(String visitName) {
        this.visitName = visitName;
	}

	public String getVisitName() {
        return this.visitName;
	}

	public void setOriginalBinRange(String originalBinRange) {
        this.originalBinRange = originalBinRange;
	}

	public String getOriginalBinRange() {
        return this.originalBinRange;
	}
	
	public void setCalculatedBinRange(String calculatedBinRange) {
        this.calculatedBinRange = calculatedBinRange;
	}

	public String getCalculatedBinRange() {
        return this.calculatedBinRange;
	}
	
}

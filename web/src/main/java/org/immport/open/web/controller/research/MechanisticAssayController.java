/**
 * 
 */
package org.immport.open.web.controller.research;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.immport.data.shared.model.Experiment;
import org.immport.data.shared.model.MechanisticAssay;
import org.immport.data.shared.model.TreatmentToExpSampleInfo;
import org.immport.data.shared.service.ExperimentService;
import org.immport.data.shared.service.TreatmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import flexjson.JSONSerializer;

/**
 * @author fmonteiro
 *
 */
@Controller
@RequestMapping("/public/research/mechanisticAssay/*")
public class MechanisticAssayController {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(MechanisticAssayController.class);

    @Autowired
    private ExperimentService experimentService;
    
    @Autowired
    private TreatmentService treatmentService;


    @RequestMapping(value="displayMechanisticAssayHtml/{studyAccession}", method=RequestMethod.GET)
    public String displayProtocolHtml(@PathVariable String studyAccession, Model model) {
        log.info("Study Accession : " + studyAccession);
        List <MechanisticAssay> assays = experimentService.getMechanisticAssays(studyAccession);
        model.addAttribute("assays",assays);
        List<TreatmentToExpSampleInfo> objTreatmentToExpSample = treatmentService.getTreatmentToExpSample(studyAccession);
        
        
        model.addAttribute("treatments",objTreatmentToExpSample);
        
        return "public/research/mechanisticAssay/displayMechanisticAssayHtml";
    }
    
    @RequestMapping(value="getExperiment/{experimentAccession}", method=RequestMethod.GET)
    public ModelAndView getExperiment(@PathVariable String experimentAccession, Model model,
    		HttpServletResponse response) throws IOException {
   
        Experiment experiment = experimentService.getExperimentDetail(experimentAccession);
        String json = new JSONSerializer().exclude("expsamples").exclude("fcsAnalyzedResults")
        		.exclude("haiResults").exclude("workspace").exclude("study")
        		.exclude("elispotResults").exclude("standardCurves").exclude("pcrResults").exclude("hlaTypingResults")
        		.exclude("expsampleMbaaDetails").exclude("biosample2Expsamples")
        		.exclude("controlSamples").exclude("elisaMbaaResults").exclude("kirTypingResults")
        		.exclude("mbaaResults").exclude("fcsAnnotations").exclude("neutAbTiterResults")
        		.exclude("protocols.studies","protocols.subjects","protocols.biosamples","protocols.experiments","protocols.workspace")
        		.include("reagents").include("treatments.name").serialize(experiment);
        
        response.getWriter().println(json.toString());
        return null;
    }
    
    
   /* @RequestMapping(value = "getTreatmentToExpSample/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getTreatmentToExpSample(
            @PathVariable String studyAccession, HttpServletResponse response)
            throws IOException {

        System.out.println("Study Accession: " + studyAccession);
        List<GenericPivotData> objList = treatmentService.getTreatmentToExpSample(studyAccession);
                
        String sbuffer = "";
        if (objList == null) {
            System.out.println("LIST IS EMPTY");
        }
        else
        {
          sbuffer = JSONStringBuilderUtil.buildJSONStringfromList(objList,-1);
        }
        
        log.debug("Data Sent :" + sbuffer);
        System.out.println("Data Sent :" + sbuffer);
        response.getWriter().println(sbuffer);
        
        return null;
    }*/

   
    
}

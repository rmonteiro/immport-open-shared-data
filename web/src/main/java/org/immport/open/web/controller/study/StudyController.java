package org.immport.open.web.controller.study;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.immport.data.shared.model.LkDataCompleteness;
import org.immport.data.shared.model.Study;
import org.immport.data.shared.model.Study2Panel;
import org.immport.data.shared.model.StudyGlossary;
import org.immport.data.shared.model.StudyImage;
import org.immport.data.shared.model.StudyLink;
import org.immport.data.shared.model.StudyPersonnel;
import org.immport.data.shared.model.StudyPubmed;
import org.immport.data.shared.model.StudySummary;
import org.immport.data.shared.service.Study2PanelService;
import org.immport.data.shared.service.StudyGlossaryService;
import org.immport.data.shared.service.StudyLinkService;
import org.immport.data.shared.service.StudyPubmedService;
import org.immport.data.shared.service.StudyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import flexjson.JSONSerializer;


@Controller
@RequestMapping("/public/study/study/*")
public class StudyController {
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyController.class);

    @Autowired
    private StudyService studyService;

    @Autowired
    private StudyPubmedService studyPubmedService;

    @Autowired
    private StudyLinkService studyLinkService;

    @Autowired
    private StudyGlossaryService studyGlossaryService;

    @Autowired
    private Study2PanelService  study2PanelService;
    
    @Autowired
    private Environment env;
 
    @RequestMapping(value="displayStudyDetailOld/{studyAccession}", method=RequestMethod.GET)
    public String displayStudyDetailOld(@PathVariable String studyAccession, Model model) {
        Study study = studyService.findById(studyAccession);
        model.addAttribute("study",study); 
        List<Study2Panel> study2Panels = study2PanelService.getByStudy(studyAccession);
        String study2Panel = "";
        for (int i=0;i < study2Panels.size();i++) {
            study2Panel = study2Panel + study2Panels.get(i).getId().getPanelName() + " , ";
        }
        model.addAttribute("study2Panels",study2Panel); 
        return "public/study/study/displayStudyDetail";
    }
    
    @RequestMapping(value="displayStudyDetailPopUp/{studyAccession}", method=RequestMethod.GET)
    public String displayStudyDetailPopUp(@PathVariable String studyAccession, Model model) {
        Study study = studyService.findById(studyAccession);
        model.addAttribute("study",study);
        return "public/study/study/displayStudyDetailPopUp";
    }

    @RequestMapping(value="displayStudyDetail/{studyAccession}", method=RequestMethod.GET)
    public String displayStudyDetail(@PathVariable String studyAccession, Model model) {
        Study study = studyService.findById(studyAccession);
        model.addAttribute("study",study);
        List<Study2Panel> study2Panels = study2PanelService.getByStudy(studyAccession);
        String study2Panel = "";
        for (int i=0;i < study2Panels.size();i++) {
            study2Panel = study2Panel + study2Panels.get(i).getId().getPanelName() + ",";
        }
        model.addAttribute("study",study);
        model.addAttribute("study2Panels",study2Panel);
        
    	ResourceBundle resources = ResourceBundle.getBundle("application");
    	String asperaDataBrowserUrl = env.getProperty("immport.databrowser.baseurl") + env.getProperty("immport.databrowser.browsePathPrefix");
    	model.addAttribute("asperaDataBrowserUrl",asperaDataBrowserUrl);
    	
        return "public/study/study/displayStudyDetail";
    }

    @RequestMapping(value="displayStudyDetailModal/{studyAccession}", method=RequestMethod.GET)
    public String displayStudyDetailModal(@PathVariable String studyAccession, Model model) {    
        Study study = studyService.findById(studyAccession);
        model.addAttribute("study",study);
        List<Study2Panel> study2Panels = study2PanelService.getByStudy(studyAccession);
        String study2Panel = "";
        for (int i=0;i < study2Panels.size();i++) {
            study2Panel = study2Panel + study2Panels.get(i).getId().getPanelName() + ",";
        }
        model.addAttribute("study",study);
        model.addAttribute("study2Panels",study2Panel);
        ResourceBundle resources = ResourceBundle.getBundle("application");
    	String asperaDataBrowserUrl = env.getProperty("immport.databrowser.baseurl") + env.getProperty("immport.databrowser.browsePathPrefix");
    	model.addAttribute("asperaDataBrowserUrl",asperaDataBrowserUrl);
        return "public/study/study/displayStudyDetailModal";
    }
    
    
    @RequestMapping(value="displayStudyHtml/{studyAccession}", method=RequestMethod.GET)
    public String displayStudyHtml(@PathVariable String studyAccession, Model model) {    
        Study study = studyService.getStudyDataForSummaryTab(studyAccession);
        model.addAttribute("study",study);  
        
        LkDataCompleteness objDatacompleteness = study.getLkDataCompleteness();
        
        int dclId = objDatacompleteness.getId();
        String description = objDatacompleteness.getDescription();
        if (description == null)
        {
        	description = "";
        }
        String stDclId = String.valueOf(dclId) + " - " + description;
        
        model.addAttribute("dclId",stDclId);
        
        String urlStudyDownload = "https://immport.niaid.nih.gov/immportWeb/home/display.do?content=" + studyAccession;
        
        model.addAttribute("studyDownload",urlStudyDownload);
        
        Set<StudyPersonnel> objStudyPeronnels = study.getStudyPersonnels();        
        Iterator<StudyPersonnel> itStudyPersonnel = objStudyPeronnels.iterator();
        
        String stProjectPersonnel = "";
        while(itStudyPersonnel.hasNext())
        {
        	StudyPersonnel objStutyPer = itStudyPersonnel.next();
        	String stRole = objStutyPer.getRoleInStudy();
        	if (stRole != null && stRole.equalsIgnoreCase("Principal Investigator"))
        	{
        		stProjectPersonnel = stProjectPersonnel + objStutyPer.getFirstName() 
        			+ " " + objStutyPer.getLastName();
        		if (objStutyPer.getOrganization() != null) {
        			stProjectPersonnel +=  " - " + objStutyPer.getOrganization();
        		}
        	    stProjectPersonnel += "; ";
        	}
        }
        if (stProjectPersonnel.length() > 0)
        {
        	stProjectPersonnel = stProjectPersonnel.substring(0, stProjectPersonnel.length() -2);
        }
        
        model.addAttribute("studyPI",stProjectPersonnel); 
        
        String contractGrant = studyService.getContactGrant(studyAccession);
        
        model.addAttribute("contractGrant",contractGrant); 
        
        List <StudyPubmed> studyPubmeds = studyPubmedService.getByStudy(studyAccession);
        model.addAttribute("studyPubmeds",studyPubmeds);
        
        List <StudyLink> studyLinks = studyLinkService.getByStudy(studyAccession);
        model.addAttribute("studyLinks",studyLinks);
        
        List <StudyGlossary> terms = studyGlossaryService.getByStudy(studyAccession);
        model.addAttribute("terms",terms);
        
        ResourceBundle resources = ResourceBundle.getBundle("application");
    	String asperaDataBrowserUrl = env.getProperty("immport.databrowser.baseurl") + env.getProperty("immport.databrowser.browsePathPrefix");
    	model.addAttribute("asperaDataBrowserUrl",asperaDataBrowserUrl);
        
        return "public/study/study/displayStudyHtml";
    }
    
    
    @RequestMapping(value="studyDetailOption1/{studyAccession}", method=RequestMethod.GET)
    public String studyDetailOption1(@PathVariable String studyAccession, Model model) {    
        Study study = studyService.findById(studyAccession);
        model.addAttribute("study",study);
        return "public/study/study/studyDetailOption1";
    }
    
    
    @RequestMapping(value="studyDetailOption2/{studyAccession}", method=RequestMethod.GET)
    public String studyDetailOption2(@PathVariable String studyAccession, Model model) {    
        Study study = studyService.findById(studyAccession);
        model.addAttribute("study",study);
        return "public/study/study/studyDetailOption2";
    }
    
    
    @RequestMapping(value="studyDetailOption3/{studyAccession}", method=RequestMethod.GET)
    public String studyDetailOption3(@PathVariable String studyAccession, Model model) {    
        Study study = studyService.findById(studyAccession);
        model.addAttribute("study",study);
        return "public/study/study/studyDetailOption3";
    }
    
    
    @RequestMapping(value="studyDetailOption4/{studyAccession}", method=RequestMethod.GET)
    public String studyDetailOption4(@PathVariable String studyAccession, Model model) {    
        Study study = studyService.findById(studyAccession);
        model.addAttribute("study",study);
        return "public/study/study/studyDetailOption4";
    }
    
    
    @RequestMapping(value="getStudyImage",method = RequestMethod.GET)
    public ModelAndView getStudyImage(HttpServletResponse response,
            @RequestParam("studyAccession") String studyAccession)
            throws IOException {
    	String fileSystemPath = env.getProperty("immport.open.fileSystemRootDirectory");
    	log.info("fileSystemPath - " + fileSystemPath);
                        
        StudyImage studyImage = studyService.getStudyImage(studyAccession);        
        String filePath = fileSystemPath + "/study_images/" +  studyImage.getImageFilename();
        InputStream is = new FileInputStream(filePath);
        OutputStream out = response.getOutputStream();
		response.addHeader("Content-Type","imgage/jpeg");
		try {
			byte[] buf = new byte[16 * 1024]; // 16k buffer
			int nRead = 0;
			while ((nRead = is.read(buf)) != -1) {
				out.write(buf, 0, nRead);
			}
		} finally {
			//
			// MUST ALWAYS CLOSE OPEN FILE HANDLES
			//
			if (is != null) {
				is.close();
			}
		}                 
    	
        return null;
    }
    
    @RequestMapping(value="getStudySummary/{studyAccession}",method = RequestMethod.GET)
    public ModelAndView getStudySummary(@PathVariable String studyAccession, HttpServletResponse response)
            throws ServletException, IOException {

            StudySummary study = studyService.getStudySummary(studyAccession);
            String json = new JSONSerializer().prettyPrint(true).exclude("class").serialize(study);
            response.getWriter().println(json.toString());
            return null;
    }
    
    /*
     * For some reason, that I (JohnC) can not figure out, this method
     * creates a String out of bounds exception, even though it 
     * completes successfully. Hopefully someone else will be able to
     * correct the error.
     */
    @RequestMapping(value="getStudySummaryAllDetail",method = RequestMethod.GET)
    public ModelAndView getStudySummaryAllDetail(HttpServletResponse response)
            throws  ServletException, IOException {

            List <StudySummary> studies = studyService.getStudySummaryAllDetail();
            /*
             * Not using personnel currently so removing from output
            String json = new JSONSerializer().prettyPrint(true).exclude("class").include("researchFocus").include("subjectSpecies")
            		.include("biosampleTypes").include("experimentMeasurementTechnique").exclude("personnel.study").include("personnel").serialize(studies);
            */
            String json = new JSONSerializer().prettyPrint(true).exclude("class").include("researchFocus").include("subjectSpecies")
            		.include("biosampleTypes").include("experimentMeasurementTechnique").serialize(studies);
            response.getWriter().println(json.toString());
            return null;
    }
    
    @RequestMapping(value="studyDownload/{studyAccession}",method = RequestMethod.GET)
    public String studyDownload(@PathVariable String studyAccession, Model model) {
        Study study = studyService.findById(studyAccession);
        model.addAttribute("study",study);
        return "public/study/study/studyDownload";
    }
    
}

package org.immport.open.web.controller.study;

import java.util.List;

import org.immport.data.shared.model.StudyPubmed;
import org.immport.data.shared.service.StudyPubmedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/public/study/studyPubmed/*")
public class StudyPubmedController {
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyPubmedController.class);

    @Autowired
    private StudyPubmedService studyPubmedService;
    

    @RequestMapping(value="displayStudyPubmedHtml/{studyAccession}", method=RequestMethod.GET)
    public String displayStudyPubmedHtml(@PathVariable String studyAccession, Model model) {    
        List <StudyPubmed> studyPubmeds = studyPubmedService.getByStudy(studyAccession);
        model.addAttribute("studyPubmeds",studyPubmeds);
        return "public/study/studyPubmed/displayStudyPubmedHtml";
    }
    
}

package org.immport.open.web.controller.study;

import java.util.List;

import org.immport.data.shared.model.StudyGlossary;
import org.immport.data.shared.service.StudyGlossaryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/public/study/glossary/*")
public class StudyGlossaryController {
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyGlossaryController.class);

    @Autowired
    private StudyGlossaryService studyGlossaryService;
    

    @RequestMapping(value="displayGlossaryHtml/{studyAccession}",method = RequestMethod.GET)
    public String displayGlossaryHtml(@PathVariable String studyAccession,Model model) {
        List <StudyGlossary> terms = studyGlossaryService.getByStudy(studyAccession);
        model.addAttribute("terms",terms);
        return "public/study/glossary/displayGlossaryHtml";
    }
}

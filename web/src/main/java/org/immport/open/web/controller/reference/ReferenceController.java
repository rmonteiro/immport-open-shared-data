package org.immport.open.web.controller.reference;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ReferenceController {
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ReferenceController.class);

    @RequestMapping(value="/public/reference/cytokineRegistry",method = RequestMethod.GET)
    public String cytokineRegistry(Model model) {
        return "public/reference/cytokineRegistry";
    }

    @RequestMapping(value="/public/reference/fluVaccine",method = RequestMethod.GET)
    public String fluVaccine(Model model) {
        return "public/reference/fluVaccine";
    }

    @RequestMapping(value="/public/reference/fluVaccineAnalysis",method = RequestMethod.GET)
    public String fluVaccineAnalysis(Model model) {
        return "public/reference/fluVaccineAnalysis";
    }
    
    @RequestMapping(value="/public/reference/genelists",method = RequestMethod.GET)
    public String genelists(Model model) {
        return "public/reference/genelists";
    }
    
}


package org.immport.open.web.controller.research;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.immport.data.shared.model.Protocol;
import org.immport.data.shared.service.ProtocolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/public/research/protocol/*")
public class ProtocolController {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ProtocolController.class);

    @Autowired
    private ProtocolService protocolService;
    
    @Autowired
    private Environment env;


    @RequestMapping(value="displayProtocolHtml/{studyAccession}", method=RequestMethod.GET)
    public String displayProtocolHtml(@PathVariable String studyAccession, Model model) {
        log.info("Study Accession : " + studyAccession);
        List <Protocol> protocols = protocolService.getByStudy(studyAccession);
        model.addAttribute("protocols",protocols);
        return "public/research/protocol/displayProtocolHtml";
    }
    
    @RequestMapping(value="getProtocolFile/{protocolAccession}",method = RequestMethod.GET)
    public ModelAndView getProtocolFile(@PathVariable String protocolAccession,
            HttpServletResponse response)
            throws IOException {
    	String fileSystemPath = env.getProperty("immport.open.fileSystemRootDirectory");
    	log.info("fileSystemPath - " + fileSystemPath);
                        
        Protocol protocol = protocolService.findById(protocolAccession);
        OutputStream out = response.getOutputStream();
        String filePath = fileSystemPath + "/protocols/" + protocol.getFileName();
        response.addHeader("Content-Type","application/octet-stream");
        String fileName = StringUtils.replace(protocol.getFileName(), " ", "_");
        response.setHeader("Content-Disposition", "attachment; filename="
                    + fileName);

        // Tell browser to validate cache
        response.addHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Cache-Control", "max-age=0");
        // Using public somehow cures IE's inability to save attachment
        // (and doesn't seem to hurt other browsers)
        response.setHeader("Pragma", "public");

        InputStream is = new FileInputStream(filePath);
        byte[] buf = new byte[16 * 1024]; // 16k buffer
        int nRead = 0;
        while ((nRead = is.read(buf)) != -1) {
            out.write(buf, 0, nRead);
        }

        out.flush();
        out.close();
        return null;

    	
    }
    
}

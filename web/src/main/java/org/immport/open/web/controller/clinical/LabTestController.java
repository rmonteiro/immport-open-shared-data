/**
 * 
 */
package org.immport.open.web.controller.clinical;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.service.LabTestService;
import org.immport.data.shared.service.ArmOrCohortService;
import org.immport.open.web.controller.common.JSONStringBuilderUtil;
import org.immport.open.web.controller.common.JSONStringBuilderUtilNew;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author fmonteiro
 *
 */
@Controller
@RequestMapping("/public/clinical/labTest/*")
public class LabTestController {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(LabTestController.class);

    @Autowired
    private LabTestService labTestService;

    @Autowired
    private ArmOrCohortService armOrCohortService;


    @RequestMapping(value = "displayLabTestHtml/{studyAccession}", method = RequestMethod.GET)
    public String displayLabTestHtml(
            @PathVariable String studyAccession, Model model) {
        model.addAttribute("studyAccession", studyAccession);
        
        List<ArmCohortSortOrder> armorcohorts = armOrCohortService.getArmOrderAndDescByStudy(studyAccession);
        model.addAttribute("armorcohorts", armorcohorts);
        
        return "public/clinical/labTest/displayLabTestHtml";
    }

    

    @RequestMapping(value = "getLabTestPanelSummaryByStudy/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getLabTestPanelSummaryByStudy(
            @PathVariable String studyAccession, HttpServletResponse response)
            throws IOException {

        //System.out.println("Study Accession: " + studyAccession);
        List<GenericPivotData> objList = labTestService
                .getLabTestPanelSummaryByStudy(studyAccession);
        String sbuffer = "";
        if (objList != null) {               	
        	sbuffer = JSONStringBuilderUtil.buildJSONStringfromList(objList,-1);
        }

        log.debug("Data Sent :" + sbuffer);
        //System.out.println("Data Sent :" + sbuffer);
        response.getWriter().println(sbuffer);
        
        return null;
    }
    
    @RequestMapping(value = "getLabTestPanelSummaryByStudyNew/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getLabTestPanelSummaryByStudyNew(
            @PathVariable String studyAccession, HttpServletResponse response)
            throws IOException {

        //System.out.println("Study Accession: " + studyAccession);
        List<GenericPivotData> objList = labTestService
                .getLabTestPanelSummaryByStudy(studyAccession);
        String sbuffer = "";
        if (objList != null) {               	
        	sbuffer = JSONStringBuilderUtilNew.buildJSONStringfromList(objList,-1);
        }

        log.debug("Data Sent :" + sbuffer);
        //System.out.println("Data Sent :" + sbuffer);
        response.getWriter().println(sbuffer);
        
        return null;
    }
    
    @RequestMapping(value = "getLabTestComponentListByStudy/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getLabTestComponentListByStudy(
                @PathVariable String studyAccession, HttpServletResponse response,
                @RequestParam("start") Integer start,
                @RequestParam("limit") Integer limit,
                @RequestParam("sort") String sort,
                @RequestParam("dir") String dir
            )
            throws IOException {
        PageableData page = labTestService
                .getLabTestComponentListByStudy(start, limit, sort,
                        dir, studyAccession);
        
        List<GenericPivotData> objList = page.getData();
        
        
        String sbuffer = JSONStringBuilderUtil.buildJSONStringfromList(objList,page.getTotalCount());
        
        log.debug("Data Sent :" + sbuffer);
        //System.out.println("Data Sent :" + sbuffer);    
        response.getWriter().println(sbuffer);

        return null;
    }

    @RequestMapping(value = "getLabTestComponentListByStudyNew/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getLabTestComponentListByStudyNew(
                @PathVariable String studyAccession, HttpServletResponse response,
                @RequestParam("start") Integer start,
                @RequestParam("limit") Integer limit,
                @RequestParam("sort") String sort,
                @RequestParam("dir") String dir
            )
            throws IOException {
        PageableData page = labTestService
                .getLabTestComponentListByStudy(start, limit, sort,
                        dir, studyAccession);
        
        List<GenericPivotData> objList = page.getData();
        
        
        String sbuffer = JSONStringBuilderUtilNew.buildJSONStringfromList(objList,page.getTotalCount());
        
        log.debug("Data Sent :" + sbuffer);
        //System.out.println("Data Sent :" + sbuffer);    
        response.getWriter().println(sbuffer);

        return null;
    }
    
}


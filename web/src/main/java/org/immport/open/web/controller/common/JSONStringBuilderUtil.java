/**
 * 
 */
package org.immport.open.web.controller.common;

import java.util.List;









import org.immport.data.shared.model.GenericPivotData;



/**
 * @author fmonteiro
 *
 */
public final class JSONStringBuilderUtil {
	
	
	private JSONStringBuilderUtil() {
		
	}
	
	/** 
     * Returns a JSON String using the list of objects 
     *
     * @param pDataList list
     * @return JSON String
     */
	public static String buildJSONStringfromList(List pDataList,long pTotalCount)
	{
		/*StringBuffer bufConMed = new StringBuffer();
		bufConMed.append("{\n");
		
		//build Total Count
		if (pTotalCount > 0) {
			String lTotalCount = buildTotalCount(pTotalCount);
			bufConMed.append(lTotalCount);
		}
		
		// build Data Store String
		String lStoreData = buildDataStore(pDataList);
		bufConMed.append(lStoreData);

		bufConMed.append("\"metaData\" : {\n");
		bufConMed.append("  \"totalProperty\" : \"totalCount\",\n");
		bufConMed.append("  \"root\" : \"data\",\n");
		// build Columns String
		String lColumns = buildColumns(pDataList);
		bufConMed.append(lColumns);

		// build fields String
		String lFields = buildFields(pDataList);
		bufConMed.append(lFields);
		
		bufConMed.append("\n}");

		bufConMed.append("}\n");
		
		return bufConMed.toString();*/
		return buildJSONStringfromList(pDataList,pTotalCount,true);
		
	}
	
	
	public static String buildJSONStringfromList(List pDataList,long pTotalCount,boolean success)
	{
		StringBuffer bufConMed = new StringBuffer();
		bufConMed.append("{\n");
		
		//build Total Count
		if (pTotalCount > 0) {
			String lTotalCount = buildTotalCount(pTotalCount);
			bufConMed.append(lTotalCount);
		}
		
		// build Data Store String
		String lStoreData = buildDataStore(pDataList);
		bufConMed.append(lStoreData);
		bufConMed.append("\"metaData\" : {\n");
		bufConMed.append("  \"totalProperty\" : \"totalCount\",\n");
		if (success) {	
			bufConMed.append("  \"success\" : true,\n");
		}
		else {			
			bufConMed.append("  \"success\" : false,\n");			
		}
		bufConMed.append("  \"root\" : \"data\",\n");
		// build Columns String
		String lColumns = buildColumns(pDataList);
		bufConMed.append(lColumns);

		// build fields String
		String lFields = buildFields(pDataList);
		bufConMed.append(lFields);
		
		bufConMed.append("\n}");

		bufConMed.append("}\n");
		
		return bufConMed.toString();
	}
	
	
	/** 
     * Returns a JSON String for the Data Store 
     *
     * @param pDataList list
     * @return JSON String
     */
	private static String buildDataStore(List objList) {
		// building the data store
		String lStoreData = "";
		lStoreData = "\"data\" : [ \n";
		for (int cnt = 0; cnt < objList.size(); cnt++) {
			Object objData = objList.get(cnt);

			String lData = "  " + buildDataStoreForObject(objData);
			if (cnt == (objList.size() - 1)) {
				lStoreData = lStoreData + lData + "\n";
			} else {
				lStoreData = lStoreData + lData + ",\n";
			}

		}
		lStoreData = lStoreData + "],\n ";
		return lStoreData;
	}
	
	/** 
     * Returns a JSON String for the object passed in 
     *
     * @param Object list
     * @return JSON String
     */
	private static String buildDataStoreForObject(Object objData) {
		String stDataStore = null;
		List<String> objList = null;

		stDataStore = "{";
		int count = 0;

		if (objData instanceof GenericPivotData){
			List<String> objDataLst = ((GenericPivotData)objData).getBaseData();
			
			for (int i=0;i<objDataLst.size();i++)
			{
				stDataStore = stDataStore + "col" + count++ + ": \""
						+ objDataLst.get(i) + "\",";
			}
			
			objList = ((GenericPivotData) objData).getArmData();
		}
			
        if (objList != null) {
			for (int i = 0; i < objList.size(); i++) {
				if (i == (objList.size() - 1)) {
					stDataStore = stDataStore + "col" + count++ + ": \""
							+ ((objList.get(i) == null) ? "" : objList.get(i))
							+ "\"";
				} else {
					stDataStore = stDataStore + "col" + count++ + ": \""
							+ ((objList.get(i) == null) ? "" : objList.get(i))
							+ "\",";
				}
			}
        }
		stDataStore = stDataStore + "}";
		return stDataStore;
	}

	
	/** 
     * Returns a JSON String for the Columns 
     *
     * @param Object list
     * @return JSON String
     */
	private static String buildColumns(List objList) {
		String lColumns = null;
		boolean isArmNames = false;
		List<String> objArmData = null;
		List<String> objArmName = null;
		lColumns = "  \"columns\" : [ ";

		int count = 0;

		if (objList.size() > 0) {
			Object obj = objList.get(0);
			
			if (obj instanceof GenericPivotData){
				List<String> objDataLst = ((GenericPivotData)obj).getBaseNames();
				List<String> objDataLstSort = ((GenericPivotData)obj).getBaseNamesSort();
				List<String>  objbaseNamesSortParameter = ((GenericPivotData)obj).getBaseNamesSortParameter();
				for (int i=0;i<objDataLst.size();i++)
				{

					if ((objDataLstSort != null) && (objDataLstSort.size() > 0)) {
						if (objDataLstSort.get(i).equalsIgnoreCase("true") && !(objbaseNamesSortParameter.get(i).equalsIgnoreCase("null"))) {
							lColumns = lColumns
									+ "{ text: \"" + objDataLst.get(i) + "\", width: 200, sortable: " + objDataLstSort.get(i) + ", dataIndex: \"col"
									+ count++
									+ "\", renderer: renderTip, getSortParam: function() {return '" + objbaseNamesSortParameter.get(i) + "';} }, ";
									
						}
						else {
							lColumns = lColumns
									+ "{ text: \"" + objDataLst.get(i) + "\", width: 200, sortable: " + objDataLstSort.get(i) + ", dataIndex: \"col"
									+ count++
									+ "\" }, ";
						}
					}
					else {	
						lColumns = lColumns
							+ "{ text: \"" + objDataLst.get(i) + "\", width: 200, sortable: false, dataIndex: \"col"
							+ count++
							+ "\" }, ";
					}
				}
				objArmData = ((GenericPivotData) obj).getArmData();
				objArmName = ((GenericPivotData) obj).getArmNames();
				isArmNames = true;
			}

		
			if ((obj instanceof GenericPivotData) ||  isArmNames) {				
				lColumns = lColumns +  buildArmColumns(objArmData,objArmName,count);
				
				
			}			
		}
		lColumns = lColumns + "],\n ";
		return lColumns;
	}

	
	private static String buildArmColumns(List<String> objArmData,List<String> objArmName,int inCount)
	{
		    int count = inCount;
			String lColumns = "";
			//int armcount = 1;
			if (objArmData != null) {
				for (int i = 0; i < objArmData.size(); i++) {
					if (i == (objArmData.size() - 1)) {
						lColumns = lColumns + "{ text: \"" + objArmName.get(i)
								+ "\", width: 100, sortable: false, dataIndex: \"col" + count++
								+ "\" }";
	
					} else {
						lColumns = lColumns + "{ text: \"" + objArmName.get(i)
								+ "\", width: 100, sortable: false, dataIndex: \"col" + count++
								+ "\" },";
					}
					//armcount++;
				}
			}
			return lColumns;
		
	}
	/** 
     * Returns a JSON String for the Fields 
     *
     * @param Object list
     * @return JSON String
     */
	private static String buildFields(List objList) {
		String lFields = null;
		List<String> objArmData = null;

		lFields = "  \"fields\" : [ ";

		int count = 0;

		if (objList.size() > 0) {
			Object obj = objList.get(0);
			
			if (obj instanceof GenericPivotData){
				List<String> objDataLst = ((GenericPivotData)obj).getBaseNames();
				
				for (int i=0;i<objDataLst.size();i++)
				{
					lFields = lFields + "{ name: \"col" + count++
							+ "\", type: \"string\" },";
				}
				objArmData = ((GenericPivotData) obj).getArmData();				
			}
			

		}
		//int armcount = 1;
		if (objArmData != null) {
			for (int i = 0; i < objArmData.size(); i++) {
				if (i == (objArmData.size() - 1)) {
					lFields = lFields + "{ name: \"col" + count++
							+ "\", type: \"string\" }";

				} else {
					lFields = lFields + "{ name: \"col" + count++
							+ "\", type: \"string\" },";
				}
				//armcount++;
			}

		}
		lFields = lFields + "] \n ";
		return lFields;
	}
	
	/** 
     * Returns a JSON String for the Total Count 
     *
     * @param long
     * @return JSON String
     */
	private static String buildTotalCount(long pTotalCount)	{
		String lTotalCount = "\"totalCount\":" + String.valueOf(pTotalCount) + ",\n ";
		return lTotalCount;
	}


}

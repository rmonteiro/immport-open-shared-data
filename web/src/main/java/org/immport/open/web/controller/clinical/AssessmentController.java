/**
 * 
 */
package org.immport.open.web.controller.clinical;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.service.AssessmentService;
import org.immport.data.shared.service.ArmOrCohortService;
import org.immport.open.web.controller.common.JSONStringBuilderUtil;
import org.immport.open.web.controller.common.JSONStringBuilderUtilNew;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/public/clinical/assessment/*")
public class AssessmentController {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(AssessmentController.class);

    @Autowired
    private AssessmentService assessmentService;

    @Autowired
    private ArmOrCohortService armOrCohortService;
    
    @RequestMapping(value = "displayAssessmentHtml/{studyAccession}", method = RequestMethod.GET)
    public String displayAssessmentHtml(
            @PathVariable String studyAccession, Model model) {
        model.addAttribute("studyAccession", studyAccession);
        List<ArmCohortSortOrder> armorcohorts = armOrCohortService.getArmOrderAndDescByStudy(studyAccession);
        model.addAttribute("armorcohorts", armorcohorts);  
        return "public/clinical/assessment/displayAssessmentHtml";
    }

    @RequestMapping(value = "getAssessmentSummaryByStudy/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getAssessmentSummaryByStudy(
                @PathVariable String studyAccession, HttpServletResponse response,
                @RequestParam("start") Integer start,
                @RequestParam("limit") Integer limit,
                @RequestParam("sort") String sort,
                @RequestParam("dir") String dir
            )
            throws IOException {
        PageableData page = assessmentService
                .getAssessmentSummaryByStudy(start, limit, sort,
                        dir, studyAccession);
        
        List<GenericPivotData> objList = page.getData();
        String sbuffer = JSONStringBuilderUtil.buildJSONStringfromList(objList,page.getTotalCount()); 
        log.debug("Data Sent :" + sbuffer);   
        response.getWriter().println(sbuffer);
        return null;
    }

    @RequestMapping(value = "getAssessmentSummaryByStudyNew/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getAssessmentSummaryByStudyNew(
                @PathVariable String studyAccession, HttpServletResponse response,
                @RequestParam("start") Integer start,
                @RequestParam("limit") Integer limit,
                @RequestParam("sort") String sort,
                @RequestParam("dir") String dir
            )
            throws IOException {
        PageableData page = assessmentService
                .getAssessmentSummaryByStudy(start, limit, sort,
                        dir, studyAccession);
        
        List<GenericPivotData> objList = page.getData();
        String sbuffer = JSONStringBuilderUtilNew.buildJSONStringfromList(objList,page.getTotalCount());
        response.getWriter().println(sbuffer);
        return null;
    }
    
    @RequestMapping(value = "getAssessmentComponentListByStudy/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getAssessmentComponentListByStudy(
                @PathVariable String studyAccession, HttpServletResponse response,
                @RequestParam("start") Integer start,
                @RequestParam("limit") Integer limit,
                @RequestParam("sort") String sort,
                @RequestParam("dir") String dir
            )
            throws IOException {
        PageableData page = assessmentService
                .getAssessmentComponentListByStudy(start, limit, sort,
                		dir, studyAccession);
        
        List<GenericPivotData> objList = page.getData();
        String sbuffer = JSONStringBuilderUtil.buildJSONStringfromList(objList,page.getTotalCount());  
        log.debug("Data Sent :" + sbuffer);    
        response.getWriter().println(sbuffer);
        return null;
    }
    
    @RequestMapping(value = "getAssessmentComponentListByStudyNew/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getAssessmentComponentListByStudyNew(
                @PathVariable String studyAccession, HttpServletResponse response,
                @RequestParam("start") Integer start,
                @RequestParam("limit") Integer limit,
                @RequestParam("sort") String sort,
                @RequestParam("dir") String dir
            )
            throws IOException {
        PageableData page = assessmentService
                .getAssessmentComponentListByStudy(start, limit, sort,
                		dir, studyAccession);
        
        List<GenericPivotData> objList = page.getData();
        String sbuffer = JSONStringBuilderUtilNew.buildJSONStringfromList(objList,page.getTotalCount());
        response.getWriter().println(sbuffer);
        return null;
    }
    
    @RequestMapping(value = "getAssessmentStudyTimeListByStudy/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getAssessmentStudyTimeListByStudy(
                @PathVariable String studyAccession, HttpServletResponse response,
                @RequestParam("start") Integer start,
                @RequestParam("limit") Integer limit,
                @RequestParam("sort") String sort,
                @RequestParam("dir") String dir
            )
            throws IOException {
        PageableData page = assessmentService
                .getStudyTimeCollectedByAssessmentsPageable(start, limit, sort,
                		dir, studyAccession);
        
        List<GenericPivotData> objList = page.getData();    
        String sbuffer = JSONStringBuilderUtil.buildJSONStringfromList(objList,page.getTotalCount());
        
        log.debug("Data Sent :" + sbuffer);  
        response.getWriter().println(sbuffer);
        return null;
    }
    
    @RequestMapping(value = "getAssessmentStudyTimeListByStudyNew/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getAssessmentStudyTimeListByStudyNew(
                @PathVariable String studyAccession, HttpServletResponse response,
                @RequestParam("start") Integer start,
                @RequestParam("limit") Integer limit,
                @RequestParam("sort") String sort,
                @RequestParam("dir") String dir
            )
            throws IOException {
        PageableData page = assessmentService
                .getStudyTimeCollectedByAssessmentsPageable(start, limit, sort,
                		dir, studyAccession);
        
        List<GenericPivotData> objList = page.getData();       
        String sbuffer = JSONStringBuilderUtilNew.buildJSONStringfromList(objList,page.getTotalCount());      
        log.debug("Data Sent :" + sbuffer);    
        response.getWriter().println(sbuffer);
        return null;
    }
}


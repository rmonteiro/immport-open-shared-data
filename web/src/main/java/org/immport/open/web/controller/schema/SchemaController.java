package org.immport.open.web.controller.schema;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/public/schema/*")
public class SchemaController {
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SchemaController.class);

    @RequestMapping(value="erDiagrams",method = RequestMethod.GET)
    public String erDiagrams(Model model) {
        return "public/schema/erDiagrams";
    }
    
    
    @RequestMapping(value="schemaDefinition/{tableName}",method = RequestMethod.GET)
    public String schemaDefinition(@PathVariable String tableName, Model model) {
        model.addAttribute("tableName",tableName);
        return "public/schema/schemaDefinition";
    }
    
    @RequestMapping(value="schemaDiagram/{schemaDiagram}",method = RequestMethod.GET)
    public String schemaDiagram(@PathVariable String schemaDiagram, Model model) {
        model.addAttribute("schemaDiagram",schemaDiagram);
        return "public/schema/schemaDiagram";
    }

    @RequestMapping(value="templateDocumentation",method = RequestMethod.GET)
    public String templateDocumentation(Model model) {
        return "public/schema/templateDocumentation";
    }

    @RequestMapping(value="schemaTree",method = RequestMethod.GET)
    public String schemaTree(Model model) {
        return "public/schema/schemaTree";
    }
}
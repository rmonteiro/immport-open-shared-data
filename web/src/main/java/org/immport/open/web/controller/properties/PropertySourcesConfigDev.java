package org.immport.open.web.controller.properties;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;


@Configuration
@Profile("dev")
@PropertySource("classpath:application-dev.properties")
public class PropertySourcesConfigDev {

	 /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(PropertySourcesConfigDev.class);
    
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    	 return new PropertySourcesPlaceholderConfigurer();
    }
    
   

}
/**
 * 
 */
package org.immport.open.web.controller.study;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.immport.data.shared.model.InclusionExclusion;
import org.immport.data.shared.model.PlannedVisit;
import org.immport.data.shared.model.PlannedVisitBin;
import org.immport.data.shared.model.PlannedVisitData;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.Protocol;
import org.immport.data.shared.model.Study;
import org.immport.data.shared.service.InclusionExclusionService;
import org.immport.data.shared.service.PlannedVisitService;
import org.immport.data.shared.service.ProtocolService;
import org.immport.data.shared.service.StudyService;
import org.immport.open.web.controller.common.JSONStringBuilderUtilNew;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author fmonteiro
 *
 */
@Controller
@RequestMapping("/public/study/studyDesign/*")
public class StudyDesignController {
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyDesignController.class);

    @Autowired
    private InclusionExclusionService inclusionExclusionService;

    @Autowired
    private ProtocolService protocolService;
    
    @Autowired
    private StudyService studyService;

    @Autowired
    private PlannedVisitService plannedVisitService;

    @RequestMapping(value="displayStudyDesignHtml/{studyAccession}",method = RequestMethod.GET)
    public String displayStudyDesignHtml(@PathVariable String studyAccession,Model model) {
        log.info("Study Accession : " + studyAccession);
        
        List <InclusionExclusion> inclusionExclusions = inclusionExclusionService.getByStudy(studyAccession);
        model.addAttribute("inclusionExclusions",inclusionExclusions);
        
        List <Protocol> protocols = protocolService.getByStudy(studyAccession);
        model.addAttribute("protocols",protocols);
        
        Study study = studyService.getStudyDataForSummaryTab(studyAccession);
        model.addAttribute("studyArm",study);   
        
        PlannedVisitData plannedVisitDataExperiment = plannedVisitService.getPlannedVisitsByType(studyAccession, "EXPERIMENT");

        List<PlannedVisitDisplayLegend> plannedVisitDisplayLegends = new ArrayList<PlannedVisitDisplayLegend>();
        if (plannedVisitDataExperiment.getErrorCode() == 0 || plannedVisitDataExperiment.getErrorCode() == 2) {
        
	        List<PlannedVisit> plannedVisits = plannedVisitDataExperiment.getPlannedVisits();
	        List<PlannedVisitBin> plannedVisitBins = plannedVisitDataExperiment.getPlannedVisitBins();
	       
	        for(int i=0;i<plannedVisits.size();i++){
	        		PlannedVisit plannedVisit = plannedVisits.get(i);
	        		PlannedVisitDisplayLegend plannedVisitDisplayLegend = new PlannedVisitDisplayLegend();
	        		String visitName = plannedVisit.getName();
	        		plannedVisitDisplayLegend.setPlannedVisitAccession(plannedVisit.getPlannedVisitAccession());
	        		plannedVisitDisplayLegend.setVisitName(visitName);
	        		double minStartDay = plannedVisit.getMinStartDay().doubleValue();
	        		double maxStartDay = plannedVisit.getMaxStartDay().doubleValue();
	        		plannedVisitDisplayLegend.setOriginalBinRange(String.valueOf(minStartDay) + " to " + String.valueOf(maxStartDay));
	        		plannedVisitDisplayLegends.add(plannedVisitDisplayLegend);        	
	        }

	        model.addAttribute("plannedVisitDisplayLegends",plannedVisitDisplayLegends);
        }        
        
        return "public/study/studyDesign/displayStudyDesignHtml";
    }
    
    
    @RequestMapping(value="getProtocolFile/{protocolAccession}",method = RequestMethod.GET)
    public ModelAndView getProtocolFile(@PathVariable String protocolAccession,
            HttpServletResponse response)
            throws IOException {

    	/*
        Protocol protocol = protocolService.findById(protocolAccession);
        OutputStream out = response.getOutputStream();
        response.addHeader("Content-Type","application/octet-stream");
        String fileName = StringUtils.replace(protocol.getFileName(), " ", "_");
        response.setHeader("Content-Disposition", "attachment; filename="
                    + fileName);

        // Tell browser to validate cache
        response.addHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Cache-Control", "max-age=0");
        // Using public somehow cures IE's inability to save attachment
        // (and doesn't seem to hurt other browsers)
        response.setHeader("Pragma", "public");

        InputStream is = new ByteArrayInputStream(protocol.getProtocolFile());
        byte[] buf = new byte[16 * 1024]; // 16k buffer
        int nRead = 0;
        while ((nRead = is.read(buf)) != -1) {
            out.write(buf, 0, nRead);
        }
        out.flush();
        out.close();
        */
        return null;
    }
    
    @RequestMapping(value = "getPlannedVisitsByExperiment/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getPlannedVisitsByExperiment(
            @PathVariable String studyAccession, HttpServletResponse response)
            throws IOException {

    		PlannedVisitData plannedVisitDataExperiment = plannedVisitService.getPlannedVisitsByType(studyAccession, "EXPERIMENT");
    		String sbuffer = "";
    		if (plannedVisitDataExperiment != null && (plannedVisitDataExperiment.getErrorCode() == 0)) {
    			List<GenericPivotData> objList = plannedVisitDataExperiment.getGenericPivotData();
    			if (objList != null) {
    				sbuffer = JSONStringBuilderUtilNew.buildJSONStringfromList(objList,-1);
    			}
    		}
        response.getWriter().println(sbuffer);       
        return null;
    }
    
    @RequestMapping(value = "getPlannedVisitsByLabTest/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getPlannedVisitsByLabTest(
            @PathVariable String studyAccession, HttpServletResponse response)
            throws IOException {
    	
    		PlannedVisitData plannedVisitDataLabTest = plannedVisitService.getPlannedVisitsByType(studyAccession, "LABTEST");
    		String sbuffer = "";
    		if (plannedVisitDataLabTest != null && (plannedVisitDataLabTest.getErrorCode() == 0)) {
	        List<GenericPivotData> objList = plannedVisitDataLabTest.getGenericPivotData();
	       
	        if (objList != null) {
	        		sbuffer = JSONStringBuilderUtilNew.buildJSONStringfromList(objList,-1);
	        }
    		}  
        response.getWriter().println(sbuffer);    
        return null;
    }
    
    @RequestMapping(value = "getPlannedVisitsByAssessment/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getPlannedVisitsByAssessment(
            @PathVariable String studyAccession, HttpServletResponse response)
            throws IOException {
    	
    		PlannedVisitData plannedVisitDataAssessment = plannedVisitService.getPlannedVisitsByType(studyAccession, "ASSESSMENT");
    		String sbuffer = "";
    		if (plannedVisitDataAssessment != null && (plannedVisitDataAssessment.getErrorCode() == 0)) {
	        List<GenericPivotData> objList = plannedVisitDataAssessment.getGenericPivotData();
	       
	        if (objList != null) {
	        		sbuffer = JSONStringBuilderUtilNew.buildJSONStringfromList(objList,-1);
	        }
    		}
        response.getWriter().println(sbuffer);       
        return null;
    }
    
}


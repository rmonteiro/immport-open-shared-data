package org.immport.open.web.controller.exception;

import java.util.ArrayList;
import java.util.List;

import org.immport.data.shared.model.GenericPivotData;
import org.immport.open.web.controller.common.JSONStringBuilderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


@ControllerAdvice
public class GlobalExceptionHandler {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);


    @ExceptionHandler
    public ModelAndView handleException(Exception exception) {
    	 // log.info(exception.toString());
    	//if (exception instanceof GridException) {
    	// return null;	
    	//}
    	//else {
	        log.debug("");
	        log.debug("Exception handled by GlobalExceptionHandler:");
	        log.debug("");
	        log.debug(exception.getMessage());
	        log.debug("");
	        log.debug("Redirecting to /error...");
	        log.debug("");
	        //log.error(exception.getMessage(), exception);
	        logError(exception);
	        String genericErrorMessage = "This Page has encountered an error. It will be investigated.";        
	        return new ModelAndView("error", "errorMessage",genericErrorMessage);
    	//}
    }
    
    @ExceptionHandler
    public @ResponseBody String handleException(GridException ex) {
    	log.debug("");
        log.debug("Exception handled by GlobalExceptionHandler:GridException");
        log.debug("");
        log.debug(ex.getMessage());
        log.debug("");
        List pDataList  = new ArrayList<GenericPivotData>(); 		
		String sbuffer = JSONStringBuilderUtil.buildJSONStringfromList(pDataList,0,false);
    	return sbuffer;
    }
    
    public static void logError(Exception exception){
    	log.error(exception.getMessage(), exception);
    }

}

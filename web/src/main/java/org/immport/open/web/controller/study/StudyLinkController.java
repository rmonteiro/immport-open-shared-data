package org.immport.open.web.controller.study;

import java.util.List;

import org.immport.data.shared.model.StudyLink;
import org.immport.data.shared.service.StudyLinkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/public/study/studyLink/*")
public class StudyLinkController {
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyLinkController.class);

    @Autowired
    private StudyLinkService studyLinkService;
    

    @RequestMapping(value="displayStudyLinkHtml/{studyAccession}", method=RequestMethod.GET)
    public String displayStudyLinkHtml(@PathVariable String studyAccession, Model model) {    
        List <StudyLink> studyLinks = studyLinkService.getByStudy(studyAccession);
        model.addAttribute("studyLinks",studyLinks);
        return "public/study/studyLink/displayStudyLinkHtml";
    }

}

package org.immport.open.web.controller.clinical;

import java.util.List;


import org.immport.data.shared.model.InclusionExclusion;
import org.immport.data.shared.service.InclusionExclusionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/public/clinical/inclusionExclusion/*")
public class InclusionExclusionController {
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(InclusionExclusionController.class);

    @Autowired
    private InclusionExclusionService inclusionExclusionService;
    

    @RequestMapping(value="displayInclusionExclusionHtml/{studyAccession}",method = RequestMethod.GET)
    public String displayInclusionExclusionHtml(@PathVariable String studyAccession,Model model) {
        List <InclusionExclusion> inclusionExclusions = inclusionExclusionService.getByStudy(studyAccession);
        model.addAttribute("inclusionExclusions",inclusionExclusions);
        return "public/clinical/inclusionExclusion/displayInclusionExclusionHtml";
    }
    
}

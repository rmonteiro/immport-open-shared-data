/**
 * 
 */
package org.immport.open.web.controller.research;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.SubjectDemographic;
import org.immport.data.shared.service.SubjectService;
import org.immport.data.shared.service.ArmOrCohortService;
import org.immport.open.web.controller.common.JSONStringBuilderUtil;
import org.immport.open.web.controller.common.JSONStringBuilderUtilNew;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import flexjson.JSONSerializer;

/**
 * @author fmonteiro
 *
 */
@Controller
@RequestMapping("/public/research/subject/*")
public class SubjectController {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SubjectController.class);
    
    @Autowired
    private SubjectService subjectService;
    
    @Autowired
    private ArmOrCohortService armOrCohortService;


    @RequestMapping(value = "displaySubjectHtml/{studyAccession}", method = RequestMethod.GET)
    public String displaySubjectHtml(
            @PathVariable String studyAccession, Model model) {
        model.addAttribute("studyAccession", studyAccession);
        
        List<ArmCohortSortOrder> armorcohorts = armOrCohortService.getArmOrderAndDescByStudy(studyAccession);
        model.addAttribute("armorcohorts", armorcohorts);
        
        return "public/research/subject/displaySubjectHtml";
    }

    

    @RequestMapping(value = "getDemograhicSummaryByStudy/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getDemograhicSummaryByStudy(
            @PathVariable String studyAccession, HttpServletResponse response)
            throws IOException {

        //System.out.println("Study Accession: " + studyAccession);
        List<GenericPivotData> objList = subjectService
                .getDemograhicSummaryByStudy(studyAccession);
        String sbuffer = "";
        if (objList != null) {                
        	sbuffer = JSONStringBuilderUtil.buildJSONStringfromList(objList,-1);
        }
        
        log.debug("Data Sent :" + sbuffer);
        //System.out.println("Data Sent :" + sbuffer);
        response.getWriter().println(sbuffer);
        
        return null;
    }
    
    @RequestMapping(value = "getDemograhicSummaryByStudyNew/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getDemograhicSummaryByStudyNew(
            @PathVariable String studyAccession, HttpServletResponse response)
            throws IOException {

        //System.out.println("Study Accession: " + studyAccession);
        List<GenericPivotData> objList = subjectService
                .getDemograhicSummaryByStudy(studyAccession);
        String sbuffer = "";
        if (objList != null) {                
        	sbuffer = JSONStringBuilderUtilNew.buildJSONStringfromList(objList,-1);
        }
        
        log.debug("Data Sent :" + sbuffer);
        //System.out.println("Data Sent :" + sbuffer);
        response.getWriter().println(sbuffer);
        
        return null;
    }
    
    
    @RequestMapping(value = "getSubjectDemographicsByStudy/{studyAccession}", method = RequestMethod.GET)
    public ModelAndView getSubjectDemograhicsByStudy(
            @PathVariable String studyAccession, HttpServletResponse response)
            throws IOException {

    	List <SubjectDemographic> results = subjectService.getSubjectDemographicsByStudy(studyAccession);
    	String json = new JSONSerializer().include(new String[] { "data" }).serialize(results);
        response.getWriter().println(json.toString());

        return null;
    }

    
}


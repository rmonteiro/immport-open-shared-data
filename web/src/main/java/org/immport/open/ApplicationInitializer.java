package org.immport.open;


import java.util.Iterator;
import java.util.Properties;
import java.util.ResourceBundle;


import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.mitre.dsmiley.httpproxy.ProxyServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.env.AbstractEnvironment;
import org.springframework.util.StringUtils;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;


/**
 * This class instantiates the objects, such as servlets, which need to be
 * initialized based on properties files and then added to the Servlet Context.
 *
 * IMPORTANT NOTE:
 *
 * Classes which implement the WebApplicationInitializer interface are detected
 * automatically by class SpringServletContainerInitializer, which itself is
 * bootstrapped automatically by any Servlet 3.0 container. See the javadoc for class
 * SpringServletContainerInitializer for details on this bootstrapping mechanism. 
 */
public class ApplicationInitializer implements WebApplicationInitializer {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ApplicationInitializer.class);

    private static final String DELIMITER = ",";
    
   

    @Override
    public void onStartup(ServletContext servletContext) {
        /*
         * Get the application properties
         */
        ResourceBundle applicationResources = getResources("application");

        /*
         * Set the default profiles and list the active profiles (if any)
         */
        Properties systemProperties = System.getProperties();
        String defaultProfiles = applicationResources.getString(
                AbstractEnvironment.DEFAULT_PROFILES_PROPERTY_NAME);
        log.info("Setting " + AbstractEnvironment.DEFAULT_PROFILES_PROPERTY_NAME + " to "
                + defaultProfiles);
        systemProperties.setProperty(
                AbstractEnvironment.DEFAULT_PROFILES_PROPERTY_NAME, defaultProfiles);
        String activeProfiles = systemProperties.getProperty(
                AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME);
        if (activeProfiles != null) {
            log.info("Active profiles set to " + activeProfiles);
        } else{
        	activeProfiles = defaultProfiles;
        	 log.info("Active profiles set to " + activeProfiles);
        }

        /*
         * Instantiate the Dispatcher Servlet and add it to the Servlet Context
         */
        log.info("Instantiating the Dispatcher Servlet and adding it to the Servlet Context");
        String servletName = "dispatcher";
        String[] contextConfigLocations = StringUtils.tokenizeToStringArray(
                applicationResources.getString("servlet." + servletName + ".contextConfigLocations"),
                DELIMITER);
        for (String contextConfig: contextConfigLocations)
        {
        	log.info("contextConfigLocations = " + contextConfig);
        }
        
        XmlWebApplicationContext webApplicationContext = new XmlWebApplicationContext();
        webApplicationContext.setConfigLocations(contextConfigLocations);
        webApplicationContext.getEnvironment().setActiveProfiles(activeProfiles);
//        webApplicationContext.refresh();
        ServletRegistration.Dynamic servletRegistration =
                servletContext.addServlet(servletName, new DispatcherServlet(webApplicationContext));
        servletRegistration.setLoadOnStartup(1);
        servletRegistration.addMapping("/");

        /*
         * Instantiate the HTTP Proxy Servlet and add it to the Servlet Context
         */
        log.info("Instantiating the HTTP Proxy Servlet and adding it to the Servlet Context");
        servletName = "httpProxy";
        contextConfigLocations = StringUtils.tokenizeToStringArray(
                applicationResources.getString("servlet." + servletName + ".contextConfigLocations"),
                DELIMITER);
        for (String contextConfig: contextConfigLocations)
        {
        	 log.info("contextConfigLocations = " + contextConfig);
        }
       
        webApplicationContext = new XmlWebApplicationContext();
        webApplicationContext.setConfigLocations(contextConfigLocations);
        webApplicationContext.refresh();
        String targetUrl = (String) webApplicationContext.getBean("targetUrl");
        log.info("targetUrl = " + targetUrl);
        servletRegistration = servletContext.addServlet(servletName, new ProxyServlet());
        servletRegistration.setInitParameter("targetUri", targetUrl);
        servletRegistration.setInitParameter("log", "true");
        servletRegistration.setLoadOnStartup(2);
        servletRegistration.addMapping("/search/*");
    }


    private ResourceBundle getResources(String resourceBundleName) {
        log.debug("resourceBundleName = " + resourceBundleName);
        ResourceBundle resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
        return resources;
    }

    
    

}

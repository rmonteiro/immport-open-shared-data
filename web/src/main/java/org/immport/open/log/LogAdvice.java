package org.immport.open.log;



import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.immport.open.web.controller.exception.GlobalExceptionHandler;
import org.immport.open.web.controller.exception.GridException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class LogAdvice {
	
	 /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(LogAdvice.class);
	
	 /**
     *This method is an advice that is going to be applied to the methods defined in the pointcut.
     *
     * @param  ProceedingJoinPoint
     * @return object
     */
	public Object simpleAroundAdvice(ProceedingJoinPoint pjp) throws Throwable {		
			Long startTime = System.currentTimeMillis();
			Date startDate = new Date();
			Object retVal = pjp.proceed();
			Long endTime = System.currentTimeMillis();
			Long duration = endTime - startTime;
			Date endDate = new Date();
			saveInfo(pjp, duration,startDate,endDate);
			return retVal;
	}
	
	 /**
     * Saves the information for Profiling - Class Name, Method Name, Method Execution Time, Session ID, 
     * 										 IP address , start date-time , end date-time 
     *
     * @param  ProceedingJoinPoint
     * @return object
     */
	
	private void saveInfo(ProceedingJoinPoint pjp, long duration, Date startDate, Date endDate) {
	    

		StringBuffer stBuffer = new StringBuffer("Method Name: ");
		//stBuffer.append(padRight(pjp.getSignature().getName(),30));
		stBuffer.append(pjp.getSignature().getName());
		stBuffer.append("\t");
		stBuffer.append("Class Name: ");
		//stBuffer.append(padRight(pjp.getSignature().getDeclaringTypeName(),30));
		stBuffer.append(pjp.getSignature().getDeclaringTypeName());
		stBuffer.append("\t");
		
		//System.out.println("AOP XML Annotation");
		//System.out.println("Method Name: " + pjp.getSignature().getName());
		//System.out.println("Class Name: " +
		//			pjp.getSignature().getDeclaringTypeName());
			
		String args = "";
		for(final Object argument : pjp.getArgs()){
		      
				 if (!(argument.toString().indexOf("ResponseFacade") > 0)) {
					 args = args + argument + " ,";		 
				 }
				
		    }
		
		if (args.length() > 0) {
		    args = args.substring(0, args.length() - 1);
		} else {
			args = "No Arguments";
		}
		stBuffer.append("Arguments: ");
		//stBuffer.append(padRight(args,40));
		stBuffer.append(args);
		stBuffer.append("\t");
		stBuffer.append("Took: ");
		stBuffer.append(String.valueOf(duration));
		stBuffer.append("\t");
		//System.out.println("Arguments:" + args);			
		//System.out.println("Took: " + duration);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String stStartDate = dateFormat.format(startDate);
		String stEndDate = dateFormat.format(endDate);
		
		//System.out.println("Start Date Time: " + stStartDate);
		stBuffer.append("Start Date Time: ");
		stBuffer.append(stStartDate);
		stBuffer.append("\t");
		//System.out.println("End Date Time: " + stEndDate);
		stBuffer.append("End Date Time: ");
		stBuffer.append(stEndDate);
		stBuffer.append("\t");
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		String userIpAddress = attr.getRequest().getRemoteAddr();
		//System.out.println("Session Id: " + session.getId());
		stBuffer.append("Session Id: ");
		stBuffer.append(session.getId());
		stBuffer.append("\t ");
		//System.out.println("User IP address: " + userIpAddress);
		stBuffer.append("User IP address: ");
		stBuffer.append(userIpAddress);
		//System.out.println(stBuffer.toString());
		log.info(stBuffer.toString());
		
	}	
	
	public static String padLeft(String s, int n) {
	    return String.format("%1$" + n + "s", s);  
	}
	
	public static String padRight(String s, int n) {
	     return String.format("%1$-" + n + "s", s);  
	}
	
	
	public void afterThrowing(JoinPoint jp,
			Exception ex) throws Throwable {
		
			
			GlobalExceptionHandler.logError(ex);
			
			
			throw new GridException(jp.getSignature().getName());

	}


}

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/css/dataTables.colVis.css" />" >
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/css/dataTables.tableTools.css" />" >
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/external/select2-3.5.1/select2.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/external/select2-3.5.1/select2-bootstrap.css'/>"/>

<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/media/js/jquery.dataTables.js" />" type="text/javascript"></script>
<script src="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/dataTables.colResize.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/js/dataTables.colVis.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/js/dataTables.tableTools.js" />" type="text/javascript"></script>
<script src="<c:url value='/resources/external/select2-3.5.1/select2.js'/>" type="text/javascript"></script>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <h2>Gene Lists</h2>
        <div id="errorMsg"></div>
        <select id="tableChoice" style="width:300px;">
                <option value="Cykn">Cytokine</option>
                <option value="IL">Interleukins</option>
                <option value="IFN">Interferons</option>
                <option value="TNF">TNF Family Members</option>
                <option value="TGFb">TGF-b Family Members</option>
                <option value="Cmkn">Chemokines</option>
                <option value="CyknR">Cytokine Receptors</option>
                <option value="ILR">Interleukin Receptors</option>
                <option value="IFNR">Interferon Receptors</option>
                <option value="TNFR">TNF Family Member Receptors</option>
                <option value="TGFbR">TGF-b Family Member Receptors</option>
                <option value="CmknR">Chemokine Receptors</option>
                <option value="TCRsig">TCR Signaling Pathway</option>
                <option value="BCRsig">BCR Signaling Pathway</option>
                <option value="NKcyto">Natural Killer Cell</option>
                <option value="Ag_Process">Antigen Processing and Presentation</option>
                <option value="Abx">Antimicrobials<option>
        </select>
    </div>
</div>
<div class="row">
&nbsp;
</div>

<div class="row">
  <div id="geneListDiv" class="col-md-12">
     <table id="geneListTable" class="table table-striped table-hover table-bordered table-condensed table-responsive">
        <thead>
            <tr>
                <th>ID</th>
                <th>Symbol</th>
                <th>Synonyms</th>
                <th>Chromosome</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Symbol</th>
                <th>Synonyms</th>
                <th>Chromosome</th>
            </tr>
        </tfoot>
        <tbody>
       </tbody>
       </table>
    </div>
</div>
<div class="row">
<p>
Download the <a href="<%= request.getContextPath() %>/resources/data/GOappend1.xls" target="_new">Gene Ontology Summary</a> and <a href="<%= request.getContextPath() %>/resources/data/Geneappend3.xls" target="_new">Gene Summary</a> (Updated: June 2016)<br>
</p>
<p>
<b>Additional Resources:</b><br>
Chaussabel D & Baldwin N. 2014. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=24662387" target="_new">Democratizing systems immunology with modular transcriptional repertoire analyses. Nat Rev Immunol.</a> Apr;14(4):271-80. doi: 10.1038/nri3642. </br>
Li et al., 2015. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=24336226" target="_new">Molecular signatures of antibody responses derived from a systems biology study of five human vaccines.</a>  Nat Immunol. Feb;15(2):195-204. doi: 10.1038/ni.2789. Epub 2013 Dec 15.</br>
<a href="http://www.interactivefigures.com/nri/miniURL/view/Iz" target="_new">MAT Analysis: GSE30119 Test Staph aureus vs HC FC2 FDR005 (Spot Chart)</a><br>
<a href="http://www.interactivefigures.com/nri/miniURL/view/Ix" target="_new">MAT Analysis: GSE30119 Test Staph aureus vs HC FC2 FDR005 (Clustered Spot Chart)</a><br>
<a href="http://www.interactivefigures.com/nri/analysis/metaCompare/2" target="_new">MetaCat Compare: 2013 Oct 04 Bact/Viral</a>
</p>
</div>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
//var table = $('#geneListTable');
//var dataTable;
var geneListName = 'Abx'
var lastIdx = null;

$().ready(function(){
    $("#tableChoice").select2();
    displayGeneListData(geneListName);
    $("#tableChoice").on("change", function(e) {
      	var geneListName = e.val;
      	console.log(geneListName);
    	displayGeneListData(geneListName);
    })
});
	
function displayGeneListData(geneListName) {
	var tableHTML = [
     '<table id="geneListTable" class="table table-striped table-hover table-bordered table-condensed table-responsive">',
     '   <thead>',
     '       <tr>',
     '           <th>ID</th>',
     '           <th>Symbol</th>',
     '           <th>Synonyms</th>',
     '           <th>Chromosome</th>',
     '       </tr>',
     '   </thead>',
     '   <tfoot>',
     '       <tr>',
     '           <th>ID</th>',
     '           <th>Symbol</th>',
     '           <th>Synonyms</th>',
     '           <th>Chromosome</th>',
     '       </tr>',
     '   </tfoot>',
     '   <tbody>',
     '  </tbody>',
     '  </table>'
    ]
	
	$('#geneListDiv').empty();
	$('#geneListDiv').html(tableHTML.join("\n"))
    var searchURL = CONTEXT_ROOT + '/resources/data/genelists/' + geneListName + '.txt';
	var table = $('#geneListTable');
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "text",
		"success": function(data) {
			var genes = [];
			var lines = data.split("\n");
			var len = lines.length - 1;
	        for (var i = 1; i < len; i++) {
	        	  var record = lines[i].split("\t");
	        	  genes.push(record);
	        }
			dataTable = table.DataTable({
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"order": [[1,"asc"]],
				"colResize": {
		            "handleWidth": 20
		        },
		        "columnDefs": [
		           {
		        	   "width": "20%",
		        	   "render": function(data,type,row) {
		        		   if (data != "") {
        	    	           return '<a href="http://www.ncbi.nlm.nih.gov/gene/' + data + '" target="_blank">' + data + '</a>';
		        		   } else {
		        			   return "";
		        		   }
        	           },
		        	   "targets": 0
		           },
		           {
		        	   "width": "20%",
		        	   "targets": 1
		           },
		           {
		        	   "width": "50%",
		        	   "targets": 2
		           },
		           {
		        	   "width": "10%",
		        	    "targets": 3
		           }
		        ],
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollX": false,
				"data": genes
			});
			
			$('#geneListTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dataTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dataTable.cells().nodes() ).removeClass( 'active' );
		            $( dataTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dataTable.cells().nodes() ).removeClass( 'active' );
		    } );
			
		},
		"error": function() {
			var msg="GeneList file is not available";
			$('#errorMsg').append(msg);	
		}	
 });
}

</script>
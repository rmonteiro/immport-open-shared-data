<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <h3>ImmPort Schema Diagrams</h3>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="col-md-12 diagram-container">
    	<h4>RESEARCH TABLES</h4>
    	<div class="diagram-wrapper">
    	    <img src="<c:url value='/resources/images/erd/ResearchTables1.png'/>" />"
    	</div>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="col-md-12 diagram-container">
    	<h4>EXPERIMENT RESULT TABLES 1</h4>
    	<div class="diagram-wrapper">
    		<img src="<c:url value='/resources/images/erd/ExperimentResultTables1.png'/>" />"
    	</div>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="col-md-12 diagram-container">
    	<h4>EXPERIMENT RESULT TABLES 2</h4>
    	<div class="diagram-wrapper">
    	    <img src="<c:url value='/resources/images/erd/ExperimentResultTables2.png'/>" />"
    	</div>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="col-md-12 diagram-container">
    	<h4>STUDY TABLES DIAGRAM 1</h4>
    	<div class="diagram-wrapper">
    	    <img src="<c:url value='/resources/images/erd/StudyTables1.png'/>" />"
    	</div>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="col-md-12 diagram-container">
    	<h4>STUDY TABLES DIAGRAM 2</h4>
    	<div class="diagram-wrapper">
    	    <img src="<c:url value='/resources/images/erd/StudyTables2.png'/>" />"
    	</div>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="col-md-12 diagram-container">
    	<h4>STUDY TABLES DIAGRAM 3</h4>
    	<div class="diagram-wrapper">
    	    <img src="<c:url value='/resources/images/erd/StudyTables3.png'/>" />"
    	</div>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="col-md-12 diagram-container">
    	<h4>STUDY TABLES DIAGRAM 4</h4>
    	<div class="diagram-wrapper">
    	    <img src="<c:url value='/resources/images/erd/StudyTables4.png'/>" />"
    	</div>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="col-md-12 diagram-container">
    	<h4>SUBJECT TABLES</h4>
    	<div class="diagram-wrapper">
    	    <img src="<c:url value='/resources/images/erd/SubjectTables1.png'/>" />"
    	</div>
    </div>
</div>


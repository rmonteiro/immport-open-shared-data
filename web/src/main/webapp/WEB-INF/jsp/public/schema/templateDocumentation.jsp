<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value='/resources/external/select2-3.5.1/select2.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/external/select2-3.5.1/select2-bootstrap.css'/>" rel="stylesheet" type="text/css" />

<script src="<c:url value="/resources/external/handlebars/handlebars-v1.3.0.js" />" type="text/javascript"></script>
<script src="<c:url value='/resources/external/select2-3.5.1/select2.js'/>" type="text/javascript"></script>

<script id="introduction-template" type="text/x-handlebars-template">
{{#each this}}
<h4>{{sectionHeader}}</h4>
{{#each paragraphs}}
<p>
{{.}}
</p>
{{/each}}

{{#if table}}
<table class="table table-bordered table-definition">
<thead>
<tr>
{{#each table.tableColumns}}
<th>{{.}}</th>
{{/each}}
</tr>
</thead>
{{#if isSection3 }} 
{{#each table.rows}}
<tr>
<td>{{[Field Name]}}</td><td>{{[Description]}}</td>
</tr>
{{/each}}
{{/if}}
{{#if isSection4 }} 
{{#each table.rows}}
<tr>
<td>{{[Template File Loading Order]}}</td>
</tr>
{{/each}}
{{/if}}

</table>
{{/if}}
{{/each}}
</script>

<script id="template-template" type="text/x-handlebars-template">
{{#if isCompound}}
{{#each templates}}
<h2 class="study-title">Template: {{name}}</h2>
<table class="table table-bordered table-definition">
<thead>
  <tr>
    <th>Template Column</th>
    <th>Required</th>
    <th>Description</th>
    <th>CV</th>
  </tr>
</thead>
<tbody>
{{#each columns}}
<tr>
  <td>{{templateColumn}}</td>
  <td>{{required}}</td>
  <td>{{description}}</td>
  <td>{{{cvTableName}}} {{{pvTableName}}}</td>
</tr>
{{/each}}
</tbody>
</table>
{{/each}}
{{else}}
<h2 class="study-title">Template: {{name}}</h2>
<dl class="dl-horizontal">
  <dt>Description</dt>
  <dd>{{description}}</dd>
</dl>
<table class="table table-bordered table-definition">
<thead>
  <tr>
    <th>Template Column</th>
    <th>Required</th>
    <th>Description</th>
    <th>CV</th>
  </tr>
</thead>
<tbody>
{{#each columns}}
<tr>
  <td>{{templateColumn}}</td>
  <td>{{required}}</td>
  <td>{{description}}</td>
  <td>{{{cvTableName}}} {{{pvTableName}}}</td>
</tr>
{{/each}}
</tbody>
</table>
{{/if}}

</script>

<script id="lkTable-template" type="text/x-handlebars-template">
<h2 class="study-title">LK Table: {{name}}</h2>
<table class="table table-bordered table-definition">
<thead>
  <tr>
    <th>Name</th>
    <th>Description</th>
    <th>Link</th>
    <th>ID</th>
  </tr>
</thead>
<tbody>
{{#each rows}}
<tr>
  <td>{{name}}</td>
  <td>{{description}}</td>
  <td>{{{link}}}</td>
  <td>{{id}}</td>
</tr>
{{/each}}
</tbody>
</table>
</script>

<div class="row">
    <div class="col-md-12">
        <h2>Template Documentation</h2>
    </div>
</div>

<div class="row" style="padding-bottom:10px;min-height:500px;">
    <div  class="col-md-12">
	    <ul class="nav nav-tabs tabs-main">
	        <li class="active homeTab" data-value="introduction"><a href="#introduction" data-toggle="tab">Introduction</a></li>
	        <li class="homeTab" data-value="templates"><a href="#templates" data-toggle="tab">Templates</a></li>
	        <li class="homeTab" data-value="lkTables"><a href="#lkTables" data-toggle="tab">Lookup Tables</a></li>
	    </ul>
	        
	    <div class="tab-content">
	        <div class="tab-pane active" id="introduction" style="min-height:500px;">
	            <div id="introductionDiv"></div>
	        </div>
           
	        <div class="tab-pane" id="templates" style="min-height:500px;">
	            Choose Template:
	            <input type="hidden" id="templateChoice" style="width:300px;">
	            <div id="templateDiv"></div>
	        </div>
	            
	        <div class="tab-pane" id="lkTables" style="min-height:500px;">
                Choose Lk Table:
	            <input type="hidden" id="lkTableChoice" style="width:300px;">
	            <div id="lkTableDiv"></div>
            </div>        
        </div>
    </div>
</div>
<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
</script>
<script src="<c:url value="/resources/js/IP/schema/templateDefinition.js" />" type="text/javascript"></script>
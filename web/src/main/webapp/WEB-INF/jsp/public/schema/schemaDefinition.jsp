<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value='/resources/external/select2-3.5.1/select2.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/external/select2-3.5.1/select2-bootstrap.css'/>" rel="stylesheet" type="text/css" />

<script src="<c:url value="/resources/external/handlebars/handlebars-v1.3.0.js" />" type="text/javascript"></script>
<script src="<c:url value='/resources/external/select2-3.5.1/select2.js'/>" type="text/javascript"></script>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var tableName = '<c:out value="${tableName}"></c:out>';
</script>

<script id="table-summary-template" type="text/x-handlebars-template">
   <h4 class="study-title">TABLE: {{ TABLE_NAME }}</h4>
   {{ TABLE_COMMENT }}
</script>

<script id="table-definition-template" type="text/x-handlebars-template">
  {{#fields}}
  <tr>
  <td>{{ COLUMN_NAME }}</td>
  <td>{{ COLUMN_TYPE }}</td>
  <td>{{ COLUMN_COMMENT }}</td>
  <td>
  {{#each FOREIGN_KEYS}}
      {{ REFERENCED_TABLE_NAME }}.{{  REFERENCED_COLUMN_NAME }}<br>
  {{/each}}
  </td>
  </tr>
  {{/fields}}
</script>

<script id="index-definition-template" type="text/x-handlebars-template">
  {{#indexes}}
  <tr>
  <td>{{ Key_name }}</td>
  <td>{{ Column_name }}</td>
  <td>{{ Index_type }}</td>
  </tr>
  {{/indexes}}
</script>

<script id="reference-definition-template" type="text/x-handlebars-template">
  {{#references}}
  <tr>
  <td>{{ TABLE_NAME }}</td>
  <td>{{ COLUMN_NAME }}</td>
  <td>{{ REFERENCED_TABLE_NAME }}</td>
  <td>{{ REFERENCED_COLUMN_NAME }}</td>
  </tr>
  {{/references}}
</script>

<style>
.row {
    margin-bottom: 5px;
}
table caption {
    font-size: 1.1em;
    margin-bottom: 10px;
}
.section-summary h2 {
    margin-bottom: 20px;
}
.section-summary h4 {
    margin-bottom: 10px;
}
.section-summary p {
    margin-bottom: 10px;
}
.table-summary {
	margin-top: 10px;
}
.table-definition {
	margin-top: 15px;
}
</style>
<div class="container">
    <div class="row section-summary">
        <div class="col-md-12">
        Choose Table: 
        <select id="tableChoice" style="width:300px;">
        <option value="adverse_event">adverse_event</option>
        <option value="arm_2_subject">arm_2_subject</option>
        <option value="arm_or_cohort">arm_or_cohort</option>
        <option value="assessment_2_file_info">assessment_2_file_info</option>
        <option value="assessment_component">assessment_component</option>
        <option value="assessment_panel">assessment_panel</option>
        <option value="biosample">biosample</option>
        <option value="biosample_2_treatment">biosample_2_treatment</option>
        <option value="biosample">biosample</option>
        <option value="contract_grant">contract_grant</option>
        <option value="contract_grant_2_personnel">contract_grant_2_personnel</option>
        <option value="contract_grant_2_study">contract_grant_2_study</option>
        <option value="control_sample">control_sample</option>
        <option value="control_sample_2_file_info">control_sample_2_file_info</option>
        <option value="elisa_result">elisa_result</option>
        <option value="elispot_result">elispot_result</option>
        <option value="experiment">experiment</option>
        <option value="experiment_2_protocol">experiment_2_protocol</option>
        <option value="expsample">expsample</option>
        <option value="expsample_2_biosample">expsample_2_biosample</option>
        <option value="expsample_2_file_info">expsample_2_file_info</option>
        <option value="expsample_2_reagent">expsample_2_reagent</option>
        <option value="expsample_2_treatment">expsample_2_treatment</option>
        <option value="expsample_mbaa_detail">expsample_mbaa_detail</option>
        <option value="expsample_public_repository">expsample_public_repository</option>
        <option value="fcs_analyzed_result">fcs_analyzed_result</option>
        <option value="fcs_analyzed_result_marker">fcs_analyzed_result_marker</option>
        <option value="fcs_header">fcs_header</option>
        <option value="fcs_header_marker">fcs_header_marker</option>
        <option value="fcs_header_marker_2_reagent">fcs_header_marker_2_reagent</option>
        <option value="file_info">file_info</option>
        <option value="hai_result">hai_result</option>
        <option value="hla_typing_result">hla_typing_result</option>
        <option value="inclusion_exclusion">inclusion_exclusion</option>
        <option value="intervention">intervention</option>
        <option value="kir_typing_result">kir_typing_result</option>
        <option value="lab_test">lab_test</option>
        <option value="lab_test_panel">lab_test_panel</option>
        <option value="lab_test_panel_2_protocol">lab_test_panel_2_protocol</option>
        <option value="lk_adverse_event_severity">lk_adverse_event_severity</option>
        <option value="lk_age_event">lk_age_event</option>
        <option value="lk_analyte">lk_analyte</option>
        <option value="lk_ancestral_population">lk_ancestral_population</option>
        <option value="lk_cell_population">lk_cell_population</option>
        <option value="lk_compound_role">lk_compound_role</option>
        <option value="lk_criterion_category">lk_criterion_category</option>
        <option value="lk_data_completeness">lk_data_completeness</option>
        <option value="lk_data_format">lk_data_format</option>
        <option value="lk_ethnicity">lk_ethnicity</option>
        <option value="lk_exp_measurement_tech">lk_exp_measurement_tech</option>
        <option value="lk_experiment_purpose">lk_experiment_purpose</option>
        <option value="lk_expsample_result_schema">lk_expsample_result_schema</option>
        <option value="lk_file_detail">lk_file_detail</option>
        <option value="lk_file_purpose">lk_file_purpose</option>
        <option value="lk_gender">lk_gender</option>
        <option value="lk_kir_gene">lk_kir_gene</option>
        <option value="lk_kir_locus">lk_kir_locus</option>
        <option value="lk_kir_present_absent">lk_kir_present_absent</option>
        <option value="lk_lab_test_name">lk_lab_test_name</option>
        <option value="lk_lab_test_panel_name">lk_lab_test_panel_name</option>
        <option value="lk_locus_name">lk_locus_name</option>
        <option value="lk_organization">lk_organization</option>
        <option value="lk_personnel_role">lk_personnel_role</option>
        <option value="lk_plate_type">lk_plate_type</option>
        <option value="lk_protocol_type">lk_protocol_type</option>
        <option value="lk_public_repository">lk_public_repository</option>
        <option value="lk_race">lk_race</option>
        <option value="lk_reagent_type">lk_reagent_type</option>
        <option value="lk_research_focus">lk_research_focus</option>
        <option value="lk_sample_type">lk_sample_type</option>
        <option value="lk_source_type">lk_source_type</option>
        <option value="lk_species">lk_species</option>
        <option value="lk_study_file_type">lk_study_file_type</option>
        <option value="lk_study_panel">lk_study_panel</option>
        <option value="lk_study_type">lk_study_type</option>
        <option value="lk_t0_event">lk_t0_event</option>
        <option value="lk_time_unit">lk_time_unit</option>
        <option value="lk_unit_of_measure">lk_unit_of_measure</option>
        <option value="lk_user_role_type">lk_user_role_type</option>
        <option value="lk_virus_strain">lk_virus_strain</option>
        <option value="lk_visibility_category">lk_visibility_category</option>
        <option value="mbaa_result">mbaa_result</option>
        <option value="neut_ab_titer_result">neut_ab_titer_result</option>
        <option value="pcr_result">pcr_result</option>
        <option value="period">period</option>
        <option value="planned_visit">planned_visit</option>
        <option value="planned_visit_2_arm">planned_visit_2_arm</option>
        <option value="program">program</option>
        <option value="protocol">protocol</option>
        <option value="protocol_deviation">protocol_deviation</option>
        <option value="reagent">reagent</option>
        <option value="reagent_set_2_reagent">reagent_set_2_reagent</option>
        <option value="reference_range">reference_range</option>
        <option value="reported_early_termination">reported_early_termination</option>
        <option value="standard_curve_2_file_info">standard_curve_2_file_info</option>
        <option value="standard_curve">standard_curve</option>
        <option value="study" selected>study</option>
        <option value="study_2_panel">study_2_panel</option>
        <option value="study_2_protocol">study_2_protocol</option>
        <option value="study_categorization">study_categorization</option>
        <option value="study_file">study_file</option>
        <option value="study_glossary">study_glossary</option>
        <option value="study_image">study_image</option>
        <option value="study_link">study_link</option>
        <option value="study_personnel">study_personnel</option>
        <option value="study_pubmed">study_pubmed</option>
        <option value="subject_2_protocol">subject_2_protocol</option>
        <option value="subject">subject</option>
        <option value="subject_measure_definition">subject_measure_definition</option>
        <option value="subject_measure_result">subject_measure_result</option>
        <option value="treatment">treatment</option>
        <option value="workspace">workspace</option>
    </select>
        </div>
    </div>
    
    <div class="row section-summary">
        <div class="col-md-12 table-summary">
        </div>
    </div>
    
    <div class="row" >
    <div class="col-md-12">
		<table class="table table-bordered table-definition">
			<thead>
				<tr>
					<th>Name</th>
					<th>Type</th>
					<th>Description</th>
					<th>Foreign Key</th>
				</tr>
			</thead>
			<tbody>
			    <!--
				<tr class="tr0">
					<td>actual_visit_accession</td>
					<td>varchar(15)</td>
					<td>
						<i>PRIMARY KEY</i>
						<div>Primary Key.</div>
					</td>
					<td>
					</td>
				</tr>
				-->
			</tbody>
		</table>
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-12">
	<h4>Indexes</h4>
	</div>
	</div>
	
	<div class="row" >
    <div class="col-md-12">
		<table class="table table-bordered index-definition">
			<thead>
				<tr>
					<th>Name</th>
					<th>Column</th>
					<th>Description</th>
				</tr>
			</thead>
			<tbody>
			    <!--
				<tr class="tr0">
					<td>PRIMARY</td>
					<td>actual_visit_accession</td>
					<td>BTREE</td>
				</tr>
				-->
			</tbody>
		</table>
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-12">
	<h4>Tables that Reference this Table</h4>
	</div>
	</div>
    <div class="row" >
    <div class="col-md-12">
		<table class="table table-bordered reference-definition">
			<thead>
				<tr>
					<th>Name</th>
					<th>Column</th>
					<th>Table Referenced</th>
					<th>Column Referenced</th>
				</tr>
			</thead>
			<tbody>
			    <!--
				<tr class="tr0">
					<td>planned_visit</td>
				</tr>
				-->
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="<c:url value="/resources/js/IP/schema/schemaDefinition.js" />" type="text/javascript"></script>
<script>
$().ready(function() {
	 $("#tableChoice").select2();
    setupTemplates();
    getData(tableName, updateDisplay);
    $("#tableChoice").on("change", function(e) {
    	var table = e.val;
    	$('.table-summary').empty();
        $('.table.table-definition tbody').empty();
        $('.table.index-definition tbody').empty();
        $('.table.reference-definition tbody').empty();
    	getData(table,updateDisplay);
    })
});
</script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value='/resources/external/select2-3.5.1/select2.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/external/select2-3.5.1/select2-bootstrap.css'/>" rel="stylesheet" type="text/css" />

<script src="<c:url value="/resources/external/handlebars/handlebars-v1.3.0.js" />" type="text/javascript"></script>
<script src="<c:url value='/resources/external/select2-3.5.1/select2.js'/>" type="text/javascript"></script>

<script src="<c:url value="/resources/js/IP/schema/schemaDefinition.js" />" type="text/javascript"></script>
<script src="<c:url value='/resources/js/IP/schema/displaySchemaTree.js'/>" type="text/javascript"></script>

<style>
#schemaModal .modal-dialog {
    width: 90%;
}

/* Used By D3JS */
.node {
    cursor: pointer;
}

.node circle {
    fill: #fff;
    stroke-width: 3px;
}

.node text {
    font-size:12px;
    font-family:sans-serif;
}

.link {
    fill: none;
    stroke: #ccc;
    stroke-width: 2px;
}

</style>

<div class="container">
  <div id="schemaDisplay"></div>
  <!-- Modal box for study detail -->
  <div id="schemaModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title" id="schemaModalHeader">
            <div class="popupHeaderToolbar">
               <button type="button" class="btn btn-sm btn-default" style="min-width:80px;" data-dismiss="modal">
                    <i class="glyphicon glyphicon-remove"></i> Close
               </button>
            </div>
          </div>
        </div>
        <div class="modal-body" id="schemaModalDetail">
          <div class="row section-summary">
            <div class="col-md-12 table-summary"></div>
          </div>

          <div class="row" >
          <div class="col-md-12">
             <table class="table table-bordered table-definition">
             <thead>
             <tr>
             <th>Name</th>
             <th>Type</th>
             <th>Description</th>
             <th>Foreign Key</th>
             </tr>
             </thead>
             <tbody>
             <!--
             <tr class="tr0">
             <td>actual_visit_accession</td>
             <td>varchar(15)</td>
             <td><i>PRIMARY KEY</i><div>Primary Key.</div></td>
             <td></td>
             </tr>
             -->
             </tbody>
             </table>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <h4>Indexes</h4>
          </div>
        </div>

        <div class="row" >
          <div class="col-md-12">
             <table class="table table-bordered index-definition">
             <thead>
             <tr>
             <th>Name</th>
             <th>Column</th>
             <th>Description</th>
             </tr>
             </thead>
             <tbody>
             <!--
             <tr class="tr0">
             <td>PRIMARY</td>
             <td>actual_visit_accession</td>
             <td>BTREE</td>
             </tr>
             -->
             </tbody>
             </table>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <h4>Tables that Reference this Table</h4>
          </div>
        </div>

        <div class="row" >
          <div class="col-md-12">
             <table class="table table-bordered reference-definition">
             <thead>
             <tr>
             <th>Name</th>
             <th>Column</th>
             <th>Table Referenced</th>
             <th>Column Referenced</th>
             </tr>
             </thead>
             <tbody>
             <!--
             <tr class="tr0">
             <td>planned_visit</td>
             </tr>
             -->
             </tbody>
             </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<script src="<c:url value="/resources/external/handlebars/handlebars-v1.3.0.js" />" type="text/javascript"></script>
<script src="<c:url value='/resources/external/select2-3.5.1/select2.js'/>" type="text/javascript"></script>

<script id="table-summary-template" type="text/x-handlebars-template">
   <h4 class="study-title">TABLE: {{ TABLE_NAME }}</h4>
   {{ TABLE_COMMENT }}
</script>

<script id="table-definition-template" type="text/x-handlebars-template">
  {{#fields}}
  <tr>
  <td>{{ COLUMN_NAME }}</td>
  <td>{{ COLUMN_TYPE }}</td>
  <td>{{ COLUMN_COMMENT }}</td>
  <td>
  {{#each FOREIGN_KEYS}}
      {{ REFERENCED_TABLE_NAME }}.{{  REFERENCED_COLUMN_NAME }}<br>
  {{/each}}
  </td>
  </tr>
  {{/fields}}
</script>

<script id="index-definition-template" type="text/x-handlebars-template">
  {{#indexes}}
  <tr>
  <td>{{ Key_name }}</td>
  <td>{{ Column_name }}</td>
  <td>{{ Index_type }}</td>
  </tr>
  {{/indexes}}
</script>

<script id="reference-definition-template" type="text/x-handlebars-template">
  {{#references}}
  <tr>
  <td>{{ TABLE_NAME }}</td>
  <td>{{ COLUMN_NAME }}</td>
  <td>{{ REFERENCED_TABLE_NAME }}</td>
  <td>{{ REFERENCED_COLUMN_NAME }}</td>
  </tr>
  {{/references}}
</script>


<script>
var onDataError = function(error) {
    if (error) {
        alert("File could not be loaded");
        throw new Error("Dataset could not be loaded");
    }
}

var clickNode = function(d) {
    if (d.table_name === "") {
        return;
    }
    getData(d.table_name,updateDisplay);
    $('#schemaModal').modal();
}

var CONTEXT_ROOT = '<%= request.getContextPath() %>';

var displayTree = function() {
    setupTemplates();
    $('#schemaDisplay').empty();
    var h = $(window).height() - 200;
    $('#schemaDisplay').height(h);
    
    var url = CONTEXT_ROOT + "/resources/data/schemaDefinition/studyTree.json";

    d3.json(url,function(error,data) {
        onDataError(error);
        displaySchemaTree(data,'#schemaDisplay');
    });
};

        
$().ready(function() {
    displayTree();
    $(window).resize(displayTree);
});
</script>

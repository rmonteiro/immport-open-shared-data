<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value='/resources/external/select2-3.5.1/select2.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/external/select2-3.5.1/select2-bootstrap.css'/>" rel="stylesheet" type="text/css" />

<script src="<c:url value='/resources/external/jquery/jquery.panzoom.min.js'/>" type="text/javascript"></script>
<script src="<c:url value='/resources/external/select2-3.5.1/select2.js'/>" type="text/javascript"></script>

<div class="container">
  <div class="row section-summary">
        <div class="col-md-6">
        Choose Diagram: 
        <select id="diagramChoice" style="width:300px;">
        <option value="AllTables">All Tables</option>
        <option value="ExperimentResultTables1">Experiment Result Tables 1</option>
        <option value="ExperimentResultTables2">Experiment Result Tables 2</option>
        <option value="ResearchTables1">Research Tables</option>
        <option value="SubjectTables1">Subject Tables</option>
        <option value="StudyTables1">Study Tables 1</option>
        <option value="StudyTables2">Study Tables 2</option>
        <option value="StudyTables3">Study Tables 3</option>
        <option value="StudyTables4">Study Tables 4</option>
        </select>
       </div>
       <div class="col-md-6">
       	<div id="zoomControls">
            <button class="zoom-in">Zoom In</button>
            <button class="zoom-out">Zoom Out</button>
            <button class="reset">Reset</button>
         </div>
       </div>
  </div>
       
  <div class="row" style="margin-top:10px;">
    <div class="col-md-12 diagram-container">
         <div id="diagram-wrapper">
             <div id="panzoom">
                 <img id="diagram_image" src="<c:url value='/resources/images/erd/'/><c:out value="${schemaDiagram}"></c:out>.svg" />
             </div>
        </div>
    </div>
  </div>
</div>

<script>
  var CONTEXT_ROOT = '<%= request.getContextPath() %>';
  var schemaDiagram = '<c:out value="${schemaDiagram}"></c:out>';
        (function() {         
          var panzoom = $("#panzoom")
          panzoom.panzoom({
            $zoomIn: $(".zoom-in"),
            $zoomOut: $(".zoom-out"),
            $reset: $(".reset"),
            minScale: 0.5,
            maxScale: 5,
            increment: 0.1,
            contain: false
          });
        })();
        
  $().ready(function() {
		 $("#diagramChoice").select2();
		 $("#diagramChoice").on("change", function(e) {
		    var diagram = e.val;
		    var url = CONTEXT_ROOT + "/resources/images/erd/" + diagram + ".svg";
		    $('#diagram_image').attr('src',url);
		    
		  });	
  });
</script>

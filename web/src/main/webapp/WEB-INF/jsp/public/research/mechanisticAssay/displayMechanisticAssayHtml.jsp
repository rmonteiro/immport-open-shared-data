<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="<c:url value="/resources/external/handlebars/handlebars-v2.0.0.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/js/IP/research/mechanisticAssay/displayMechanisticAssayHtml.js" />" type="text/javascript"></script>

<script id="assay-detail-template" type="text/x-handlebars-template">
<table class="mechanistic-assay-detail table table-bordered">
    <tbody>
        <tr>
            <td class="property-name">Title</td>
            <td class="property-value">{{ title }}</td>
        </tr>
        <tr>
            <td class="property-name">Purpose</td>
            <td class="property-value">{{ purpose }}</td>
        </tr>
        <tr>
            <td class="property-name">Measurement Technique</td>
            <td class="property-value">{{ measurementTechnique }}</td>
        </tr>
        <tr>
            <td class="property-name">Description</td>
            <td class="property-value">{{ description }}</td>
        </tr>
    </tbody>
</table>
</script>

<script id="protocol-detail-template" type="text/x-handlebars-template">
<tr data-accession="{{ protocolAccession }}">
    <td class="detailCol2">{{ protocolAccession }}</td>
    <td class="detailCol2">{{ name }}</td>
    <td class="detailCol2">{{ fileName }}</td>
    <td class="detailCol2">{{ type }}</td>
    <td class="detailCol2">{{ description }}</td>
</tr>
</script>

<script id="reagent-detail-template" type="text/x-handlebars-template">
<tr data-accession="{{ reagentAccession }}">
    <td class="detailCol2">{{ reagentAccession }}</td>
    <td class="detailCol2">{{ name }}</td>
    <td class="detailCol2">{{ manufacturer }}</td>
    <td class="detailCol2">{{ catalogNumber }}</td>
    <td class="detailCol2">{{ lotNumber }}</td>
</tr>
</script>

<script id="treatment-detail-template" type="text/x-handlebars-template">
<tr data-accession="{{ treatmentAccession }}">
    <td class="detailCol2">{{ treatmentAccession }}</td>
    <td class="detailCol2">{{ name }}</td>
    <td class="detailCol2">{{ amount }}</td>
    <td class="detailCol2">{{ duration }}</td>
    <td class="detailCol2">{{ temperature }}</td>
</tr>
</script>
       
<style>
table.table-mechanistic-assays tbody tr:hover {
    background-color: #eee;
    cursor: pointer; 
}

table.table-mechanistic-assays tr.row-detail-info {
	display: none; /* hide detail row by default */
}

table.assay-detail {
	/* padding: 10px; */
}

table.assay-detail td {
	/* padding: 5px; */
}

.panel-heading {
	padding: 15px 15px;
}

.nav-item-protocols {
	cursor: pointer;
}
.nav-item-reagents {
	cursor: pointer;
}
.nav-item-treatments {
	cursor: pointer;
}

.nav > li > a {
	padding: 6px 15px;
}
</style>

<c:if test="${empty assays}">
  <h5>No Mechanistic Assays</h5>
</c:if>

<c:if test="${not empty assays}">
<div>
  <h5 class="heading">Mechanistic Assays</h5>
</div>

<table class="detail table-mechanistic-assays"> 
<thead>
  <tr class="detail">
      <th scope="col" class="detail"></th>
      <th scope="col" class="detail">Accession</th>
      <th scope="col" class="detail">Title</th>
      <th scope="col" class="detail">Type</th>
      <th scope="col" class="detail">Measurement Technique</th>
      <th scope="col" class="detail"># of Samples</th>
  </tr>
</thead>
<tbody>
  <c:forEach items="${assays}" var="assay">
  <tr data-accession="${assay.experimentAccession}">
    <td class="detailCol2 detailIndicator"><i class="fa"></i></td>
    <td class="detailCol2">${assay.experimentAccession}</td>
    <td class="detailCol2">${assay.title}</td>
    <td class="detailCol2">${assay.purpose}</td>
    <td class="detailCol2">${assay.measurementTechnique}</td>
    <td class="detailCol2">${assay.countExpSamples}</td>
  </tr>
  </c:forEach>
</tbody>
</table>

<div class="row" style="margin-top:18px;margin-left:-5px;margin-right:-5px;">
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-eye-open" style="color:#999;padding-left:8px;padding-right:15px;"></i>
            Experiment Detail for <span class="experiment-accession-number"></span>
            <ul class="nav nav-pills nav-items pull-right" role="tablist" style="margin-top:-6px;">
                <li class="nav-item nav-item-protocols" id="protocols"><a>Protocols</a></li>
                <li class="nav-item nav-item-reagents" id="reagents"><a>Reagents</a></li>
                <li class="nav-item nav-item-treatments" id="treatments"><a>Treatments</a></li>
            </ul>
        </div>
        
        <div class="panel-body">
            
            <table class="detail table-protocols" style="display: none;">
                <caption style="text-align:left;font-weight:bold;">Protocols</caption>
                <thead>
                    <tr class="detail">
                        <th scope="col" class="detail">Protocol</th>
                        <th scope="col" class="detail">Name</th>
                        <th scope="col" class="detail">Filename</th>
                        <th scope="col" class="detail">Type</th>
                        <th scope="col" class="detail">Description</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            
            <table class="detail table-reagents" style="display: none;">
                <caption style="text-align:left;font-weight:bold;">Reagents</caption>
                <thead>
                    <tr class="detail">
                        <th scope="col" class="detail">Reagent</th>
                        <th scope="col" class="detail">Name</th>
                        <th scope="col" class="detail">Manufacturer</th>
                        <th scope="col" class="detail">Catalog Number</th>
                        <th scope="col" class="detail">Lot Number</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            
            <table class="detail table-treatments" style="display: none;">
                <caption style="text-align:left;font-weight:bold;">Treatments</caption>
                <thead>
                    <tr class="detail">
                        <th scope="col" class="detail">Treatment</th>
                        <th scope="col" class="detail">Name</th>
                        <th scope="col" class="detail">Amount</th>
                        <th scope="col" class="detail">Duration</th>
                        <th scope="col" class="detail">Temperature</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            
        </div>
        
    </div>
</div>
</div>

</c:if>
 

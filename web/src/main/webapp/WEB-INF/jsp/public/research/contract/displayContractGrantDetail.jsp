
<h3>CONTRACT-GRANT DETAILS</h3>

<table width="100%">
  <tr>
      <td class="detailHeader" valign="top" width="15%">Contract-Grant Title: </td><td class="even">${contract.title}</td>      
  </tr>
  <tr>
      <td class="detailHeader" valign="top" width="15%">Contract-Grant Number: </td><td class="even">${contract.contractNumber}</td>      
  </tr>  
  <tr>
      <td class="detailHeader" valign="top" width="15%">Project Officer: </td><td>${contract.projectOfficer }</td>      
  </tr>
  <tr>
      <td class="detailHeader" valign="top" width="15%">Keywords: </td><td class="even">${contract.keywords}</td>      
  </tr>  
  <tr>
      <td class="detailHeader" valign="top" width="15%">Abstract: </td><td class="even">${contract.summary}</td>      
  </tr>   
</table>
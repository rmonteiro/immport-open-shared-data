<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="<c:url value="/resources/external/tipsy/jquery.tipsy.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/d3/d3.v3.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/commond3/boxplot/box.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/commond3/boxplot/boxplot.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/commond3/barchart/barchart.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/commond3/legendchart/legendchart.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/js/IP/research/subject/displaySubjectHtml.js" />" type="text/javascript"></script>

<link href="<c:url value="/resources/external/tipsy/tipsy.css" />" rel="stylesheet">
<link href="<c:url value="/resources/external/commond3/barchart/barchart.css" />" rel="stylesheet">
<link href="<c:url value="/resources/external/commond3/boxplot/boxplot.css" />" rel="stylesheet">
<link href="<c:url value="/resources/external/commond3/legendchart/legendchart.css" />" rel="stylesheet">

<style>
.chart-wrapper {
    float:left;
}

.chart {
    height: 400px;
    border: solid 1px #aaa;
    padding: 10px 0 10px 0;
}

.chart-legend {
    height: 400px;
    border: solid 1px #aaa;
    padding: 5px 0 5px 0;
}

.main-title {
    font-size: 1em;
}
</style>


<div id="subjectChartContainer" style="margin-top:20px;">
    <div class="row" style="margin-bottom:15px;">
        <h5 class="col-md-8" style="color:#62676A;text-align:center;">Demographics Chart</h5>
        <div class="btn-group-container" style="float:right;margin-right:25px;">
	        <div class="btn-group" id="btnGroup" style="margin-right:15px;">
	            <button type="button" id="btnGender" class="btn btn-default btn-primary">Gender</button>
	            <button type="button" id="btnRace" class="btn btn-default">Race</button>
	            <button type="button" id="btnEthnicity" class="btn btn-default">Ethnicity</button>
	        </div>
	        <div class="btn-group" id="btnChartTypeGroup">
	            <button type="button" id="btnBarPlot" class="btn btn-default btn-primary">Barplot</button>
	            <button type="button" id="btnBoxPlot" class="btn btn-default">Boxplot</button>
	        </div>
        </div>
    </div>
    <div class="row">
        <div id="chartContainer"></div>
    </div>
</div>

<div style="margin-top:20px;color:blue;">
    <h5 class="heading">Demographics</h5>
</div>  

<div id="subjectByArmGrid" style="margin-top:20px;">
    <table id="subjectTable" class="table table-striped table-hover table-bordered table-condensed table-responsive"></table>
</div>

<div id="subjectByArmLegend" style="margin-top:20px;">
    <span id="legend">
      <c:forEach items="${armorcohorts}" var="armorcohort">    
          &nbsp;${armorcohort.armAccession} = ${armorcohort.name}<br>
      </c:forEach>
    </span>
</div>

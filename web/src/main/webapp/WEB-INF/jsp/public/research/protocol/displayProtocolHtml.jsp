<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

  <table class="detail">
  <tr><th colspan="4" class="detail">Protocols</th></tr>
    <tr class="detail">
        <th scope="col" class="detail">Type</th>
        <th scope="col" class="detail">Name</th>
        <th scope="col" class="detail">File Name</th>
        <th scope="col" class="detail">Summary</th>
    </tr>
  <c:forEach items="${protocols}" var="protocol">
    <tr>
      <td class="detailCol2">${protocol.type}</td>
      <td class="detailCol2">${protocol.name}</td>
      <td class="detailCol2"><a href="<%= request.getContextPath() %>/public/research/protocol/getProtocolFile/${protocol.protocolAccession}">${protocol.fileName}</a></td>
      <td class="detailCol2">${protocol.summary}</td>
    </tr>
  </c:forEach>
  </table>
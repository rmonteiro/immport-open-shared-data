<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/css/dataTables.colVis.css" />" >
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/css/dataTables.tableTools.css" />" >

<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/media/js/jquery.dataTables.js" />" type="text/javascript"></script>
<script src="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/dataTables.colResize.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/js/dataTables.colVis.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/js/dataTables.tableTools.js" />" type="text/javascript"></script>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <h2>ImmPort Team</h2>
        <div id="errorMsg"></div>
    </div>
</div>
<div class="row">
  <div class="col-md-3">
  <strong>Northrop Grumman Information Technology,<br>Health Solutions</strong><br>
  <a href="mailto:jeff.wiser@ngc.com">Jeff Wiser </a>(Program Manager)<br>
  </div>
  <div class="col-md-3">
  <strong>University of California, San Francisco</strong></br>
  <a href="http://www.ucsf.edu/news/2015/01/122656/ucsf-taps-atul-butte-lead-big-data-center" target="_blank">Atul Butte, M.D., Ph.D. (PI)</a>
  </div>
  <div class="col-md-3">
    <strong>Stanford University</strong><br>
    <a href='http://med.stanford.edu/profiles/mark-davis' target="_blank">Mark Davis, Ph.D. (co-PI)</a><br>
	<a href='http://med.stanford.edu/profiles/garry-nolan' target="_blank">Garry Nolan, Ph.D. (co-PI)</a><br>
  </div>
  <div class="col-md-3">
      <strong>University of Buffalo</strong><br>
    <a href='http://ontology.buffalo.edu/smith' target="_blank">Barry Smith, Ph.D. (co-PI)</a><br>
  </div>
</div>
  
<div class="row">
&nbsp;
</div>

<div class="col-md-12 row">
  <div class="col-md-12">
     <table id="currentTeamTable" class="table table-striped table-hover table-bordered table-condensed table-responsive">
        <thead>
            <tr>
                <th>Name</th>
                <th>Institution</th>
                <th>Link</th>
                <th>Status</th>
            </tr>
        </thead>
<!--         <tfoot>
            <tr>
                <th>Name</th>
                <th>Institution</th>
                <th>Link</th>
                <th>Status</th>
            </tr>
        </tfoot> -->
        <tbody>
       </tbody>
       </table>
    </div>
</div>
<div class="row">
&nbsp;
</div>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var searchURL = CONTEXT_ROOT + '/resources/data/immportTeam.txt';
var table = $('#currentTeamTable');
var dataTable;
var lastIdx = null;
var radioHTML = '<span class="teamControls">' +
  '<input name="radioTeam" id="activeTeam" value="Active" type="radio" checked=""> Active Members' +
  ' <input name="radioTeam" id="inactiveTeam" value="Inactive" type="radio"> Inactive Members' +
  ' <input name="radioTeam" id="allTeam" value="All" type="radio"> All Members' +
  '</span>'

$().ready(function(){
	
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "text",
		"success": function(data) {
			var members = [];
			var lines = data.split("\n");
			var len = lines.length - 1;
	        for (var i = 1; i < len; i++) {
	        	  var record = lines[i].split("\t");
	        	  members.push(record);
	        }
			dataTable = table.DataTable({
				"dom": 'Z<"row"<"col-sm-3"l><"col-sm-5"<"radiobar">><"col-sm-4"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"pageLength": 50,
				"order": [[0,"asc"]],
				"colResize": {
		            "handleWidth": 20
		        },
		        "columnDefs": [
		           {
		        	   "width": "30%",
		        	   "targets": 0
		           },
		           {
		        	   "width": "40%",
		        	   "targets": 1
		           },
		           {
		        	   "width": "10%",
		        	   "render": function(data,type,row) {
		        		   if (data != "") {
        	    	           return '<a href="' + data + '" target="_blank"><i class="fa fa-external-link fa-lg"><i></a>';
		        		   } else {
		        			   return "";
		        		   }
        	           },
		        	   "targets": 2
		           },
		           {
		        	   "width": "10%",
		        	    "targets": 3
		           }
		        ],
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollX": false,
				"data": members
			});
			
			$('#currentTeamTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dataTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dataTable.cells().nodes() ).removeClass( 'active' );
		            $( dataTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dataTable.cells().nodes() ).removeClass( 'active' );
		    } );
			
			$("div.radiobar").html(radioHTML);
			
			dataTable
			.column(3)
	        .search( "Active",false,false,false )
	        .draw();
			
			
			$('.teamControls').find('input').bind('click',function(event) {
				var id = $(this).attr('id');
				var searchValue ="";
				if (id == "activeTeam") {
					searchValue="Active";
				} else if (id == "inactiveTeam" ) {
					searchValue="Inactive";
				} else {
					searchValue="";
				}

				dataTable
				.column(3)
		        .search( searchValue,false,false,false )
		        .draw();
			});
			
		},
		"error": function() {
			var msg="Members file is not available";
			$('#errorMsg').append(msg);	
		}	
	});
});

</script>
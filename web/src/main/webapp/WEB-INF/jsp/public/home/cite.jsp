<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <h2>Citing ImmPort </h2>
        <p style="margin-top:15px;">
    </div>
</div>
<div class="row">
<div class="col-md-12">
<p>
When referencing ImmPort for publication, please cite
<a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=24791905" target="_blank">Bhattacharya, S., Andorf, S., Gomes, L., et al., (2014).
ImmPort: disseminating data to the public for the future of immunology. Immunologic Research, May;58(2-3), p. 234-9.
doi: 10.1007/s12026-014-8516-1. PMID: 24791905</a>
</p>
<p>When using specific ImmPort data sets for publication, please cite Open ImmPort Study Title, Study Accession.  Available from Open ImmPort web site: 
(e.g. <a href="http://www.immport.org/immport-open/public/home/studySearch?searchTerm=SDY144">http://www.immport.org/immport-open/public/home/studySearch?searchTerm=SDY144</a> )
</p>
<p>
We encourage you to collaborate with the scientific team who generated that data in future publications; at minimum, please cite
the originators of the data by citing the listed publication or contacting the data provider for the most appropriate publication
to reflect their work. When citing one of the novel or integrated open source tools, please reference the publication listed in
the tool description or the website for the integrated, open source tool. If you have any questions, please contact the ImmPort
Helpdesk or one of the individuals listed on the Contacts page.
</p>
</div>
</div>
<div class="row">
&nbsp;
</div>


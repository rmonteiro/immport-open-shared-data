<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h5>ImmPort Version 2.30 November 2015</h5>
<span class="label">Templates</span>
	<ul>
		<li>basic_study_design</li>
			<ul>
				<li>The basic_study_design template column 'actual_start_date' has been modified to accept two(2) date formats, "dd-MMM-yy" and "dd-MMM-yyyy"</li>
			</ul>
		<li>basic_study_design_optional</li>
			<ul>
				<li>The template basic_study_design_optional has been renamed to study_design_edit.</li>
			</ul>
	</ul>
<br>
<h5>ImmPort Version 2.29 September 2015</h5>
<span class="label">Templates</span>
	<ul>
		<li>expSample.ELISA and ELISA_Result</li>
			<ul>
				<li>Removed Sample Type column in expSample.ELISA and ELISA_Result template</li>
			</ul>
		<li>flowTextFiles</li>
			<ul>
				<li>Removed the experiment sample ID.</li>
			</ul>
		<li>PCR Results</li>
			<ul>
				<li>Modified PCR Results to be more general and support wider range of units.</li>
			</ul>
		<li>reagents.flow_cytometry.txt and reagents.mass_cytometry.txt</li>
			<ul>
				<li>Replaced column header column name 'Detector Name' to 'Clone Name'.</li>
			</ul>
		<li>subjectsHuman.txt</li>
			<ul>
				<li>Changed the template column 'Population(s)' to 'Ancestry Population(s)'.</li>
			</ul>
		<li>substanceMerges.txt</li>
			<ul>
				<li>Changed substanceMerges.txt template name to interventions.txt.</li>
			</ul>
		</ul>
<br>
<h5>ImmPort Version 2.28 May 2015</h5>
<span class="label">Templates</span>
	<ul>
		<li>AdverseEvents</li>
			<ul>
				<li>The 'Start Day' and 'End Day' has been changed to optional.</li>
			</ul>
		<li>Assessments</li>
			<ul>
				<li>Added assessment_panel and assessment_component.</li>
			</ul>
		<li>bioSample</li>
			<ul>
				<li>When biological sample type is 'Other', a subtype is required.</li>
			</ul>
		<li>controlSamples, standardCurves, and experimentSamples</li>
			<ul>
				<li>Combined templates for controlSamples, standardCurves, and experimentSamples</li>
				<li>At least one result is required for expirmentSamples.</li>
			</ul>
		<li>Genotyping and Gene_Expression</li>
			<ul>
				<li>Renamed reagents.Gene_Expression_Array.txt to reagents.Array.txt</li>
				<li>Removed the reagents.Genotyping_Array.txt so that there is only an array reagent.</li>
			</ul>
		<li>Lab Test</li>
			<ul>
				<li>Combined Lab test panel with lab test where lab test is a result.</li>
				<li>At least one result is required for lab test.</li>
			</ul>
		<li>PCR Results</li>
			<ul>
				<li>The template name pcr_results has been changed to pcr_results.</li>
			</ul>
		<li>SubstanceMerge</li>
			<ul>
				<li>The 'Start Day' and 'End Day' has been changed to optional.</li>
			</ul>
			
	</ul>
<br>
<h5>ImmPort Version 2.27 December 2014</h5>
<span class="label">Templates</span>
	<ul>
		<li>Virus_Neutralization_Results and HAI_Results</li>
			<ul>
				<li>Added a required column for study.</li>
			</ul>
		<li>MBAA</li>
			<ul>
				<li>Disallowing the string 'NaN' as a legitimate floating value.</li>
				<li>Allow all other forms of floating point values that are successfully converted and storable in Oracle.</li>
			</ul>
		</ul>
<br>
<h5>ImmPort Version 2.26 October 2014</h5>
<span class="label">Templates</span>
	<ul>
		<li>Experiment</li>
			<ul>
				<li>Add a required column for study.</li>
			</ul>
		<li>Flow Cytometry Derived Results</li>
			<ul>
				<li>Display a list of preferred cell population names from HIPC (http://www.immuneprofiling.org/).</li>
				<li>Display a list of preferred gating definitions from HIPC. </li>
				<li>Display enhanced set of preferred units for describing cell population statistics.
				<li>Display a list of preferred cell population names from HIPC for base population.</li>
			</ul>
		<li>experimentSamples.flowCytometry</li>
			<ul>
				<li>fcs files not converted to text upon upload.</li>
			</ul>
		<li>Lab Test Panel and Lab Test</li>
			<ul>
				<li>Created a new template for lab test panel and lab tests.</li>
				<li>Displaying a preferred set of CDISC/SnoMED CT derived names for lab test panels and lab tests.</li>
			</ul>
	</ul>
<br>
<h5>ImmPort Version 2.25 July 2014</h5>
<span class="label">Templates</span>
	<ul>
		<li>BasicStudy Design Optional</li>
			<ul>
				<li>Created a template to edit the study design.</li>
			</ul>
			<li>Protocol</li>
			<ul>
				<li>Name column is now required.</li>
			</ul>
		<li>Substance Merge</li>
			<ul>
				<li>Created a new template for reporting treatments, concomitant medications, etc for subjects.</li>
			</ul>
		<li>Adverse Event</li>
			<ul>
				<li>Created a new template for reporting adverse events.</li>
			</ul>
		<li>Subjects</li>
			<ul>
				<li>Populations column displays list of population of origin from allelefrequencies.net.</li>
			</ul>
	</ul>		
			
<br>
<h5>ImmPort Version 2.24 June 2014</h5>
<span class="label">Templates</span>
	<ul>
		<li>Templates</li>
			<ul>
				<li>All templates have schema version and content version tags.</li>
			</ul>
		<li>Basic Study Design</li>
			<ul>
				<li>Removed user_defined_id as required field in child blocks.</li>
				<li>Added a template to update study file, pubmed info, study link. </li>
			</ul>
		<li>bioSample</li>
			<ul>
				<li>Added a required column for planned visit.</li>
				<li>Updated the preferred terms for bioSample type.</li>
			</ul>
			<li>reagent</li>
			<ul>
				<li>Created a new template for each assay type supported by experimentSample.</li>
				<li>Created a new template for reagent sets.</li>
			</ul>
			<li>Flow cytometry derived data</li>
			<ul>
				<li>Edited column headers.</li>
				<li>Modified derived data</li>
			</ul>
			<li>Experiment</li>
			<ul>
				<li>Updated the preferred terms for measurement technique</li>
			</ul>
	</ul>
<br>
<h5>ImmPort Version 2.20 August 2013</h5>
<span class="label">Templates</span>
	<ul>
		<li>experiment</li>
			<ul>
				<li>Added a required column for experiment name.</li>
			</ul>
		<li>treatment</li>
			<ul>
				<li>Added a required column for treatment name.</li>
			</ul>
	</ul>
	<br>
<h5>ImmPort Version 2.19 May 2013</h5>
<span class="label">Templates</span>
	<ul>

		<li>treatment</li>
			<ul>
				<li>This is a new template to capture treatments applied in vitro to biological (optional) and experiment samples (required).  Treatments are captured as amount, duration, and temperature.</li>
			</ul>
<br>
		<li>experimentSamples</li>
			<ul>
				<li>Updated the required data elements to include treatment references.</li>
				<li>Experiment samples link to only one biological sample.</li>
			</ul>
<br>
		<li>biologicalSamples</li>
			<ul>
				<li>Updated the optional data elements to include treatment references.</li>
			</ul>
<br>
		<li>MBAA</li>
			<ul>
				<li>Updated the experiment Sample.MBAA template to capture MBAA specific attributes per HIPC (http://www.immuneprofiling.org).</li>
				<li>Added templates to capture control samples, standard curve, and MBAA results. Note the edits to the MBA_Results templates from previous version.</li>
			</ul>
	</ul>
<br>
<span class="label">XML Templates</span>
	<ul>XML templates are deprecated and no longer supported.	</ul>
<br>

<h5>ImmPort Version 2.18 February 2013</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>

		<li>basic_study_design.xls</li>
			<ul>
				<li>Added inclusion/exclusion criteria as a required attribute.</li>
				<li>Optionally, one can enter PubMed links, other links, and study files to the study.</li>
			</ul>
<br>
		<li>experimentSamples</li>
			<ul>
				<li>Updated the required data elements to remove protocol references.</li>
			</ul>
<br>
		<li>experimentSamples.Flow_Cytometry.xls</li>
			<ul>
				<li>Updated the required data elements to include compensation/control files from the same cytometer run as the sample generated fcs file.</li>
			</ul>
	</ul>
<span class="label">XML Templates</span>
	<ul>
		<li>experimentSamples.xml</li>
			<ul>
				<li>Updated the required data elements to remove protocol references.</li>
			</ul>
	</ul>
<br>

<h5>ImmPort Version 2.13 March 2012</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>

		<li>basic_study_design.xls</li>
			<ul>
				<li>A study design must be completed first to enable loading of other data.</li>
			</ul>
<br>
		<li>subjectsHuman.xls</li>
			<ul>
				<li>Updated the required data elements</li>
			</ul>
<br>

		<li>subjectsAnimal.xls</li>
			<ul>
				<li>Updated the required data elements</li>
			</ul>
<br>

		<li>bioSamples.xls</li>
			<ul>
				<li>Updated the required data elements</li>
			</ul>
<br>
		<li>experimentSamples.xls</li>
			<ul>
				<li>Added experimentSamples.HIA.xls and experimentSamples.Virus_Neutralization.xls</li>
			</ul>

<br>
		<li>Derived data results</li>
			<ul><li>There are derived data templates for HIA and Virus Neutralization assay results.</li></ul>
<br>
	</ul>
<span class="label">XML Templates</span>
	<ul>
		<li>subjects.xml</li>
			<ul>
				<li>Updated the required data elements</li>
			</ul>
			<br>
		<li>bioSamples.xml</li>
			<ul>
				<li>Updated the required data elements</li>
			</ul>

	</ul>
<h5>ImmPort Version 2.12.3 September 2011</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>

		<li>basic_study_design.xls</li>
			<ul><li>Added basic study design templates as a single file version to describe basic studies.</li>
			</ul>
	</ul>
<br>

	<ul>

		<li>CompleteStudyDesign-V1.6.1.xlt</li>
			<ul><li>Added complete study design templates as a multiple worksheet template version to describe more completely studies.</li>
			</ul>
	</ul>
<br>
		<li>ClinicalDataWorkbook-V6.3.xls</li>
			<ul><li>Added multiple worksheet template to capture clinical results.</li>
			</ul>
	</ul>
<br>


<h5>ImmPort Version 2.12 May 2011</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>

		<li>experimentSamples.xls</li>
			<ul><li>Added experimentSamples.Gene_Expression.Illumina_GA.xls, experimentSamples.Image_Histology.xls, and experimentSamples.Neutralizing_Antibody_Titer.xls templates.</li>
			</ul>
	</ul>
<br>


<h5>ImmPort Version 2.9.2 December 2010</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>

		<li>experimentSamples.xls</li>
			<ul><li>The experimentSamples.xls spreadsheet with multiple worksheets was separated into a set of spreadsheets with each spreadsheet named for an assay type.</li>
			<li>The result file(s) linked to an experimentSample record must now be included in the data upload package with the completed experiment sample form.</li>
			<li>The flow cytometry experiment sample template now supports the automatic conversion of uploaded fcs files for use in the ImmPort FLOW analysis module.  Use of the flowTextFiles.txt form is no longer needed.</li>
			</ul>
		<li>Derived data results</li>
			<ul><li>There are derived data templates for ELISA, ELISPOT, Flow Cytometry, HLA typing, and MBAA assay results.</li></ul>
	</ul>
	<br>
	<span class="label">XML Templates</span>
	<ul>
		<li>biologicalSamples.xml</li>
			<ul><li>The Study Day Collected element is now required.</li></ul>
		<li>protocols.xml</li>
			<ul><li>A protocol file must now be linked to each protocol that is described.</li></ul>
	</ul>
<br>

<h5>ImmPort Version 2.8 April 2010</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>
		<li>biologicalSamples.xls</li>
			<ul><li>Added new required column "Study Day Collected".  This is an integer value and not a date.</li></ul>
		<li>experimentSamples.xls</li>
			<ul><li>Updated listings of supported data formats.</li></ul>
		<li>reagents.xls</li>
			<ul><li>Updated the list of exons/introns interrogated terms.</li></ul>
		<li>HLA_Typing_System_Allele_Ambiguity.xls</li>
			<ul><li>Updated the comments on how to represent the set of alleles reported by an ambiguous HLA typing system.</li></ul>

	</ul>
<br>
<span class="label">XML Templates</span>
	<ul>
		<li>biologicalSamples.xml</li>
			<ul><li>Reference to a subject is now required.</li></ul>
		<li>experimentSamples.xml</li>
			<ul><li>Updated listings of supported data formats.</li></ul>
			<ul><li>Updated reference to a biological sample as a required attribute.</li></ul>
	</ul>
<br>

<h5>ImmPort Version 2.7 Jan 2010</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>
		<li>subjectHumans.xls</li>
			<ul><li>Updated column header to 'Subject Phenotype' from 'Affection Phenotype'.  'Subject Phenotype' is now a required column.</li></ul>
		<li>subjectAnimals.xls</li>
			<ul><li>Updated column header to 'Subject Phenotype' from 'Affection Phenotype'.  'Subject Phenotype' is now a required column.</li></ul>
		<li>biologicalSamples.xls</li>
			<ul><li>Reference to a subject is now required.</li></ul>
		<li>experimentSamples.xls</li>
			<ul><li>Removed listings of obsolete data formats.</li></ul>
	</ul>
<br>
<span class="label">XML Templates</span>
	<ul>
		<li>subjects.xml</li>
			<ul><li>Updated element name to 'Subject Phenotype' from 'Affection Phenotype'.  'Subject Phenotype' is now a required element.</li></ul>
		<li>biologicalSamples.xml</li>
			<ul><li>Reference to a subject is now required.</li></ul>
		<li>experimentSamples.xml</li>
			<ul><li>Removed listings of obsolete data formats.</li></ul>
	</ul>
<br>
<h5>ImmPort Version 2.5 May 2009</h5>
<span class="label">XML Templates</span>
	<ul><li>reagents.xml</li>
			<ul><li>Updated the XML schema to better support ELISA, ELISPOT, MBAA, and HLA reagents.</li></ul>
		<li>expSamples.xml</li>
			<ul><li>Updated the XML schema to better support ELISA, ELISPOT, MBAA results.</li></ul>
	</ul>
<br>
<h5>ImmPort Version 2.3.2 September 2008</h5>
<span class="label">Spreadsheet Templates</span>
	<ul><li>experimentSamples.xls</li>
			<ul><li>New worksheet added for ELISA, ELISOT, MBAA assay results.</li></ul>
			<ul><li>Earlier worksheet modifed for GE, GT, FCM, and Other assay results.</li></ul>
		<li>reagents.xls</li>
			<ul><li>New worksheets for ELISA, ELISOT, MBAA reagents.</li></ul>
		<li>ELISA_ELISPOT_MBAA_Results.xls</li>
			<ul><li>New template to capture the interpreted ELISA, ELISOT, MBAA results.</li></ul>
	</ul>
<br>
<h5>ImmPort Version 2.3 August 2008</h5>
<span class="label">DAIT Minimum Information Standards</span>
	<ul><li>The DAIT Minimum Information Standards will affect many aspects of the ImmPort data upload process.</li>
			<ul><li>Links to Protocols from Subjects, Biological Samples, Experiment Samples, and Experiments are required.</li></ul>
			<ul><li>Links to Reagents from Experiment Samples are required.</li></ul>
			<ul><li>Reagent descriptions will require inclusion of manufacturer and catalog number.</li></ul>
	</ul>
<span class="label">HLA Typing System Description 'Silver Standard'</span>
	<ul><li>The HLA consortium recommended enhancing the information captured by ImmPort to describe HL Typing Systems.</li>
	</ul>
<span class="label">Spreadsheet Templates</span>
	<ul><li>bioSamples.xls</li>
			<ul><li>Link to protocol required.</li></ul>
			<ul><li>Protocol column names consolidated and column header names edited.</li></ul>
		<li>experiments.xls</li>
			<ul><li>Experiment Name is optional.</li></ul>
			<ul><li>Link to protocol required.</li></ul>
			<ul><li>Added optional 'Responding Variables' column'.</li></ul>
		<li>experimentSamples.xls</li>
			<ul><li>Link to protocol required.</li></ul>
			<ul><li>.cel file required if Data Format field set to 'Affy_GCOS-MAS5_Gene_Expression_output'.</li></ul>
			<ul><li>.fcs file required if Data Format field set to 'FCM_report_file'.</li></ul>
		<li>reagents.xls</li>
			<ul><li>Updated HLA Typing sytem worksheet.</li></ul>
			<ul><li>Added new worksheet for ELISPOT.</li></ul>
			<ul><li>manufacturer and catalog number are required.</li></ul>
		<li>subjectHumans.xls</li>
			<ul><li>Link to protocol required.</li></ul>
			<ul><li>Added \91Language\92 column.</li></ul>
			<ul><li>Added \91Birthplace\92 column.</li></ul>
			<ul><li>Convert Ethnicity column to free text from controlled vocabulary.</li></ul>
		<li>subjectAnimals.xls</li>
			<ul><li>Link to protocol required.</li></ul>
		<li>HLA_Typing_System_Features.xls</li>
			<ul><li>New template to further describe HLA typing systems (Optional).</li></ul>
		<li>HLA_Typing_System_Allele_Ambiguity.xls</li>
			<ul><li>New template to further describe HLA typing systems (Optional).</li></ul>
	</ul>
<span class="label">XML Templates</span>
	<ul><li>bioSamples.xsd</li>
			<ul><li>protocol element is required.</li></ul>
			<ul><li>collection_date element is obsolesced.</li></ul>
			<ul><li>collector element is obsolesced.</li></ul>
			<ul><li>sample_plate_name element is deprecated.</li></ul>
		<li>experiments.xsd</li>
			<ul><li>protocol element is required.</li></ul>
			<ul><li>title element is optional.</li></ul>
			<ul><li>submitter element is deprecated.</li></ul>
		<li>protocols.xsd</li>
			<ul><li>submitter element is deprecated.</li></ul>
		<li>reagents.xsd</li>
			<ul><li>added additional elements for ELISPOT</li></ul>
			<ul><li>manufacturer and catalog number are required elements.</li></ul>
		<li>subjects.xsd</li>
			<ul><li>protocol element is required.</li></ul>
			<ul><li>submitter element is deprecated.</li></ul>
			<ul><li>Added optional element birth_place.</li></ul>
			<ul><li>Added optional element language. </li></ul>
			<ul><li>taxon_id element is optional.</li></ul>
			<ul><li>atax_scientific_name  is required.</li></ul>
	</ul>
<br>
<h5>ImmPort Version 2.2 March 2008</h5>
<span class="label">Spreadsheet Templates</span>
	<ul><li>Added comment in cell A2 of all .xls templates to indicate version.</li>
		<li>experiments.xls</li>
			<ul><li>Updated measurement technique term from "FACS " to 'FCM'.</li></ul>
		<li>experimentSamples.xls</li>
			<ul><li>Updated data format term from 'FACS_report_file' to 'FCM_report_file'.</li></ul>
	</ul>
<span class="label">XML Templates</span>
	<ul><li>simple_types.xsd (to support changes to experiments.xsd and experimentSamples.xsd)</li>
			<ul><li>&lt;dataFormatType&gt;</li>
					<ul><li>Updated enumerated term from "FACS " to 'FCM'.</li></ul>
			</ul>
			<ul><li>&lt;expMeasTechType&gt;</li>
					<ul><li>Updated enumerated term from 'FACS_report_file' to 'FCM_report_file'.</li></ul>
			</ul>
	</ul>
<br>
<h5>ImmPort Version 2.1.2 December 2007<br><br></h5>
<span class="label">XML Templates</span>
	<ul><li>Added comment to .xml templates to indicate version.</li>
	</ul><br>

<h5>ImmPort Version 2.1 August 2007<br><br></h5>
<span class="label">Spreadsheet Templates</span>
 		<ul><li>protocols.xls</li>
				<ul><li>Edited protocol types to 'Assay', 'Biomaterial_Transformation' and 'Data_Transformation'.</li></ul>
			<li>reagents.xls</li>
				<ul><li>Add worksheet to capture HLA typing reagents.</li></ul>
		</ul>

<span class="label">XML Templates</span>
 	<ul><li>simple_types.xsd (to support changes to protocols.xsd)</li>
			<ul><li>Edited protocol types to 'Assay', 'Biomaterial_Transformation' and 'Data_Transformation'.</li></ul>
		<li>reagents.xsd</li>
			<ul><li>Added elements to capture HLA typing reagents.</li></ul>
	</ul>
<br><br>


<br>
<h5>ImmPort Version 2.13 March 2012</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>

		<li>basic_study_design.xls</li>
			<ul>
				<li>A study design must be completed first to enable loading of other data.</li>
			</ul>
	</ul>
<br>

	<ul>
		<li>subjectsHuman.xls</li>
			<ul>
				<li>Updated the required data elements</li>
			</ul>
	</ul>
<br>
		<li>subjectsAnimal.xls</li>
			<ul>
				<li>Updated the required data elements</li>
			</ul>
		<li>bioSamples.xls</li>
			<ul>
				<li>Updated the required data elements</li>
			</ul>
	</ul>
<br>

<h5>ImmPort Version 2.12.3 September 2011</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>

		<li>basic_study_design.xls</li>
			<ul><li>Added basic study design templates as a single file version to describe basic studies.</li>
			</ul>
	</ul>
<br>

	<ul>

		<li>CompleteStudyDesign-V1.6.1.xlt</li>
			<ul><li>Added complete study design templates as a multiple worksheet template version to describe more completely studies.</li>
			</ul>
	</ul>
<br>
		<li>ClinicalDataWorkbook-V6.3.xls</li>
			<ul><li>Added multiple worksheet template to capture clinical results.</li>
			</ul>
	</ul>
<br>


<h5>ImmPort Version 2.12 May 2011</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>

		<li>experimentSamples.xls</li>
			<ul><li>Added experimentSamples.Gene_Expression.Illumina_GA.xls, experimentSamples.Image_Histology.xls, and experimentSamples.Neutralizing_Antibody_Titer.xls templates.</li>
			</ul>
	</ul>
<br>


<h5>ImmPort Version 2.9.2 December 2010</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>

		<li>experimentSamples.xls</li>
			<ul><li>The experimentSamples.xls spreadsheet with multiple worksheets was separated into a set of spreadsheets with each spreadsheet named for an assay type.</li>
			<li>The result file(s) linked to an experimentSample record must now be included in the data upload package with the completed experiment sample form.</li>
			<li>The flow cytometry experiment sample template now supports the automatic conversion of uploaded fcs files for use in the ImmPort FLOW analysis module.  Use of the flowTextFiles.txt form is no longer needed.</li>
			</ul>
		<li>Derived data results</li>
			<ul><li>There are derived data templates for ELISA, ELISPOT, Flow Cytometry, HLA typing, and MBAA assay results.</li></ul>
	</ul>
	<br>
	<span class="label">XML Templates</span>
	<ul>
		<li>biologicalSamples.xml</li>
			<ul><li>The Study Day Collected element is now required.</li></ul>
		<li>protocols.xml</li>
			<ul><li>A protocol file must now be linked to each protocol that is described.</li></ul>
	</ul>
<br>

<h5>ImmPort Version 2.8 April 2010</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>
		<li>biologicalSamples.xls</li>
			<ul><li>Added new required column "Study Day Collected".  This is an integer value and not a date.</li></ul>
		<li>experimentSamples.xls</li>
			<ul><li>Updated listings of supported data formats.</li></ul>
		<li>reagents.xls</li>
			<ul><li>Updated the list of exons/introns interrogated terms.</li></ul>
		<li>HLA_Typing_System_Allele_Ambiguity.xls</li>
			<ul><li>Updated the comments on how to represent the set of alleles reported by an ambiguous HLA typing system.</li></ul>

	</ul>
<br>
<span class="label">XML Templates</span>
	<ul>
		<li>biologicalSamples.xml</li>
			<ul><li>Reference to a subject is now required.</li></ul>
		<li>experimentSamples.xml</li>
			<ul><li>Updated listings of supported data formats.</li></ul>
			<ul><li>Updated reference to a biological sample as a required attribute.</li></ul>
	</ul>
<br>

<h5>ImmPort Version 2.7 Jan 2010</h5>
<span class="label">Spreadsheet Templates</span>
	<ul>
		<li>subjectHumans.xls</li>
			<ul><li>Updated column header to 'Subject Phenotype' from 'Affection Phenotype'.  'Subject Phenotype' is now a required column.</li></ul>
		<li>subjectAnimals.xls</li>
			<ul><li>Updated column header to 'Subject Phenotype' from 'Affection Phenotype'.  'Subject Phenotype' is now a required column.</li></ul>
		<li>biologicalSamples.xls</li>
			<ul><li>Reference to a subject is now required.</li></ul>
		<li>experimentSamples.xls</li>
			<ul><li>Removed listings of obsolete data formats.</li></ul>
	</ul>
<br>
<span class="label">XML Templates</span>
	<ul>
		<li>subjects.xml</li>
			<ul><li>Updated element name to 'Subject Phenotype' from 'Affection Phenotype'.  'Subject Phenotype' is now a required element.</li></ul>
		<li>biologicalSamples.xml</li>
			<ul><li>Reference to a subject is now required.</li></ul>
		<li>experimentSamples.xml</li>
			<ul><li>Removed listings of obsolete data formats.</li></ul>
	</ul>
<br>
<h5>ImmPort Version 2.5 May 2009</h5>
<span class="label">XML Templates</span>
	<ul><li>reagents.xml</li>
			<ul><li>Updated the XML schema to better support ELISA, ELISPOT, MBAA, and HLA reagents.</li></ul>
		<li>expSamples.xml</li>
			<ul><li>Updated the XML schema to better support ELISA, ELISPOT, MBAA results.</li></ul>
	</ul>
<br>
<h5>ImmPort Version 2.3.2 September 2008</h5>
<span class="label">Spreadsheet Templates</span>
	<ul><li>experimentSamples.xls</li>
			<ul><li>New worksheet added for ELISA, ELISOT, MBAA assay results.</li></ul>
			<ul><li>Earlier worksheet modifed for GE, GT, FCM, and Other assay results.</li></ul>
		<li>reagents.xls</li>
			<ul><li>New worksheets for ELISA, ELISOT, MBAA reagents.</li></ul>
		<li>ELISA_ELISPOT_MBAA_Results.xls</li>
			<ul><li>New template to capture the interpreted ELISA, ELISOT, MBAA results.</li></ul>
	</ul>
<br>
<h5>ImmPort Version 2.3 August 2008</h5>
<span class="label">DAIT Minimum Information Standards</span>
	<ul><li>The DAIT Minimum Information Standards will affect many aspects of the ImmPort data upload process.</li>
			<ul><li>Links to Protocols from Subjects, Biological Samples, Experiment Samples, and Experiments are required.</li></ul>
			<ul><li>Links to Reagents from Experiment Samples are required.</li></ul>
			<ul><li>Reagent descriptions will require inclusion of manufacturer and catalog number.</li></ul>
	</ul>
<span class="label">HLA Typing System Description 'Silver Standard'</span>
	<ul><li>The HLA consortium recommended enhancing the information captured by ImmPort to describe HL Typing Systems.</li>
	</ul>
<span class="label">Spreadsheet Templates</span>
	<ul><li>bioSamples.xls</li>
			<ul><li>Link to protocol required.</li></ul>
			<ul><li>Protocol column names consolidated and column header names edited.</li></ul>
		<li>experiments.xls</li>
			<ul><li>Experiment Name is optional.</li></ul>
			<ul><li>Link to protocol required.</li></ul>
			<ul><li>Added optional 'Responding Variables' column'.</li></ul>
		<li>experimentSamples.xls</li>
			<ul><li>Link to protocol required.</li></ul>
			<ul><li>.cel file required if Data Format field set to 'Affy_GCOS-MAS5_Gene_Expression_output'.</li></ul>
			<ul><li>.fcs file required if Data Format field set to 'FCM_report_file'.</li></ul>
		<li>reagents.xls</li>
			<ul><li>Updated HLA Typing sytem worksheet.</li></ul>
			<ul><li>Added new worksheet for ELISPOT.</li></ul>
			<ul><li>manufacturer and catalog number are required.</li></ul>
		<li>subjectHumans.xls</li>
			<ul><li>Link to protocol required.</li></ul>
			<ul><li>Added 'Language' column.</li></ul>
			<ul><li>Added 'Birthplace' column.</li></ul>
			<ul><li>Convert Ethnicity column to free text from controlled vocabulary.</li></ul>
		<li>subjectAnimals.xls</li>
			<ul><li>Link to protocol required.</li></ul>
		<li>HLA_Typing_System_Features.xls</li>
			<ul><li>New template to further describe HLA typing systems (Optional).</li></ul>
		<li>HLA_Typing_System_Allele_Ambiguity.xls</li>
			<ul><li>New template to further describe HLA typing systems (Optional).</li></ul>
	</ul>
<span class="label">XML Templates</span>
	<ul><li>bioSamples.xsd</li>
			<ul><li>protocol element is required.</li></ul>
			<ul><li>collection_date element is obsolesced.</li></ul>
			<ul><li>collector element is obsolesced.</li></ul>
			<ul><li>sample_plate_name element is deprecated.</li></ul>
		<li>experiments.xsd</li>
			<ul><li>protocol element is required.</li></ul>
			<ul><li>title element is optional.</li></ul>
			<ul><li>submitter element is deprecated.</li></ul>
		<li>protocols.xsd</li>
			<ul><li>submitter element is deprecated.</li></ul>
		<li>reagents.xsd</li>
			<ul><li>added additional elements for ELISPOT</li></ul>
			<ul><li>manufacturer and catalog number are required elements.</li></ul>
		<li>subjects.xsd</li>
			<ul><li>protocol element is required.</li></ul>
			<ul><li>submitter element is deprecated.</li></ul>
			<ul><li>Added optional element birth_place.</li></ul>
			<ul><li>Added optional element language. </li></ul>
			<ul><li>taxon_id element is optional.</li></ul>
			<ul><li>atax_scientific_name  is required.</li></ul>
	</ul>
<br>
<h5>ImmPort Version 2.2 March 2008</h5>
<span class="label">Spreadsheet Templates</span>
	<ul><li>Added comment in cell A2 of all .xls templates to indicate version.</li>
		<li>experiments.xls</li>
			<ul><li>Updated measurement technique term from "FACS " to 'FCM'.</li></ul>
		<li>experimentSamples.xls</li>
			<ul><li>Updated data format term from 'FACS_report_file' to 'FCM_report_file'.</li></ul>
	</ul>
<span class="label">XML Templates</span>
	<ul><li>simple_types.xsd (to support changes to experiments.xsd and experimentSamples.xsd)</li>
			<ul><li>&lt;dataFormatType&gt;</li>
					<ul><li>Updated enumerated term from "FACS " to 'FCM'.</li></ul>
			</ul>
			<ul><li>&lt;expMeasTechType&gt;</li>
					<ul><li>Updated enumerated term from 'FACS_report_file' to 'FCM_report_file'.</li></ul>
			</ul>
	</ul>
<br>
<h5>ImmPort Version 2.1.2 December 2007<br><br></h5>
<span class="label">XML Templates</span>
	<ul><li>Added comment to .xml templates to indicate version.</li>
	</ul><br>

<h5>ImmPort Version 2.1 August 2007<br><br></h5>
<span class="label">Spreadsheet Templates</span>
 		<ul><li>protocols.xls</li>
				<ul><li>Edited protocol types to 'Assay', 'Biomaterial_Transformation' and 'Data_Transformation'.</li></ul>
			<li>reagents.xls</li>
				<ul><li>Add worksheet to capture HLA typing reagents.</li></ul>
		</ul>

<span class="label">XML Templates</span>
 	<ul><li>simple_types.xsd (to support changes to protocols.xsd)</li>
			<ul><li>Edited protocol types to 'Assay', 'Biomaterial_Transformation' and 'Data_Transformation'.</li></ul>
		<li>reagents.xsd</li>
			<ul><li>Added elements to capture HLA typing reagents.</li></ul>
	</ul>
<br><br>



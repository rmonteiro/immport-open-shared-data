<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/css/dataTables.colVis.css" />" >
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/css/dataTables.tableTools.css" />" >

<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/media/js/jquery.dataTables.js" />" type="text/javascript"></script>
<script src="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/dataTables.colResize.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/js/dataTables.colVis.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/js/dataTables.tableTools.js" />" type="text/javascript"></script>

<div class="row" style="margin-top:20px;">
        <h2>Data Providers</h2>
</div>
<div class="row">
    <div class="col-md-12">
     <table id="dataProvidersTable" class="table table-striped table-hover table-bordered table-condensed table-responsive">
        <thead>
            <tr>
                <th>Program</th>
                <th>Funding Source</th>
                <th>Assay Results</th>
                <th>Date</th>
                <th>Studies</th>
                <th>Link</th>
                <th>Clinical Trials</th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
            	<th>Program</th>
                <th>Funding Source</th>
                <th>Assay Results</th>
                <th>Date</th>
                <th>Studies</th>
                <th>Link</th>
                <th>Clinical Trials</th>
            </tr>
        </tfoot>
 
        <tbody>
       </tbody>
       </table>
    </div>
</div>
<div class="row">
&nbsp;
</div>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var searchURL = CONTEXT_ROOT + '/resources/data/dataProviders.txt';
var table = $('#dataProvidersTable');
var lastIdx = null;

$().ready(function(){
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "text",
		"success": function(data) {
			var news = [];
			var lines = data.split("\n");
			var len = lines.length - 1;
	        for (var i = 1; i < len; i++) {
	        	  var record = lines[i].split("\t");
	        	  news.push(record);
	        }
			var dTable = table.DataTable({
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"order": [[0,"asc"]],
				"colResize": {
		            "handleWidth": 20
		        },
		        "columnDefs": [
		           {
		        	   "width": "45%",
		        	   "targets": 0
		           },
		           {
		        	   "width": "10%",
		        	   "targets": 1
		           },
		           {
		        	   "width": "20%",
		        	   "targets": 2
		           },
		           {
		        	   "width": "10%",
		        	   "targets": 3
		           },
		           {
		        	   "width": "15%",
		        	   "targets": 4,
		        	   "render": function(data,type,row) {
		        		   var studies = data.split(",");
		        		   var len = studies.length;
		        		   var hrefs = [];
		        		   for (var i = 0; i < len; i++) {
		        			   var study = studies[i].trim();
		        			   var url = '<a href="' + CONTEXT_ROOT + '/public/study/study/displayStudyDetail/'
		        			             + study + '">' + study + '</a>';
		        			   hrefs.push(url)
		        		   }
		        		   return hrefs.join(" ");
		               }
		           },
		           {
		        	   "width": "5%",
		        	   "targets": 5,
		        	   "render": function(data,type,row) {
		        		   var links = data.split(",");
		        		   var len = links.length;
		        		   var hrefs = [];
		        		   for (var i = 0; i < len; i++) {
		        			   var link = links[i].trim();
		        			   var url = '<a href="http://' + link + '" target="_blank"><i class="fa fa-external-link"></i></a>';
		        			   hrefs.push(url)
		        		   }
		        		   return hrefs.join(" ");
		               }
		           },
		           {
		        	   "width": "5%",
		        	   "targets": 6,
		        	   "render": function(data,type,row) {
		        		   var links = data.split(",");
		        		   var len = links.length;
		        		   var hrefs = [];
		        		   for (var i = 0; i < len; i++) {
		        			   var link = links[i].trim();
		        			   if (link != "") {
		        			       var url = '<a href="http://clinicaltrials.gov/show/' + link + '" target="_blank"><i class="fa fa-external-link"></i></a>';
		        			       hrefs.push(url)
		        		       }
		        		   }
		        		   return hrefs.join(" ");
		               }
		           }
		           
		        ],
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollX": false,
				"data": news
			});
			$('#dataProvidersTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			var msg="Data providers file is not available";
			$('#errorMsg').append(msg);	
		}	
	});
});

</script>

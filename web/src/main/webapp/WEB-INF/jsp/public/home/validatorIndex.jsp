<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
    <div class="col-md-12">
        <h2>Data Summary</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
		<table class="dataSummary">
		   <tr>
		     <td class="detailCol1w">Studies</td>
		     <td class="detailCol2w">143</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">Subjects</td>
		    <td class="detailCol2w" >22434</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">Experiments</td>
		    <td class="detailCol2w" >799</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">Total Results</td>
		    <td class="detailCol2w" >648525</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;ELISA Results</td>
		    <td class="detailCol2w" >179847</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;ELISPOT Results</td>
		    <td class="detailCol2w" >25354</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;Flow Cytometry Results</td>
		    <td class="detailCol2w" >74715</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;Gene Expression Results</td>
		    <td class="detailCol2w" >29282</td>
		  </tr>
		      <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;HAI Results</td>
		    <td class="detailCol2w" >3986</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;HLA Typing Results</td>
		    <td class="detailCol2w" >2497</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;Multiplex Results</td>
		    <td class="detailCol2w" >325279</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;Neutralizing Antibody Results</td>
		    <td class="detailCol2w" >7565</td>
		  </tr>
		</table>
		<div style="margin-top:15px;margin-bottom:25px;">
			Updated September 2015
		</div>
    </div>
</div>


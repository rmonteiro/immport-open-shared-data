<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/css/dataTables.colVis.css" />" >
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/css/dataTables.tableTools.css" />" >

<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/media/js/jquery.dataTables.js" />" type="text/javascript"></script>
<script src="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/dataTables.colResize.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/js/dataTables.colVis.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/js/dataTables.tableTools.js" />" type="text/javascript"></script>

<style>
.dl-horizontal dt {
    width: 200px;
	margin-top: 20px;
}
.dl-horizontal dd {
    margin-left: 220px;
	margin-top: 20px;
}
</style>

<div class="row">
    <div class="col-md-12">
        <h2>Shared Study Download</h2>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
	<ul>
    <li>Study data is available in 2 formats for registered users: MySQL and Tab Separated (TVS) format.  (<a href="https://immport.niaid.nih.gov/immportWeb/logon/displayRegistrationAgreement.do">Registration</a> is free.)</li>
    <li>MySQL format can be used to upload a schema into a MySQL database. Installation instructions are included in the download package as a README file.</li>
    <li>The Tab format can be used for viewing in Excel, reading into R, etc.</li>
    <li>Study data may be downloaded for each individual study, or the entire set of data for all studies may be downloaded as one file. 
    <li>The link to download an individual study file is available on each study detail page</li>
    <li>An <a href="<c:out value="${asperaDataBrowserUrl}"></c:out>ALLSTUDIES" target="_data_browser">All Studies MySQL package</a> is available.</li>
    <li>An <a href="<c:out value="${asperaDataBrowserUrl}"></c:out>ALLSTUDIES" target="_data_browser">All Studies TSV package</a> is available.</li>
    <li>Data Release Notes are available <a href="<c:url value="/public/home/dataReleaseNotes"/>">here.</a></li>
    <li>When utilizing shared data in ImmPort for publication, please cite the resource by referencing that the data was obtained from ImmPort, available at <a href="https://immport.niaid.nih.gov/immportWeb/display.do?content=CitingImmPort">https://immport.niaid.nih.gov</a>. We encourage you to collaborate with the scientific team who generated that data in future publications; at minimum, <br>please cite the originators of the data by citing the listed publication or contacting the data provider for the most appropriate publication to reflect their work.</li>
    </ul>
    <p>
		<strong>Schema Documentation</strong><br>
		The table and column documentation for the Public ImmPort Schema are available
		<a href="<c:url value="/public/schema/schemaDefinition/study"/>">here.</a>
	</p>
	<p>
		Entity-Relationship diagrams are available
		<a href="<c:url value="/public/schema/schemaDiagram/AllTables"/>">here.</a>
	</p>
	</div>
</div>

<div class="row">
    <div class="col-md-12">
     <table id="sharedStudyTable" class="table table-striped table-hover table-bordered table-condensed table-responsive">
        <thead>
            <tr>
                <th>Study</th>
                <th>Title</th>
                <th>Description</th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
            	<th>Study</th>
                <th>Title</th>
                <th>Description</th>
            </tr>
        </tfoot>
 
        <tbody>
       </tbody>
       </table>
    </div>
</div>
<div class="row">
&nbsp;
</div>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var searchURL = CONTEXT_ROOT + '/resources/data/studies_treemap.json';
var table = $('#sharedStudyTable');
var lastIdx = null;
var asperaDataBrowserUrl = '<c:out value="${asperaDataBrowserUrl}"></c:out>'

$().ready(function(){
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "json",
		"success": function(data) {
			var studies = [];
			for (var i = 0; i < data.length; i++) {
				studies[i] = [ data[i]['studyAccession'],data[i]['briefTitle'],data[i]['briefDescription']]
			}
			var dTable = table.DataTable({
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"order": [[0,"asc"]],
				"colResize": {
		            "handleWidth": 20
		        },
		        "columnDefs": [
		           {
		        	   "width": "10%",
		        	   "targets": 0,
		        	   "render": function(study,type,row) {
	        			   var url = '<a href="' + asperaDataBrowserUrl
	        			             + study + '" target="_data_browser">' + study + '</a>';
		        		   return url;
		               }
		           },
		           {
		        	   "width": "40%",
		        	   "targets": 1
		           },
		           {
		        	   "width": "50%",
		        	   "targets": 2
		           }
		           
		        ],
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollX": false,
				"data": studies 
			});
			$('#sharedStudyTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			var msg="Study file is not available";
			$('#errorMsg').append(msg);	
		}	
	});
});

</script>
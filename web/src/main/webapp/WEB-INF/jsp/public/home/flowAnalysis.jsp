<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript">
$().ready(function() {
	// update header menu
	$('#navbar-target-menu-collapse > ul.navbar-left > li').removeClass('active');
	$('#navbar-target-menu-collapse > ul.navbar-left > li.menu-flow-analysis').addClass('active');
})
</script>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <h2>Flow Analysis ImmPort </h2>
    </div>
</div>

<div class="row">
  <div class="col-md-12">
  <img src="<c:url value='/resources/images/flowAnalysis/flowWorkFlow.png'/>" width="735" height="108px" title="Flow Workflow"/>
  </div>
</div>

<div class="row">
  <div class="col-md-4">
  Two Dimensional Visualization<br>
  <img src="<c:url value='/resources/images/flowAnalysis/2Dflow.png'/>" width="365" height="253px" title="Flow Workflow"/>
  </div>
   <div class="col-md-4">
   Three Dimensional Visualization<br>
  <img src="<c:url value='/resources/images/flowAnalysis/3Dflow.png'/>" width="334" height="253px" title="Flow Workflow"/>
  </div>
  <div class="col-md-4">
  &nbsp;
  </div>
</div>
<div class="row">&nbsp;</div>

<div class="row">
<div class="col-md-12">
<p>The links below will direct you to the ImmPort Flow Cytometry Analysis tools which require a
login to keep your data private.
</p>
<p>ImmPort's flow cytometry analysis component includes:</p>
<ul>
<li>
Data Management for Single File Upload, Multiple File Upload and Dataset Generation
<a href="https://immportgalaxy.org" target="_blank">
<span class="fa fa-arrow-circle-right"></span></a>
</li>
<li>
Automated population identification using the FLOCK algorithm for individual sample or dataset
<a href="https://immportgalaxy.org" target="_blank">
<span class="fa fa-arrow-circle-right"></span></a>
</li>
<li>
Automated mapping populations across sample for Cross-Sample Comparison
<a href="https://immportgalaxy.org" target="_blank">
<span class="fa fa-arrow-circle-right"></span></a>
</li>
<li>
Result visualization and statistical analysis of population characteristics
<a href="https://immportgalaxy.org" target="_blank">
<span class="fa fa-arrow-circle-right"></span></a>
</li>
</ul> 
</div>
</div>

<div class="row">
<h4>FLOCK Algorithm Overview</h4>
</div>
<div class="row">
<div class="col-md-8">
The FLOCK algorithm consists of four core steps: hyper-grid creation, identifying dense hyper-regions, merging-neighboring
dense hyper-regions, clustering based on centroids derived from the merged dense hyper-regions.
<ul>
<li>
A) A gridding example. Each dimension corresponds to one measured characteristics(e.g. fluorescence at at given
wavelength).Each dot is one event(e.g. data about a cell) with characteristic values (e.g. fluorescence intensity)
as its coordinates. A number of equal-sized partitions (in this example the number is ten) are created on each dimension
to generate a hyper-grid.
</li>
<li>
B) Dense data hyper-regions. Density of a data hyper-region is calculated based on the number of events inside the region.
A density threshold is determined by a data-driven approach described in the Methods section. Hyper-regions having larger
number of events than the density threshold is set at two and the resulting dense hyper-regions are marked in red.
</li>
<li>
C) Merging dense hyper-regions. Dense hyper-regions are merged if they neighbor each other. The number of merged hyper-region
groups (in this example the number is two, highlighted in red and yellow) is used as the number of final clusters, and the
centroid of the events in a given hyper-region group calculated.
</li>
<li>
D) Final clustering. Dots (events) are assigned to the closest centroid of the merged dense region group based on Euclidean distance
to form the two populations.
</li>
</ul>
</div>
<div class="col-md-4">
 <img src="<c:url value='/resources/images/flowAnalysis/flockClustering.png'/>" width="334" height="253px" title="Flow Workflow"/>
</div>
</div>
<div class="row">
&nbsp;
</div>
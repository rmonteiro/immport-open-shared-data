<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/css/dataTables.colVis.css" />" >
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/css/dataTables.tableTools.css" />" >

<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/media/js/jquery.dataTables.js" />" type="text/javascript"></script>
<script src="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/dataTables.colResize.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/js/dataTables.colVis.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/js/dataTables.tableTools.js" />" type="text/javascript"></script>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <h2>Software Release Notes</h2>
        <div id="errorMsg"></div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
    <table id="softwareReleaseTable" class="table table-striped table-hover table-bordered table-condensed table-responsive">
        <thead>
            <tr>
                <th>Date</th>
                <th>Release</th>
                <th>Description</th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
                <th>Date</th>
                <th>Release</th>
                <th>Description</th>
            </tr>
        </tfoot>
 
        <tbody>
       </tbody>
       </table>
    </div>
</div>
<div class="row">
&nbsp;
</div>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var searchURL = CONTEXT_ROOT + '/resources/data/softwareRelease.json';
var table = $('#softwareReleaseTable');
var lastIdx = null;

$().ready(function(){
  var dTable = table.DataTable({
	  "ajax": searchURL,
	  "dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
	  "order": [[1,"desc"]],
	  "columns": [
	     { 
	       "data": "ReleaseDate",
	       "width": "15%"
	     },
	     { 
	       "data": "Release",
	       "width": "10%"
	     },
         {
	       "data": "Description",
	       "width": "75%"
	     }
	  ]
  })
});

</script>

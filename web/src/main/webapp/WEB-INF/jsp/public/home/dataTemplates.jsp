<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <h2>Data Templates</h2>
    </div>
</div>
<div class="row">
<div class="col-md-12" >

<OL>
	<li>Which Data Upload Templates do you need?<br>
		Please contact us by email at <a class="buttonlink" href="mailto: BISC_Helpdesk@niaid.nih.gov">BISC_Helpdesk@niaid.nih.gov</a>.<br>
		The <a class="buttonlink" href="<c:out value="${dataUploadUserGuideUrl}"></c:out>" target="_new" title="User Guide">User Guide</a> is a reference you can use to determine which templates need to be completed.
	</li>
	<li>Complete the templates that are needed. <br> Note: Please save spreadsheet .xls templates as tab delimited .txt files.</li>
	<li>Create a .zip file that contains the files you want to submit (e.g. results,
		protocols, bioSamples template, experimentSamples template, etc.).</li>
	<li>Please check that you are using the <a href="<c:out value="${dataUploadTemplateHistoryUrl}"></c:out>">latest version</a> of the ImmPort data transfer templates.</li>
	<li>ImmPort Upload Templates Description is available as a <a href="<c:out value="${dataUploadTemplateDescriptionPdfUrl}"></c:out>" target="_new">pdf file</a> or from the open ImmPort <a href="http://immport.org/immport-open/public/schema/templateDocumentation">website</a>.</li>
	<li><a href="<c:out value="${dataUploadTemplatesZipUrl}"></c:out>" target="_new">ImmPort Templates zip file</a></li>


</OL>
</div>
<br></br>
<div class="col-md-12" >
<br>
<table class="table table-hover table-bordered">
	 <thead>
	  	<tr>
		<th >Data To Upload to ImmPort</th>
		<th>Purpose</th>
		<th >Spreadsheet Template</th>
		<th >Required Data to Complete Metadata Form</th>
		<th >Latest Version/<br>Date Available </th>
		</tr>
	</thead>

 	<tbody>
	<tr>
			<td rowspan="2">Basic Study</td>
			<td >Describes a study in terms of title, goals, endpoints, criteria for study participation, subject grouping (arms or cohorts), personnel, planned visits or encounters and protocols using a single worksheet.<br> A study design and protocol should be uploaded first.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/basic_study_design.xls" target="_new">basic_study_design.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/basic_study_design.txt" target="_new">basic_study_design.txt</a></td>
			<td >
				<ul>
					<li>Study User Defined ID</li>
					<li>Title</li>
					<li>Description</li>
					<li>Endpoints</li>
					<li>Actual Start Date</li>
					<li>Inclusion and Exclusion Criteria</li>
					<li>Arms or Cohorts</li>
					<li>Personnel</li>
					<li>Planned Visit</li>
					<li>Protocol</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<td >Lists the protocols associated with the study. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>

	</tr>
	<tr>
			<td >Edit Study Design</td>
			<td >Update a study design, add files, publications, or subjects.<br> A study design should be uploaded first.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/study_design_edit.xls" target="_new">study_design_edit.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/study_design_edit.txt" target="_new">study_design_edit.txt</a></td>
			<td >
				<ul>
					<li>Study ID</li>
					<li>Study File Name</li>
					<li>Pubmed ID</li>

				</ul>
			</td>

			<td >December 2016</td>
	</tr>
	<!--
	<tr>
			<td >Complete Study</td>
			<td >The multiple worksheet workbook that enables a detailed description of a study (especially useful for clinical studies).</td>
			<td ><a class="buttonlink" href="http://172.16.1.162/downloads/templates/CompleteStudyDesign-V1.6.1.xls" target="_new">CompleteStudyDesign-V1.6.1.xls</a></td>
			<td >
				<ul>
				<li>Study User Defined ID</li>
				<li>Title, Description, Endpoints, Inclusion and Exclusion Criteria, Arms or Cohorts, Personnel, Period, Planned Visit, Protocol</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
 -->
<!--
		<tr>
			<td >Protocols</td>
			<td >Lists the protocols associated with the study, subjects, samples, and experiments being submitted. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
-->
	<tr>
		<td  rowspan="2">Subjects</td>
		<td >Subjects may be patients or animals from which samples are taken for analysis. Two .xls templates (one for human and one for animal subjects) are available for recording subject information. There is one .xml file that can be used for human and animal subjects. In these files, treatment protocols used on the subjects can also be listed as well as many other details.</td>
		<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/subjectsHuman.xls" target="_new">subjectsHuman.xls</a><br>
			<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/subjectsAnimal.xls" target="_new">subjectsAnimal.xls</a><br>
			<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/subjectsHuman.txt" target="_new">subjectsHuman.txt</a><br>
			<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/subjectsAnimal.txt" target="_new">subjectsAnimal.txt</a>
		</td>
		<td >Human
			<ul>
		        <li>User Defined ID </li>
		        <li>Protocol</li>
		        <li>Gender</li>
		        <li>Age</li>
		        <li>Age Unit</li>
		        <li>Age Event</li>
		        <li>Ethnicity</li>
		        <li>Race</li>
		        <li>Phenotype</li>
		        <li>Study Arm or Cohort</li>
		        <li>Population</li>
			</ul>

	    				Animal
			<ul>
		        <li>User Defined ID </li>
		        <li>Species Name</li>
		        <li>Strain Name</li>
		        <li>Protocol</li>
		        <li>Gender</li>
		        <li>Age</li>
		        <li>Age Unit</li>
		        <li>Age Event</li>
		        <li>Phenotype</li>
		        <li>Study Arm or Cohort</li>
			</ul>
		</td>

		<td >December 2016</td>
	</tr>
	<tr>
			<td >Lists the protocols associated with the study subjects. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<td  rowspan="2">Clinical Lab Test</td>
			<td >Captures the Lab Test User-Defined ID, Lab Panel ID, Biological Sample ID, Lab Test Name, Result Value and Result Unit for lab tests.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/labTests.xls" target="_new">labTests.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/labTests.txt" target="_new">labTests.txt</a></td>
			<td >
				<ul>
					<li>Lab Test User Defined ID</li>
					<li>Lab Panel ID</li>
					<li>Biological Sample ID</li>
					<li>Lab Test Name</li>
					<li>Result Value</li>
					<li>Result Unit</li>
					<li>Lab Test</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			
			<td >Lists the protocols associated with the lab tests. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<td  rowspan="1">Assessment</td>
			<td >Captures the Assessment Name, Type, Is Standard, Status, CRF File Names, and Assessment User-Defined ID for assessments.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/assessments.xls" target="_new">assessments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/assessments.txt" target="_new">assessments.txt</a></td>
			<td >
				<ul>
				<li>Assessment Name</li>
				<li>Type</li>
				<li>Is Standard</li>
				<li>Status</li>
				<li>CRF File Names</li>
				<li>Assessment User-Defined ID</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	
		<tr>
			<td  rowspan="1">Intervention</td>
			<td >Captures the study indicated interventions, concomitant medications, and substance use for subjects in a study.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/interventions.xls" target="_new">interventions.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/interventions.txt" target="_new">interventions.txt</a></td>
			<td >
				<ul>
					<li>Substance ID</li>
					<li>Study ID</li>
					<li>Subject ID</li>
					<li>Compund Role</li>
					<li>Dose</li>
					<li>Start Day</li>
					<li>End Day</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<td >Adverse Events</td>
			<td >Captures the adverse events reported during a study.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/adverseEvents.xls" target="_new">adverseEvents.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/adverseEvents.txt" target="_new">adverseEvents.txt</a></td>
			<td >
				<ul>
					<li>Adverse Event User-Defined ID</li>
					<li>Subject ID</li>
					<li>Study ID</li>
					<li>Name Reported</li>
					<li>Severity</li>
					<li>Outcome</li>
					<li>Start Day</li>
					<li>End Day</li>
					<li>Relation to Study Treatment</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<td  rowspan="6">CYTOF Assays</td>
			<td >Describes the samples, reagents, and results from a CYTOF experiment.<br>May be used to describe experiments and biological samples.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.CYTOF.xls" target="_new">experimentSamples.<br>CYTOF.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.CYTOF.txt" target="_new">experimentSamples.<br>CYTOF.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>Result file</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >CyTOF protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Mass cytrometry Reagents</td>-->
			<td >Captures detailed information about the reagents used in a CyTOF (Mass cytometry) experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Mass_Cytometry.xls" target="_new">reagents.Mass_Cytometry.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Mass_Cytometry.txt" target="_new">reagents.Mass_Cytometry.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				<li>Analyte Name</li>
				<li>Detector Name</li>
				<li>Reporter Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Reagent Sets</td>-->
			<td >Groups of reagents used together in an assay may be defined as a reagent set to simply referring to reagents.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/Reagent_Sets.xls" target="_new">reagent_sets.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/Reagent_Sets.txt" target="_new">reagent_sets.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Reagent Set Components ID</li>
				<li>Description</li>
				<li>Name</li>
				<li>Type Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
		<!--<td >Mass Cytometry Derived and Interpreted Results </td>-->
		<td >Describe CyTOF (Mass Cytometry) results in a format to facilitate sharing of results.</td>
		<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/FCM_Derived_data.xls" target="_new">FCM_Derived_data.xls</a><br>
								<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/FCM_Derived_data.txt" target="_new">FCM_Derived_data.txt</a></td>
		<td >
			<ul>
			<li>Experiment Sample ID</li>
			<li>Population Name</li>
			<li>Population Definition</li>
			<li>Population Cell Number</li>
			<li>Population Cell Number Unit</li>
			<li>Workspace File</li>
			</ul>
		</td>
		<td >December 2016</td>
	<tr>
			<td  rowspan="4">ELISA Assays</td>
			<td >Captures the samples, reagents, and results from an ELISA experiment. <br>May be used to describe experiments, biological samples, and report results.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.ELISA.xls" target="_new">experimentSamples.<br>ELISA.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.ELISA.txt" target="_new">experimentSamples.<br>ELISA.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>ImmPort Template?</li>
					<li>Result file</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >ELISA protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >ELISA Reagents</td>-->
			<td >Captures detailed information about the reagents used in an ELISA experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.ELISA.xls" target="_new">reagents.ELISA.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.ELISA.txt" target="_new">reagents.ELISA.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				<li>Analyte Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<td rowspan="4">ELISPOT Assays </td>
			<td >Captures the samples, reagents, and results from an ELISPOT experiment. <br>May be used to describe experiments, biological samples, and report results.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.ELISPOT.xls" target="_new">experimentSamples.<br>ELISPOT.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.ELISPOT.txt" target="_new">experimentSamples.<br>ELISPOT.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>Result file</li>
					<li>ImmPort Template?</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >ELISPOT protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!-- <td >ELISPOT Reagents</td>-->
			<td >Captures detailed information about the reagents used in an ELISPOT experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.ELISPOT.xls" target="_new">reagents.ELISPOT.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.ELISPOT.txt" target="_new">reagents.ELISPOT.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				<li>Analyte Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<td  rowspan="6">Flow Cytometry Assays </td>
			<td >Captures the samples, reagents, and results from a Flow Cytometry experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Flow_Cytometry.xls" target="_new">experimentSamples.<br>Flow_Cytometry.xls</a><br>
				<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Flow_Cytometry.txt" target="_new">experimentSamples.<br>Flow_Cytometry.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>FCS Result file</li>
					<li>FCS Compensation/Control file(s)</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Flow protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Flow Cytometry Reagents</td>-->
			<td >Captures detailed information about the reagents used in an Flow Cytometry experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Flow_Cytometry.xls" target="_new">reagents.Flow_Cytometry.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Flow_Cytometry.txt" target="_new">reagents.Flow_Cytometry.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				<li>Analyte Name</li>
				<li>Detector Name</li>
				<li>Reporter Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Reagent Sets</td>-->
			<td >Groups of reagents used together in an assay may be defined as a reagent set to simply referring to reagents.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/Reagent_Sets.xls" target="_new">reagent_sets.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/Reagent_Sets.txt" target="_new">reagent_sets.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Reagent Set Components ID</li>
				<li>Description</li>
				<li>Name</li>
				<li>Type Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
		<!--<td >Flow Cytometry Derived and Interpreted Results </td>-->
		<td >A form to describe Flow Cytometry results  in a format to facilitate sharing of results.</td>
		<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/FCM_Derived_data.xls" target="_new">FCM_Derived_data.xls</a><br>
								<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/FCM_Derived_data.txt" target="_new">FCM_Derived_data.txt</a></td>
		<td >
			<ul>
			<li>Experiment Sample ID</li>
			<li>Population Name</li>
			<li>Population Definition</li>
			<li>Population Cell Number</li>
			<li>Population Cell Number Unit</li>
			<li>Workspace File</li>
			</ul>
		</td>
		<td >December 2016</td>
	</tr>
	<tr>
			<td  rowspan="2">Flow Cytometry File Management</td>
			<td >Perl script and property file to make all files in a directory and its sub-directory unique.  This is useful for editing control and compensation file names which may be repeated in different cytometer runs.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/makeFileNamesUnique.pl" target="_new">makeFileNamesUnique.pl</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/makeFileNamesUnique.properties" target="_new">makeFileNamesUnique.properties</a><br></td>
			<td >			</td>
			<td >December 2014</td>
	</tr>
	<tr>
			<!--<td >Flow Cytometry Assays </td>-->
			<td >Perl script for very large files (>100 Mb) that edits the binary data start and end tags in the fcs header to enable files to be processed by flowCore software.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/fixFcsHeaderDataCoords.pl" target="_new">fixFcsHeaderDataCoords.pl</a><br>
			<td >			</td>
			<td >December 2014</td>
	</tr>

<!--	<tr>
		<td >flow Textfiles</td>
		<td >A form to describe flow text files in a structure that can be interpreted by ImmPort to facilitate searching and display of results.</td>
		<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/flowTextFiles.xls" target="_new">flowTextFiles.xls</a><br>
								<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/flowTextFiles.txt" target="_new">flowTextFiles.txt</a></td>
		<td >
			<ul>
			<li>Preferred Display Name</li>
			</ul>
		</td>
		<td >December 2016</td>
	</tr>
	-->

	<tr>
			<td  rowspan="4">Gene Expression Assays</td>
			<td >Captures the samples, reagents, and results from a Gene Expression experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Gene_Expression_Array.xls" target="_new">experimentSamples.Gene_Expression_Array.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Gene_Expression_Array.txt" target="_new">experimentSamples.Gene_Expression_Array.txt</a>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.RNA.Sequencing.xls" target="_new">experimentSamples.RNA.Sequencing.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.RNA.Sequencing.txt" target="_new">experimentSamples.RNA.Sequencing.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User ID</li>
					<li>Experiment ID</li>
					<li>Biological Sample ID</li>
					<li>Reagent</li>
					<li>Treatment</li>
					<li>Repository Name</li>
					<li>Repository Accession</li>
			</ul>
			</td>
			<td >June 2016</td>
	</tr>
	<tr>
			<!--<td >Gene Expression protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Gene Expression Reagents</td>-->
			<td >Captures detailed information about the reagents used in a Gene Expression experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Array.xls" target="_new">reagents.Array.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Array.txt" target="_new">reagents.Array.txt</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Sequencing.xls" target="_new">reagents.Sequencing.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Sequencing.txt" target="_new">reagents.Sequencing.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<td  rowspan="4">Genotyping Assays</td>
			<td >Captures the samples, reagents, and results from a Genotyping experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Genotyping_Array.xls" target="_new">experimentSamples.Genotyping_Array.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Genotyping_Array.txt" target="_new">experimentSamples.Genotyping_Array.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User ID</li>
					<li>Experiment ID</li>
					<li>Biological Sample ID</li>
					<li>Reagent</li>
					<li>Treatment</li>
					<li>Repository Name</li>
					<li>Repository Accession</li>
			</ul>
			</td>
			<td >June 2016</td>
	</tr>
	<tr>
			<!--<td >Genotyping protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Genotyping Reagents</td>-->
			<td >Captures detailed information about the reagents used in a Genotyping experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Array.xls" target="_new">reagents.Array.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Array.txt" target="_new">reagents.Array.txt</a><br></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>


	<tr>
			<td  rowspan="4">Hemaglutinnation Inhibition Assays </td>
			<td >Captures the samples, reagents, and results from a HAI experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.HAI.xls" target="_new">experimentSamples.HAI.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.HAI.txt" target="_new">experimentSamples.HAI.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>ImmPort Template?</li>
					<li>Result file</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >HAI protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Hemaglutinnation Inhibition Reagents</td>-->
			<td >Captures detailed information about the reagents used in a HAI experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.HAI.xls" target="_new">reagents.HAI.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.HAI.txt" target="_new">reagents.HAI.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<td  rowspan="5">HLA Typing Assays </td>
			<td >Captures the samples, reagents, and results from a HLA typing experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.HLA.xls" target="_new">experimentSamples.HLA.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.HLA.txt" target="_new">experimentSamples.HLA.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>ImmPort Template?</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >HLA protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >HLA Typing Reagents</td>-->
			<td >Captures detailed information about the reagents used in a HLA typing experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.HLA_Typing.xls" target="_new">reagents.HLA_Typing.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.HLA_Typing.txt" target="_new">reagents.HLA_Typing.txt</a></td>
			<td >
				<ul>
				<li>HLA Typing System User-Defined ID</li>
				<li>Locus Genotyped</li>
				<li>Exons/Intron Interrogated</li>
				<li>Typing System Manufacturer</li>
				<li>Typing System Version</li>
				<li>Typing Method</li>
				<li>HLA Typing Date</li>
				<li>Last Pertinent Who Report/Update</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
		<!--<td >HLA typing Derived and Intepreted Results</td>-->
		<td >A form to describe HLA typing results  in a format to facilitate sharing of results.</td>
		<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/HLA_Typing.xls" target="_new">HLA_Typing.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/HLA_Typing.txt" target="_new">HLA_Typing.txt</a></td>
		<td >
				<ul>
					<li>Experiment Sample ID</li>
					<li>Population</li>
				</ul>
		</td>
		<td >December 2016</td>
	</tr>

	<tr>
			<td  rowspan="4">Image Histology </td>
			<td >Captures the samples, reagents, and results from a Image Histology experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Image_Histology.xls" target="_new">experimentSamples.<br>Image_Histology.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Image_Histology.txt" target="_new">experimentSamples.<br>Image_Histology.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>Result file</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Image Histology protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Image Histology Reagents</td>-->
			<td >Captures detailed information about the reagents used in a Image Histology experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Other.xls" target="_new">reagents.Other.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Other.txt" target="_new">reagents.Other.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<td  rowspan="6">KIR Assays </td>
			<td >Captures the samples, reagents, and results from a KIR typing experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.KIR.xls" target="_new">experimentSamples.KIR.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.KIR.txt" target="_new">experimentSamples.KIR.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>ImmPort Template?</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<!--<td >KIR protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >KIR Typing Reagents</td>-->
			<td >Captures detailed information about the reagents used in a HLA typing experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.KIR_Typing.xls" target="_new">reagents.KIR_Typing.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.KIR_Typing.txt" target="_new">reagents.KIR_Typing.txt</a></td>
			<td >
				<ul>
				<li>KIR Typing System User-Defined ID</li>
				<li>Locus Genotyped</li>
				<li>Reagent Type</li>
				<li>Exons/Intron Interrogated</li>
				<li>Primer/Probe Sequence</li>
				<li>Allele(s) Detected</li>
				<li>Last Pertinent IPD KIR Database Nomenclature Release</li>
				<li>KIR Typing Date</li>
				<li>Typing System Manufacturer</li>
				<li>Typing System Version</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	
	<tr>
		<!--<td >KIR typing summary- EBI format</td>-->
		<td >A form to describe KIR typing results in a structure that can be interpreted by ImmPort to facilitate searching and display of results.</td>
		<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/KIR_Typing_Results_EBI_Results.xls" target="_new">KIR_Typing_Results_EBI_Results.xls</a><br>
								<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/KIR_Typing_Results_EBI_Results.txt" target="_new">KIR_Typing_Results_EBI_Results.txt</a></td>
		<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Population</li>
				</ul>
		</td>
		<td >December 2016</td>
	</tr>

	<tr>
		<!--<td >KIR typing summary- column format</td>-->
		<td >A form to describe KIR typing results in a structure that can be interpreted by ImmPort to facilitate searching and display of results.</td>
		<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/KIR_Typing_Results_Column.xls" target="_new">KIR_Typing_Results_Column.xls</a><br>
								<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/KIR_Typing_Results_Column.txt" target="_new">KIR_Typing_Results_Column.txt</a></td>
		<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Population</li>
				</ul>
		</td>
		<td >December 2016</td>
	</tr>


	<tr>
			<td  rowspan="4">Mass spectrometry Assays </td>
			<td >Captures the samples, reagents, and results from a mass spectrometry experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Mass_Spectrometry.xls" target="_new">experimentSamples.<br>Mass_Spectrometry.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Mass_Spectrometry.txt" target="_new">experimentSamples.<br>Mass_Spectrometry.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>Result file</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Mass spectrometry protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Mass spectrometry Reagents</td>-->
			<td >Captures detailed information about the reagents used in a Image Histology experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Other.xls" target="_new">reagents.Other.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Other.txt" target="_new">reagents.Other.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<td  rowspan="7">Multiplex Bead Array Assays (MBAA) Assays</td>
			<td >Captures the samples, reagents, treatments, and results from a MBAA experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.MBAA.xls" target="_new">experimentSamples.MBAA.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.MBAA.txt" target="_new">experimentSamples.MBAA.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>ImmPort Template?</li>
					<li>Assay ID</li>
					<li>Dilution Factor</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Mass spectrometry protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >MBAA Reagents</td>-->
			<td >Captures detailed information about the reagents used in a MBAA experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.MBAA.xls" target="_new">reagents.MBAA.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.MBAA.txt" target="_new">reagents.MBAA.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				<li>Analyte Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
				<!--<td >Control Samples</td>-->
				<td >A form to describe control samples  in a structure that can be interpreted by ImmPort to facilitate searching and display of results.</td>
				<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/controlSamples.xls" target="_new">controlSamples.xls</a><br>
										<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/controlSamples.txt" target="_new">controlSamples.txt</a></td>
				<td >
					<ul>
						<li>Control Sample User Defined ID</li>
						<li>Experiment</li>
						<li>Source</li>
						<li>Catalog ID</li>
						<li>Dilution Factor</li>
						<li>Assay ID</li>
						<li>ImmPort Template?</li>
					</ul>
				</td>
				<td >December 2016</td>
	</tr>

	<tr>
					<!--<td >Standard Curve</td>-->
					<td >A form to describe a standard curve  in a structure that can be interpreted by ImmPort to facilitate searching and display of results.</td>
					<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/standardCurves.xls" target="_new">standardCurves.xls</a><br>
											<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/standardCurves.txt" target="_new">standardCurves.txt</a></td>
					<td >
						<ul>
							<li>Standard Curve User-Defined ID</li>
							<li>Experiment</li>
							<li>Formula</li>
							<li>Analyte Name</li>
							<li>Assay ID</li>
							<li>Lower Limit</li>
							<li>Lower Limit Unit</li>
							<li>Upper Limit</li>
							<li>Upper Limit Unit</li>
							<li>ImmPort Template?</li>
						</ul>
					</td>
					<td >December 2016</td>
		</tr>


	<tr>
			<!--<td >MBAA Derived and Interpreted Results </td>-->
			<td >A form to describe MBAA results in a structure that can be interpreted by ImmPort to facilitate searching and display of results.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/MBAA_Results.xls" target="_new">MBAA_Results.xls</a><br>
										<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/MBAA_Results.txt" target="_new">MBAA_Results.txt</a></td>
			<td >
				<ul>
					<li>Source ID</li>
					<li>Assay ID</li>
					<li>Analyte Name</li>
					<li>Calculated Concentration Value</li>
					<li>Calculated Concentration Unit</li>
					<li>Mfi</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<td  rowspan="4">Neutralizing Antibody Titer Assays</td>
			<td >Captures the samples, reagents, and results from a Neutralizing Antibody Titer experiment. The literature uses a number of terms to describe similar assay methods and ImmPort employs some of these assay names.</td>
				<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Neutralizing_Antibody_Titer.xls" target="_new">experimentSamples.<br>Neutralizing_Antibody_Titer.xls</a><br>
										<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Neutralizing_Antibody_Titer.txt" target="_new">experimentSamples.<br>Neutralizing_Antibody_Titer.txt</a></td>
				<td >
					<ul>
						<li>Experiment Sample User Defined ID</li>
						<li>Experiment</li>
						<li>Reagent</li>
						<li>Biological Sample</li>
						<li>Treatment</li>
						<li>ImmPort Template?</li>
					</ul>
				</td>
				<td >December 2016</td>
		</tr>
	<tr>
			<!--<td >Virus Neut protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Virus Neutralization Reagents</td>-->
			<td >Captures detailed information about the reagents used in a MBAA experiment.</td>
				<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Virus_Neutralization.xls" target="_new">reagents.<br>Virus_Neutralization.xls</a><br>
										<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Virus_Neutralization.txt" target="_new">reagents.<br>Virus_Neutralization.txt</a></td>
				<td >
					<ul>
						<li>Reagent User Defined ID</li>
						<li>Manufacturer</li>
						<li>Catalog Number</li>
					</ul>
				</td>
				<td >December 2016</td>
		</tr>	<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>


	<tr>
			<td  rowspan="4">QRT-PCR Assays </td>
			<td >Captures the samples, reagents, and results from a quantitative PCR experiment.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.QRT-PCR.xls" target="_new">experimentSamples.QRT-PCR.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.QRT-PCR.txt" target="_new">experimentSamples.QRT-PCR.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>ImmPort Template?</li>
				</ul>
				</td>
			<td >June 2016</td>
	</tr>
	<tr>
			<!--<td >PCR protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >PCR Reagents</td>-->
			<td >Captures detailed information about the reagents used in a PCR experiment.</td>
				<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.PCR.xls" target="_new">reagents.<br>PCR.xls</a><br>
										<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.PCR.txt" target="_new">reagents.<br>PCR.txt</a></td>
				<td >
					<ul>
						<li>Reagent User Defined ID</li>
						<li>Manufacturer</li>
						<li>Catalog Number</li>
					</ul>
				</td>
				<td >December 2016</td>
		</tr>
		<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<td  rowspan="4">Other Assays </td>
			<td >Captures the samples, reagents, and results from an experiment type not characterized by ImmPort.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Other.xls" target="_new">experimentSamples.Other.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experimentSamples.Other.txt" target="_new">experimentSamples.Other.txt</a></td>
			<td >
				<ul>
					<li>Experiment Sample User Defined ID</li>
					<li>Experiment</li>
					<li>Reagent</li>
					<li>Biological Sample</li>
					<li>Treatment</li>
					<li>Result file</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<!--<td >Other protocols</td>-->
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>	<tr>
			<!--<td >Reagents Other</td>-->
			<td >Captures detailed information about the reagents used in an experiment. Since different analysis platforms employ very different reagents, the reagents.xls file contains several tabs for entering platform specific details of the reagents that were used.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Other.xls" target="_new">reagents.Other.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Other.txt" target="_new">reagents.Other.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
		<tr>
			<!--<td >Sample Treatments</td>-->
			<td >Captures  information about the treatments applied in vitro to experiment samples or biological samples. Three types of treatments are supported: amount of agent, duration, and temperature.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.xls" target="_new">treatments.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/treatments.txt" target="_new">treatments.txt</a></td>
			<td >
				<ul>
				<li>Treatment User Defined ID</li>
				<li>Treatment Name</li>
				<li>Treatment?</li>
				<li>Treatment Specific Details</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	
	</tbody>
</table>
<br></br>
<br></br>
<table class="table table-hover table-bordered">
	<thead>
	<tr>
		<td >Standalone Templates</td>
		<td>Purpose</td>
		<td >Spreadsheet Template</td>
		<td >Required Data to Complete Metadata Form</td>
		<td >Latest Version/<br>Date Available </td>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td  rowspan="2">Biological Samples</td>
		<td >Describe the samples used for experiments or lab tests. <br>  N.B. Samples may also be described in the experiment sample template.</td>
		<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/bioSamples.xls" target="_new">bioSamples.xls</a><br>
								<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/bioSamples.txt" target="_new">bioSamples.txt</a></td>
		<td >
			<ul>
		        <li>Biological Sample User Defined ID</li>
		        <li>Sample Type</li>
		        <li>Subject</li>
		        <li>Protocol</li>
		     	<li>Study </li>
		     	<li>Planned Visit</li>
		        <li>Study Time Collected</li>
		        <li>Study Time Event</li>
			</ul>
	    </td>
		<td >December 2016</td>
	</tr>
	<tr>

			<td >Lists the protocols associated with the biological samples. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
		<td  rowspan="2">Experiments</td>
		<td >Describes the type of experiment, measurement technique and protocols used in the experiment.<br>  N.B. Experiments may also be described in the experiment sample templates.</td>
		<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experiments.xls" target="_new">experiments.xls</a><br>
								<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/experiments.txt" target="_new">experiments.txt</a></td>
		<td >
			<ul>
				<li>Experiment User Defined ID</li>
				<li>Experiment Name</li>
				<li>Purpose</li>
				<li>Measurement Technique</li>
		     	<li>Protocol</li>
		    </ul>

		</td>
		<td >December 2016</td>
	</tr>
	<tr>
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<td >ELISA Derived and Interpreted Results </td>
			<td >A form to describe ELISA results in a structure that can be interpreted by ImmPort to facilitate searching and display of results.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/ELISA_Results.xls" target="_new">ELISA_Results.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/ELISA_Results.txt" target="_new">ELISA_Results.txt</a></td>
			<td >
			<ul>
		        <li>Experiment Sample ID</li>
		        <li>Analyte</li>
		        <li>Calculated Concentration Value</li>
		        <li>Calculated Concentration Unit</li>
		        <li>Parent Specimen</li>
			</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<td >ELISPOT Derived and Interpreted Results </td>
			<td >A form to describe ELISPOT results in a structure that can be interpreted by ImmPort to facilitate searching and display of results.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/ELISPOT_Results.xls" target="_new">ELISPOT_Results.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/ELISPOT_Results.txt" target="_new">ELISPOT_Results.txt</a></td>
			<td >
			<ul>
		        <li>Experiment Sample ID</li>
		        <li>Analyte</li>
		        <li>Number of Spots per Well</li>
		        <li>Base/Parent Population value</li>
		        <li>Base/Parent Population unit</li>
				<li>Autogenerated Descriptive Phrase</li>
			</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
		<td >Hemaglutinnation Inhibition Derived and Interpreted Results</td>
		<td >A form to describe HAI results in a structure that can be interpreted by ImmPort to facilitate searching and display of results.</td>
		<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/HAI_Results.xls" target="_new">HAI_Results.xls</a><br>
								<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/HAI_Results.txt" target="_new">HAI_Results.txt</a></td>
		<td >
				<ul>
					<li>Experiment Sample</li>
					<li>Virus Strain</li>
					<li>Titration Dilution Value</li>
				</ul>
		</td>
		<td >December 2016</td>
	</tr>



	<tr>
			<td rowspan="2">Lab Test Panels</td>
			<td >Captures the Lab Panel Name, Study ID and Protocol ID for lab tests.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/labTestPanels.xls" target="_new">labTestPanels.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/labTestPanels.txt" target="_new">labTestPanels.txt</a></td>
			<td >
				<ul>
					<li>Lab Test User Defined ID</li>
					<li>Name</li>
					<li>Study ID</li>
					<li>Protocol ID</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>
	<tr>
			<td >Lists the protocols associated with the experiments. Protocols may be PDF files, Word documents, Excel or other file types.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.xls" target="_new">protocols.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/protocols.txt" target="_new">protocols.txt</a></td>
			<td >
				<ul>
				<li>Protocol User Defined ID</li>
				<li>Protocol File Name</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
			<td >Reagents Sequencing</td>
			<td >Captures detailed information about the reagents used in an experiment. Since different analysis platforms employ very different reagents, the reagents.xls file contains several tabs for entering platform specific details of the reagents that were used.</td>
			<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Sequencing.xls" target="_new">reagents.Sequencing.xls</a><br>
									<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/reagents.Sequencing.txt" target="_new">reagents.Sequencing.txt</a></td>
			<td >
				<ul>
				<li>Reagent User Defined ID</li>
				<li>Manufacturer</li>
				<li>Catalog Number</li>
				</ul>
			</td>
			<td >December 2016</td>
	</tr>

	<tr>
				<td >PCR Derived and Interpreted Results</td>
				<td >A form to describe PCR results in a structure that can be interpreted by ImmPort to facilitate searching and display of results.</td>
				<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/PCR_Results.xls" target="_new">PCR_Results.xls</a><br>
										<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/PCR_Results.txt" target="_new">PCR_Results.txt</a></td>
				<td >
					<ul>
						<li>Experiment Sample</li>
						<li>Entrez Gene ID</li>
						<li>Threshold cycles (Ct)</li>
					</ul>
				</td>
				<td >December 2016</td>
	</tr>

<tr>
					<td >Virus Neutralization Titers Derived and Interpreted Results</td>
					<td >A form to describe the interpeted results of sample (e.g. serum) dilution series to quantitate the viral neutralizing activity of a sample. The Virus Neutralization Results capture the interpreted titer results in a commonly used format that can be interpreted by ImmPort to facilitate searching and display of results.</td>
					<td ><a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/Virus_Neutralization_Results.xls" target="_new">Virus_Neutralization_Results.xls</a><br>
											<a class="buttonlink" href="<c:out value="${dataUploadTemplatesUrl}"></c:out>/Virus_Neutralization_Results.txt" target="_new">Virus_Neutralization_Results.txt</a></td>
					<td >
						<ul>
							<li>Experiment Sample</li>
							<li>Virus Strain</li>
							<li>Titration Dilution Value</li>
						</ul>
					</td>
					<td >December 2016</td>
	</tr>
 </tbody>


</table>
</div></div>
<br><br>
<span class="tipmessage">
	Need help?  Please feel free to contact us by email at <a class="buttonlink" href="mailto: BISC_Helpdesk@niaid.nih.gov">BISC_Helpdesk@niaid.nih.gov</a>
</span>


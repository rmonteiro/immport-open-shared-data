<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
#queryTerm {
	width: 540px;
}
.input-group-btn {
	vertical-align: top !important;
}
#schematicModalDetail {
	overflow-y: scroll;
	margin-top: 0px;
	margion-bottom: 8px;
}
#facet-container .panel {
	margin-bottom: 10px;
}
#facet-container .panel-heading {
	padding: 5px 10px;
	background-color: #E8DFD7;
}
#facet-container .panel-heading .panel-title {
	font-size: 14px;
}
#facet-container .panel-body {
	padding: 5px 10px;
}
</style>


<div class="row">
    <div class="col-md-12">
        <h6 style="margin-top:20px">
        <button type="button" class="btn btn-action" id="btnSearch">Search</button>
        <button id="btnClearQuery" class="btn btn-default" type="button">
        <i class="glyphicon glyphicon-remove"></i>
        </button>
        </h6>
        <div id="facet-container" class="catalog-panel" style="padding-top:5px;"></div>
    </div>
    <div class="col-md-12">
    </div>
</div>



<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';

</script>

<script src="<c:url value='/resources/js/IP/search/facetSearch.js'/>" type="text/javascript"></script>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/css/dataTables.colVis.css" />" >
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/css/dataTables.tableTools.css" />" >

<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/media/js/jquery.dataTables.js" />" type="text/javascript"></script>
<script src="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/dataTables.colResize.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/js/dataTables.colVis.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/js/dataTables.tableTools.js" />" type="text/javascript"></script>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <h2 style="margin-top:10px;margin-bottom:20px;">Publications</h2>
     <div id="errorMsg"></div>
     <table id="publicationsTable" class="table table-striped table-hover table-bordered table-condensed table-responsive">
        <thead>
            <tr>
                <th>Year</th>
                <th>Study Acc.</th>
                <th>Reference Type</th>
                <th>Journal</th>
                <th>Title</th>
                <th>Link</th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
                <th>Year</th>
                <th>Study Acc.</th>
                <th>Reference Type</th>
                <th>Journal</th>
                <th>Title</th>
                <th>Link</th>
            </tr>
        </tfoot>
 
        <tbody>
       </tbody>
       </table>
    </div>
</div>
<div class="row">
&nbsp;
</div>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var searchURL = CONTEXT_ROOT + '/resources/data/publications.txt';
var table = $('#publicationsTable');
var dataTable;
var lastIdx = null;
var radioHTML = '<span class="typeControls"> Reference Type: ' +
'<input name="radioType" id="allTypes" value="All" type="radio" checked=""> ALL' +
' <input name="radioType" id="aboutTypes" value="About" type="radio"><span data-toggle="tooltip" data-placement="bottom" title="Publication mentions ImmPort">About</span>' +
' <input name="radioType" id="dataUseTypes" value="Data Use" type="radio"><span data-toggle="tooltip" data-placement="bottom" title="Publication includes ImmPort data">Data Use</span>' +
' <input name="radioType" id="submitterTypes" value="Submitter" type="radio"><span data-toggle="tooltip" data-placement="bottom" title="Publication is authored by ImmPort data submitter">Submitter</span>' +
' <input name="radioType" id="toolUseTypes" value="Tool Use" type="radio"><span data-toggle="tooltip" data-placement="bottom" title="Publication includes ImmPort tool use">Tool Use</span>' +
'</span>'


$().ready(function(){
	$(function () { $("[data-toggle='tooltip']").tooltip(); });
	
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "text",
		"success": function(data) {
			var publications = [];
			var lines = data.split("\n");
			var len = lines.length - 1;
	        for (var i = 1; i < len; i++) {
	        	  var record = lines[i].split("\t");
	        	  publications.push(record);
	        }
			dataTable = table.DataTable({
				"dom": 'Z<"row"<"col-sm-2"l><"col-sm-7"<"radiobar">><"col-sm-3"f>><"row"<"col-sm-12"t>><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"order": [[0,"desc"]],
				"colResize": {
		            "handleWidth": 20
		        },
		        "columnDefs": [
		           {
		        	   "width": "10%",
		        	   "targets": 0
		           },
		           {
		        	   "width": "10%",
		        	   "targets": 1,
		        	   "render": function(data,type,row) {
		        		   var studies = data.split(",");
		        		   var len = studies.length;
		        		   var hrefs = [];
		        		   for (var i = 0; i < len; i++) {
		        			   var url = '<a href="' + CONTEXT_ROOT + '/public/study/study/displayStudyDetail/'
		        			             + studies[i] + '">' + studies[i] + '</a>';
		        			   hrefs.push(url)
		        		   }
		        		   return hrefs.join(",");
		               }
		           },
		           {
		        	   "width": "15%",
		        	   "targets": 2
		           },
		           {
		        	   "width": "20%",
		        	   "targets": 3
		           },
		           {
		        	   "width": "40%",
		        	   "targets": 4
		           },
		           {
		        	       "render": function(data,type,row) {
		        	    	       return '<a href="' + data + '" target="_blank"><i class="fa fa-external-link fa-lg"><i></a>';
		        	       },
		        	       "width": "5%",
		        	       "targets": 5
		           }
		        ],
				"lengthMenu": [[-1,10, 25, 50 ], ["All",10, 25, 50]],
				"scrollX": false,
				"data": publications
			});
			
			$('#publicationsTable tbody')
			  .on( 'mouseover', 'td', function () {
		        var colIdx = dataTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dataTable.cells().nodes() ).removeClass( 'active' );
		            $( dataTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		      .on( 'mouseleave', function () {
		        $( dataTable.cells().nodes() ).removeClass( 'active' );
		    } );
			
			
			$("div.radiobar").html(radioHTML);
			
			dataTable
			.column(2)
	        .search( "",false,false,false )
	        .draw();
			
			
			$('.typeControls').find('input').bind('click',function(event) {
				var id = $(this).attr('id');
				var searchValue ="";
				if (id == "aboutTypes") {
					searchValue="About";
				} else if (id == "dataUseTypes" ) {
					searchValue="Data Use";
				} else if (id == "submitterTypes" ) {
					searchValue="Submitter";
				} else if (id == "toolUseTypes" ) {
					searchValue="Tool Use";
				} else {
					searchValue="";
				}

				dataTable
				.column(2)
		        .search( searchValue,false,false,false )
		        .draw();
			});
			
		},
		"error": function() {
			var msg="Publications file is not available";
			$('#errorMsg').append(msg);	
		}	
	});
});

</script>


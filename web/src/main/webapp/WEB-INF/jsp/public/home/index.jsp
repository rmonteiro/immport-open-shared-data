<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  <head>
    <title>Instructions for the Data Upload Package Validator</title>
  </head>
  <body>
    <h2>Instructions for the Data Upload Package Validator</h2>
    <ul>
    	<li><a href="#selectInstaller" class="buttonlink">Select the appropriate installer for your system</a></li>
    	<li><a href="#security" class="buttonlink">Security Certification Exceptions</a></li>
    	<li><a href="#download" class="buttonlink">Download a Project using Safari</a></li>
    	<li><a href="#reinstall" class="buttonlink">Reinstall the latest version of the Data Upload Package Validator</a></li>
    	<li><a href="#defaultBrowser" class="buttonlink">To change your browser default selection</a></li>
	<li><a href="#validateImmPort" class="buttonlink">Run Validation against ImmPort</a></li>
	<li><a href="#validateLocal" class="buttonlink">Run Validation against local copy</a></li>
	<li><a href="#downloadData" class="buttonlink">Download Project Data</a></li>
	<li><a href="#results" class="buttonlink">Reviewing the Results</a></li>
	<li><a href="#saving" class="buttonlink">Saving the Results</a></li>
    	<li><a href="#LoggingOut" class="buttonlink">Logging Out</a></li>
    	<li><a href="#exit" class="buttonlink">Exiting</a></li>
    </ul>
   <a name="top">
	<h3><span class="label"><a name="selectInstaller">Select the appropriate installer for your system.</a></span></h3>
	<ul>
			<li><a href="installers/ImmPortValidatorApp_windows_3.0.7.RELEASE.exe">Windows 32-Bit System</a></li>
				<ul><li>For Windows 32-Bit System use the Windows 32-Bit Installer (ImmPortValidatorApp_windows_{versionNumber}.exe)</li>
				    <li>User needs to install the validator in a directory to which the user has permission.</li></ul>
			<li><a href="installers/ImmPortValidatorApp_windows-x64_3.0.7.RELEASE.exe">Windows 64-Bit System</a></li>
				<ul><li>For Windows 64-Bit System use the Windows 64-Bit Installer(ImmPortValidatorApp_windows-x64_{versionNumber}.exe)</li>
				    <li>User needs to install the validator in a directory to which the user has permission.</li></ul>
			<li><a href="installers/ImmPortValidatorApp_macos_3.0.7.RELEASE.dmg">Mac OS X -- Lion and Above -- Version 10.7+</a></li>
				<ul><li>When you download the validator and the GateKeeper security is set to Mac App store, then a dialog box is shown that the software is from an unidentified developer. Ctl + click the installer and then click Open to download the application.</li><br>
					<ul><img src="../images/GateSecurity_1.png" alt="Gate Security 1" WIDTH=480 HEIGHT=320></ul><br>
					<ul><img src="../images/GateSecurity_2.png" alt="Gate Security 2" WIDTH=480 HEIGHT=320></ul><br>
					<ul><img src="../images/GateSecurity_3.png" alt="Gate Security 3" WIDTH=480 HEIGHT=320></ul><br>
				    <li>For Mac version 10.7.3 and greater use the MAC Installer(ImmPortValidatorApp_macos_{versionNumber}.dmg).  Any version less than 10.7.3 use (ImmPortValidatorApp_macos_10_6_{versionNumber}.dmg)</li>
				</ul>
			<li><a href="installers/ImmPortValidatorApp_macos-10_6_3.0.7.RELEASE.dmg">Mac OS X -- Snow Leopard -- Version 10.6</a></li>
				<ul><li>Mac OS X 10.6 and below: Apple's Java comes pre-installed with your Mac OS. </li>
					<li>When you download the application and the GateKeeper security is set to Mac App store and Identified developers or Mac App Store, then a dialog box is shown that specifies the software is from an unidentified developer. Ctl + click then click Open to download the application.</li>
				<br><ul><img src="../images/MacOSMessage.png" alt="MacOS" WIDTH=480 HEIGHT=320></ul></ul><br>
					
			<li><a href="installers/ImmPortValidatorApp_unix_3.0.7.RELEASE.sh">Unix System</a></li>
			<ul><b>Preinstallation</b>
			<ul><li>For Unix download the Installer (ImmPortValidatorApp_unix_{versionNumber}.sh) </li>
				<li>Right click on the Installer (ImmPortValidatorApp_unix_{versionNumber}.sh) and select Properties.  The following screen will appear.</li>
				<ul><img src="../images/UnixMessage.png" alt="Unix" WIDTH=480 HEIGHT=320></ul></li><br>
				<li>Click the Permissions Tab. </li> 
				<li>Check the box &quot;Allow executing file as program&quot;</li>
				<li>Click the &quot;Close&quot; button</li>
				<li>Double click on the ImmPortValidatorApp_unix_{version}.sh file. The following screen will appear.</li>
				<ul><img src="../images/InstallerRunTerminal.png" alt="Unix" WIDTH=480 HEIGHT=120></ul></li><br>
				<li>Click on the Run in Terminal button on the screen.</li>
			</ul><br>
			<b>Install the application by following the directions.</b>
			<br><br>
			<b>Starting the Application after the Installation</b>
				<ul><li>Go to the &quot;installation directory/ImmPortValidatorApp&quot; and double click on the validator.sh</li>
				<img src="../images/RunTerminal.png" alt="Unix" WIDTH=480 HEIGHT=120><br></li>
				<li>Click on the &quot;Run in Terminal&quot; button on the screen.</li>
			</ul>	
		</ul>
	
	<a href="#top" class="buttonlink">Top</a>	
	<h3><span class="label"><a name="security">Security Certification Exceptions</a></span></h3>
	<ul>For first time users a Security Certificate Exception will appear.  Depending upon the default web browser, follow the steps below.
		<ul><b>Firefox</b>
		<ul>			
			<li>Click &quot;I understand the Risks&quot; then</li><br>
			<ul><img src="../images/FirefoxSSLMessage1.jpg" alt="I understand the Risks" WIDTH=480 HEIGHT=320></ul><br>
			<li>Click &quot;Add Exception&quot; then </li><br>
			<ul><img src="../images/FirefoxSSLMessage2.jpg" alt="Add Exception" WIDTH=480 HEIGHT=320></ul><br>
			<li>Click &quot;Confirm Security Exception&quot;</li><br>
			<ul><img src="../images/FirefoxSSLMessage3.jpg" alt="Confirm Security Exception" WIDTH=480 HEIGHT=320></ul><br>
		</ul>
		</ul>
		<ul><b>Chrome</b>
			<ul>
			<li>Click &quot;Proceed Anyway&quot;</li><br>
			<ul><img src="../images/ChromeSSLMessage.jpg" alt="Proceed Anyway" WIDTH=480 HEIGHT=320></ul><br>
			</ul>
		</ul>
		<ul><b>Safari v7+</b>
		<ul>
			<li>Click "Continue" to proceed</li><br>
			<ul><img src="../images/safarissl.png" alt="Continue with Safari" WIDTH=480 HEIGHT=240 ></ul><br>		
		</ul>
		</ul>
		<ul><b>Internet Explorer v9.0+</b>
		<ul> 
			<li>Click &quot;Continue to this website (not recommended)&quot;.</li><br>
			<ul><img src="../images/IESSLMessage.jpg" alt="Proceed Anyway" WIDTH=480 HEIGHT=320></ul><br>
		</ul>
		</ul>
</ul>
	<h3><a name="download">Download a Project using Safari</a></h3>
		<ul><li>To download Project Data, click &quot;option click&quot; to download the project data without unzipping it.</li>
			<li>If you don't want to &quot;option click&quot; every time, you need to enable a setting on Safari that prevents the Project Data File from being unzipped. 
			<br/>This can be disabled in Safari: Preferences -> General -> Uncheck the box Open "safe" files after downloading at the bottom.</li>
			<ul><img src="../images/safariPreferences.png" alt="Proceed Anyway" WIDTH=480 HEIGHT=320></ul><br>
		</ul>
	</ul>
	<h3><span class="label"><a name="reinstall">Reinstall the latest version of the Data Upload Package Validator</a></span></h3>
		<ul><li>When a new version of the Data Upload Package Validator has been released.  The installer will detect if there is a previous version on the local system, uninstall the previous version, and install the latest version.</li>
		    <li>During the reinstall the following box will appear if there is a previous version of the software on your computer.  Click "Next" to update the existing installation.</li><br>		    
			<ul><img src="../images/ReInstall.png" alt="Reinstall" WIDTH=480 HEIGHT=320></ul><br>
		    <li>Or if this is the first time installing or if you already uninstalled the previous version the following box will appear.  Click "Next" to install the installation.</li><br>		    
			<ul><img src="../images/InstallerFirstPage.png" alt="InstallerFirstPage" WIDTH=480 HEIGHT=320></ul><br>
		</ul>
	</ul>
	<h3><span class="label"><a name="defaultBrowser">To change your browser default selection</a></span></h3>
		<ul>Windows OS
			<ul><li>Open Default Programs by clicking the Start button of the Start button, and then clicking Default Programs.</li>
				<li>Click Set your default programs.</li>
				<li>Select the browser to set as default browser.</li>
				<li>Click Set this program as default.</li>
				<li>Click OK</li>
			</ul>
		</ul>
		<ul>Mac OS X 
			<ul>
			<li>Open Safari</li>
			<li>Select Preferences from the Safari menu</li>
			<li>Select the General Tab then click the "Default Web Browser" pop-up menu</li>
			<li>Once you've selected your new default browser, click the close button</li>
		</ul>
		</ul>
	<a href="#top" class="buttonlink">Top</a>
	<h3><span class="label"><a name="validateImmPort">Run Validation against ImmPort (Recommended)</a></span></h3>
		<ul><li>Select the Run Validation against ImmPort</li>
		<ul><img src="../images/validateAgainstImmPort.jpg" alt="Validate ImmPort" WIDTH=480 HEIGHT=240></ul><br>	
		 <li>Login with your ImmPort User Name and Password</li><br>
		   <ul><img src="../images/Authentication.jpg" alt="Authentication" WIDTH=480 HEIGHT=120></ul><br>
		    <li>Select the project you want to validate against.</li><br>
		  <ul><img src="../images/RunValidation.jpg" alt="RunValidation" WIDTH=480 HEIGHT=180></ul><br>
		   
		    <li>Select the Data Upload Package File in your local directory.</li>
		    <li>Check the "Do you want to save the Project Data File for later use?" if you want to save the Project Data file on your computer.</li>
		    <li>Click the Run Validation button.</li>
		    <li>Click on the Download Validation Report File link to save the report to your computer.</li><br>
		<ul><img src="../images/DownloadValidationReport.jpg" alt="DownloadValidationReport" WIDTH=320 HEIGHT=60></ul><br>
		  
		</ul>

	<h3><span class="label"><a name="validateLocal">Run Validation against local copy</a></span></h3>
		<ul><li>Select the Run Validation against Local</li><br>
		<ul><img src="../images/RunValidationLocal.jpg" alt="RunValidationLocal" WIDTH=480 HEIGHT=240></ul><br>
		    <li>Select the project data file that you want to validate against.</li>
		    <li>Select the Data Upload Package File in your local directory.</li><br>
		    <ul><img src="../images/RunAgainstLocal.jpg" alt="RunAgainstLocal" WIDTH=480 HEIGHT=180></ul><br>
		    <li>Click the Run Validation button.</li>
		    <li>Click on the Download Validation Report File link to save the report to your computer.</li>
		    <ul><img src="../images/DownloadValidationReport.jpg" alt="DownloadValidationReport" WIDTH=320 HEIGHT=60></ul><br>
		</ul>

	<h3><span class="label"><a name="downloadData">Download Project Data File</a></span></h3>
		<ul><li>Select Download Project Data File</li>
		 <ul><img src="../images/DownloadProjectDataFile.jpg" alt="DownloadProjectDataFile" WIDTH=480 HEIGHT=180></ul><br>
		   <li>Login with your ImmPort User Name and Password</li><br>
			<ul><img src="../images/Authentication.jpg" alt="Authentication" WIDTH=480 HEIGHT=120></ul><br>
		   <li>Select the Project to download data.</li><br> 
			<ul><img src="../images/DownloadAProject.jpg" alt="DownloadAProject" WIDTH=480 HEIGHT=180></ul><br>	    
		   <li>Click the Download Project Data button.</li>
		   		    
    		<li>Click the Download Project Data File link to save.</li>
		   
		</ul>
		

	<h3><a name="results">Reviewing the Results</a></h3>
	<ul>
		<li>From the home page, click the "View Report" </li><br>
		<ul><img src="../images/ViewReport.jpg" alt="view Report" WIDTH=480 HEIGHT=180></ul><br>
		<b>Or</b>
		<li>To view the report generated, you can select the &quot;Show/Hide Validation Details. Click Me.&quot; button on the Validation screen.</li>
		<li>The &quot;Download Validation Report File&quot; option allows you to retain copies of the reports for future reference.</li>
		<li>The report will note if any issue(s) were detected, indicate the nature of the issue(s), the files where the issue(s) was found and the line(s) in the file that were affected. </li>
		<li>When no issue(s) are noted by the Data Upload Package Validator, please submit the data upload package (the &quot;.zip&quot; file) to ImmPort.</li>
</ul>

<h3><a name="saving">Saving the Results</a></h3>
<ul>
	<li>Click the &quot;Download Validation Report File&quot; button.  You will be asked to save/open to your local directory. </li>
</ul>

	
<h3><a name="LoggingOut">Logout:</h3><ul><li>Logs user out of the system.</a></li></ul>
<h3><a name="exit">Exit:</h3><ul><li>Terminates the application.</li></ul></a>


</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/css/dataTables.colVis.css" />" >
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/css/dataTables.tableTools.css" />" >

<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/media/js/jquery.dataTables.js" />" type="text/javascript"></script>
<script src="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/dataTables.colResize.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/js/dataTables.colVis.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/js/dataTables.tableTools.js" />" type="text/javascript"></script>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <h2>Data Release Notes</h2>
        <div id="errorMsg"></div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
    <table id="dataReleaseTable" class="table table-striped table-hover table-bordered table-condensed table-responsive">
        <thead>
            <tr>
                <th>Date</th>
                <th>Release</th>
                <th>Description</th>
                <th>Notes</th>
                <th>PDF Notes</th>
                
            </tr>
        </thead>
 
        <tfoot>
            <tr>
                <th>Date</th>
                <th>Release</th>
                <th>Description</th>
                <th>Notes</th>
                <th>PDF Notes</th>
                
            </tr>
        </tfoot>
 
        <tbody>
       </tbody>
       </table>
    </div>
</div>
<div class="row">
&nbsp;
</div>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var searchURL = CONTEXT_ROOT + '/resources/data/dataRelease.txt';
var table = $('#dataReleaseTable');
var lastIdx = null;

$().ready(function(){
	
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "text",
		"success": function(data) {
			var releases = [];
			var lines = data.split("\n");
			// Ignore the header line
			// And for some reason need to strip off the last line, which is empty ??
			var len = lines.length - 1;
	        for (var i = 1; i < len; i++) {
	        	  var record = lines[i].split("\t");
	        	  releases.push(record);
	        }
			var dTable = table.DataTable({
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"order": [[1,"desc"]],
				"colResize": {
		            "handleWidth": 20
		        },
		        "columnDefs": [
		           {
		        	   "width": "20%",
		        	   "targets": 0
		           },
		           {
		        	   "width": "10%",
		        	   "targets": 1
		           },
		           {
		        	   "width": "40%",
		        	   "targets": 2
		           },
		           {
		        	   "width": "5%",
		        	   "targets": 3,
		        	   "render": function(data,type,row) {
		        		   var detailURL = CONTEXT_ROOT + '/resources/data/dataReleaseNotes/' + data + ".html";
		        		   //return '<a href="' + detailURL + '" target="_blank">'+ data + '</a>';
		        		   return '<a href="' + detailURL + '" target="_blank"><i class="fa fa-external-link fa-lg"><i></a>';
		        	   }
		        	  
		           },
		           {
		        	   "width": "10%",
		        	   "targets": 4,
		        	   "render": function(data,type,row) {
		        		   return '<a href="' + data + '" target="_blank"><i class="fa fa-file-pdf-o fa-lg"><i></a>';
		        	   }
        	       },
		           
		        ],
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollX": false,
				"data": releases
			});
			$('#dataReleaseTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			var msg="Data Release file is not available";
			$('#errorMsg').append(msg);	
		}	
	});
});

</script>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <h2>About ImmPort </h2>
    </div>
</div>
<div class="row">
<div class="col-md-12">
<p>
The Immunology Database and Analysis Portal (ImmPort) has been developed under the
<a href="http://www.niaid.nih.gov/about/organization/dait/Pages/bisc.aspx" target="_blank">
Bioinformatics Integration Support Contract (BISC) Phase II</a> by the Northrop Grumman Information Technology Health Solutions
team for the National Institutes of Health (NIH), National Institute of Allergy and Infectious Diseases (NIAID),
Division of Allergy, Immunology, and Transplantation (DAIT).
</p>
<p>
ImmPort is a partnership between researchers at the University of California-San Francisco, Stanford University,
the University of Buffalo, the Technion - Israel Institute of Technology, and Northrop Grumman.
</p>
<p>
The goals of the ImmPort project are to:
</p>
<ul>
<li>Provide an open access platform for research data sharing</li>
<li>Create an integrated environment that broadens the usefulness of scientific data and advances
hypothesis-driven and hypothesis-generating research</li>
<li>Accelerate scientific discovery while extending the value of scientific data in all areas of immunological research</li>
<li>Promote rapid availability of important findings, making new discoveries available to the research community for further
analysis and interpretation</li>
<li>Provide analysis tools to advance research in basic and clinical immunology</li>
</ul>
<p>
The ImmPort project provides advanced information technology support in the archiving and exchange of scientific data for
the diverse community of life science researchers supported by NIAID/DAIT and serves as a long-term, sustainable archive of
research and clinical data. The core component of ImmPort is an extensive data warehouse containing experimental data and
metadata describing the purpose of the study and the methods of data generation. The functionality of ImmPort will be
expanded continuously over the life of the BISC project to accommodate the needs of expanding research communities.
The shared research and clinical data, as well as the analytical tools in ImmPort are available to any researcher after registration.
</p>
<p>
Private data and pre-release data are stored in private workspaces of investigators at the ImmPort site located at NIAID,
<a href="http://immport.niaid.nih.gov" target="_blank">http://immport.niaid.nih.gov</a>.
Once shared, studies may be searched at the Open ImmPort site: <a href="http://www.immport.org">www.immport.org.</a>
</p>
</div>
</div>
<div class="row">
&nbsp;
</div>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
    <div class="col-md-12">
        <h2>Data Summary</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
		<table class="dataSummary">
		   <tr>
		     <td class="detailCol1w">Studies</td>
		     <td class="detailCol2w">222</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">Subjects</td>
		    <td class="detailCol2w" >37140</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">Experiments</td>
		    <td class="detailCol2w" >1011</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">Total Results</td>
		    <td class="detailCol2w" >940340</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;ELISA Results</td>
		    <td class="detailCol2w" >210620</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;ELISPOT Results</td>
		    <td class="detailCol2w" >30525</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;Flow Cytometry Results</td>
		    <td class="detailCol2w" >90466</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;Gene Expression Results</td>
		    <td class="detailCol2w" >43473</td>
		  </tr>
		      <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;HAI Results</td>
		    <td class="detailCol2w" >8722</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;HLA Typing Results</td>
		    <td class="detailCol2w" >6582</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;Multiplex Results</td>
		    <td class="detailCol2w" >538572</td>
		  </tr>
		  <tr>
		    <td class="detailCol1w">&nbsp;&nbsp;Neutralizing Antibody Results</td>
		    <td class="detailCol2w" >11380</td>
		  </tr>
		</table>
		<div style="margin-top:15px;margin-bottom:25px;">
			Updated June 2016
		</div>
    </div>
</div>


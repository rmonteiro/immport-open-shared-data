<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12">
        <h2>Data Upload</h2>
    </div>
</div>
<div class="row">
<div class="col-md-12">
<p>
Do you want to upload data to ImmPort?
</p>
<p>
If you are interested in using ImmPort as a means to share your data with the scientific community, please
contact our NIAID Program officers Ashley Xia (axia@niaid.nih.gov) or Quan Chen (quan.chen@nih.gov).
</p>
<p>
Data is stored at a secure hosting facility within NIAID at <a href="https://immport.niaid.nih.gov" target="_blank">immport.niaid.nih.gov</a> and remains private
until you inform our team that it is ready to be shared. At that time, the data is distributed to the wider
community via this website (<a href="http://www.immport.org" target="blank">www.immport.org</a>) and Amazon AWS.
</p>
<p>
For more information about our data upload process, please see our documentation and instructional videos below.
Need help? Please feel free to contact us by email at <a href="mailto:BISC_Helpdesk@niaid.nih.gov">BISC_Helpdesk@niaid.nih.gov</a>
and we are happy to provide a walkthrough.
</p>
<p>
<p>
To start uploading data, you will need to register for an ImmPort account to create
your private workspace 
<a href="<c:out value="${registerUrl}"></c:out>" target="_blanK">
<i class="fa fa-external-link"></i></a>.
</p>
<dt>Template Documentation</dt>
<p>
The data upload process is described in the User Guide
<a href="<c:out value="${dataUploadUserGuideUrl}"></c:out>" target="_blank">
<i class="fa fa-external-link"></i></a>.
</p>

<p>
The ImmPort templates descriptions are available in interactive form 
<a href="<c:out value="${dataUploadTemplateDescriptionInteractiveUrl}"></c:out>">here.</a>
</p>
<p>The ImmPort templates are available to download and fill 
<a href="<c:out value="${dataUploadTemplateListUrl}"></c:out>">here.</a>
</p>
<p>The ImmPort template's history is available  
<a href="<c:out value="${dataUploadTemplateHistoryUrl}"></c:out>">here.</a>
</p>
<p>
The ImmPort templates descriptions in PDF form are available
<a href="<c:out value="${dataUploadTemplateDescriptionPdfUrl}"></c:out>">here.</a>
</p>
<p>
Data Model Viewer is available 
<a href="<c:url value="/public/schema/schemaTree"/>">here.</a>
</p>
<dt>Example Data Packages</dt>
<p>The following examples of data upload packages are provided to illustrate the organization of metadata documents and associated files in a package and how to create links between entities declared in metadata documents or referenced in ImmPort.</p>
<p>The example human and animal study data upload packages are organized to highlight ImmPort's support for incremental data upload.</p>
<p>Load the study package first and then any or all of the assay result packages for that study.</p>
<br>
<ul>
  <li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_human_study_package.zip" class="buttonlink" target="_new" title="example_human_study_package.zip">example_human_study_package.zip</a></li>
     <ul>
       <li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_Gene_Expression_Affymetrix_human_study.zip" class="buttonlink" target="_new" title="example_Gene_Expression_Affymetrix_human_study.zip">example_Gene_Expression_Affymetrix_human_study.zip</a></li>
       <li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_HLA_Typing_human_study.zip" class="buttonlink" target="_new" title="example_HLA_Typing_human_study.zip">example_HLA_Typing_human_study.zip</a></li>
       <li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_MBAA_human_study.zip" class="buttonlink" target="_new" title="example_MBAA_human_study.zip">example_MBAA_human_study.zip</a></li>
       <li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_adverseEvents_human_study.zip" class="buttonlink" target="_new" title="example_adverseEvents_human_study.zip">example_adverseEvents_human_study.zip</a></li>
       <li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_assessments_human_study.zip" class="buttonlink" target="_new" title="example_assessments_human_study.zip">example_assessments_human_study.zip</a></li>
       <li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_lab_tests_human_study.zip" class="buttonlink" target="_new" title="example_lab_tests_human_study.zip">example_lab_tests_human_study.zip</a></li>
       <li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_interventions_human_study.zip" class="buttonlink" target="_new" title="example_interventions_human_study.zip">example_interventions_human_study.zip</a></li>
     </ul>
<br>
    <li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_animal_study_package.zip" class="buttonlink" target="_new" title="example_animal_study_package.zip">example_animal_study_package.zip</a></li>
    <ul>
	<li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_ELISA_results_animal_study.zip" class="buttonlink" target="_new" title="example_ELISA_results_animal_study.zip">example_ELISA_results_animal_study.zip</a></li>
    <li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_ELISPOT_results_animal_study.zip" target="_new" title="example_ELISPOT_results_animal_study.zip" class="buttonlink">example_ELISPOT_results_animal_study.zip</a></li>
    <li><a href="<c:out value="${dataUploadExamplePackagesUrl}"></c:out>/example_Flow_Cytometry_results_animal_study.zip" class="buttonlink" target="_new" title="example_Flow_Cytometry_results_animal_study.zip">example_Flow_Cytometry_results_animal_study.zip</a></li>
    </ul>
</ul>
<br> 
<dl><dt>Data Package Validator</dt>
<dd>This tool checks the format and content of the files in the data package and reports issues. </dd>
<dd>The tool is downloadable client side and executed from a graphical user interface.</dd>
<dd>The Validator is a downloadable software application that is run on your computer from a web browser.</dd>
<dd>The Data Upload Package Validator checks the format and content of the files in a data upload package and reports issues.</dd>
<dd>If there are any errors in the validation report, fix the first errors that are indicated and rerun the validation process. The errors encountered first may have effects on later files that are validated.</dd>
<dd>It is highly recommended to use the Validator before sending data into ImmPort as part of the data upload process.</dd>
<dd>To get started, click <a href="<c:out value="${dataUploadValidatorDocumentationUrl}"></c:out>" target="_new">here</a> to select the appropriate installer/uninstaller 
</dd></dl>
<dl>
<dt>Videos</dt>
<dd>
<a href="http://youtu.be/xmNPujBGaQQ" title="Introduction to ImmPort data loading, part 1 confirming access to your ImmPort project"  target="_blank">Introduction to ImmPort data loading, part 1 confirming access to your ImmPort project</a>
</dd>
<dd>
<a href="http://youtu.be/W6tqh5cHTc0" title="Introduction to ImmPort data loading, part 2 basic study design and protocols templates"  target="_blank">Introduction to ImmPort data loading, part 2 basic study design and protocols templates</a>
</dd>
<dd>
<a href="http://youtu.be/Qz25FGqxFNs" title="Introduction to ImmPort data loading, part 3 creating your zip package"  target="_blank">Introduction to ImmPort data loading, part 3 creating your zip package</a>
</dd>
<dd>
<a href="http://youtu.be/4n5DcKyACtE" title="Introduction to ImmPort data loading, part 4 installing the ImmPort validator on windows" target="_blank">Introduction to ImmPort data loading, part 4 installing the ImmPort validator on windows</a>
</dd>
<dd>
<a href="http://youtu.be/dB9dKR7eu3w" title="Introduction to ImmPort data loading, part 5 validating your data upload package" target="_blank">Introduction to ImmPort data loading, part 5 validating your data upload package</a>
</dd>
</dl>
</div>
</div>
<div class="row">
&nbsp;
</div>


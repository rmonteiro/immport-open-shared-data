<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="<c:url value="/resources/js/IP/common/ItemSet.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/js/IP/home/immport-open-home.js" />"type="text/javascript"></script>
<script src="<c:url value="/resources/external/handlebars/handlebars-v1.3.0.js" />" type="text/javascript"></script>

<script id="carousel-indicator-template" type="text/x-handlebars-template">
	<li data-target="{{ itemTarget }}" data-slide-to="{{ itemIndex }}" class="{{ itemActiveText }}"></li>
</script>
<script id="carousel-item-template" type="text/x-handlebars-template">
    <div class="item carousel-item-{{ itemIndex }} {{ itemActiveText }}">
		<div class="carousel-item-title" style="display:none;">{{{ title }}}</div>
        <div class="col-md-5 carousel-item-left">
			{{#if paragraph1}}<p class="text-justify">{{ paragraph1 }}</p>{{/if}}
			{{#if paragraph2}}<p class="text-justify">{{ paragraph2 }}</p>{{/if}}
			{{#if paragraph3}}<p class="text-justify">{{ paragraph3 }}</p>{{/if}}
			{{#if paragraph4}}<p class="text-justify">{{ paragraph4 }}</p>{{/if}}
			<p>{{{ link }}}</p>
        </div>
        <div class="col-md-7 carousel-item-right">
			{{#if image1}}<img class="carousel-item-image" src="<c:url value="/resources/images/carousel/{{ image1 }}" />" />{{/if}}
			{{#if image2}}<img class="carousel-item-image" src="<c:url value="/resources/images/carousel/{{ image2 }}" />" />{{/if}}
			{{#if image3}}<img class="carousel-item-image" src="<c:url value="/resources/images/carousel/{{ image3 }}" />" />{{/if}}
		</div>
    </div>
</script>
<script id="announcement-info-template" type="text/x-handlebars-template">
	<div class="info-block">
		<div>{{{ content }}}</div>
        {{#if display }}
		<div class="pull-right">
			<a class="announcement-link" data-type="{{ type }}" data-link="{{ link }}" data-display="{{ display }}" >
				more <i class="glyphicon glyphicon-chevron-right"></i>
			</a>
		</div>
        {{/if}}
	</div>
</script>

<script type="text/javascript">
	function preventDefaultAction(e){
		e.preventDefault();
	}

	setTimeout(function() {
    	$('#messageDiv').fadeOut('slow'); 
	}, 10000);
</script>
					
<style>
#dataTreemap {
	min-height:500px;
}
.right-info-panels {
	margin-top: 5px;
}
.right-info-panels .panel-heading {
	padding: 5px 10px 5px 10px;
}
.right-info-panels .panel-body {
	padding: 5px 10px 5px 10px;
	margin-bottom: 15px;
	max-height: 298px;
	overflow-y: scroll;
}
.right-info-panels .panel-body .info-block {
	padding: 8px 5px 23px 5px;
}
.right-info-panels .panel-body .info-block:hover {
	background-color: #f2f2f2;
}
.form-study-search {
	margin-top: 2px;
	margin-bottom: 0px;
	padding-right: 0px;
}

#facet-container .panel {
    margin-bottom: 10px;
}
#facet-container .panel-heading {
    padding: 5px 10px;
    background-color: #E8DFD7;
}
#facet-container .panel-heading .panel-title {
    font-size: 14px;
}
#facet-container .panel-body {
   	padding: 5px 10px;
}

.carousel-container {
	padding-bottom: 10px;
	min-height: 350px;
}

.applicationDiv .logo-panel .panel-link-list {
	margin-top: 20px;
	text-align: left;
	margin-left: 35px;
}
</style>

<div class="container" style="margin-top:10px;">
    
    <div class="row">
<!--     	<div class="col-md-12">
    		<div class="immport-notice" style="display:none;">
		        <div class="alert alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span></button>
					<div class="immport-notice-content"></div>
				</div>
	        </div>
	    </div>
 -->
	<c:if test="${not empty message}">
		<c:if test = "${message == 'account_verified'}">
		    <div id="messageDiv" class="alert alert-success" style="padding:5px;">
				<span class="glyphicon glyphicon-ok-sign" style="font-size:30px;"></span> &nbsp;<span style="vertical-align: 10px;">
					Thank you for activating your ImmPort account.
				</span> 
			</div>
		</c:if>
		<c:if test = "${message == 'set_password'}">
		    <div id="messageDiv" class="alert alert-success" style="padding:5px;">
				<span class="glyphicon glyphicon-ok-sign" style="font-size:30px;"></span> &nbsp;<span style="vertical-align: 10px;">
					Your password has been successfully set and your ImmPort Account has been activated.
				</span> 
			</div>
		</c:if>
		<c:if test = "${message == 'reset_password'}">
		    <div id="messageDiv" class="alert alert-success" style="padding:5px;">
				<span class="glyphicon glyphicon-ok-sign" style="font-size:30px;"></span> &nbsp;<span style="vertical-align: 10px;">
					Your password has been successfully reset. Please login with your username and new password.
				</span> 
			</div>
		</c:if>
		<c:if test = "${message == 'account_alreadyverified'}">
		    <div id="messageDiv" class="alert alert-info" style="padding:5px;">
				<span class="glyphicon glyphicon-info-sign" style="font-size:30px;"></span> &nbsp;<span style="vertical-align: 10px;">
					This user account has already been activated.
				</span> 
			</div>
		</c:if>
		<c:if test = "${message == 'password_alreadyset'}">
		    <div id="messageDiv" class="alert alert-warning" style="padding:5px;">
				<span class="glyphicon glyphicon-warning-sign" style="font-size:30px;"></span> &nbsp;<span style="vertical-align: 10px;">
					The password for this account was already set. If you want to reset your password again, please click on the Forgot Password link.
				</span> 
			</div>
		</c:if>
		<c:if test = "${message == 'password_alreadyreset'}">
		    <div id="messageDiv" class="alert alert-warning" style="padding:5px;">
				<span class="glyphicon glyphicon-warning-sign" style="font-size:30px;"></span> &nbsp;<span style="vertical-align: 10px;">
					The password for this account was already reset. If you want to reset your password again, please click on the Forgot Password link.
				</span> 
			</div>
		</c:if>
		<c:if test = "${message == 'newtokensent_accountverfication'}">
		    <div id="messageDiv" class="alert alert-info" style="padding:5px;">
				<span class="glyphicon glyphicon-info-sign" style="font-size:30px;"></span> &nbsp;<span style="vertical-align: 10px;">
					A new account activation link was sent to your email. Please contact the help desk at BISC_HELPDESK@mail.nih.gov if you have any issues.
				</span> 
			</div>
		</c:if>
		<c:if test = "${message == 'newtokensent_passwordset'}">
		    <div id="messageDiv" class="alert alert-info" style="padding:5px;">
				<span class="glyphicon glyphicon-info-sign" style="font-size:30px;"></span> &nbsp;<span style="vertical-align: 10px;">
					A new link was sent for setting your password on your email account. Please contact the help desk at BISC_HELPDESK@mail.nih.gov if you have any issues.
				</span> 
			</div>
		</c:if>
		<c:if test = "${message == 'newtokensent_passwordreset'}">
		    <div id="messageDiv" class="alert alert-info" style="padding:5px;">
				<span class="glyphicon glyphicon-info-sign" style="font-size:30px;"></span> &nbsp;<span style="vertical-align: 10px;">
					A new link was sent for password reset to your email. Please contact the help desk at BISC_HELPDESK@mail.nih.gov if you have any issues.
				</span> 
			</div>
		</c:if>
		
	</c:if>	    
	    
	    
		<div class="col-md-12">	        
    		<div class="immport-about">
    			<p>
            		<img src="/immport-open/resources/images/immport-logo.png"/>
            	</p>
			</div>
		</div>	
		<div class="col-md-9 ">            	
		    <p class="immport-banner text-justify">
	            <a href="/immport-open/">ImmPort</a> is funded by the NIH, NIAID and DAIT in support of the NIH mission to share data with the public.   Data shared through ImmPort has been provided by NIH-funded programs, other research organizations and individual scientists  ensuring these discoveries will be the foundation of future research.
	        </p>
		    <p>
                <a href="https://immport.niaid.nih.gov" target="_new"></a> 
	        </p>
	
		    <div class="row" >
		
		    	<div class="col-sm-4 applicationDiv">
		    		<div class="panel panel-default logo-panel orange-logo-panel">
						<div class="panel-heading logo-panel-heading text-center">
							<img style="height:98px;" src="/immport-open/resources/images/home/immport-icon-orange.png"/>
						</div>
						<div class="panel-body text-center">
							<span class="home-heading"><a href="<c:out value="${privateDataUrl}"></c:out>"  target="_blank">Private Data</a></span><br/>	
							<ul class="panel-link-list">
								<li><a href="<c:out value="${privateDataUrl}"></c:out>/upload/data/uploadDataMain"  target="_blank">Upload</a></li>
								<li><a href="<c:out value="${dataUploadValidatorDocumentationUrl}"></c:out>"  target="_blank">Validator</a></li>
								<li><a href="<c:out value="${privateDataUrl}"></c:out>/research/study/studysearchmain#/studysearch"  target="_blank">Search/Download</a></li>
							</ul>								
						</div>
		    		</div>		
		    	</div>	
		    	
		    	<div class="col-sm-4 applicationDiv">
		    		<div class="panel panel-default logo-panel blue-logo-panel">
						<div class="panel-heading logo-panel-heading text-center">
							<img style="height:98px;" src="/immport-open/resources/images/home/immport-icon-blue.png"/>
						</div>
						<div class="panel-body text-center">
							<span class="home-heading"><a href="<c:out value="${sharedDataStudySearchUrl}"></c:out>"  target="_blank">Shared Data</a></span><br/>
							<ul class="panel-link-list">
								<li><a href="<c:out value="${sharedDataTutorialsUrl}"></c:out>"  target="_blank">Tutorials</a></li>
								<li><a href="<c:out value="${sharedDataGeneListsUrl}"></c:out>"  target="_blank">Gene Lists</a></li>
								<li><a href="<c:out value="${sharedDataStudySearchUrl}"></c:out>"  target="_blank">Search/Download</a></li>
							</ul>
						</div>
					</div>
		    	</div>
		
		    	<div class="col-sm-4 applicationDiv">
		    		<div class="panel panel-default logo-panel green-logo-panel">
						<div class="panel-heading logo-panel-heading text-center">
							<img style="height:98px;" src="/immport-open/resources/images/home/immport-icon-green.png"/>
		 				</div>
						<div class="panel-body text-center">
							<span class="home-heading"><a href="<c:out value="${dataAnalysisUrl}"></c:out>"  target="_blank">Data Analysis</a></span><br/>
							<ul class="panel-link-list">
								<!-- <li><a href="http://immportgalaxy.org/workflow/list_published">Analysis Workflow</a></li> -->
								<li><a href="<c:out value="${dataAnalysisUrl}"></c:out>/workflow/list_published"  target="_blank">Analysis Workflow</a></li>
								<li><a href="<c:out value="${dataAnalysisUrl}"></c:out>"  target="_blank">Automated Clustering</a></li>
								<li><a href="<c:out value="${dataAnalysisTutorialsUrl}"></c:out>"  target="_blank">Tutorials</a></li>
							</ul>
						</div>
		    		</div>
		    	</div>
		    	
		    </div>
		            
	    </div>

	    <div class="col-md-3 right-info-panels" style="margin-top: 10px;">
			<div class="panel panel-default">
				<div class="panel-heading announcements">Announcements</div>
				<div class="panel-body"></div>
			</div>
	    </div> 	        
	        
    </div>
    
    <div class="row">
        <div class="col-md-12">
        
            <div class="carousel-container">
                <div id="carousel-immport-share" class="carousel slide" data-ride="carousel">
                    <div class="carousel-top-header">
                        <div class="carousel-top-title col-md-9">
                            &nbsp;<!-- Slide Title -->
                        </div>
                        <ol class="carousel-indicators col-md-3 pull-right">
                        </ol>
                    </div>
                    <div class="carousel-inner">
	                </div>
				</div>
				
				<a class="left carousel-control" href="#carousel-immport-share" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#carousel-immport-share" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right"></span>
				</a>
				
            </div>
          <br>
	    </div>
	        
	</div>
</div>

<!-- Modal box for popup news -->
<div id="newsModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Announcement</h4>
		    </div>
			<div class="modal-body">
				<iframe class="news-dialog-content" src="" frameborder="0"></iframe>
			</div>
		</div>
	</div>
</div>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';

$().ready(function() {
    // Send event for default tab, then add listener for all home page tabs.
	ga('send','event','Home Page Tabs','click','topStudies')
	$(".homeTab").click(function(){
		var tab = $(this).attr('data-value');
		ga('send','event','Home Page Tabs','click',tab)
	});
})
</script>
<script src="<c:url value='/resources/js/IP/search/facetSearch.js'/>" type="text/javascript"></script>


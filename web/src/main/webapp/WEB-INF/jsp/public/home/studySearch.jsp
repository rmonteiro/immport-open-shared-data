<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
#queryTerm {
	width: 540px;
}
.input-group-btn {
	vertical-align: top !important;
}
#schematicModalDetail {
	overflow-y: scroll;
	margin-top: 0px;
	margion-bottom: 8px;
}
#facet-container .panel {
	margin-bottom: 10px;
}
#facet-container .panel-heading {
	padding: 5px 10px;
	background-color: #E8DFD7;
}
#facet-container .panel-heading .panel-title {
	font-size: 14px;
}
#facet-container .panel-body {
	padding: 5px 10px;
}
</style>

<div class="row">
            <div class="immport-about col-sm-3">
    			<p>
            		<img style="height: 100px;" src="<%= request.getContextPath() %>/resources/images/open-immport-logo-large-text.png"/>
            	</p>
	        </div>
            
            <div class="col-sm-9">
    			<p style="margin-top:20px;">
    				<h4>What is Open ImmPort?</h4>
    			</p>
		        <p>
		       Open ImmPort is your resource for searching and downloading shared biomedical research data funded from NIAID DAIT and DMID, and other NIH and non-government funding agencies. Additional resources available at OpenImmPort include step-by-step data reuse tutorials including example R and Python analysis code, the Cell Ontology Visualizer, the Cytokine Registry, and ImmuneXpresso - the cytokine and cell interaction literature mining tool.
		       </p>
<!-- 		       <p>
               <a href="https://immport.niaid.nih.gov" target="_new">ImmPort</a>, is the data upload portal where researchers upload, QC, and curate their data prior to sharing in OpenImmPort. 
		        </p>
 -->	        </div>
</div>
<hr class="immport-hr"/>
<div class="row">
	
    <div>
        <form action="#" class="form-inline col-sm-12" role="form" style="padding:10px 0px 10px 15px;" >
				

            <div class="input-group">
                <input id="queryTerm" type="text" class="form-control" placeholder="e.g. influenza">
                <span class="input-group-btn">
                    <button id="btnSearch" class="btn btn-default" type="button">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                    <button id="btnClearQuery" class="btn btn-default" type="button">
                        <i class="glyphicon glyphicon-remove"></i>
                    </button>
                </span>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <h6 style="margin-top:20px;">Filter Option</h6>
        <div id="facet-container" class="catalog-panel" style="padding-top:5px;"></div>
    </div>
    <div class="col-md-9">
        <div id="result-toolbar-top" style="margin-top:8px;">
            <div class="col-md-6 catalog-panel">
                <span id="setting-container" class="setting-container"><i class="glyphicon glyphicon-cog"></i></span>
                <span id="result-container"></span>
            </div>
            <div id="result-pager-top" class="col-md-6 catalog-pager">
                <ul class="pagination" style="float:right;">
                    <li><a href="#" class="pager-button pager-prev">&laquo;</a></li>
                    <li><a href="#" class="pager-button pager-1 pager-active">1</a></li>
                    <li><a href="#" class="pager-button pager-2">2</a></li>
                    <li><a href="#" class="pager-button pager-3">3</a></li>
                    <li><a href="#" class="pager-button pager-4">4</a></li>
                    <li><a href="#" class="pager-button pager-5">5</a></li>
                    <li><a href="#" class="pager-button pager-next">&raquo;</a></li>
                </ul>
            </div>
        </div>
        <div id="catalog-container" class="catalog-panel"></div>
        <div id="result-pager-bottom" class="col-md-12 catalog-pager">
            <ul class="pagination" style="float:right;">
                <li><a href="#" class="pager-button pager-prev">&laquo;</a></li>
                <li><a href="#" class="pager-button pager-1 pager-active">1</a></li>
                <li><a href="#" class="pager-button pager-2">2</a></li>
                <li><a href="#" class="pager-button pager-3">3</a></li>
                <li><a href="#" class="pager-button pager-4">4</a></li>
                <li><a href="#" class="pager-button pager-5">5</a></li>
                <li><a href="#" class="pager-button pager-next">&raquo;</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="modal fade" id="dialogModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">ImmPort Open</h4>
            </div>
            <div class="modal-body">
                <form>
                    <fieldset>
                        <legend>Result Display Setting</legend>
                        <div style="margin-left:20px;">
	                        <label class="checkbox">
	                            <input type="checkbox" id="checkboxShowSummary" checked> Show summary
	                        </label>
	                        <label class="checkbox">
	                            <input type="checkbox" id="checkboxShowHighlight" checked> Show highlighted text
	                        </label>
                        </div>
                      </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary btn-next">OK</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal box for study detail -->
<div id="studyModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
         <div class="modal-title" id="studyModalHeader">
           Study <span id="studyModalStudyId"></span>
           <div class="popupHeaderToolbar">
                <button type="button" class="btn btn-sm btn-default" style="min-width:80px;" id="btnPopPrevStudy">
                    <i class="glyphicon glyphicon-chevron-left"></i> Previous
                </button>
                <button type="button" class="btn btn-sm btn-default" style="min-width:80px;" id="btnPopNextStudy">
                    <i class="glyphicon glyphicon-chevron-right"></i> Next
                </button>
                <button type="button" class="btn btn-sm btn-default" style="min-width:80px;" id="btnPopOpenStudy">
                    <i class="glyphicon glyphicon-new-window"></i> Open
                </button>
                <button type="button" class="btn btn-sm btn-default" style="min-width:80px;" id="btnPopDownloadStudy">
                    <i class="glyphicon glyphicon-save"></i> Download
                </button>
                <button type="button" class="btn btn-sm btn-default" style="min-width:80px;" data-dismiss="modal">
                    <i class="glyphicon glyphicon-remove"></i> Close
                </button>
            </div>
         </div>
      </div>
      <div class="modal-body" id="studyModalDetail">
        <p>Study detail content</p>
      </div>
    </div>
  </div>
</div>

<!-- Modal box for study schematic -->
<div id="schematicModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
         <div class="modal-title" id="schematicModalHeader">
           Study <span id="schematicModalStudyId"></span>
           <div class="popupHeaderToolbar">
                <button type="button" class="btn btn-sm btn-default" style="min-width:80px;" data-dismiss="modal">
                    <i class="glyphicon glyphicon-remove"></i> Close
                </button>
            </div>
         </div>
      </div>
      <div class="modal-body" id="schematicModalDetail">
        <p>Study schematic content</p>
      </div>
    </div>
  </div>
</div>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';

var searchTerm = '<c:out value="${searchTerm}"></c:out>';
var clinicalTrial = '<c:out value="${clinicalTrial}"></c:out>'.split(',');
var researchFocus = '<c:out value="${researchFocus}"></c:out>'.split(',');
var studyType = '<c:out value="${studyType}"></c:out>'.split(',');
var subjectSpecies = '<c:out value="${subjectSpecies}"></c:out>'.split(',');
var biosampleType = '<c:out value="${biosampleType}"></c:out>'.split(',');
var experimentMeasurementTechnique = '<c:out value="${experimentMeasurementTechnique}"></c:out>'.split(',');
var asperaDataBrowserUrl = '<c:out value="${asperaDataBrowserUrl}"></c:out>'
</script>

<link href="<c:url value='/resources/external/twitter-typeahead/typeahead-bootstrap3.css'/>" rel="stylesheet" type="text/css" />
<script src="<c:url value="/resources/external/handlebars/handlebars-v2.0.0.js" />" type="text/javascript"></script>
<script src="<c:url value='/resources/external/twitter-typeahead/typeahead.js'/>" type="text/javascript"></script>
<script src="<c:url value='/resources/js/IP/search/studySearch.js'/>" type="text/javascript"></script>

<script id="search-result-template" type="text/x-handlebars-template">
  Found {{ hitCount }} Studies in {{ qTime }} ms
</script>

<script id="study-result-template" type="text/x-handlebars-template">
  {{#each studies}}
  <div class="study-panel">
    <div class="row" style="margin-bottom:10px;">
      <div class="col-md-2">
        <span class="study-id-label">{{ studyAccession }}</span>
        <a href="{{ ../context_root }}/public/study/study/displayStudyDetail/{{ studyAccession }}" target="_blank">
            <i class="fa fa-external-link" style="padding-bottom:3px;"></i> 
        </a>
        <div class="study-download">
	      <a href="{{ ../asperaDataBrowserUrl }}{{ studyAccession }}" target="_data_browser" class="btn btn-action">Download</a>
	    </div>
      </div>
      <div class="col-md-10" style="white-space:normal;">
        <span class="study-title" id="{{studyAccession}}">{{ briefTitle }}</span>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9">
        <div class="study-description muted" style="white-space:normal;">
          {{{ briefDescription }}}
        </div>
        <div class="study-highlight" style="white-space:normal;">
          {{#each highlighting}}
          <tpl for="highlighting">
            ...{{{ . }}}...<br/>
          {{/each}}
        </div>
        <div class="study-highlight" style="white-space:normal;">
          {{#each personnel}}
            {{ . }}<br/>
          {{/each}}
        </div>
      </div>
      <div class="col-md-3">
        <div class="study-graph">
          <img src="{{ ../context_root }}/public/study/study/getStudyImage?studyAccession={{ studyAccession }}" class="study-graph-thumbview" />
        </div>
      </div>
    </div>
  </div>
  {{/each}}
</script>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<style>
.fa-calendar {
	padding-right:8px;
}
.release-info-head {
	margin-top:20px;
}

.release-info-row {
	margin-top:20px;
	margin-bottom:30px;
}
.release-info-block {
    margin-left:20px; 
    margin-top:20px;
}
.release-pdf-link {
	padding-left:8px;
}
</style>

<div class="row release-info-head">
    <div class="col-md-12">
        <h3>Study Data Release</h3>
    </div>
</div>

<div class="row release-info-row">
    <div class="col-md-12">
        <h4><i class="fa fa-calendar"></i>June 2014</h4>
    </div>
    <div class="col-md-12">
    	<div class="release-info-block">
	        <a href="https://immport.niaid.nih.gov/immportWeb/home/display.do?content=DataRelease10" title="Data Release Notes 10">
	        	Data Release Notes 10
	        </a>
	        <a href="https://immport.niaid.nih.gov/docs/DataReleaseNotes/ImmPort_Release10_summaryv1-1.pdf" target="new">
	        	<span class="release-pdf-link">
	        		<i class="fa fa-file-pdf-o"></i>
	        	</span>
	        </a>
        </div>
    </div>
</div>

<div class="row release-info-row">
    <div class="col-md-12">
        <h4><i class="fa fa-calendar"></i>April 2014</h4>
    </div>
    <div class="col-md-12">
    	<div class="release-info-block">
	        <a href="https://immport.niaid.nih.gov/immportWeb/home/display.do?content=DataRelease9" title="Data Release Notes 9">
	        	Data Release Notes 9
	        </a>
	        <a href="https://immport.niaid.nih.gov/docs/DataReleaseNotes/ImmPort_Release9_summaryv1.pdf" target="new">
	        	<span class="release-pdf-link">
	        		<i class="fa fa-file-pdf-o"></i>
	        	</span>
	        </a>
        </div>
    </div>
    <div class="col-md-12">
    	<div class="release-info-block">
	        <a href="https://immport.niaid.nih.gov/immportWeb/home/display.do?content=DataRelease8" title="Data Release Notes 8">
	        	Data Release Notes 8
	        </a>
	        <a href="https://immport.niaid.nih.gov/docs/DataReleaseNotes/ImmPort_Release8_summaryv3.pdf" target="new">
	        	<span class="release-pdf-link">
	        		<i class="fa fa-file-pdf-o"></i>
	        	</span>
	        </a>
        </div>
    </div>
</div>

<div class="row release-info-row">
    <div class="col-md-12">
        <h4><i class="fa fa-calendar"></i>January 2014</h4>
    </div>
    <div class="col-md-12">
    	<div class="release-info-block">
	        <a href="https://immport.niaid.nih.gov/immportWeb/home/display.do?content=DataRelease7" title="Data Release Notes 7">
	        	Data Release Notes 7
	        </a>
	        <a href="https://immport.niaid.nih.gov/docs/DataReleaseNotes/ImmPort_Release7_summaryv2-1.pdf" target="new">
	        	<span class="release-pdf-link">
	        		<i class="fa fa-file-pdf-o"></i>
	        	</span>
	        </a>
        </div>
    </div>
</div>

<div class="row release-info-row">
    <div class="col-md-12">
        <h4><i class="fa fa-calendar"></i>June 2014</h4>
    </div>
    <div class="col-md-12">
    	<div class="release-info-block">
	        <a href="https://immport.niaid.nih.gov/immportWeb/home/display.do?content=DataRelease6" title="Data Release Notes 6">
	        	Data Release Notes 6
	        </a>
	        <a href="https://immport.niaid.nih.gov/docs/DataReleaseNotes/ImmPort_Release6_summaryv3.pdf" target="new">
	        	<span class="release-pdf-link">
	        		<i class="fa fa-file-pdf-o"></i>
	        	</span>
	        </a>
        </div>
    </div>
</div>

<div class="row release-info-row">
    <div class="col-md-12">
        <h4><i class="fa fa-calendar"></i>November 2013</h4>
    </div>
    <div class="col-md-12">
    	<div class="release-info-block">
	        <a href="https://immport.niaid.nih.gov/immportWeb/home/display.do?content=DataRelease5" title="Data Release Notes 5">
	        	Data Release Notes 5
	        </a>
	        <a href="https://immport.niaid.nih.gov/docs/DataReleaseNotes/ImmPort_Release5_summaryv2.pdf" target="new">
	        	<span class="release-pdf-link">
	        		<i class="fa fa-file-pdf-o"></i>
	        	</span>
	        </a>
        </div>
    </div>
</div>

<div class="row release-info-row">
    <div class="col-md-12">
        <h4><i class="fa fa-calendar"></i>September 2013</h4>
    </div>
    <div class="col-md-12">
    	<div class="release-info-block">
	        <a href="https://immport.niaid.nih.gov/immportWeb/home/display.do?content=DataRelease4" title="Data Release Notes 4">
	        	Data Release Notes 4
	        </a>
	        <a href="https://immport.niaid.nih.gov/docs/DataReleaseNotes/ImmPort_Release4_summaryv1.pdf" target="new">
	        	<span class="release-pdf-link">
	        		<i class="fa fa-file-pdf-o"></i>
	        	</span>
	        </a>
        </div>
    </div>
</div>

<div class="row release-info-row">
    <div class="col-md-12">
        <h4><i class="fa fa-calendar"></i>June 2014</h4>
    </div>
    <div class="col-md-12">
    	<div class="release-info-block">
	        <a href="https://immport.niaid.nih.gov/immportWeb/home/display.do?content=DataRelease3" title="Data Release Notes 3">
	        	Data Release Notes 3
	        </a>
	        <a href="https://immport.niaid.nih.gov/docs/DataReleaseNotes/ImmPort_Release3_summaryFinal.pdf" target="new">
	        	<span class="release-pdf-link">
	        		<i class="fa fa-file-pdf-o"></i>
	        	</span>
	        </a>
        </div>
    </div>
</div>

<div class="row release-info-row">
    <div class="col-md-12">
        <h4><i class="fa fa-calendar"></i>June 2013</h4>
    </div>
    <div class="col-md-12">
    	<div class="release-info-block">
	        <a href="https://immport.niaid.nih.gov/immportWeb/home/display.do?content=DataRelease2" title="Data Release Notes 2">
	        	Data Release Notes 2
	        </a>
	        <a href="https://immport.niaid.nih.gov/docs/DataReleaseNotes/ImmPort_Release2_summary.pdf" target="new">
	        	<span class="release-pdf-link">
	        		<i class="fa fa-file-pdf-o"></i>
	        	</span>
	        </a>
        </div>
    </div>
</div>

<div class="row release-info-row">
    <div class="col-md-12">
        <h4><i class="fa fa-calendar"></i>April 2013</h4>
    </div>
    <div class="col-md-12">
    	<div class="release-info-block">
	        <a href="https://immport.niaid.nih.gov/immportWeb/home/display.do?content=DataRelease1" title="Data Release Notes 1">
	        	Data Release Notes 1
	        </a>
	        <a href="https://immport.niaid.nih.gov/docs/DataReleaseNotes/ImmPort_Release1_summary.pdf" target="new">
	        	<span class="release-pdf-link">
	        		<i class="fa fa-file-pdf-o"></i>
	        	</span>
	        </a>
        </div>
    </div>
</div>

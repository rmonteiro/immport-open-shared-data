<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row" style="margin-top:20px;">
    <div class="col-md-10 col-md-offset-1">
        <h2>Navigation</h2>
    </div>
</div>

<div class="row">
  <div class="col-md-10 col-md-offset-1">
  <p>
  The ImmPort home page presents study
  access or study search options.
  </p>
  <p>
  <img src="<c:url value='/resources/images/help/Help_18.png'/>" class="img-responsive" title="Featured Studies"/>
  <br>
  <strong>Study Filter</strong>, a search feature, presents a comprehensive search display for refining study data searches. 
  As category selections are made counts for each reflect the available number of studies fitting the criteria.
  </p><br>
  <img src="<c:url value='/resources/images/help/Help_19.png'/>" class="img-responsive" title="Study Filter"/>
  <p style="margin-top:20px;">
  The free-text search field is combined with Auto-Suggest which begins returning results as you type.  Possible search-term matches
  are displayed in a drop-down list.  While ImmPort does not strictly support ID searches, string matching with PubMed IDs will
  return studies with that specific numeric string.
  </p>
  <img src="<c:url value='/resources/images/help/Help_20.png'/>" class="img-responsive" title="Study Search"/>
  <p style="margin-top:20px;">
  </div>
</div>
<div class="row">
  <div class="col-md-10 col-md-offset-1">
  &nbsp;
  </div>
</div>

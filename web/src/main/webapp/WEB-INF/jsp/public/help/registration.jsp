<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row" style="margin-top:20px;">
    <div class="col-md-10 col-md-offset-1">
        <h2>Registration</h2>
    </div>
</div>

<div class="row">
  <div class="col-md-10 col-md-offset-1">
  <p>
  <strong>Why get an ImmPort account?</strong>
  </p>
  <p>
  You won't need an ImmPort account to search for compelling studies, peruse study demographics, interventions and mechanistic assays. 
  But why stop there?  What you really want to do is download the study, look at each experiment in detail including individual
  ELISA results and flow cytometry files.  Perhaps you want to take those flow cytometry files for a test drive using FLOCK in the
  ImmPort flow cytometry module.  To download all that interesting data you will need to register for ImmPort access.
  </p>
  <p>
  Good news getting an account is simple and quick!  To register:
  <ul>
  <li>
  Go to <a href="https://immport.niaid.nih.gov" target="_blank">https://immport.niaid.nih.gov</a>
  </li>
  <li>
  Click 'Sign up Free' in upper right corner
  </li>
  </ul>
  </p>
  
  </div>
</div>

<div class="row">
  <div class="col-md-10 col-md-offset-1">
  <img src="<c:url value='/resources/images/help/Help_12.png'/>" class="img-responsive" title="Home Page Overview"/>
  <p style="margin-top:20px;">
  <ul>
  <li>
  At Register User: Notice page read through the text explaining registration, then click Continue
  </li>
  </ul>
  </p>
  <img src="<c:url value='/resources/images/help/Help_13.png'/>" class="img-responsive" title="Home Page Toolbar"/>
  <p style="margin-top:20px;">
  <ul>
  <li>
  Click 'Continue'
  </li>
  </ul>
  </p>
  <p style="margin-top:20px;">
  <ul>
  <li>
   From the final screen provide User details in the available fields and click 'Register'. You will receive an email
   informing you about your ImmPort account.  Congratulations!
  </li>
  </ul>
  </p>
  <img src="<c:url value='/resources/images/help/Help_14.png'/>" class="img-responsive" title="Home Page Toolbar"/>
  </div>
</div>

<div class="row">
  <div class="col-md-10 col-md-offset-1">
  &nbsp;
  </div>
</div>

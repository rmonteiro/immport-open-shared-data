<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
table.dataTable tbody th.dt-body-right,
table.dataTable tbody td.dt-body-right {
  text-align: right;
}
</style>

<div class="container">
  <h5 class="heading">Adverse Event Summary</h5>
  <div id="adEvtByArmGrid">
  	<div class="col-md-12">
      <table id="adEvtByArmTable" class="table table-hover table-bordered table-condensed table-responsive"></table>
    </div>
  </div>
  <hr>
  <h5 class="heading">Adverse Event Detail</h5>
  <div id="adEvtGrid">
    <table id="adEvtTable" class="table table-striped table-hover table-bordered table-condensed table-responsive"></table>
  </div>
  <br>
  <div>
    <span id="legend">
      <c:forEach items="${armorcohorts}" var="armorcohort">    
        &nbsp;${armorcohort.armAccession} = ${armorcohort.name}<br>
      </c:forEach>
    </span>
  </div>
</div>


<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var studyAccession = '<c:out value="${studyAccession}"></c:out>';

var displayAdEvtByArmTable = function() {
	var searchURL = CONTEXT_ROOT + '/public/clinical/adverseEvent/getAdverseEventSummaryByArmNew/' + studyAccession + '?start=0&limit=10000';
	var table = $('#adEvtByArmTable');
	var lastIdx = null;
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "json",
		"success": function(json) {
			// Right align the number columns
			var numColumns = json.metaData.columns.length;
			var targets = [];
			for (var i = 1; i < numColumns; i++) {
				targets.push(i);
			}
			var dTable = table.DataTable({
				"columns": json.metaData.columns,
				"columnDefs": [
				    { "targets": targets,"className": "dt-body-right"}
				],
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"colResize": {
		            "handleWidth": 20
		        },
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollX": true,
				"data": json.data
			});
			$('#adEvtByArmTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			console.log("Failure");		
		}	
	});
}

var displayAdEvtTable = function() { 
	var searchURL = CONTEXT_ROOT + '/public/clinical/adverseEvent/getAdverseEventDetailByStudyNew/' + studyAccession + '?start=0&limit=10000&sort=lkAdverseEventSeverity.sorOrder&dir=DESC';
	var table = $('#adEvtTable');
	var lastIdx = null;
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "json",
		"success": function(json) {
			var numColumns = json.metaData.columns.length;
			var targets = [];
			for (var i = 2; i < numColumns; i++) {
				targets.push(i);
			}
			var dTable = table.DataTable({
				"columns": json.metaData.columns,
				"columnDefs": [
				    { "targets": targets,"className": "dt-body-right"}
				],
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"colResize": {
		            "handleWidth": 20
		        },
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollX": true,
				"data": json.data
			});
			$('#adEvtTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			console.log("Failure");		
		}	
	});
}
$().ready(function() {
	displayAdEvtByArmTable();
	displayAdEvtTable();
});

</script>

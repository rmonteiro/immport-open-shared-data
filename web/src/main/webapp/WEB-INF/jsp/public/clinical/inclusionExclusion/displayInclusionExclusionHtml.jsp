<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

  <table class="detail">
  <c:forEach items="${inclusionExclusions}" var="inclusionExclusion">
    <tr>
      <td class="detailCol1">${inclusionExclusion.criterionCategory}</td>
      <td class="detailCol2">${inclusionExclusion.criterion}</td>
    </tr>
  </c:forEach>  
  </table>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
table.dataTable tbody th.dt-body-right,
table.dataTable tbody td.dt-body-right {
  text-align: right;
}
</style>

<div class="container">
  <h5 class="heading">Clinical Lab Test Panels</h5>
  <div id="labTestGrid">
    <table id="labTestTable" class="table table-striped table-hover table-bordered table-condensed table-responsive"></table>
  </div>
  <hr>
  <h5 class="heading">Clinical Lab Tests</h5>
  <div id="labTestCompGrid">
    <table id="labTestCompTable" class="table table-striped table-hover table-bordered table-condensed table-responsive"></table>
  </div>
  <br>
  <div>
    <span id="legend">
      <c:forEach items="${armorcohorts}" var="armorcohort">    
        &nbsp;${armorcohort.armAccession} = ${armorcohort.name}<br>
      </c:forEach>
    </span>
  </div>
</div>


<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var studyAccession = '<c:out value="${studyAccession}"></c:out>';

var displayLabTestTable = function() {
	var searchURL = CONTEXT_ROOT + '/public/clinical/labTest/getLabTestPanelSummaryByStudyNew/' + studyAccession + '?start=0&limit=10000';
	var table = $('#labTestTable');
	var lastIdx = null;
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "json",
		"success": function(json) {
			// Right align the number columns
			var numColumns = json.metaData.columns.length;
			var targets = [];
			for (var i = 2; i < numColumns; i++) {
				targets.push(i);
			}
			var dTable = table.DataTable({
				"columns": json.metaData.columns,
				"columnDefs": [
				    { "targets": targets,"className": "dt-body-right"}
				],
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"colResize": {
		            "handleWidth": 20
		        },
		        "scrollX": true,
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"data": json.data
			});
			$('#labTestTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			console.log("Failure");		
		}	
	});
}

var displayLabTestCompTable = function() {  
	var searchURL = CONTEXT_ROOT + '/public/clinical/labTest/getLabTestComponentListByStudyNew/' + studyAccession + '?start=0&limit=10000&sort=nameReported&dir=ASC';
	var table = $('#labTestCompTable');
	var lastIdx = null;
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "json",
		"success": function(json) {
			var dTable = table.DataTable({
				"columns": json.metaData.columns,
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"colResize": {
		            "handleWidth": 20
		        },
		        "scrollX": true,
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"data": json.data
			});
			$('#labTestCompTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			console.log("Failure");		
		}	
	});
}
$().ready(function() {
	displayLabTestTable();
	displayLabTestCompTable();
});

</script>
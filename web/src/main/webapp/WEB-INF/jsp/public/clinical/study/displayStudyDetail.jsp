<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="<c:url value="/resources/js/IP/clinical/assessment/Assessment.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/js/IP/clinical/inclusionExclusion/InclusionExclusion.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/extjs4/RowExpander.js" />" type="text/javascript"></script>

<div id="panel0" class="panel" style="display:block;">
<div style="display: block;" id="detail" class="plaintext">
  <table width="100%" cellspacing="0" cellpadding="0">                      
    <tr>
      <td height="4" colspan="2"><img src="<c:url value="/resources/images/spacer.jpg" />" height="4" border="0" alt=""></td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%">Study Title:</td>
      <td class="dataManagTd">${study.briefTitle}&nbsp;</td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%">Study PI:</td>
      <td class="dataManagTd">&nbsp; </td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Study Type:</td>
      <td class="dataManagTd">${study.type}&nbsp;</td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Condition Studied:</td>
      <td class="dataManagTd">${study.conditionStudied}&nbsp;</td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Brief Description:</td>
      <td class="dataManagTd">${study.briefDescription}&nbsp;</td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Start Date:</td>
      <td class="dataManagTd">${study.actualStartDate}&nbsp;</td>
    </tr>
    <tr>
     <td class="dataManagTd" width="25%" align="left" valign="top">Schematic:</td>
     <td style="color:#cc541f;" class="dataManagTd"><a href="#" onclick="displaySchematic('${study.studyAccession}');return false;">Schematic Image&nbsp;</a></td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Detailed Description:</td>
      <td class="dataManagTd"><a href="#" onclick="displayDescription('${studyAccession}');return false;">${study.shortDescription}</a></td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Objectives:</td>
      <td class="dataManagTd"><a href="#" onclick="displayObjectives('${studyAccession}');return false;">${study.shortObjectives}</a></td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Endpoints:</td>
      <td class="dataManagTd"><a href="#" onclick="displayEndpoints('${studyAccession}');return false;">${study.shortEndpoints}</a></td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Gender Included:</td>
      <td class="dataManagTd">${study.genderIncluded}&nbsp;</td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Subjects Number:</td>
      <td class="dataManagTd">${study.actualEnrollment}&nbsp;</td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Study Summary:</td>
      <td align="left" class="dataManagTd">&nbsp;</td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Study Download Packages:</td>
      <td align="left" class="dataManagTd">&nbsp;</td>
    </tr>
    <tr>
      <td class="dataManagTd" width="25%" align="left" valign="top">Contract/Grant:</td>
      <td align="left" class="dataManagTd">&nbsp;</td>
    </tr>   
  </table>
  <br>
  <div><h4>Assessments</h4></div>
  <div id="assessmentGrid"></div>
  <div><h4>Inclusion Exclusion</h4></div>
  <div id="inclusionExclusionGrid"></div>
</div>
</div>

<div id="studyDescription"></div>

<script>

displaySchematic = function(studyAccession) {
    var imageUrl = '<img src=' + '<c:url value="/public/clinical/study/getStudyImage?studyAccession="/>' + studyAccession
                    + ' height="690" width="690" />';
    Ext.create('Ext.window.Window',{
        width: 700,
        height: 700,
        constrain: true,
        model: true,
        resizable: false,
        draggable: false,
        title: "Study Schematic",
        items: [
            {
                xtype: 'panel',
                html: imageUrl
            }
        ]
    }).show();
    
}

displayDescription = function(studyAccession) {
    var descriptionUrl = '<c:url value="/public/clinical/study/getStudyDescription?studyAccession="/>' + studyAccession;
    Ext.create('Ext.window.Window',{
        width: 700,
        height: 700,
        constrain: true,
        model: true,
        resizable: false,
        draggable: false,
        title: "Study Description",
        items: [
            {
                xtype: 'panel',
                height: 690,
                width: 690,
                html: '<table width="99%" cellpadding=0 cellspacing=0 border="0">' +
                      '<tr><td><h2 style="font-size: 1.8em;">Study Detailed Description: </h2></td></tr>' +
                      '<tr><td><br/><div class="descriptionDiv" id="descriptionDiv"></div></td></tr></table>'
            }
        ]
    }).show();
    
    var description = Ext.get('descriptionDiv');
    description.load({
        url: descriptionUrl
    });    
}

displayObjectives = function(studyAccession) {
    var url = '<c:url value="/public/clinical/study/getStudyObjectives?studyAccession="/>' + studyAccession;
    Ext.create('Ext.window.Window',{
        width: 700,
        height: 700,
        constrain: true,
        model: true,
        resizable: false,
        draggable: false,
        title: "Study Objectives",
        items: [
            {
                xtype: 'panel',
                height: 690,
                width: 690,
                html: '<table width="99%" cellpadding=0 cellspacing=0 border="0">' +
                      '<tr><td><h2 style="font-size: 1.8em;">Study Objectives: </h2></td></tr>' +
                      '<tr><td><br/><div class="descriptionDiv" id="objectivesDiv"></div></td></tr></table>'
            }
        ]
    }).show();
    
    var el = Ext.get('objectivesDiv');
    el.load({
        url: url
    });    
}

displayEndpoints = function(studyAccession) {
    var url = '<c:url value="/public/clinical/study/getStudyEndpoints?studyAccession="/>' + studyAccession;
    Ext.create('Ext.window.Window',{
        width: 700,
        height: 700,
        constrain: true,
        model: true,
        resizable: false,
        draggable: false,
        title: "Study Endpoints",
        items: [
            {
                xtype: 'panel',
                height: 690,
                width: 690,
                html: '<table width="99%" cellpadding=0 cellspacing=0 border="0">' +
                      '<tr><td><h2 style="font-size: 1.8em;">Study Endpoints: </h2></td></tr>' +
                      '<tr><td><br/><div class="descriptionDiv" id="endpointsDiv"></div></td></tr></table>'
            }
        ]
    }).show();
    
    var el = Ext.get('endpointsDiv');
    el.load({
        url: url
    });    
}

Ext.onReady(function() {
    var studyAccession = "${study.studyAccession}";
    //var assessmentGrid = Ext.create("IP.clinical.assessment.AssessmentGrid",{ studyAccession: studyAccession, width: 920, height: 260, displayType: 'table'});
    //assessmentGrid.render('assessmentGrid');
    //var inclusionExclusionGrid = Ext.create("IP.clinical.inclusionExclusion.InclusionExclusionGrid",{ studyAccession: studyAccession, width: 920, height: 260, displayType: 'table'});
    //inclusionExclusionGrid.render('inclusionExclusionGrid');
    
});
</script>

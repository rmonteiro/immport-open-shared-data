<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="<c:url value="/resources/js/IP/common/DynamicGrid.js" />" type="text/javascript"></script>

Welcome to the Concomitant Medications - ${studyAccession}

<div id="conMedGrid"></div>

<script>
Ext.onReady(function() {
    var url = "../getConcomitantMedicationsByArm/${studyAccession}";
    var conMedGrid = Ext.create("IP.common.DynamicGrid",{ url: url, width: 600, height: 300, gridDiv: 'conMedGrid'});
    conMedGrid.render('conMedGrid');
 
});
</script>
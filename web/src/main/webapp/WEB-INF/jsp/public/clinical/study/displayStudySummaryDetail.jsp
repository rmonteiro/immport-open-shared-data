<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="<c:url value="/resources/js/IP/clinical/study/Study.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/extjs4/RowExpander.js" />" type="text/javascript"></script>

Welcome to the Study Summary Detail

<div id="studyGrid"></div>

<script>
Ext.onReady(function() {
    var studyGrid = Ext.create("IP.clinical.study.StudyGrid",{ width: 940, height: 600, displayType: 'detail'});
    studyGrid.render('studyGrid');
});
</script>

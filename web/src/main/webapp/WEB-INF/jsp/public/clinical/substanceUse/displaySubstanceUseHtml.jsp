<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<style>
table.dataTable tbody th.dt-body-right,
table.dataTable tbody td.dt-body-right {
  text-align: right;
}
</style>

<div class="container">
  <h5 class="heading">Substance Use Summary</h5>
  <div id="subUseByArmGrid">
    <table id="subUseByArmTable" class="table table-striped table-hover table-bordered table-condensed table-responsive"></table>
  </div>
  <hr>
  <h5 class="heading">Substance Use Detail</h5>
  <div id="subUseGrid">
    <table id="subUseTable" class="table table-striped table-hover table-bordered table-condensed table-responsive"></table>
  </div>
  <br>
  <div>
    <span id="legend">
      <c:forEach items="${armorcohorts}" var="armorcohort">    
        &nbsp;${armorcohort.armAccession} = ${armorcohort.name}<br>
      </c:forEach>
    </span>
  </div>
</div>


<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';

var studyAccession = '<c:out value="${studyAccession}"></c:out>';

var displaySubUseByArmTable = function() {
	var searchURL = CONTEXT_ROOT + '/public/clinical/substanceUse/getSubstanceUseSummaryByStudyNew/' + studyAccession;
	var table = $('#subUseByArmTable');
	var lastIdx = null;
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "json",
		"success": function(json) {
			// Right align the number columns
			var numColumns = json.metaData.columns.length;
			var targets = [];
			for (var i = 1; i < numColumns; i++) {
				targets.push(i);
			}
			var dTable = table.DataTable({
				"columns": json.metaData.columns,
				"columnDefs": [
				    { "targets": targets,"className": "dt-body-right"}
				],
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"colResize": {
		            "handleWidth": 20
		        },
		        "scrollX": true,
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"data": json.data
			});
			$('#subUseByArmTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			console.log("Failure");		
		}	
	});
}

var displaySubUseTable = function() {    
	var searchURL = CONTEXT_ROOT + '/public/clinical/substanceUse/getSubstanceUseDetailByStudyNew/' + studyAccession + '?start=0&limit=10000&sort=compoundNameReported&dir=ASC';
	var table = $('#subUseTable');
	var lastIdx = null;
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "json",
		"success": function(json) {
			var numColumns = json.metaData.columns.length;
			var targets = [];
			for (var i = 1; i < numColumns; i++) {
				targets.push(i);
			}
			var dTable = table.DataTable({
				"columns": json.metaData.columns,
				"columnDefs": [
							    { "targets": targets,"className": "dt-body-right"}
							],
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"colResize": {
		            "handleWidth": 20
		        },
		        "scrollX": true,
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"data": json.data
			});
			$('#subUseTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			console.log("Failure");		
		}	
	});
}
$().ready(function() {
	displaySubUseByArmTable();
	displaySubUseTable();
});

</script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

  <table class="detail">
  <tr><th colspan="8" class="detail">Publications</th></tr>
    <tr class="detail">
        <th scope="col" class="detail">Pubmed Id</th>
        <th scope="col" class="detail">Title</th>
        <th scope="col" class="detail">Journal</th>
        <th scope="col" class="detail">Year</th>
        <th scope="col" class="detail">Month</th>
        <th scope="col" class="detail">Issue</th>
        <th scope="col" class="detail">Pages</th>
        <th scope="col" class="detail">Authors</th>
    </tr>
  <c:forEach items="${studyPubmeds}" var="studyPubmed">
    <tr>
      <td class="detailCol2">${studyPubmed.id.pubmedId}</td>
      <td class="detailCol2">${studyPubmed.title}</td>
      <td class="detailCol2">${studyPubmed.journal}</td>
      <td class="detailCol2">${studyPubmed.year}</td>
      <td class="detailCol2">${studyPubmed.month}</td>
      <td class="detailCol2">${studyPubmed.issue}</td>
      <td class="detailCol2">${studyPubmed.pages}</td>
      <td class="detailCol2">${studyPubmed.authors}</td>
    </tr>
  </c:forEach>
  </table>
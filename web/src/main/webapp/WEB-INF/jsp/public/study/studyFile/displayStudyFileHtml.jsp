<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<div >
<h5 id="heading">Study Files</h5>
</div>
	  
	   
 	<table class="detail"> 
    <tr class="detail">
        <th scope="col" class="detail">File Name</th>
        <th scope="col" class="detail">File Type</th>
        <th scope="col" class="detail">Description</th>        
    </tr>
  <c:forEach items="${studyfiles}" var="studyfile">
    <tr>
     <td class="detailCol2">${studyfile.fileName}</td> 
      <td class="detailCol2">${studyfile.studyFileType}</td>
      <td class="detailCol2">${studyfile.description}</td>
    </tr>
  </c:forEach>
  </table>
  
  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
table.dataTable tbody th.dt-body-right,
table.dataTable tbody td.dt-body-right {
  text-align: right;
}
</style>


<div><h5 class="heading">Arms or Cohorts</h5></div>
<table class="detail">
<tr class="detail">
        <th scope="col" class="detail">Accession</th>      
        <th scope="col" class="detail">Name</th>
        <th scope="col" class="detail">Description</th>
</tr>   
<c:forEach items="${studyArm.armOrCohorts}" var="arm" >
    <tr>
    	<td class="detailCol2">${arm.armAccession}</td>      	
      	<td class="detailCol2">${arm.name}</td>
      	<td class="detailCol2">${arm.description}</td>
    </tr>
</c:forEach>
</table>

<c:if test="${not empty inclusionExclusions}">  
<br>

	  <div><h5 class="heading">Inclusion Exclusion Criteria</h5></div>



  <table class="detail">
  <tr class="detail">
        <th scope="col" class="detail">Criteria Category</th>
        <th scope="col" class="detail">Criteria</th>      
</tr>  
  <c:forEach items="${inclusionExclusions}" var="inclusionExclusion">
    <tr>
      <td class="detailCol1">${inclusionExclusion.criterionCategory}</td>
      <td class="detailCol2">${inclusionExclusion.criterion}</td>
    </tr>
  </c:forEach>
  </table>
 </c:if>
 
 <c:if test="${empty inclusionExclusions}">  
<br>
	  <div><h5 class="heading">No Inclusion Exclusion Criteria</h5></div> 
</c:if>
 
 <c:if test="${not empty protocols}">    
<br>
	  <div><h5 class="heading">Documentation</h5></div>

   <table class="detail">  
    <tr class="detail">
        <th scope="col" class="detail">Type</th>
        <th scope="col" class="detail">Name</th>
        <th scope="col" class="detail">File Name</th>
        <th scope="col" class="detail">Description</th>
    </tr>
  <c:forEach items="${protocols}" var="protocol">
    <tr>
      <td class="detailCol2">${protocol.type}</td>
      <td class="detailCol2">${protocol.name}</td>
      <td class="detailCol2"><a href="<%= request.getContextPath() %>/public/research/protocol/getProtocolFile/${protocol.protocolAccession}">${protocol.fileName}</a></td>
      <td class="detailCol2">${protocol.description}</td>
    </tr>
  </c:forEach>
  </table>
   </c:if>
   
   
<c:if test="${empty protocols}">  
<br>
	  <div><h5 class="heading">No Documentation</h5></div> 
</c:if>

<div style="color: blue;">
<br>
<h5 class="heading">Planned Visit Data</h5>
</div>

<c:if test="${not empty plannedVisitDisplayLegends}">    
<br>	 
<div>
<span id="legend1">
  <table>  
    <tr >
        <th class="detail">Planned Visit Accession</th>
        <th class="detail" >Visit Name</th>
        <th class="detail">Range (Days)</th>
       <!-- <th class="detail">Calculated Bin</th> -->
    </tr>
  <c:forEach items="${plannedVisitDisplayLegends}" var="plannedVisitDisplayLegend">
    <tr>
      <td class="detailCol2">${plannedVisitDisplayLegend.plannedVisitAccession}</td>
      <td class="detailCol2">${plannedVisitDisplayLegend.visitName}</td>
      <td class="detailCol2">${plannedVisitDisplayLegend.originalBinRange}</td>
      <!--  <td class="detailCol2">${plannedVisitDisplayLegend.calculatedBinRange}</td> -->
    </tr>
  </c:forEach>
  </table>
</span>
</div>
</c:if>

<div class="container">
  <h5 class="heading">Planned Visits By Experiments</h5>
  <div id="plannedVisitByExpGrid">
    <table id="plannedVisitByExpTable" class="table table-striped table-hover table-bordered table-condensed table-responsive"></table>
  </div>
  <hr>
  <h5 class="heading">Planned Visits By Lab Tests</h5>
  <div id="plannedVisitByLabTestGrid">
    <table id="plannedVisitByLabTestTable" class="table table-striped table-hover table-bordered table-condensed table-responsive"></table>
  </div>
  <hr>
  <h5 class="heading">Planned Visits By Assessments</h5>
  <div id="plannedVisitByAssessmentGrid">
    <table id="plannedVisitByAssessmentTable" class="table table-striped table-hover table-bordered table-condensed table-responsive"></table>
  </div>
  <hr>
</div>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var studyAccession = '<c:out value="${studyAccession}"></c:out>';

var displayPlannedVisitByExpTable = function() {
	var searchURL = CONTEXT_ROOT + '/public/study/studyDesign/getPlannedVisitsByExperiment/' + studyAccession + '?start=0&limit=10000';
	var table = $('#plannedVisitByExpTable');
	var lastIdx = null;
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "json",
		"success": function(json) {
			// Right align the number columns
			var numColumns = json.metaData.columns.length;
			var targets = [];
			for (var i = 2; i < numColumns; i++) {
				targets.push(i);
			}
			var dTable = table.DataTable({
				"columns": json.metaData.columns,
				"columnDefs": [
				    { "targets": targets,"className": "dt-body-right"}
				],
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"colResize": {
		            "handleWidth": 20
		        },
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollX": true,
				"data": json.data
			});
			$('#plannedVisitByExpTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			var msg="No Planned Visit Data is available for Study Time Collected By Experiment";
			$('#plannedVisitByExpGrid').append(msg);	
		}	
	});
}

var displayPlannedVisitByLabTestTable = function() {
	var searchURL = CONTEXT_ROOT + '/public/study/studyDesign/getPlannedVisitsByLabTest/' + studyAccession + '?start=0&limit=10000';
	var table = $('#plannedVisitByLabTestTable');
	var lastIdx = null;
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "json",
		"success": function(json) {
			// Right align the number columns
			var numColumns = json.metaData.columns.length;
			var targets = [];
			for (var i = 1; i < numColumns; i++) {
				targets.push(i);
			}
			var dTable = table.DataTable({
				"columns": json.metaData.columns,
				"columnDefs": [
				    { "targets": targets,"className": "dt-body-right"}
				],
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"colResize": {
		            "handleWidth": 20
		        },
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollX": true,
				"data": json.data
			});
			$('#plannedVisitByLabTestTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			var msg="No Planned Visit Data is available for Study Time Collected By Lab Test";
			$('#plannedVisitByLabTestGrid').append(msg);	
		}	
	});
}

var displayPlannedVisitByAssessmentTable = function() {
	var searchURL = CONTEXT_ROOT + '/public/study/studyDesign/getPlannedVisitsByAssessment/' + studyAccession + '?start=0&limit=10000';
	var table = $('#plannedVisitByAssessmentTable');
	var lastIdx = null;
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "json",
		"success": function(json) {
			// Right align the number columns
			var numColumns = json.metaData.columns.length;
			var targets = [];
			for (var i = 1; i < numColumns; i++) {
				targets.push(i);
			}
			var dTable = table.DataTable({
				"columns": json.metaData.columns,
				"columnDefs": [
				    { "targets": targets,"className": "dt-body-right"}
				],
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"colResize": {
		            "handleWidth": 20
		        },
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollX": true,
				"data": json.data
			});
			$('#plannedVisitByAssessmentTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			var msg="No Planned Visit Data is available for Study Time Collected By Assessment";
			$('#plannedVisitByAssessmentGrid').append(msg);	
		}	
	});
}

$().ready(function() {
	displayPlannedVisitByExpTable();
	displayPlannedVisitByLabTestTable();
	displayPlannedVisitByAssessmentTable();
});

</script>

  
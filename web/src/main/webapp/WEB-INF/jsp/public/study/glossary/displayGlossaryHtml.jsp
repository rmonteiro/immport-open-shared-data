<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

  <table class="detail">
    <tr class="detail">
        <th scope="col" class="detail">Term</th>
        <th scope="col" class="detail">Definition</th>
    </tr>
  <c:forEach items="${terms}" var="term">
    <tr>
      <td class="detailCol2">${term.id.term}</td>
      <td class="detailCol2">${term.definition}</td>
    </tr>
  </c:forEach>
  </table>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


  <table class="detail">
    <tr>
      <td class="detailCol1">Accession:</td>
      <td class="detailCol2"><c:out value="${study.studyAccession}"/>&nbsp;</td>
    </tr>                      
    <tr>
      <td class="detailCol1">Title:</td>
      <td class="detailCol2"><c:out value="${study.briefTitle}"/>&nbsp;</td>
    </tr>
    <tr>
      <td class="detailCol1">PI:</td>
      <td class="detailCol2"><c:out value="${studyPI}"/>&nbsp; </td>
    </tr>
    <tr>
      <td class="detailCol1">Type:</td>
      <td class="detailCol2"><c:out value="${study.type}"/>&nbsp;</td>
    </tr>
    <tr>
      <td class="detailCol1">Condition Studied:</td>
      <td class="detailCol2"><c:out value="${study.conditionStudied}"/>&nbsp;</td>
    </tr>
    <tr>
      <td class="detailCol1">Brief Description:</td>
      <td class="detailCol2"><c:out value="${study.briefDescription}" escapeXml="false"/>&nbsp;</td>
    </tr>
    <tr>
      <td class="detailCol1">Start Date:</td>
      <td class="detailCol2"><c:out value="${study.actualStartDate}"/>&nbsp;</td>
    </tr>
    <tr>
     <td class="detailCol1">Schematic: <button id="schematicShow" type="button" class="btn btn-primary btn-xs">Show</button></td>
     <td style="color:#cc541f;" class="detailCol2">
     <div id="schematicImage" style="display: none;">
     <img src="<c:url value="/public/study/study/getStudyImage?studyAccession"/>=<c:out value="${study.studyAccession}"/>" height="690" width="690" />"
     </div>
     </td>
    </tr>
    <tr>
      <td class="detailCol1">Detailed Description:</td>
      <td class="detailCol2">${study.description}</td>
    </tr>
    <tr>
      <td class="detailCol1">Objectives:</td>
      <td class="detailCol2">${study.objectives}</td>
    </tr>
    <tr>
      <td class="detailCol1">Endpoints:</td>
      <td class="detailCol2">${study.endpoints}</td>
    </tr>
    <tr>
      <td class="detailCol1">Gender Included:</td>
      <td class="detailCol2"><c:out value="${study.genderIncluded}"/>&nbsp;</td>
    </tr>
    <tr>
      <td class="detailCol1">Subjects Number:</td>
      <td class="detailCol2"><c:out value="${study.actualEnrollment}"/>&nbsp;</td>
    </tr>   
    <tr>
      <td class="detailCol1">Download Packages:</td>     
       <td class="detailCol2"><a href="<c:out value="${asperaDataBrowserUrl }"/><c:out value="${study.studyAccession}"/>" target="_data_browser">Study Download Packages&nbsp;</a></td>
    </tr>
    <tr>
      <td class="detailCol1">Contract/Grant:</td>
      <td class="detailCol2"><c:out value="${contractGrant}"/>&nbsp;</td>
    </tr>  
     <tr>
      <td class="detailCol1">Data Completeness:</td>
      <td class="detailCol2"><c:out value="${dclId}"/>&nbsp;</td>
    </tr> 
  </table>
 
<c:if test="${not empty studyPubmeds}">    
 <br>
	  <div> <h5 id="heading">Publications</h5></div> 
	 
  
   <table class="detail">  
    <tr class="detail">
        <th scope="col" class="detail">Pubmed Id</th>
        <th scope="col" class="detail">Title</th>
        <th scope="col" class="detail">Journal</th>
        <th scope="col" class="detail">Year</th>
        <th scope="col" class="detail">Month</th>
        <th scope="col" class="detail">Issue</th>
        <th scope="col" class="detail">Pages</th>
        <th scope="col" class="detail">Authors</th>
    </tr>
  <c:forEach items="${studyPubmeds}" var="studyPubmed">
    <tr>
      <td class="detailCol2"><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids=${studyPubmed.id.pubmedId}" target="_blank">${studyPubmed.id.pubmedId}</a></td>
      <td class="detailCol2">${studyPubmed.title}</td>
      <td class="detailCol2">${studyPubmed.journal}</td>
      <td class="detailCol2">${studyPubmed.year}</td>
      <td class="detailCol2">${studyPubmed.month}</td>
      <td class="detailCol2">${studyPubmed.issue}</td>
      <td class="detailCol2">${studyPubmed.pages}</td>
      <td class="detailCol2">${studyPubmed.authors}</td>
    </tr>
  </c:forEach>
  </table>
</c:if>

<c:if test="${empty studyPubmeds}">  
<br>
	  <div><h5 id="heading">No Publications</h5></div> 
</c:if>

<c:if test="${not empty studyLinks}">  
<br>
	  <div> <h5 id="heading">Study Links</h5></div>
  <table class="detail"> 
    <tr class="detail">
        <th scope="col" class="detail">Type</th>
        <th scope="col" class="detail">Name</th>
        <th scope="col" class="detail">Value</th>
    </tr>
  <c:forEach items="${studyLinks}" var="studyLink">
    <tr>
      <td class="detailCol2">${studyLink.type}</td>
      <td class="detailCol2">${studyLink.name}</td>
      <td class="detailCol2"><a href="${studyLink.value}">${studyLink.value}</a></td>
    </tr>
  </c:forEach>
  </table>
</c:if>

<c:if test="${empty studyLinks}">  
<br>
	  <div><h5 id="heading">No Study Links</h5></div> 
</c:if>

<c:if test="${not empty terms}">  
<br>
	  <div><h5 id="heading">Glossary</h5></div> 
 <table class="detail">
    <tr class="detail">
        <th scope="col" class="detail">Term</th>
        <th scope="col" class="detail">Definition</th>
    </tr>
  <c:forEach items="${terms}" var="term">
    <tr>
      <td class="detailCol2">${term.id.term}</td>
      <td class="detailCol2">${term.definition}</td>
    </tr>
  </c:forEach>
  </table>
 </c:if>
 
 <c:if test="${empty terms}">  
<br>
	  <div><h5 id="heading">No Glossary</h5></div> 
</c:if>
 
<script>
$().ready(function() {
	$('#schematicShow').on("click",function() {
		var $this = $(this);
		if ($this.text() == "Show") {
			$this.text("Hide");
		} else {
			$this.text("Show");
		}
		$('#schematicImage').toggle();
	});
})

</script>

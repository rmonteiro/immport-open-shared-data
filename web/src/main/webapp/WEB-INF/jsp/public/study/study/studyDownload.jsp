<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="<c:url value="/resources/external/handlebars/handlebars-v1.3.0.js" />" type="text/javascript"></script>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var studyAccession = '<c:out value="${studyAccession}"></c:out>';
</script>

<script id="study-summary-template" type="text/x-handlebars-template">
   <h4 class="study-title">{{ studyAccession }}: {{ studyTitle }}</h4>
    <p>The Data Completeness Level for this study is <span class="badge data-complete-level-{{ dataCompleteness }}">{{ dataCompleteness }}</span>.</p>
    <p>
        The Meta information for a study is available for download in 2 formats:
        <a href="{{ downloadBaseURL }}{{ studyAccession }}&amp;fileName={{ studyAccession }}-{{ dataRelease }}_MySQL.zip" target="_download">MySQL</a> and
        <a href="{{ downloadBaseURL }}{{ studyAccession }}&amp;fileName={{ studyAccession }}-{{ dataRelease }}_Tab.zip" target="_download">Tab.</a>
    </p>
    <p>
        The table and column documentation for the MySQL Schema are available
        <a href="{{ CONTEXT_ROOT }}/public/schema/schemaDefinition/study">here.</a>
        Entity-Relationship diagrams are available <a href="{{ CONTEXT_ROOT }}/public/schema/schemaDiagram/AllTables">here.</a>
    </p>
</script>
<script id="table-list-template" type="text/x-handlebars-template">
   {{#studyTables}}
    <tr>
        <td>{{ name }}</td>
        <td align="right">{{ count }}</td>
    </tr>
    {{/studyTables}}
</script>
<script id="table-download-template" type="text/x-handlebars-template">
   {{#downloadFiles}}
    <tr>
        <td>{{ filePurpose }}</td>
        <td>
            {{#fileNames}}
            <div>
                <a href="{{ ../../downloadBaseURL }}{{ ../../studyAccession }}&amp;fileName={{ name }}" target="_download">
                    <span class="glyphicon glyphicon-download"></span>
                    {{ name }} ({{ size }})
                </a><br>
                <a href="{{ ../../downloadBaseURL }}{{ ../../studyAccession }}&amp;fileName={{ manifest }}" target="_download">
                    <span class="glyphicon glyphicon-download"></span>
                    {{ manifest }}
                </a>
            </div>
            {{/fileNames}}
        </td>
        <td>
            {{#if manifest}}
            <div>
                <a href="{{ ../../downloadBaseURL }}{{ ../../studyAccession }}&amp;fileName={{ manifest }}" target="_download">
                    <span class="glyphicon glyphicon-download"></span>
                    Download
                </a>
            </div>
            {{/if}}
        </td>
    </tr>
    {{/downloadFiles}}
</script>
<style>
.row {
    margin-bottom: 25px;
}
table caption {
    font-size: 1.1em;
    margin-bottom: 10px;
}
.section-summary h2 {
    margin-bottom: 20px;
}
.section-summary h4 {
    margin-bottom: 20px;
}
.section-summary p {
    margin-bottom: 10px;
}
</style>
<div class="container">
<div class="row" style="background:#E8DFD7;margin-bottom:5px;">
<div class="col-md-6 col-md-push-4">Study data available for download for
    <span style="color:blue;" id="registerUser" data-toggle="popover" data-trigger="hover" title="User Registration"
    data-content="Registration for an account is free. Registration details are available at immport.niaid.nih.gov">
    Registered Users</span></div>
    
</div>
    <div class="row section-summary">
        <div class="col-md-12 study-summary">
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <table class="table table-bordered table-study-tables">
                <caption>
                    Table List
                </caption>
                <thead>
                    <tr>
                        <th>Table</th>
                        <th>Count of Rows</th>
                    </tr>
                </thead>
                <tbody>
                    <!--
                    <tr>
                        <td>Subject</td>
                        <td>419</td>
                    </tr>
                    -->
                </tbody>
            </table>
        </div>
        
        <div class="col-md-8">
            <table class="table table-bordered table-download">
                <caption>
                    Components Available For Download
                </caption>
                <thead>
                    <tr>
                        <th>File Purpose</th>
                        <th>Download</th>
                        <th>Manifest</th>
                    </tr>
                </thead>
                <tbody>
                    <!--
                    <tr>
                        <td>Protocols</td>
                        <td>
                            <a href="/immportWeb/clinical/study/studyPackage.do?studyAccNum=SDY211&amp;fileName=SDY211_Protocols_12-18-2013_1.zip">
                                <span class="glyphicon glyphicon-download"></span>
                                Download
                            </a>
                        </td>
                        <td></td>
                    </tr>
                    -->
                </tbody>
            </table>
        </div>
        
        <div class="col-md-6">
            <table class="table table-bordered">
                <caption>
                    Data Completeness Level
                </caption>
                <thead>
                    <tr>
                        <th>Level</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span class="badge data-complete-level-0">0</span></td>
                        <td>Descriptive data and results as originally received from the data provider.</td>
                    </tr>
                    <tr>
                        <td><span class="badge data-complete-level-1">1</span></td>
                        <td>Includes updates to the original data upload short of completeness.</td>
                    </tr>
                    <tr>
                        <td><span class="badge data-complete-level-2">2</span></td>
                        <td>Complete set of descriptive data and results, as ascertained by ImmPort.</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>

<script>
$().ready(function() {
	$('#registerUser').popover();
});
</script>

<script src="<c:url value="/resources/js/IP/study/studyDownload.js" />" type="text/javascript"></script>
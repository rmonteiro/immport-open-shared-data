<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/css/dataTables.colVis.css" />" >
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/css/dataTables.tableTools.css" />" >

<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/media/js/jquery.dataTables.js" />" type="text/javascript"></script>
<script src="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/dataTables.colResize.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/ColVis/js/dataTables.colVis.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/external/DataTables/DataTables-1.10.4/extensions/TableTools/js/dataTables.tableTools.js" />" type="text/javascript"></script>

<div class="row" style="background:#E8DFD7;margin-bottom:5px;">
<div class="col-md-6 col-md-push-4">
Study data available for <a href="<c:out value="${asperaDataBrowserUrl}"/><c:out value="${study.studyAccession}"/>" target="_data_browser">download</a> for 
<span style="color:blue;" id="registerUser" data-toggle="popover" data-trigger="hover" title="User Registration"
    data-content="Registration for an account is free. Registration details are available at immport.niaid.nih.gov">
    Registered Users</span>
 </div>
</div>
<div class="row-fluid tabContainer">
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tabSummary" data-toggle="tab">Summary</a></li>
        </ul>
        <div class="tab-content" style="margin-top:20px;">
            <div class="tab-pane active" id="tabSummary">
                <div id="panelSummary"></div>
            </div>
        </div>
    </div>
</div>

<script>
var CONTEXT_ROOT = '<%= request.getContextPath() %>';
var studyAccession = '<c:out value="${studyAccession}"></c:out>';
var study2Panels = '<c:out value="${study2Panels}"></c:out>';

$().ready(function() {
	$('#registerUser').popover();
});

</script>
<script src="<c:url value='/resources/js/IP/search/studyDetailOption.js'/>" type="text/javascript"></script>

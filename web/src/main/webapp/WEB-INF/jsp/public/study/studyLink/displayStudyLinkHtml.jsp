<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

   <table class="detail">
  <tr><th colspan="3" class="detail">Study Links</th></tr>
    <tr class="detail">
        <th scope="col" class="detail">Type</th>
        <th scope="col" class="detail">Name</th>
        <th scope="col" class="detail">Value</th>
    </tr>
  <c:forEach items="${studyLinks}" var="studyLink">
    <tr>
      <td class="detailCol2">${studyLink.type}</td>
      <td class="detailCol2">${studyLink.name}</td>
      <td class="detailCol2"><a href="${studyLink.value}">${studyLink.value}</a></td>
    </tr>
  </c:forEach>
  </table>
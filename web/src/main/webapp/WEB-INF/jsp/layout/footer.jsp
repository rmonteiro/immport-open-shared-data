<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
.row-footer {
	padding-top: 10px;
	/* border-top: solid 1px #e2e2e2; */
	color: #999;
	font-size: 0.9em;
}
.row-footer a {
	color: #999;
}
.row-footer .block-header {
	padding-bottom: 8px;
}
.row-footer .block-body {
	padding-left: 20px;
}
</style>
<div class="row row-footer">
	<div class="col-md-5">
		<div class="block-header">Sponsored by:</div>
		<ul>
			<li><a href="http://www.niaid.nih.gov/Pages/default.aspx" target="_blank">National Institute of Allergy and Infectious Diseases (NIAID)</a></li>
			<li><a href="http://www.nih.gov/" target="_blank">National Institutes of Health (NIH)</a></li>
			<li><a href="http://www.hhs.gov/" target="_blank">Health and Human Services (HHS)</a></li>
		</ul>
	</div>
	<div class="col-md-5">
		<div class="block-header">
			<a href="mailto:BISC_Helpdesk@niaid.nih.gov">Contact Us</a> | 
			<a href="<c:url value='/public/home/privacyPolicy'/>">Privacy Policy</a> | 
			<a href="<c:url value="/public/home/disclaimer"/>">Disclaimer</a> | 
			<a href="<c:url value="/public/home/accessibility"/>">Accessibility</a>
		</div>
		<div class="block-header">
			Recommended Browsers: Chrome, Firefox, Safari v7+, Internet Explorer v9.0+
		</div>
	</div>
	<div class="col-md-2">
   		<div id="socialmedia-block" class="homePageText">
			<div class="title" style="margin-bottom:7px;">Connect with Us</div>
			<div class="socialmedia-block-inner">
				<ul>
					<script>(function(d, s, id) {
				  	var js, fjs = d.getElementsByTagName(s)[0];
				  	if (d.getElementById(id)) return;
				  	js = d.createElement(s); js.id = id;
				  	js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  	fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
					</script>
					<div class="fb-like" data-href="https://www.facebook.com/Immport" 
						style="padding-bottom:3px;"
						data-send="false" data-layout="button_count" data-width="650" 
						data-show-faces="false" data-font="lucida grande"></div>
					<!-- 
					<li class="facebook-link">
						<a class="facebook" href="https://www.facebook.com/Immport" style="text-decoration: none" target="_blank">
							<span>Facebook</span>
						</a>
					</li>
					-->
					<li class="twitter-link">
						<a class="twitter" href="https://twitter.com/#!/ImmPortDB" style="text-decoration: none" target="_blank">
							<span>Twitter</span>
						</a>
					</li>
					<li class="rss-link">
						<a href="<%= request.getContextPath() %>/resources/rss/newsfeed.xml"  style="text-decoration: none" target="_blank">
							<span>RSS Feed</span>
						</a>
					</li>
					<li class="google-link">
						<a href=" https://groups.google.com/d/forum/immport"  style="text-decoration: none" target="_blank">
							<span>Google Group</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
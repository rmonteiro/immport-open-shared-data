<!DOCTYPE html>
<html lang="en">
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
    <title><tiles:insertAttribute name="title" /></title>
    <meta charset="utf-8">
    <meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS files -->
    <link rel="shortcut icon" href="/resources/images/pend.ico">
    <link href="<c:url value="/resources/external/extjs4/resources/css/ext-all.css" />" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/resources/external/bootstrap/css/bootstrap.css" />" rel="stylesheet" type="text/css" media="screen" />
    <link href="<c:url value="/resources/external/bootstrap-lightbox/bootstrap-lightbox.css" />" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet" type="text/css" />
	<link href="<c:url value="/resources/css/immPortStandardUI.css" />" rel="stylesheet" type="text/css" />
    
    <!-- JS files -->
    <script src="<c:url value="/resources/external/jquery/jquery.min.js" />" type="text/javascript"></script>
    <script src="<c:url value="/resources/external/bootstrap/js/bootstrap.js" />" type="text/javascript"></script>
    <script src="<c:url value="/resources/external/bootstrap-lightbox/bootstrap-lightbox.js" />" type="text/javascript"></script>
    <script src="<c:url value="/resources/external/extjs4/ext-all-debug.js" />" type="text/javascript"></script>
    <script src="<c:url value="/resources/js/IP/common/common.js" />" type="text/javascript"></script>
</head>
<body>
<div class="container">
    <tiles:insertAttribute name="header" />
    <tiles:insertAttribute name="body" />
    <tiles:insertAttribute name="footer" />
</div>
</body>
</html>
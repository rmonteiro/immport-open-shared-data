<!DOCTYPE html>
<html lang="en">
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
    <title><tiles:insertAttribute name="title" /></title>
    <meta charset="utf-8">
    <meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS files -->
    <link rel="shortcut icon" href="<c:url value="/resources/images/home/open-immport-favicon.ico" />" type="image/x-icon" />
    <link href="<c:url value="/resources/external/extjs4/resources/css/ext-all.css" />" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/resources/external/bootstrap3/css/bootstrap.css" />" rel="stylesheet" type="text/css" media="screen" />
    <link href="<c:url value="/resources/external/bootstrap-lightbox/bootstrap-lightbox.css" />" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet" type="text/css" />
	<link href="<c:url value="/resources/css/immPortStandardUI.css" />" rel="stylesheet" type="text/css" />
    
    <!-- JS files -->
    <script src="<c:url value="/resources/external/jquery/jquery.min.js" />" type="text/javascript"></script>
    <script src="<c:url value="/resources/external/bootstrap3/js/bootstrap.js" />" type="text/javascript"></script>
    <script src="<c:url value="/resources/external/bootstrap-lightbox/bootstrap-lightbox.js" />" type="text/javascript"></script>
    <script src="<c:url value="/resources/external/extjs4/ext-all-debug.js" />" type="text/javascript"></script>
    <script src="<c:url value="/resources/js/IP/common/common.js" />" type="text/javascript"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<c:url value="/resources/external/bootstrap3/assets/js/html5shiv.js" />"></script>
      <script src="<c:url value="/resources/external/bootstrap3/assets/js/respond.min.js" />"></script>
    <![endif]-->
   <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45290069-1', 'immport-labs.org');
  ga('send', 'pageview');

	</script>
    
</head>
<body>
<div class="container">
    <tiles:insertAttribute name="header" />
    <tiles:insertAttribute name="body" />
    <tiles:insertAttribute name="footer" />
</div>
</body>
</html>
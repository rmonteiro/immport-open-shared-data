<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container-fluid ecoSystemHeader">
          
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-target-menu-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div class="collapse navbar-collapse navbar-site-main" id="navbar-target-menu-collapse">
                        <ul class="nav navbar-nav navbar-left">
                			<li class="menu-immport-org">
                    			<a href="<%= request.getContextPath() %>/">
                        			<img class="main-site-logo" src="<%= request.getContextPath() %>/resources/images/home/immport-logo-black-background.png">
                        			<span class="title-menu">ImmPort</span>
                        			<span class="sr-only">(current)</span>
                    			</a>
                			</li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Applications <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<c:out value="${privateDataUrl}"></c:out>"  target="_blank">
                                        <img class="menu-dropdown-logo" src="<%= request.getContextPath() %>/resources/images/home/immport-icon-orange.png" />
                                        Private Data
                                    </a></li>
                                    <li><a href="<c:out value="${sharedDataStudySearchUrl}"></c:out>"  target="_blank">
                                        <img class="menu-dropdown-logo" src="<%= request.getContextPath() %>/resources/images/home/immport-icon-blue.png" />
                                        Shared Data
                                    </a></li>                                    
                                    <li><a href="<c:out value="${dataAnalysisUrl}"></c:out>"  target="_blank">
                                        <img class="menu-dropdown-logo" src="<%= request.getContextPath() %>/resources/images/home/immport-icon-green.png" />
                                        Data Analysis
                                    </a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
				          	<li><a href="<c:out value="${documentationUrl}"></c:out>">Documentation</a></li>
		    		      	<li><a href="<c:out value="${publicationsUrl}"></c:out>">Publications</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">About <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
					    			<li><a href="<c:out value="${dataProvidersUrl}"></c:out>">Data Providers</a></li>
					    			<li><a href="<c:out value="${dataSummaryUrl}"></c:out>">Data Summary</a></li>
					    			<li><a href="<c:out value="${citeUrl}"></c:out>">How to cite ImmPort</a></li>
					    			<li><a href="<c:out value="${newsUrl}"></c:out>">News/Events</a></li>
					    			<li><a href="<c:out value="${teamUrl}"></c:out>">Team</a></li>
					    			<li><a href="<c:out value="${aboutUrl}"></c:out>">What is ImmPort?</a></li>
								</ul>
		        			</li>
		        			<li><a href="<c:out value="${registerUrl}"></c:out>" target="_blank"> Register </a></li>       
                        </ul>
                    </div>
                </div>
            </nav>
            
	<nav class="navbar navbar-inverse navbar-fixed-top applicationHeader" role="navigation">
	<div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-target-menu-collapse">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
            </button>
        </div>
        
        <div class="collapse navbar-collapse navbar-site-main" id="navbar-target-menu-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li class="menu-immport-org">
                    <a href="<c:out value="${sharedDataStudySearchUrl}"  ></c:out>">
                        <img class="main-site-logo" src="<%= request.getContextPath() %>/resources/images/open-immport-logo-small.png">
                        <span class="title-menu">Open ImmPort</span>
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Resources <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<c:out value="${cellOntologyUrl}"  ></c:out>">
                            <img class="menu-dropdown-logo" src="<%= request.getContextPath() %>/resources/images/logo-cell-ontology.png"/>
                            Cell Ontology
                        </a></li>
                        <li><a href="<c:out value="${cytokineRegistryUrl}"  ></c:out>">
                            <img class="menu-dropdown-logo" src="<%= request.getContextPath() %>/resources/images/logo-cytokine-registry.png"/>
                            Cytokine Registry
                        </a></li>
                        <li><a href="<c:out value="${sharedDataGeneListsUrl}"  ></c:out>">
                            <img class="menu-dropdown-logo" src="<%= request.getContextPath() %>/resources/images/logo-cytokine-registry.png"/>
                            Gene Lists 
                        </a></li>
                        <li><a href="<c:out value="${dataAnalysisUrl}"  ></c:out>">
                            <img class="menu-dropdown-logo" src="<%= request.getContextPath() %>/resources/images/logo-flow-analysis.png"/>
                            Flow Analysis
                        </a></li>
                        <li><a href="<c:out value="${sharedDataTutorialsUrl}"  ></c:out>">
                            <img class="menu-dropdown-logo" src="<%= request.getContextPath() %>/resources/images/logo-vaccine-analysis.png"/>
                            Flu Vaccine Analysis Tutorial</a>
                        </li>
                        <li><a href="<c:out value="${immuneXpressoUrl}"  ></c:out>">
                            <img class="menu-dropdown-logo" src="<%= request.getContextPath() %>/resources/images/logo-immunexpresso.png"/>
                            immuneXpresso
                        </a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
	          	<li><a href="<c:out value="${sharedDataStudySearchUrl}"  ></c:out>">Studies</a></li>
	          	<li><a href="<c:out value="${dataUploadDocumentationUrl}"  ></c:out>">Upload</a></li>
	          	<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
					    <li><a href="<c:out value="${sharedDataOverviewUrl}"  ></c:out>">Overview</a></li>
					    <li><a href="<c:out value="${sharedDataNavigationUrl}"  ></c:out>">Navigation</a></li>
					    <li><a href="<c:out value="${sharedDataRegistrationUrl}"  ></c:out>">Registration</a></li>
					</ul>
		        </li>
	        </ul>
        </div>
        
	</div>
</nav>            
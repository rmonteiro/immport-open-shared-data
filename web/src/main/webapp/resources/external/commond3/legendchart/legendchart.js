/**
 * LegendChart constructor
 * 
 * setting example:
 * {
 *     element: '#chart',
 *     width: 600,  // container width
 *     height: 400, // container height
 *     margin: {top: 20, right: 20, bottom: 40, left: 40} // chart margins
 * }
 */
function LegendChart(setting) {
    // set chart parameter
    this.element = setting.element || '#chart';
    
    this.title = setting.title || '';
    this.subTitle = setting.subTitle || '';
    this.titleMaxLength = 20;
    
    this.containerWidth = setting.width;
    this.containerHeight = setting.height;
    this.margin = setting.margin || {top: 20, right: 20, bottom: 40, left: 40};
    this.padding = setting.padding || {top: 10, right: 20, bottom: 20, left: 0};
    this.iconsize = setting.iconsize || {width: 20, height: 20 };
    this.textsize = setting.textsize || {width: 50, height: 20 };
    this.iconpadding = setting.iconpadding || {top:20, left:10}; // space between icon and text
    this.textOffsetY = setting.textOffsetY || 14; // text vertical offset
    this.layout = setting.layout || 'v'; // layout: horizontal or vertical 
    this.width = this.containerWidth - this.margin.left - this.margin.right;
    this.height = this.containerHeight - this.margin.top - this.margin.bottom;
    this.nameProperty = setting.nameProperty || 'name';
    this.namePropertyLabel = setting.namePropertyLabel || this.nameProperty;
    this.valueProperty = setting.valueProperty || 'value';
    this.valuePropertyLabel = setting.valuePropertyLabel || this.valueProperty;
    this.tickValues = setting.tickValues || null;
    
    // setup chart on dom
    this.setupChart();
}

LegendChart.prototype.setupChart = function() {
    $(this.element).html(''); // clear container content
    this.svgChart = d3.select(this.element).append("svg")
        .attr("width", this.containerWidth)
        .attr("height", this.containerHeight);
    this.groupTitle = this.svgChart
        .append("g")
        .attr("width", this.containerWidth)
        .attr("height", this.margin.top)
        .attr("transform", "translate(" + this.margin.left + "," + 0 + ")");
    this.groupChart = this.svgChart
        .append("g")
        .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
};

LegendChart.prototype.plot = function(data) {

    // setup colors
    if (data.length <= 10) {
        this.color = d3.scale.category10();
    } else {
        this.color = d3.scale.category20();
    }
    
    if (this.title) {
        var titleText = this.title;
        if (titleText.length > this.titleMaxLength) {
            titleText = titleText.substr(0, this.titleMaxLength) + '...';
        }
        this.titleGroup = this.groupTitle.append("g")
            .attr("class", "title-container")
            .attr("transform", "translate(0, 0)");
        this.titleGroup.append("text")
            .attr("y", 20)
            .attr("class", "main-title")
            .text(titleText);
    }
    
    this.groupChart.selectAll(".legend-icon")
        .data(data)
        .enter().append("rect")
        .attr("class", "legend-icon")
        .attr("x", function(d, i) { return this.getPosition(d, i, 'x', 'icon'); }.bind(this))
        .attr("y", function(d, i) { return this.getPosition(d, i, 'y', 'icon'); }.bind(this))
        .attr("width", this.iconsize.width)
        .attr("height", this.iconsize.height)
        .attr("fill", function(d, i) { return this.color(i); }.bind(this));
        
    
    this.groupChart.selectAll(".tick-text")
        .data(data)
        .enter().append("text")
        .attr("class", "tick-text")
        //.attr("title", function(d, i) { return this.tickValues[i] }.bind(this))
        .attr("x", function(d, i) { return this.getPosition(d, i, 'x', 'icon') + 6; }.bind(this))
        .attr("y", function(d, i) { return this.getPosition(d, i, 'y', 'icon') + 14; }.bind(this))
        .text(this.getTickText.bind(this));
    
    this.groupChart.selectAll(".legend-text")
        .data(data)
        .enter().append("text")
        .attr("class", "legend-text")
        .attr("title", function(d, i) { return d[this.nameProperty] }.bind(this))
        .attr("x", function(d, i) { return this.getPosition(d, i, 'x', 'text'); }.bind(this))
        .attr("y", function(d, i) { return this.getPosition(d, i, 'y', 'text'); }.bind(this))
        .text(this.getLegendText.bind(this));
    
    // add tooltip to legend text element
    $('text.legend-text').tipsy({ 
        gravity: 'sw'
    });
};

/**
 * Get position
 *
 * xy: 'x'|'y'
 * type: 'icon'|'text'
 */
LegendChart.prototype.getPosition = function(d, i, xy, type) {
    var result = 0;
    if (this.layout == 'v') {
        // vertical layout
        if (type == 'icon') {
            if (xy == 'x') {
                result = this.padding.left;
            } else if (xy == 'y') {
                result = this.padding.top + i * (this.iconsize.height + this.iconpadding.top);
            }
        } else if (type == 'text') {
            if (xy == 'x') {
                result = this.padding.left + this.iconsize.width + this.iconpadding.left;
            } else if (xy == 'y') {
                result = this.padding.top + i * (this.iconsize.height + this.iconpadding.top) + this.textOffsetY;
            }
        }
    } else if (this.layout == 'h') {
        // horizontal layout
        if (type == 'icon') {
            if (xy == 'x') {
                result = this.padding.left +
                    i * (this.iconsize.width + this.iconpadding.left + this.textsize.width);
            } else if (xy == 'y') {
                result = this.padding.top;
            }
        } else if (type == 'text') {
            if (xy == 'x') {
                result = this.padding.left + this.iconsize.width + this.iconpadding.left +
                    i * (this.iconsize.width + this.iconpadding.left + this.textsize.width);
            } else if (xy == 'y') {
                result = this.padding.top + this.textOffsetY;
            }
        }
    }
    return result;
};

LegendChart.prototype.getLegendText = function(d, i) {
    var legendText = d[this.nameProperty];
    if (legendText.length > this.titleMaxLength) {
        legendText = legendText.substr(0, this.titleMaxLength) + '...';
    }
    return legendText;
};

LegendChart.prototype.getTickText = function(d, i) {
    var tickText = '';
    if (this.tickValues && this.tickValues.length > 0) {
        if (this.tickValues[i].length <= 1) {
            tickText = this.tickValues[i] ;
        }
    }
    
    return tickText;
};

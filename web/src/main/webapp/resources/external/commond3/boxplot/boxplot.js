/**
 * BoxPlot constructor
 * 
 * setting example:
 * {
 *     element: '#chart',
 *     width: 600,  // container width
 *     height: 400, // container height
 *     margin: {top: 20, right: 20, bottom: 40, left: 40} // chart margins
 * }
 */
function BoxPlot(setting) {
    this.element = setting.element || '#chart';
    
    this.title = setting.title || '';
    this.subTitle = setting.subTitle || '';
    this.titleMaxLength = setting.titleMaxLength || 35;
    this.subTitleMaxLength = setting.subTitleMaxLength || 40;
    this.range = setting.range || [0, 100];
    
    this.boxplotWidth = setting.boxplotWidth || 30;
    this.boxplotPadding = setting.boxplotPadding || 50;
    
    this.containerWidth = setting.width;
    this.containerHeight = setting.height;
    this.margin = setting.margin || {top: 20, right: 20, bottom: 40, left: 40};
    
    this.nameProperty = setting.nameProperty || 'name';
    this.namePropertyLabel = setting.namePropertyLabel || this.nameProperty;
    this.valueProperty = setting.valueProperty || 'value';
    this.valuePropertyLabel = setting.valuePropertyLabel || this.valueProperty;
    this.showXAxis = true;
    if (setting.hasOwnProperty('showXAxis')) {
        this.showXAxis = setting.showXAxis;
    }
    this.showYAxis = true;
    if (setting.hasOwnProperty('showYAxis')) {
        this.showYAxis = setting.showYAxis;
    }
    this.showXAxisLabel = setting.showXAxisLabel || false;
    this.showYAxisLabel = setting.showYAxisLabel || false;
    //this.xAxisLabelFormat = setting.xAxisLabelFormat || '';
    this.yAxisLabelFormat = setting.yAxisLabelFormat || '';  // ".0%"
    this.xDomainValue = setting.xDomainValue || ['A'];
    this.yDomainValue = setting.yDomainValue || [0, 100];
    
    this.tickValues = setting.tickValues || null;
}

BoxPlot.prototype.setupChart = function() {
    
    this.xValue = function(d) { return d[ this.nameProperty]; }.bind(this); // data -> value
    this.xScale = d3.scale.ordinal().rangeRoundBands([0, this.width], .1); // value -> display
    this.xMap = function(d) { return this.xScale(this.xValue(d)); }.bind(this); // data -> display
    this.xLabelFormat = d3.format(this.xAxisLabelFormat);
    this.xAxis = d3.svg.axis().scale(this.xScale).orient("bottom");
    
    this.yValue = function(d) { return d[this.valueProperty]; }.bind(this); // data -> value
    this.yScale = d3.scale.linear().range([this.height, 0]); // value -> display
    this.yMap = function(d) { return this.yScale(this.yValue(d)); }.bind(this); // data -> display
    this.yLabelFormat = d3.format(this.yAxisLabelFormat);
    this.yAxis = d3.svg.axis().scale(this.yScale).orient("left").tickFormat(this.yLabelFormat);
    
    this.chart = d3.box()
        .whiskers(this.iqr(1.5))
        .width(this.boxplotWidth)
        .height(this.height);
};

BoxPlot.prototype.plot = function(data, range) {
    // make sure data exists
    if (data.length <= 0) {
        return;
    }
    
    this.chartWidth = this.boxplotPadding * 2 + this.boxplotWidth;
    this.chartHeight = this.containerHeight - this.margin.top - this.margin.bottom;
    this.chartGroupWidth = data.length * this.chartWidth;
    this.width =  this.chartWidth;
    this.height = this.chartHeight;
    this.setupChart();
    
    this.chart.domain(range); // range = [min, max]
    var chartGroupWidth = this.margin.left + 2 * this.boxplotPadding +
        (data.length * (2 * this.boxplotPadding + this.boxplotWidth));
    
    this.containerGroup = d3.select(this.element).append("svg")
        .attr("width", chartGroupWidth)
        .attr("height", this.containerHeight);
    this.chartGroup = this.containerGroup
        .append("g")
        .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
    
    if (this.title) {
        var titleText = this.title;
        if (titleText.length > this.titleMaxLength) {
            titleText = titleText.substr(0, this.titleMaxLength) + '...';
        }
        this.titleGroup = this.containerGroup.append("g")
            .attr("class", "title-container")
            .attr("transform", 'translate(' + this.margin.left + ',0)');
            
        this.titleGroup.append("text")
            .attr("y", 15)
            .attr("class", "main-title")
            .attr("title", this.title)
            .text(titleText);
        // add tooltip to title element
        $('text.main-title').tipsy({ 
            gravity: 's'
        });
    }
    
    
    // x axis
    this.xScale = d3.scale.ordinal()
        .rangeRoundBands([0, this.chartGroupWidth], .2);
    this.xAxis = d3.svg.axis().scale(this.xScale);
    
    // y axis
    this.yScale = d3.scale.linear()
        .domain(this.range)
        .range([this.height, 0]);
    this.yAxis = d3.svg.axis()
        .scale(this.yScale)
        .ticks(5)
        .orient("left");;
    if (this.tickValues) {
        this.xAxis.tickValues(this.tickValues);
    }
    
    // setup domain if not set already
    if (this.xDomainValue) {
        this.xScale.domain(this.xDomainValue);
    }
    if (this.yDomainValue) {
        this.yScale.domain(this.yDomainValue);
    }
    
    // plot x and y axis
    this.yAxisGroup = this.chartGroup.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(0,0)");
    this.yAxisGroup.call(this.yAxis);
    
    this.xAxisGroup = this.chartGroup.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + this.chartHeight + ")")
        .call(this.xAxis);
    
    if (this.showYAxisLabel) {
        this.yAxisGroup
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(this.valuePropertyLabel);
    }
    
    // plot boxplot
    var boxplotData = [];
    for (var i = 0; i < data.length; i++) {
        boxplotData.push(data[i].values);
    }
    this.chartGroup.selectAll("svg")
        .data(boxplotData)
        .enter().append("svg")
        .attr("class", "box")
        .attr("width", this.containerWidth)
        .attr("height", this.height)
        .append("g")
        .attr("transform", function(d,i) {
            var shiftX = this.boxplotPadding +
                i * (2 * this.boxplotPadding + this.boxplotWidth);
            return "translate(" + shiftX + "," + 0 + ")";
        }.bind(this))
        .call(this.chart);
};

// Returns a function to compute the interquartile range.
BoxPlot.prototype.iqr = function(k) {
    return function(d, i) {
        var q1 = d.quartiles[0],
            q3 = d.quartiles[2],
            iqr = (q3 - q1) * k,
            j = d.length;
        i = -1;
        while (d[++i] < q1 - iqr);
        while (d[--j] > q3 + iqr);
        return [i, j];
    };
}


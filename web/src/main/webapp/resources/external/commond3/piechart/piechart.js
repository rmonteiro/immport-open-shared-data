/**
 * PieChart constructor
 * 
 * setting example:
 * {
 *     element: '#chart',
 *     width: 600,  // container width
 *     height: 400, // container height
 *     margin: {top: 20, right: 20, bottom: 40, left: 40} // chart margins
 * }
 */
function PieChart(setting) {
    
    this.element = setting.element || '#chart';
    this.containerWidth = setting.width;
    this.containerHeight = setting.height;
    this.margin = setting.margin || {top: 20, right: 20, bottom: 40, left: 40};
    this.width = this.containerWidth - this.margin.left - this.margin.right;
    this.height = this.containerHeight - this.margin.top - this.margin.bottom;
    this.radius = Math.min(this.width, this.height) / 2;
    this.center = {
        x: this.margin.left + this.width / 2,
        y: this.margin.top + this.height / 2
    };
    this.valueProperty = setting.valueProperty || 'value';
    this.valuePropertyLabel = setting.valuePropertyLabel || this.valueProperty;
    
    this.setupChart();
}

PieChart.prototype.setupChart = function() {
    
    this.pie = d3.layout.pie()
        .value(function(d) { return d[this.valueProperty]; }.bind(this))
        .sort(null);
    
    this.arc = d3.svg.arc()
        .innerRadius(this.radius - 100)
        .outerRadius(this.radius - 20);
    
    this.svg = d3.select(this.element).append("svg")
        .attr("width", this.containerWidth)
        .attr("height", this.containerHeight)
        .append("g")
        .attr("transform", "translate(" + this.center.x + "," + this.center.y + ")");
    
};

PieChart.prototype.plot = function(data) {

    var color = d3.scale.category20();
    var path = this.svg.datum(data).selectAll("path")
        .data(this.pie)
        .enter().append("path")
        .attr("fill", function(d, i) { return color(i); })
        .attr("d", this.arc)
        .each(function(d) { this._current = d; }); // store the initial angles
    
    /*
    d3.selectAll("input")
        .on("change", change);
    
    var timeout = setTimeout(function() {
        d3.select("input[value=\"oranges\"]").property("checked", true).each(change);
    }, 2000);
    
    function change() {
        var value = this.value;
        clearTimeout(timeout);
        this.pie.value(function(d) { return d[value]; }); // change the value function
        path = path.data(this.pie); // compute the new angles
        path.transition().duration(750).attrTween("d", arcTween); // redraw the arcs
    }
    */
};
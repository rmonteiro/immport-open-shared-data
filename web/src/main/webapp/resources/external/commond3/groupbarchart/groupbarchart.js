/**
 * GroupBarChart constructor
 * 
 * setting example:
 * {
 *     element: '#chart',
 *     width: 600,  // container width
 *     height: 400, // container height
 *     margin: {top: 20, right: 20, bottom: 40, left: 40}, // chart margins
 *     showLegend: true // show legend of barcharts
 * }
 */
function GroupBarChart(setting) {
    this.element = setting.element || '#chart';
    this.containerWidth = setting.width;
    this.containerHeight = setting.height;
    this.margin = setting.margin || {top: 20, right: 20, bottom: 40, left: 40};
    this.width = this.containerWidth - this.margin.left - this.margin.right;
    this.height = this.containerHeight - this.margin.top - this.margin.bottom;
    this.nameProperty = setting.nameProperty || 'name';
    this.namePropertyLabel = setting.namePropertyLabel || this.nameProperty;
    this.valueProperty = setting.valueProperty || 'value';
    this.valuePropertyLabel = setting.valuePropertyLabel || this.valueProperty;
    this.showLegend = setting.showLegend || false;
}

GroupBarChart.prototype.setupChart = function(data) {
    
};

GroupBarChart.prototype.plot = function(data) {
    this.setupChart(data);
    
};
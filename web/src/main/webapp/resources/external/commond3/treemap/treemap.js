/**
 * TreeMap constructor
 * 
 * setting example:
 * {
 *     element: '#treemap',
 *     nameField: 'name',
 *     childrenField: 'children',
 *     width: 600,  // container width
 *     height: 400, // container height
 *     margin: {top: 20, right: 20, bottom: 40, left: 40} // chart margins
 * }
 */
function TreeMap(setting) {
    // set chart parameter
    this.element = setting.element || '#treemap';
    
    this.margin = setting.margin;
    this.width = setting.width - this.margin.left - this.margin.right;
    this.height = setting.height - this.margin.top - this.margin.bottom;
    
    this.sizeField = setting.sizeField || 'size'; // property name for the size field
    this.nameField = setting.nameField || 'name'; // property name for the name field
    this.childrenField = setting.childrenField || 'children'; // property name for the children field
    
    this.color = d3.scale.category20c(); 
    this.treemap = d3.layout.treemap()
        .size([this.width, this.height])
        .sticky(true)
        .value(function(d) { return d[this.sizeField]; }.bind(this))
        .children(function(d){ return d[this.childrenField]; }.bind(this));
    
    // setup on dom
    this.setup();
}

TreeMap.prototype.setup = function() {
    this.container = d3.select(this.element)
        .style("position", "relative")
        .style("width", (this.width + this.margin.left + this.margin.right) + "px")
        .style("height", (this.height + this.margin.top + this.margin.bottom) + "px")
        .style("left", this.margin.left + "px")
        .style("top", this.margin.top + "px");
};

/**
Example data:
{
    "name": "flare",
    "children": [
        {
            "name": "analytics",
            "children": [
                {
                    "name": "cluster",
                    "children": [
                        {
                            "name": "AgglomerativeCluster",
                            "size": 3938
                        },
                        {
                            "name": "CommunityStructure",
                            "size": 3812
                        }, ...
*/
TreeMap.prototype.plot = function(data) {
    this.node = this.container.datum(data)
        .selectAll(".node")
        .data(this.treemap.nodes)
        .enter().append("div")
        .attr("class", "node")
        .call(this.position)
        .style("background", function(d) {
            return d[this.childrenField] ? this.color(d[this.nameField]) : null;
        }.bind(this))
        .text(function(d) {
            return d[this.childrenField] ? null : d[this.nameField];
        }.bind(this));
};

TreeMap.prototype.position = function() {
    this.style("left", function(d) { return d.x + "px"; })
        .style("top", function(d) { return d.y + "px"; })
        .style("width", function(d) { return Math.max(0, d.dx - 1) + "px"; })
        .style("height", function(d) { return Math.max(0, d.dy - 1) + "px"; });
};

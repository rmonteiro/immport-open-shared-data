/**
 * ForceCircle constructor
 * 
 * setting example:
 * {
 *     element: '#chart',
 *     width: 600,  // container width
 *     height: 400, // container height
 *     margin: {top: 20, right: 20, bottom: 40, left: 40} // chart margins
 * }
 */
function ForceCircle(setting) {
    this.element = setting.element || '#chart';
    this.containerWidth = setting.width;
    this.containerHeight = setting.height;
    this.margin = setting.margin || {top: 20, right: 20, bottom: 40, left: 40};
    this.width = this.containerWidth - this.margin.left - this.margin.right;
    this.height = this.containerHeight - this.margin.top - this.margin.bottom;
    this.setupChart();
}

ForceCircle.prototype.setupChart = function() {
    //Create SVG element
    this.svg = d3.select(this.element)
        .append("svg")
        .attr("width", this.width)
        .attr("height", this.height);
    // create force layout
    this.force = d3.layout.force()
    .gravity(.05)
    .distance(120)
    .charge(-100)
    .size([this.width, this.height]);
};

ForceCircle.prototype.plot = function(data) {

    this.force
        .nodes(data.nodes)
        .links(data.links)
        .start();
  
    var link = this.svg.selectAll(".link")
        .data(data.links)
        .enter().append("line")
        .attr("class", "link");
  
    var node = this.svg.selectAll(".node")
        .data(data.nodes)
        .enter().append("g")
        .attr("class", "node")
        .call(this.force.drag);
  
    /*
    node.append("image")
        .attr("xlink:href", "https://github.com/favicon.ico")
        .attr("x", -8)
        .attr("y", -8)
        .attr("width", 16)
        .attr("height", 16);
    */
    
    node.append("circle")
    .attr("class", function(node, index) {
        return index === 0 ? 'center' : 'surface'
    })
    .attr("fill", function(node, index) {
        return index === 0 ? '#aaa' : '#ccc'
    })
    .attr("r", 10);
    
    node.append("text")
        .attr("dx", 12)
        .attr("dy", ".35em")
        .text(function(d) { return d.name });
  
    this.force.on("tick", function() {
        link.attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });
        node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
    });
    
};
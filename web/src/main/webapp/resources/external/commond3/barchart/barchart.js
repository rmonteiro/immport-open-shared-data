/**
 * BarChart constructor
 * 
 * setting example:
 * {
 *     element: '#chart',
 *     width: 600,  // container width
 *     height: 400, // container height
 *     margin: {top: 20, right: 20, bottom: 40, left: 40} // chart margins
 * }
 */
function BarChart(setting) {
    // set chart parameter
    this.element = setting.element || '#chart';
    
    this.title = setting.title || '';
    this.subTitle = setting.subTitle || '';
    this.titleMaxLength = setting.titleMaxLength || 35;
    this.subTitleMaxLength = setting.subTitleMaxLength || 40;
    
    this.containerWidth = setting.width;
    this.containerHeight = setting.height;
    this.margin = setting.margin || {top: 20, right: 20, bottom: 40, left: 40};
    this.width = this.containerWidth - this.margin.left - this.margin.right;
    this.height = this.containerHeight - this.margin.top - this.margin.bottom;
    this.nameProperty = setting.nameProperty || 'name';
    this.namePropertyLabel = setting.namePropertyLabel || this.nameProperty;
    this.valueProperty = setting.valueProperty || 'value';
    this.valuePropertyLabel = setting.valuePropertyLabel || this.valueProperty;
    this.showXAxis = true;
    if (setting.hasOwnProperty('showXAxis')) {
        this.showXAxis = setting.showXAxis;
    }
    this.showYAxis = true;
    if (setting.hasOwnProperty('showYAxis')) {
        this.showYAxis = setting.showYAxis;
    }
    this.showXAxisLabel = setting.showXAxisLabel || false;
    this.showYAxisLabel = setting.showYAxisLabel || false;
    //this.xAxisLabelFormat = setting.xAxisLabelFormat || '';
    this.yAxisLabelFormat = setting.yAxisLabelFormat || '';  // ".0%"
    this.xDomainValue = setting.xDomainValue || null;
    this.yDomainValue = setting.yDomainValue || null;
    this.tickValues = setting.tickValues || null;
    
    // legend
    this.showLegend = setting.showLegend || false;
    
    // setup chart on dom
    this.setupChart();
}

BarChart.prototype.setupChart = function() {
    
    this.xValue = function(d) { return d[ this.nameProperty]; }.bind(this); // data -> value
    this.xScale = d3.scale.ordinal().rangeRoundBands([0, this.width], .1); // value -> display
    this.xMap = function(d) { return this.xScale(this.xValue(d)); }.bind(this); // data -> display
    this.xLabelFormat = d3.format(this.xAxisLabelFormat);
    this.xAxis = d3.svg.axis().scale(this.xScale).orient("bottom");
    
    this.yValue = function(d) { return d[this.valueProperty]; }.bind(this); // data -> value
    this.yScale = d3.scale.linear().range([this.height, 0]); // value -> display
    this.yMap = function(d) { return this.yScale(this.yValue(d)); }.bind(this); // data -> display
    this.yLabelFormat = d3.format(this.yAxisLabelFormat);
    this.yAxis = d3.svg.axis().scale(this.yScale).orient("left").tickFormat(this.yLabelFormat);
    
    /*
    this.svgTitle = d3.select(this.element).append("svg")
        .attr("width", this.containerWidth)
        .attr("height", this.margin.top)
        .append("g")
        .attr("transform", "translate(" + this.margin.left + "," + 0 + ")");
    */
    this.svgChart = d3.select(this.element).append("svg")
        .attr("width", this.containerWidth)
        .attr("height", this.containerHeight);
    this.groupTitle = this.svgChart
        .append("g")
        .attr("width", this.containerWidth)
        .attr("height", this.margin.top)
        .attr("transform", "translate(" + this.margin.left + "," + 0 + ")");
    this.groupChart = this.svgChart
        .append("g")
        .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

    if (this.xDomainValue) {
        this.xScale.domain(this.xDomainValue);
    }
    if (this.yDomainValue) {
        this.yScale.domain(this.yDomainValue);
    }
    
};

BarChart.prototype.plot = function(data) {

    // setup colors
    if (data.length <= 10) {
        this.color = d3.scale.category10();
    } else {
        this.color = d3.scale.category20();
    }
    
    // draw title text
    if (this.title) {
        var titleText = this.title;
        if (titleText.length > this.titleMaxLength) {
            titleText = titleText.substr(0, this.titleMaxLength) + ' ...';
        }
        this.titleGroup = this.groupTitle.append("g")
            .attr("class", "title-container")
            .attr("transform", "translate(0, 0)");
        var titleElement = this.titleGroup.append("text")
            .attr("y", 15)
            .attr("class", "main-title")
            .attr("title", this.title)
            .text(titleText);
        // add tooltip to title element
        $('text.main-title').tipsy({ 
            gravity: 's'
        });
    }
    
    // setup domain if not set already
    if (!this.xDomainValue) {
        this.xScale.domain(data.map(this.xValue));
    }
    if (!this.yDomainValue) {
        this.yScale.domain([0, d3.max(data, this.yValue)]);
    }
    
    if (this.tickValues) {
        this.xAxis.tickValues(this.tickValues);
    }
    this.xAxisGroup = this.groupChart.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + this.height + ")")
        .call(this.xAxis);
    if (!this.showXAxis) {
        this.xAxisGroup.style("display", "none");
    }
    
    this.yAxisGroup = this.groupChart.append("g")
        .attr("class", "y axis");
    this.yAxisGroup.call(this.yAxis);
    
    if (this.showYAxisLabel) {
        this.yAxisGroup
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(this.valuePropertyLabel);
    }
    if (!this.showYAxis) {
        this.yAxisGroup.style("display", "none");
    }
    
    this.groupChart.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", this.xMap)
        .attr("width", this.xScale.rangeBand)
        .attr("y", this.yMap)
        .attr("fill", function(d, i) { return this.color(i); }.bind(this))
        .attr("height", function(d) { return this.height - this.yMap(d); }.bind(this));
};
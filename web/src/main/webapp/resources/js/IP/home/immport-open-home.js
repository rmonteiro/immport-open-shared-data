var app = {
    studyData: null,
    filterFieldCol: {
        'clinicalTrial': 'Clinical Trial',
        'studyType': 'Study Type',
        'subjectSpecies': 'Subject Species',
        'researchFocus': 'Research Focus',
        'experimentMeasurementTechnique': 'Assay Type'
    }
}

$().ready(function() {
	$('input#queryString').on('keypress', onQueryKeydown); // study query form key handler
    $('#btnQuery').on('click', onQuerySubmit) // study query form search button handler
    //$('form#frmField select').on('change', onFieldChange); // add field selection listener
    //$('#btnUpdateTreemap').on('click', doUpdateTreemap); // add update button listener
    $('.right-info-panels .panel-body').on('click', onAnnouncmentClick); // announcement click
    $('#comboFieldSelect').on('change', doUpdateTreemap);
    setupTemplates();
    getStudyData();
    updateSiteNotice();
    updateAnnouncements();
    $(window).resize(doUpdateTreemap);
    // tab
    $('.tabs-main').bind('shown.bs.tab', function (e) {
    	if ($(e.target).attr('href') == '#studyNavigator') {
        	doUpdateTreemap();
    	}
    });
    // carousel
    updateCarousel();
    $('#carousel-immport-share').bind('slide.bs.carousel', function (e) {
        updateCarouselTitle(e.relatedTarget);
    });
});

function onQueryKeydown(event) {
    if (event.which == 13) {
    	onQuerySubmit();
    	event.stopPropagation();
    	event.preventDefault();
    }
}

function onQuerySubmit() {
	var searchTerm = $('input#queryString').val();
	var searchUrl = CONTEXT_ROOT + '/public/home/studySearch';
	if (searchTerm) {
		searchUrl += '?searchTerm=' + searchTerm;
	}
	window.location.href = searchUrl;
	return false;
}

function setupTemplates() {
	// announcement info template
	app.announcementInfoSource = $("#announcement-info-template").html();
	app.announcementInfoTemplate = Handlebars.compile(app.announcementInfoSource);
	// carousel indicator template
	app.carouselIndicatorSource = $("#carousel-indicator-template").html();
	app.carouselIndicatorTemplate = Handlebars.compile(app.carouselIndicatorSource);
	// carousel item body template
	app.carouselItemSource = $("#carousel-item-template").html();
	app.carouselItemTemplate = Handlebars.compile(app.carouselItemSource);
}

function updateSiteNotice() {
	var items = [];
	var slidesDataUrl = CONTEXT_ROOT + '/resources/data/site-notice.txt';
	$.get(slidesDataUrl, function(data) {
		var items = parseTsvData(data);
        displaySiteNotice(items);
	});
}

function displaySiteNotice(items) {
    if (items.length > 0) {
    	var item = items[0];
    	if (item.title && item.content) {
	    	$('.immport-notice-content').html(item.title + ' ' + item.content);
	    	$('.immport-notice').show();
    	}
    }
}

function updateAnnouncements() {
	var items = [];
	var slidesDataUrl = CONTEXT_ROOT + '/resources/data/announcements.txt';
	$.get(slidesDataUrl, function(data) {
		var items = parseTsvData(data);
        displayAnnouncements(items);
	});
}

function displayAnnouncements(items) {
	var announcementBody = $('.right-info-panels .panel-body');
	for (var i = 0; i < items.length; i++) {
		var item = items[i];
		var announcementItemHtml = app.announcementInfoTemplate(item);
		announcementBody.append(announcementItemHtml);
	}
}

function onAnnouncmentClick(event) {
	var targetElement = $(event.target);
	var announcement = {
		type: targetElement.attr('data-type'),
		link: targetElement.attr('data-link'),
		display: targetElement.attr('data-display')
	}
	switch (announcement.display) {
	case 'popup':
		$('.news-dialog-content').attr('src', announcement.link);
		$('#newsModal').modal();
		break;
	case 'internal':
		// internal link
		window.location = announcement.link;
		break;
	case 'external':
		// external link
		window.open(announcement.link, '_blank');
		break;
	}
}

function updateCarousel() {
	var items = [];
	var slidesDataUrl = CONTEXT_ROOT + '/resources/data/slides.txt';
	$.get(slidesDataUrl, function(data) {
		var items = parseTsvData(data);
        setupCarousel(items);
        if (items[0] && items[0].title) {
        	$('.carousel-top-title').html(items[0].title);
        }
	});
}

function updateCarouselTitle(slide) {
	var slideTitle = $(slide).find('.carousel-item-title').html();
	$('.carousel-top-title').html(slideTitle);
}

function setupCarousel(items) {
	var carouselIndicator = $('#carousel-immport-share .carousel-indicators');
	var carouselBody = $('#carousel-immport-share .carousel-inner');
	// add carousel items
    for (var i = 0; i < items.length; i++) {
    	var item = items[i];
    	item.itemIndex = i;
    	item.itemActiveText = (i === 0) ? 'active' : '';
    	item.itemTarget = '#carousel-immport-share';
    	// carousel item indicator
    	var carouselIndicatorHtml = app.carouselIndicatorTemplate(item);
    	carouselIndicator.append(carouselIndicatorHtml);
    	// carousel item body
    	var carouselItemHtml = app.carouselItemTemplate(item);
    	carouselBody.append(carouselItemHtml);
    }
}

function setTreemapConfig(width, height) {
    // app.treemap object
    app.treemap = {
        w: width,
        h: height,
        kx: 0,
        ky: 0,
        g: null,
        vis: null,
        partition: null
    };
    app.treemap.x = d3.scale.linear().range([0, app.treemap.w]);
    app.treemap.y = d3.scale.linear().range([0, app.treemap.h]);
    // app.treemapHeader object
    app.treemapHeader = {
        w: width,
        h: 50,
        kx: 0,
        ky: 0,
        g: null,
        vis: null,
        partition: null
    };
    app.treemapHeader.x = d3.scale.linear().range([0, app.treemapHeader.w]);
    app.treemapHeader.y = d3.scale.linear().range([0, app.treemapHeader.h]);
}

function getStudyData(callback) {	
    //var dataUrl = CONTEXT_ROOT + '/public/study/study/getStudySummaryAllDetail';
    var dataUrl = CONTEXT_ROOT + '/resources/data/studies_treemap.json';
    d3.json(dataUrl, function(error, data) {
        if (error) {
            console.log("Error Loading Data");
        } else {
            app.studyData = data;
            callback && callback();
        }
    });
}

function onFieldChange(event) {
    var field = event.currentTarget;
    var fieldId = field.id;
    var fieldValue = $('#' + fieldId).val();
    console.log('field change:', fieldId, fieldValue);    
}

function doUpdateTreemap() {
    // get field values and update treemap
    var fieldCol = getFilterFields();
    // set treemap side based on container
    var width = Math.floor($('#dataTreemap').width() * 0.9);
    var height = $('#dataTreemap').height() - 30;
    height = height > 0 ? height : 0;
    setTreemapConfig(width, height);
    updateTreemap(fieldCol);
    return false;
}

function getFilterFields() {
	var fieldCombo = $('#comboFieldSelect').val();
	if( typeof fieldCombo !== 'undefined' || fieldCombo !== null ){
	    var fields = fieldCombo && fieldCombo.split('_');
	    var result = fields && { 
	    	field1: fields[0] || '', 
	    	field2: fields[1] || '',  
	    	field3: fields[2] || '' 
	    } || {};
	    return result;
	}	
}

function updateTreemap(fieldCol) {
    // process data
    var nestData = ip.nest()
        .key(function(d) { return d[fieldCol.field1] }).sortKeys(d3.ascending)
        .key(function(d) { return d[fieldCol.field2] }).sortKeys(d3.ascending)
        .key(function(d) { return d[fieldCol.field3] }).sortKeys(d3.ascending)
        .rollup(function(leaves) {
            return (leaves.map(function(d) {
                return d.studyAccession; // return study accession as rollup value
            }).sort())
        })
        .entries(app.studyData);
    
    nestData = calculateCount({ values:nestData });
    plotTreemap(nestData);
    // plot treemap header
    var headerData = {
        total: 1,
        values: [{
            key: fieldCol.field1,
            total: 1,
            values: [{
                key: fieldCol.field2,
                total: 1,
                values: [{
                    key: fieldCol.field3,
                    total: 1,
                    values: []
                }]
            }]
        }]
    };
    plotTreemapHeader(headerData);
}

function plotTreemapHeader(headerData) {
    //console.log('plotTreemapHeader:', headerData);
    $("#dataTreemapHeader").empty();
    var vis = d3.select("#dataTreemapHeader").append("div")
        .attr("class", "chart")
        .style("width", app.treemapHeader.w + "px")
        .style("height", app.treemapHeader.h + "px")
      .append("svg:svg")
        .attr("width", app.treemapHeader.w)
        .attr("height", app.treemapHeader.h);
    
    var partition = d3.layout.partition()
        .value(function(d) {
            return Math.ceil(Math.log(d.total)) + 1;
        })
        .children(function(d) { return d.values; });
    app.headerPartitionedData = partition.nodes(headerData);
    
    app.treemapHeader.g = vis.selectAll("g")
        .data(app.headerPartitionedData)
        .enter().append("svg:g")
        .attr("transform", function(d) {
            return "translate(" + app.treemapHeader.x(d.y) + "," + app.treemapHeader.y(d.x) + ")";
        });
    
    // add filter
    processFilter(vis);
    
    app.treemapHeader.kx = app.treemapHeader.w / headerData.dx;
    app.treemapHeader.ky = app.treemapHeader.h / 1;
    
    app.treemapHeader.g.append("svg:rect")
        .attr("width", headerData.dy * app.treemapHeader.kx)
        .attr("height", function(d) { return d.dx * app.treemapHeader.ky; })
        .attr("key", function(d) { return d.key; })
        .attr("class", function(d) { return 'group-' + d.depth; })
        .on('mouseover', onNodeMouseOver)
        .on('mouseout', onNodeMouseOut);
    
    app.treemapHeader.g.append("svg:text")
        .attr("transform", "translate(8, 25)")
        .attr("dx", "1.8em")
        .attr("dy", ".35em")
        .style("color", "#cc")
        .style("opacity", function(d) { return d.dx * app.treemapHeader.ky > 12 ? 1 : 0; })
        .text(function(d) {
        	var displayText = '';
            var nodeName = d.key || '';
            if (nodeName) {
            	displayText = app.filterFieldCol[nodeName] || '';
            }
            return displayText;
        });
}

function plotTreemap(root) {
    $('#totalCount').text(root.total);
    $("#dataTreemap").empty();
    var vis = d3.select("#dataTreemap").append("div")
        .attr("class", "chart")
        .style("width", app.treemap.w + "px")
        .style("height", app.treemap.h + "px")
      .append("svg:svg")
        .attr("width", app.treemap.w)
        .attr("height", app.treemap.h);
    
    var partition = d3.layout.partition()
        .value(function(d) {
            return Math.ceil(Math.log(d.total)) + 1;
        })
        .children(function(d) { return d.values; });
    var partitionedData = partition.nodes(root);
    
    app.treemap.g = vis.selectAll("g")
        .data(partitionedData)
        .enter().append("svg:g")
        .attr("transform", function(d) {
            return "translate(" + app.treemap.x(d.y) + "," + app.treemap.y(d.x) + ")";
        })
        .on("click", onNodeclick);

    // add filter
    processFilter(vis);
    
    app.treemap.kx = app.treemap.w / root.dx;
    app.treemap.ky = app.treemap.h / 1;
    
    app.treemap.g.append("svg:rect")
        .attr("width", root.dy * app.treemap.kx)
        .attr("height", function(d) { 
        	var height = d.dx * app.treemap.ky;
        	return height > 0 ? height : 0; 
        })
        .attr("key", function(d) { return d.key; })
        .attr("class", function(d) { return 'group-' + d.depth; })
        .on('mouseover', onNodeMouseOver)
        .on('mouseout', onNodeMouseOut);
    
    app.treemap.g.append("svg:text")
        .attr("transform", transform)
        .attr("class", "tree-node-label")
        .attr("dx", "1.8em")
        .attr("dy", ".35em")
        .style("color", "#cc")
        .attr("data-value", setDataValue)
        .style("opacity", function(d) { return d.dx * app.treemap.ky > 12 ? 1 : 0; })
        .text(function(d) {
            var nodeName = d.key || 'All';
            var displayText = nodeName + ' (' + d.total + ')';
            return displayText;
        })
        .on("click", onBlockLabelClick)
        .on('mouseover', onBlockLabelMouseOver)
        .on('mouseout', onBlockLabelMouseOut);
    
    d3.select(window)
        .on("click", function() { onNodeclick(root); });
    
}

function processFilter(vis) {
    //add filter
    var defs = vis.select("g").append("defs");
 
    var filter = defs.append("svg:filter")
        .attr("id", "outerDropShadow")
        .attr("x", "-20%")
        .attr("y", "-20%")
        .attr("width", "140%")
        .attr("height", "140%");
 
    filter.append("svg:feOffset")
        .attr("result", "offOut")
        .attr("in", "SourceGraphic")
        .attr("dx", "1")
        .attr("dy", "1");
 
    filter.append("svg:feColorMatrix")
        .attr("result", "matrixOut")
        .attr("in", "offOut")
        .attr("type", "matrix")
        .attr("values", "1 0 0 0 0 0 0.1 0 0 0 0 0 0.1 0 0 0 0 0 .5 0");
 
    filter.append("svg:feGaussianBlur")
        .attr("result", "blurOut")
        .attr("in", "matrixOut")
        .attr("stdDeviation", "3");
 
    filter.append("svg:feBlend")
        .attr("in", "SourceGraphic")
        .attr("in2", "blurOut")
        .attr("mode", "normal");
}

function onHeaderNodeclick(d) {
    if (!d.children) {
        d3.event.stopPropagation();
        return;
    }
    
    app.treemapHeader.kx = (d.y ? app.treemapHeader.w - 40 : app.treemapHeader.w) / (1 - d.y);
    app.treemapHeader.ky = app.treemapHeader.h / d.dx;
    app.treemapHeader.x.domain([d.y, 1]).range([d.y ? 40 : 0, app.treemapHeader.w]);
    app.treemapHeader.y.domain([d.x, d.x + d.dx]);
  
    var t = app.treemapHeader.g.transition()
        .duration(d3.event.altKey ? 7500 : 750)
        .attr("transform", function(d) {
            return "translate(" + app.treemapHeader.x(d.y) + "," + app.treemapHeader.y(d.x) + ")";
        });
    
    t.select("rect")
        .attr("width", d.dy * app.treemapHeader.kx)
        .attr("height", function(d) { return d.dx * app.treemapHeader.ky; });
    
    d3.event.stopPropagation();
}

function onNodeclick(d) {
    if (!d.children) {
    	goFilteredStudySearch(d);
        d3.event.stopPropagation();
        return;
    }
    
    // call header node click procedure to sync up display
    onHeaderNodeclick(app.headerPartitionedData[d.depth]);

    app.treemap.kx = (d.y ? app.treemap.w - 40 : app.treemap.w) / (1 - d.y);
    app.treemap.ky = app.treemap.h / d.dx;
    app.treemap.x.domain([d.y, 1]).range([d.y ? 40 : 0, app.treemap.w]);
    app.treemap.y.domain([d.x, d.x + d.dx]);
  
    var t = app.treemap.g.transition()
        .duration(d3.event.altKey ? 7500 : 750)
        .attr("transform", function(d) {
            return "translate(" + app.treemap.x(d.y) + "," + app.treemap.y(d.x) + ")";
        });
    
    t.select("rect")
        .attr("width", d.dy * app.treemap.kx)
        .attr("height", function(d) { return d.dx * app.treemap.ky; });
    
    t.select("text")
        .attr("transform", transform)
        .style("opacity", function(d) { return d.dx * app.treemap.ky > 12 ? 1 : 0; });

    t.select("image")
        .attr("transform", transform)
        .style("display", function(d) { return d.dx * app.treemap.ky > 12 ? "block" : "none"; });
  
    d3.event.stopPropagation();
}


//data-value example: ARM3/White/Female, fields are 1. Arm, 2. Race and 3. Gender
function setDataValue(d) {
	var values = [];
	while (d && d.depth > 0) {
	    values.unshift(d.key);
	    d = d.parent;
	}
	return values.join('~');
}

function onNodeMouseOver(d) {
 d3.select(this)
     .attr("filter", "url(#outerDropShadow)")
     .select(".background")
     .style("stroke", "#000000");
}

function onNodeMouseOut(d) {
 d3.select(this)
     .attr("filter", "")
     .select(".background")
     .style("stroke", "#FFFFFF");
}

function onBlockLabelClick(d) {
    var searchValues = [];
    goFilteredStudySearch(d);
    d3.event.stopPropagation();
}

function onBlockLabelMouseOver(d) {
    //d3.select(this).attr("xlink:href", "/immportweb/resources/images/home/folder-open.png");
    //console.log('mouse over:', d);
}

function onBlockLabelMouseOut(d) {
    //d3.select(this).attr("xlink:href", "/immportweb/resources/images/home/folder-close.png");
    //console.log('mouse out:', d);
}

function transform(d) {
    return "translate(8," + d.dx * app.treemap.ky / 2 + ")";
}

// iterate accumulatively to get studies counts at all node levels
function calculateCount(data) {
    //var total = 0;
    data.itemSet = new ItemSet();
    if (data.values && data.values.length > 0) {
        for (var i = 0; i < data.values.length; i++) {
            var child = data.values[i];
            if (typeof child == 'object') {
                child = calculateCount(child);
            	data.itemSet.addSet(child.itemSet);
                //total += child.total;
            } else {
            	data.itemSet.addItem(child);
                //total += 1;
            }
        }
    }
    data.total = data.itemSet.count();
    // remove data's chilren with all numbers inside
    if (data.values && data.values.length > 0) {
        if (typeof data.values[0] !== 'object') {
            data.values = null;
        }
    }
    return data;
}

// ip.nest definition
!function() {
  var ip = {};

  ip.nest = function() {
    var nest = {};
    var keys = [];
    var sortKeys = [];
    var sortValues;
    var rollup;

    function map(mapType, array, depth) {
      if (depth >= keys.length) {
          return rollup ? rollup.call(nest, array) : sortValues ? array.sort(sortValues) : array;
      }

      var i = -1;
      var n = array.length;
      var key = keys[depth++];
      var keyValue;
      var object;
      var setter;
      var valuesByKey = new d3.map();
      var values;

      
      while (++i < n) {
        // If the Key exists, push on the array.
        // Else create a new map entry, with new array.
        var object = array[i];
        var keyValues = key(object);
        // Treat all keys as potentially multivalued
        if (!$.isArray(keyValues)) {
            keyValues = [ keyValues ]
        }
        for (j = 0; j < keyValues.length; j++) {
            var keyValue = keyValues[j];
            values = valuesByKey.get(keyValue)
            if (values) {
              values.push(object);
            } else {
              valuesByKey.set(keyValue, [ object ]);
            }
         }
      }

      if (mapType) {
        object = mapType();
        setter = function(keyValue, values) {
          object.set(keyValue, map(mapType, values, depth));
        };
      } else {
        object = {};
        setter = function(keyValue, values) {
          object[keyValue] = map(mapType, values, depth);
        };
      }

      valuesByKey.forEach(setter);
      return object;
    }

    function entries(map, depth) {
      if (depth >= keys.length) {
          return map;
      }
      var array = [];
      sortKey = sortKeys[depth++];
      map.forEach(function(key, keyMap) {
        array.push({
          key: key,
          values: entries(keyMap, depth)
        });
      });
      return sortKey ? array.sort(function(a, b) {
        return sortKey(a.key, b.key);
      }) : array;
    }
    nest.map = function(array, mapType) {
      return map(mapType, array, 0);
    };
    nest.entries = function(array) {
      return entries(map(d3.map, array, 0), 0);
    };
    nest.key = function(d) {
      keys.push(d);
      return nest;
    };
    nest.sortKeys = function(order) {
      sortKeys[keys.length - 1] = order;
      return nest;
    };
    nest.sortValues = function(order) {
      sortValues = order;
      return nest;
    };
    nest.rollup = function(f) {
      rollup = f;
      return nest;
    };
    return nest;
  };

  if (typeof define === "function" && define.amd) {
    define(ip);
  } else if (typeof module === "object" && module.exports) {
    module.exports = ip;
  } else {
    this.ip = ip;
  }
}();

function partitionByClinicalTrial() {
    $('#dataTreemap').empty();
    
    var filteredStudyData = { "key": "Studies" };
    filteredStudyData.values = ip.nest()
         .key(function(d) { return d.clinicalTrial})
         .key(function(d) { return d.subjectSpecies})
         .key(function(d) { return d.studyType})
         .key(function(d) { return d.researchFocus})
         .rollup(function(d) { return d.length;})
         .entries(studyData);
}

function goFilteredStudySearch(d) {
	var filterValues = {};
	var filterFields = getFilterFields();
	var depth = d.depth;
	var target = d;
	while (depth > 0) {
		filterValues[filterFields['field' + depth]] = target.key;
		target = target.parent;
		depth = depth - 1;
	}
	var searchValues = [];
	for (var p in filterValues) {
		searchValues.push(p + '=' + filterValues[p]); 
	}
    var url = CONTEXT_ROOT + '/public/home/studySearch?' + searchValues.join('&');
    window.location = url;
}

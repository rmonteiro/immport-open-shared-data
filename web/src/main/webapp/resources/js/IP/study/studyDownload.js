var app = app || {};

$().ready(function() {
    setupTemplates();
    getData(studyAccession, updateDisplay);
});

function setupTemplates() {
    // study title
    app.studySummarySource   = $("#study-summary-template").html();
    app.studySummaryTemplate = Handlebars.compile(app.studySummarySource);
    // study table list
    app.tableListSource   = $("#table-list-template").html();
    app.tableListTemplate = Handlebars.compile(app.tableListSource);
    // download table
    app.tableDownloadSource   = $("#table-download-template").html();
    app.downloadTableTemplate = Handlebars.compile(app.tableDownloadSource);
}

function getData(studyAccession, callback) {
    var url = CONTEXT_ROOT + '/resources/data/studyDownload/' + studyAccession + '.json';
    var parameter = {};
    $.get(url, function(data) {
    	data['downloadBaseURL'] = "https://immport.niaid.nih.gov/immportWeb/clinical/study/studyPackage.do?studyAccNum=";
    	data['CONTEXT_ROOT'] = CONTEXT_ROOT;
        callback && callback(data);
    });
}

function updateDisplay(data) {
    // study title
    var studySummaryHtml = app.studySummaryTemplate(data);
    $('.study-summary').append(studySummaryHtml);
    // study table list
    var tableListHtml = app.tableListTemplate(data);
    $('table.table-study-tables tbody').append(tableListHtml);
    // download table
    var downloadTableHtml = app.downloadTableTemplate(data);
    $('table.table-download tbody').append(downloadTableHtml);
    
}
// common javascript library

// Function bind implementation from MDN
if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
        if (typeof this !== "function") {
            // closest thing possible to the ECMAScript 5 internal IsCallable function
            throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
        }
        var aArgs = Array.prototype.slice.call(arguments, 1), 
            fToBind = this, 
            fNOP = function () {},
            fBound = function () {
              return fToBind.apply(this instanceof fNOP && oThis
                ? this : oThis, aArgs.concat(Array.prototype.slice.call(arguments)));
            };
        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();
        return fBound;
    };
}

function replaceContextRoot(input, contextRoot) {
	if (typeof(input) == 'undefined') {
		return "";
	}
	contextRoot = CONTEXT_ROOT || app.CONTEXT_ROOT;
	var result = input.replace('CONTEXT_ROOT', contextRoot);
	return result
}

function parseTsvData(data) {
	var items = [];
	var lines = data.split('\n');
	var headers = lines[0].split('\t');
	for (var i = 1; i < lines.length; i++) {
		  var content = lines[i].split('\t');
		  var item = {};
		  for (var count = 0; count < headers.length; count++) {
			  item[headers[count]] = replaceContextRoot(content[count]);
		  }
		  items.push(item);
	}
	return items;
}
function ItemSet() {
	this.collection = {};
}

ItemSet.prototype.addItem = function(item) {
	this.collection[item]  = true;
};

ItemSet.prototype.addSet = function(itemSet) {
	for (var item in itemSet.collection) {
		this.addItem(item);
	}
}

ItemSet.prototype.count = function() {
	var length = 0;
	for (var item in this.collection) {
		length = length + 1
	}
	return length;
}

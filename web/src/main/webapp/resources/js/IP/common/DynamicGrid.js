var renderTip = function(val, meta, rec, rowIndex, colIndex, store) {
    meta.tdAttr = 'data-qtip="' + val + '"';
    return val;
};

var requestMessageProcessor = function(proxy, response, grid) {
	console.log("Inside requestMessageProcessor");
	if (response && proxy) {			
		try {						
			var responseData = proxy.reader.getResponseData(response);
			
			if (responseData.message) {
				var messageDescription = 'Information';
				var messageIcon = Ext.MessageBox.INFO;
				var message = responseData.message;
				
				if (!responseData.success) {
					var messageDescription = 'Error';
					var messageIcon = Ext.MessageBox.ERROR;
					message = "There was unexpected network problem preventing the population of this grid";
				}
				
				Ext.MessageBox.show({
					title: messageDescription,
					msg: message,
					buttons: Ext.MessageBox.OK,
					icon: messageIcon
				});
			} else {
				var metaData = Ext.decode(response.responseText).metaData;
				if (typeof metaData.success != "undefined" && !metaData.success) {
					Ext.get(grid.gridDiv).setHTML('<div style="width:100%;background:red;color:white;">There was an unexpected database problem preventing the populuation of this grid</div>')
				}
			}
		}
		catch(err) {
			console.log(err);
		}
	}
}


Ext.define('IP.common.DynamicGrid',{
    extend: 'Ext.grid.Panel',
    alias: 'widget.dynamicGrid',
    height: 600,
    width:  900,
    viewConfig: {
        stripeRows: true
    },
    url: '',
    initComponent: function() {
        var me = this;
        if (me.url == '') {
            Ext.Error.raise("URL parameter is empty.");
        }
        Ext.applyIf(me, {
            columns: [],
            forceFit: true,
            resizable: true,
            store: Ext.create('Ext.data.Store', {
                fields: [],
                listeners: {
                    'metachange': function(store, meta) {
                    	me.reconfigure(store, meta.columns);
                    }
                },
                autoLoad: true,
                remoteSort: false,
                remoteFilter: false,
                remoteGroup: false,
                proxy: {
                    reader: 'json',
                      type: 'ajax',
                      url: me.url,
                      listeners: { 
                  		  exception: function(proxy, response, options, me) {
                  			requestMessageProcessor(proxy, response);
                  		  }
                  	  },
                      afterRequest: function(request,success) {
                    	  console.log("afterRequest");
                    	  requestMessageProcessor(request.scope, request.operation.response,me);
                      }
                      
                }
            })
        });

    me.callParent(arguments);
    }
}); 


Ext.define('IP.common.DynamicGridPaging',{
    extend: 'Ext.grid.Panel',
    alias: 'widget.dynamicGridPaging',
    height: 600,
    width:  900,
    direction: 'ASC',
    viewConfig: {
        stripeRows: true
    },
    url: '',
    initComponent: function() {
        var me = this;
        me.firstTime = true;
        if (me.url == '') {
            Ext.Error.raise("URL parameter is empty.");
        }
        var store = this.buildStore(me);
        var pagingToolbar = {
        		xtype : 'pagingtoolbar',
        		store : store,
        		pageSize: 10,
        		displayInfo : true
        }
        Ext.applyIf(me, {
            columns: [],
            forceFit: true,
            resizable: true,
            store: store,
            bbar : pagingToolbar
        });

        me.callParent(arguments);
    },
    buildStore : function(me) {
    	return Ext.create('Ext.data.Store', {
    		autoLoad : true,
    	    fields: [],
    	    listeners: {
    	        'metachange': function(store, meta) {
    	        	if (me.firstTime) {
    	                me.reconfigure(store, meta.columns);
    	                me.firstTime = false;
    	        	}
    	        }
    	    },
      	    remoteSort: true,
      	    sorters:{property:me.sortfield,direction: me.direction},
    	    remoteFilter: false,
    	    remoteGroup: false,
    	    pageSize: 10,
    	    proxy: {
    	        reader: 'json',
    	          type: 'ajax',
    	          url: me.url,
                  simpleSortMode: true,
                  listeners: { 
              		  exception: function(proxy, response, options) {
              			requestMessageProcessor(proxy, response, me);
              		  }
              	  },
                  afterRequest: function(request,success) {
                	  console.log("afterRequest");
                	  requestMessageProcessor(request.scope, request.operation.response,me);
                  }
    	    }
    	})
    }
});

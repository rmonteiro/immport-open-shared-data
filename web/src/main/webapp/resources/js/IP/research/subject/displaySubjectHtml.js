var app = app || {};
app.types = {
    'btnGender': { value:'gender', display:'Gender' },
    'btnRace': { value:'race', display:'Race' },
    'btnEthnicity': { value:'ethnicity', display:'Ethnicity' }
};
app.chartTypes= {
    'btnBarPlot': { value:'barplot', display:'BarPlot' },
    'btnBoxPlot': { value:'boxplot', display:'BoxPlot' }
};

var displaySubjectTable = function() {
	var searchURL = CONTEXT_ROOT + '/public/research/subject/getDemograhicSummaryByStudyNew/' + studyAccession + '?start=0&limit=10000';
	var table = $('#subjectTable');
	var lastIdx = null;
	$.ajax({
		"url": searchURL,
		"type": "GET",
		"dataType": "json",
		"success": function(json) {
			// Right align the number columns
			var numColumns = json.metaData.columns.length;
			var targets = [];
			for (var i = 2; i < numColumns; i++) {
				targets.push(i);
			}
			var dTable = table.DataTable({
				"columns": json.metaData.columns,
				"columnDefs": [
				    { "targets": targets,"className": "dt-body-right"}
				],
				"dom": 'Z<"row"<"col-sm-6"l><"col-sm-6"f>><"row"t><"row"<"col-sm-6"i><"col-sm-6"p>>',
				"colResize": {
		            "handleWidth": 20
		        },
		        "scrollX": true,
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"data": json.data
			});
			$('#subjectTable tbody')
		    .on( 'mouseover', 'td', function () {
		        var colIdx = dTable.cell(this).index().column;

		        if ( colIdx !== lastIdx ) {
		            $( dTable.cells().nodes() ).removeClass( 'active' );
		            $( dTable.column( colIdx ).nodes() ).addClass( 'active' );
		        }
		    } )
		    .on( 'mouseleave', function () {
		        $( dTable.cells().nodes() ).removeClass( 'active' );
		    } );
		},
		"error": function() {
			console.log("Failure");		
		}	
	});
}

function setupChart() {
    var dataUrl = CONTEXT_ROOT + '/public/research/subject/getSubjectDemographicsByStudy/' + studyAccession;
    $.getJSON(dataUrl, function(subjectData) {
        app.subjectData = subjectData;
        app.targetType = 'gender';
        app.chartType = 'barplot';
        displayChart();
    })
    $('#btnGroup button').on('click', onTypeButtonClick);
    $('#btnChartTypeGroup button').on('click', onChartTypeButtonClick);
}

function onTypeButtonClick(event) {
    var button = event.currentTarget;
    $('#btnGroup button').removeClass('btn-primary');
    $(button).addClass('btn-primary');
    // get target type
    var type = app.types[button.id];
    app.targetType = type.value;
    displayChart();
}

function onChartTypeButtonClick(event) {
    var button = event.currentTarget;
    $('#btnChartTypeGroup button').removeClass('btn-primary');
    $(button).addClass('btn-primary');
    // get target type
    var chartType = app.chartTypes[button.id];
    app.chartType = chartType.value;
    displayChart();
}

function displayChart() {
	var chartType = app.chartType;
    if (chartType == 'barplot') {
    	displayBarplotChart();
    } else if (chartType == 'boxplot') {
    	displayBoxplotChart();
    }
}

function displayBarplotChart() {
    var data = app.subjectData;
    var targetType = app.targetType;
    
    // clear html container
    var chartConainter = $('#chartContainer');
    chartConainter.html('');
    
    // process data
    var nestData = d3.nest()
        .key(function(d) { return d.armName })
        .key(function(d) { return d[targetType] })
        .rollup(function(leaves)
        { return (leaves.map(
            function(d) { return parseFloat(d.ageReported);})
            .sort())})
        .entries(data);
    
    // normalize data to have uniform 2nd level
    nestData = normalizeData(nestData);
    
    // get maximum count and legend data across arms
    var armCountMax = 0;
    var legendData = [];
    for (var i = 0; i < nestData.length; i++) {
        var arm = nestData[i];
        arm.values.forEach(function(genderData) {
            var maxCount = genderData.values.length;
            armCountMax = maxCount > armCountMax ? maxCount : armCountMax;
        });
        // use the largest arm values as legend data
        if (arm.values.length > legendData.length) {
            legendData = arm.values;
        }
    }
    var tickValues = [];
    for (var i = 0; i < legendData.length; i++) {
        tickValues.push(String.fromCharCode(65 + i));
    }
    armCountMax = Math.ceil(armCountMax/10) * 10; // round up armCountMax to 10s
    
    for (var i = 0; i < nestData.length; i++) {
        var chartName = 'chart' + (i+1);
        var chartHtml =
            '<div class="chart-wrapper">' +
            '    <div id="' + chartName + '" class="chart"></div>' +
            '</div>';
        chartConainter.append(chartHtml);
        var arm = nestData[i];
        arm.values.forEach(function(genderData) {
            genderData.count = genderData.values.length;
        });
        var chartTitle = arm.key;
        var chartSetting = {
            title: chartTitle,
            titleMaxLength: 17,
            element: '#' + chartName,
            width: 180,
            height: 400,
            margin: {top: 40, right: 10, bottom: 40, left: 30},
            nameProperty: 'key',
            valueProperty: 'count',
            yDomainValue: [0, armCountMax],
            showXAxis: true,
            showXAxisLabel: false,
            showYAxis: true,
            showYAxisLabel: false,
            tickValues: tickValues
        };
        console.log('barChart setting:', chartSetting);
        var barChart = new BarChart(chartSetting);
        barChart.plot(arm.values);
    };
    
    // draw legend chart
    var chartLegendHtml =
        '<div class="chart-wrapper">' +
        '    <div id="chartLegend" class="chart-legend"></div>' +
        '</div>';
    chartConainter.append(chartLegendHtml);
    var legendChartSetting = {
        title: 'Legend',
        titleMaxLength: 25,
        element: '#chartLegend',
        width: 190,
        height: 400,
        margin: {top: 40, right: 10, bottom: 40, left: 10},
        nameProperty: 'key',
        valueProperty: 'count',
        tickValues: tickValues
    };
    var legendChart = new LegendChart(legendChartSetting);
    legendChart.plot(legendData);
}

function displayBoxplotChart() {
    var data = app.subjectData;
    var targetType = app.targetType;
    
    // clear html container
    var chartConainter = $('#chartContainer');
    chartConainter.html('');
    
    // process data
    var nestData = d3.nest()
        .key(function(d) { return d.armName })
        .key(function(d) { return d[targetType] })
        .rollup(function(leaves)
        { return (leaves.map(
            function(d) { return parseFloat(d.ageReported);})
            .sort())})
        .entries(data);
    
    // normalize data to have uniform 2nd level
    nestData = normalizeData(nestData);
    
    // get maximum count and legend data across arms
    var armCountMax = 0;
    var legendData = [];
    for (var i = 0; i < nestData.length; i++) {
        var arm = nestData[i];
        arm.values.forEach(function(genderData) {
            var maxCount = genderData.values.length;
            armCountMax = maxCount > armCountMax ? maxCount : armCountMax;
        });
        // use the largest arm values as legend data
        if (arm.values.length > legendData.length) {
            legendData = arm.values;
        }
    }
    var tickValues = [];
    for (var i = 0; i < legendData.length; i++) {
        tickValues.push(String.fromCharCode(65 + i));
    }
    armCountMax = Math.ceil(armCountMax/10) * 10; // round up armCountMax to 10s
    
    // get maximum count and legend data across arms
    var dataValueMax = 0;
    for (var i = 0; i < nestData.length; i++) {
        var arm = nestData[i];
        arm.values.forEach(function(data) {
        	console.log('data:', data, data.values);
        	for (var j = 0; j < data.values.length; j++) {
        		if (data.values[j] > dataValueMax) {
        			dataValueMax = data.values[j];
        		}
        	}
        });
    }
    dataValueMax = dataValueMax + 20;
    dataValueMax = Math.ceil(dataValueMax/10) * 10; // round up dataValueMax to 10s
    
    for (var i = 0; i < nestData.length; i++) {
        var chartName = 'chart' + (i+1);
        var chartHtml =
            '<div class="chart-wrapper">' +
            '    <div id="' + chartName + '" class="chart"></div>' +
            '</div>';
        chartConainter.append(chartHtml);
        
        var arm = nestData[i];
        var armCategoryCount = arm.values.length;
        var chartTitle = arm.key;
        var range = [0, dataValueMax];
        
        var chartSetting = {
            title: chartTitle,
            titleMaxLength: 45,
            element: '#' + chartName,
            width: 130 * armCategoryCount,
            height: 380,
            margin: { top: 40, right: 30, bottom: 40, left: 40 },
            nameProperty: 'key',
            valueProperty: 'count',
            valuePropertyLabel: 'Age',
            xDomainValue: tickValues,
            yDomainValue: range,
            showXAxis: true,
            showXAxisLabel: false,
            showYAxis: true,
            showYAxisLabel: true,
            tickValues: tickValues
        };
        var boxplot = new BoxPlot(chartSetting);
        boxplot.plot(arm.values, range);
    };
    
    // draw legend chart
    var chartLegendHtml =
        '<div class="chart-wrapper">' +
        '    <div id="chartLegend" class="chart-legend"></div>' +
        '</div>';
    chartConainter.append(chartLegendHtml);
    var legendChartSetting = {
        title: 'Legend',
        titleMaxLength: 25,
        element: '#chartLegend',
        width: 190,
        height: 400,
        margin: {top: 40, right: 10, bottom: 40, left: 10},
        nameProperty: 'key',
        valueProperty: 'count',
        tickValues: tickValues
    };
    var legendChart = new LegendChart(legendChartSetting);
    legendChart.plot(legendData);
}

/* normalize targetType so all have same 2nd level data

example nestData:
[
   {
     key: 'IgE', values: [  // level 1
         { key: 'Male', values:[...], count:1 },  // level 2
         { key: 'Female', values: [...], count:34 }
     ]
   }, 
   {
     key: 'IgG', values: [
         { key: 'Female', values: [...], count:5 }
     ]
   }
]

normalize function adds { key: 'Male', values:[] } in IgG values
orders of keys in 2nd level data need to be the same 
*/
function normalizeData(input) {

    var level2KeyCol = {};
    var level2Keys = [];
    
    // get all keys in level 2
    for (var i = 0; i < input.length; i++) {
        input[i].keyedItem = {}; // store level2 data as hash list
        var level1values = input[i].values;
        for (var j = 0; j < level1values.length; j++) {
            var key = level1values[j].key + '';
            input[i].keyedItem[key] = level1values[j];
            // collect level2 keys
            if (!level2KeyCol[key]) {
                level2KeyCol[key] = true;
            }
        }
    }
    for (var key in level2KeyCol) {
        level2Keys.push(key);
    }
    level2Keys = level2Keys.sort();
    
    // create normalized data
    var output = [];
    for (var i = 0; i < input.length; i++) {
        var level1key = input[i].key;
        var level1values = input[i].values;
        var keyedItem = input[i].keyedItem;
        var newValues = [];
        for (var j = 0; j < level2Keys.length; j++) {
            var key = level2Keys[j];
            if (keyedItem[key]) {
                newValues.push(keyedItem[key]);
            } else {
                newValues.push({ key:key, values:[], count:0 });
            }
        }
        output.push({ key:level1key, values:newValues });
    }
    return output;
}

$().ready(function() {
    setupChart();
    displaySubjectTable();
});
var app = app || {};

$().ready(function() {
	app.activeAssayAccession = '';
	setupTemplates();
	$('.table-mechanistic-assays tbody tr').click(onRowClick);
	// locate detail for first assay
	var assayRow = $('.table-mechanistic-assays tbody tr:first');
	if (assayRow) {
		onRowClick({ currentTarget:assayRow });
	}
	// tab item click
	$('.nav-items .nav-item').click(onNavItemClick);
});

function setupTemplates() {
    // study title
    app.assayDetailSource   = $("#assay-detail-template").html();
    app.assayDetailTemplate = Handlebars.compile(app.assayDetailSource);
    // protocol template
    app.protocolDetailSource   = $("#protocol-detail-template").html();
    app.protocolDetailTemplate = Handlebars.compile(app.protocolDetailSource);
    // reagent template
    app.reagentDetailSource   = $("#reagent-detail-template").html();
    app.reagentDetailTemplate = Handlebars.compile(app.reagentDetailSource);
    // treatment template
    app.treatmentDetailSource   = $("#treatment-detail-template").html();
    app.treatmentDetailTemplate = Handlebars.compile(app.treatmentDetailSource);
}

function onNavItemClick(event) {
	//console.log('>', event, event.currentTarget);
	$('.nav-items .nav-item').removeClass('active');
	$(event.currentTarget).addClass('active');
	var navItem = $(event.currentTarget).attr('id');
	showNavItem(navItem);
}

function onRowClick(event) {
	// highlight rowAssay
	$('.table-mechanistic-assays tr').css('background-color', '#fff');
	$(event.currentTarget).css('background-color', '#f2f2f2');
	// show accession detail
	var rowAssay = $(event.currentTarget);
	var accession = rowAssay.attr('data-accession');
	getDetailContent(accession, updateAssayDetailDisplay);
}

function updateAssayDetailDisplay(data) {
	var accession = data.experimentAccession;
	$('.experiment-accession-number').text(accession);
	if (data.protocols && data.protocols.length > 0) {
		//console.log('show protocols:', data.protocols);
		showProtocols(data.protocols);
	}
	if (data.reagents && data.reagents.length > 0) {
		//console.log('show reagents:', data.reagents);
		showReagents(data.reagents);
	}
	if (data.treatments && data.treatments.length > 0) {
		//console.log('show treatments:', data.treatments);
		showTreatments(data.treatments);
	}
	// show protocols
	showNavItem('protocols');
}

function showNavItem(navItem) {
	$('.table-protocols').hide();
	$('.table-reagents').hide();
	$('.table-treatments').hide();
	$('.table-' + navItem).show();
	$('.nav-items .nav-item').removeClass('active');
	$('.nav-items .nav-item-' + navItem).addClass('active');
}

function showProtocols(protocols) {
	$('.table-protocols tbody').empty();
	var content = app.protocolDetailTemplate(protocols);
    for (var i = 0; i < protocols.length; i++) {
    	var content = app.protocolDetailTemplate(protocols[i]);
    	$('.table-protocols tbody').append(content);
    }
}

function showReagents(reagents) {
	$('.table-reagents tbody').empty();
	var content = app.reagentDetailTemplate(reagents);
    for (var i = 0; i < reagents.length; i++) {
    	var content = app.reagentDetailTemplate(reagents[i]);
    	$('.table-reagents tbody').append(content);
    }
}

function showTreatments(treatments) {
	$('.table-treatments tbody').empty();
	var content = app.treatmentDetailTemplate(treatments);
    for (var i = 0; i < treatments.length; i++) {
    	var content = app.treatmentDetailTemplate(treatments[i]);
    	$('.table-treatments tbody').append(content);
    }
}

function getDetailContent(accession, callback) {
	var url = CONTEXT_ROOT + '/public/research/mechanisticAssay/getExperiment/' + accession;
	var parameter = {};
	$.get(url, parameter, function(data) {
		data = JSON.parse(data);
		//console.log('experiment detail:', data);
	    var content = app.assayDetailTemplate(data);
		callback && callback(data);
	});
}


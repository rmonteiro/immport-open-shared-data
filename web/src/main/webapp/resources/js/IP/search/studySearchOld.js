var searchURL = CONTEXT_ROOT + "/search/immport-share/search-facet";
var spellURL = CONTEXT_ROOT + "/search/immport-share/spell";

var app = {
    pager: { pagerstart:1, pagesize:10, pagebuttons:5, pagemax:0 },
    studyIds: [],      // array containing study ids
    facets: [],         // stores facets from query results
    facetTreeCol: {},   // stores facet trees
    facetValueCol: {},  // stores checked values in facet trees
    facetNameHash: {},  // hash for facet display name, keyed by facet name
    searchCondition: { q:'', facetValueCol:null, start:0, rows:10 } // store search condition
};

app.facetNameHash = {
	'clinicalTrial': 'Clinical Trial',
	'subjectSpecies': 'Species',
	'biosampleType': 'Biosample Type',
    'experimentMeasurementTechnique': 'Experiment Measurement Technique',
    'studyType': 'Study Type',
    'researchFocus': 'Research Focus'
};

Ext.onReady(function() {
	if (clinicalTrial != "") {
		app.facetValueCol['clinicalTrial'] = clinicalTrial;
	}
	if (researchFocus != "") {
		app.facetValueCol['researchFocus'] = researchFocus;
	}
	if (studyType != "") {
		app.facetValueCol['studyType'] = studyType;
	}
	if (subjectSpecies != "") {
		app.facetValueCol['subjectSpecies'] = subjectSpecies;
	}
	if (biosampleType != "") {
		app.facetValueCol['biosampleType'] = biosampleType;
	}
	if (experimentMeasurementTechnique != "") {
		app.facetValueCol['experimentMeasurementTechnique'] = experimentMeasurementTechnique;
	}
	
    $('#setting-container').click(function(event) {
        $('#dialogModal').modal();
    });
    $(window).resize(function() {
        var facetTreeWidth = $('#facet-container').width();
        for (var facetName in app.facetTreeCol) {
            app.facetTreeCol[facetName].setWidth(facetTreeWidth);
        }
    })
    $('#dialogModal .btn-next').click(function(event) {
        var isShowSummary = $('#checkboxShowSummary').is(':checked');
        var isShowHighlight= $('#checkboxShowHighlight').is(':checked');
        updateSummaryDisplay({
            isShowSummary: isShowSummary,
            isShowHighlight: isShowHighlight
        });
        $('#dialogModal').modal('hide');
    });
    // set study dialogbox height to 85% of browser height
    $('#studyModal').on('shown.bs.modal', function () {
        var heightAttribute = Math.floor($(window).height() * 0.85) + 'px';
        $(this).find('.modal-body').css({'height':heightAttribute});
    });
    $('#btnPopPrevStudy').click(doPopPrevStudy);
    $('#btnPopNextStudy').click(doPopNextStudy);
    $('#btnPopOpenStudy').click(doPopOpenStudy);
    $('#btnPopDownloadStudy').click(doPopDownloadStudy);
    $('#btnQuery').click(function(event) {
        var q = getQueryString()
        solrSearch(q, app.facetValueCol, 0, 10);
    });
    $('#btnClearQuery').click(function(event) {
        clearQueryString();
        var q = getQueryString()
        app.facetValueCol = {};
        solrSearch(q, app.facetValueCol, 0, 10);
    });
    if (searchTerm) {
    	$('#queryStringLocal').val(searchTerm);
    }
    var q = getQueryString();
    solrSearch(q, app.facetValueCol, 0, 10);
});

var doDelayedSearch = function() {
    app.keyDelayTimer = -1;
    var q = Ext.get('queryStringLocal').getValue();
    solrSearch(q, null, 0, 10);
}

var studyTemplate = Ext.create('Ext.XTemplate', [
  '<tpl for=".">',
  '<div class="study-panel">',
    '<div class="row" style="margin-bottom:10px;">',
      '<div class="col-md-2">',
        '<span class="study-id-label">{studyAccession}</span> ',
        '<a href="' + CONTEXT_ROOT + '/public/study/study/displayStudyDetail10/{studyAccession}" target="_blank">',
            '<i class="glyphicon glyphicon-new-window" style="padding-bottom:3px;"></i>', 
        '</a>',
        '<div class="study-download">',
	      '<a href="' + CONTEXT_ROOT + '/public/study/study/studyDownload/{studyAccession}" class="btn btn-info">Download</a>',
	    '</div>',
      '</div>',
      '<div class="col-md-10" style="white-space:normal;">',
        '<span class="study-title" id="{studyAccession}">{ briefTitle }</span>',
      '</div>',
    '</div>',
    '<div class="row">',
      '<div class="col-md-9">',
        '<div class="study-description muted" style="white-space:normal;">',
          '{briefDescription}',
        '</div>',
        '<div class="study-highlight" style="white-space:normal;">',
          '<tpl for="highlighting">',
            '...{.}...<br/>',
          '</tpl>',
        '</div>',
        '<div class="study-highlight" style="white-space:normal;">',
          '<tpl for="personnel">',
            '{.}<br/>',
          '</tpl>',
        '</div>',
      '</div>',
      '<div class="col-md-3">',
        '<div class="study-graph">',
          '<img src="' + CONTEXT_ROOT + '/public/study/study/getStudyImage?studyAccession={studyAccession}" class="study-graph-thumbview" />',
        '</div>',
      '</div>',
    '</div>',
  '</div>',
  '</tpl>'
]);

studyTemplate.compile();

var studyTypeTemplate = Ext.create('Ext.XTemplate', [
  'Study Types<br>',
  '<ul>',
  '<tpl for=".">',
    '<li> {name} ({count})</li>',
  '</tpl>',
  '</ul>'
]);

var experimentTypeTemplate = Ext.create('Ext.XTemplate', [
   'Experiment Types<br>',
    '<ul>',
    '<tpl for=".">',
    '<li> {name} ({count})</li>',
    '</tpl>',
    '</ul>'
]);

var resultTemplate = Ext.create('Ext.XTemplate', [
  'Found {hitCount} Studies in {qTime} ms'
]);

studyTypeTemplate.compile();
experimentTypeTemplate.compile();
resultTemplate.compile();

/**
 * do Solr search
 * @param q string
 * @param facetValue object Example: { 'studyType': ['Observational'] }
 * @param start number
 * @param rows number 
 */
var solrSearch = function(q, facetValueCol, start, rows) {
    
	// ADD BACK FOR GOOGLE ANALYTICS
    //ga('send', 'pageview', {
    //    'page': '/solrSearch?q=' + q,
    //    'title': 'Solr Search'
    //});
    
    app.searchCondition = { q:q, facetValueCol:facetValueCol, start:start, rows:rows };
    var fqvalues = [];
    for (var key in facetValueCol) {
        var values = facetValueCol[key];
        var quotedValues = [];
        for (var i = 0; i < values.length; i++) {
            quotedValues.push('"' + values[i] + '"');
        }
        fqvalues.push(key + ':(' + quotedValues.join(' OR ') + ')');
    }
    
    Ext.Ajax.request({
        url: searchURL,
        params: {
            'q': q,
            'fq': fqvalues,
            'hl.simple.pre': '<span class="search-highlight-term search-highlight-mark">',
            'hl.simple.post': '</\span>',
            'start': start,
            'rows': rows
        },
        success: function(response) {
            var solrJson = Ext.decode(response.responseText);
            var highlightFields = [ "briefTitle","briefDescription",
                   "conditionStudied","description","endpoints",
                   "hypothesis","objectives","experimentDescription",
                   "experimentHypothesis", "experimentPurpose",
                   "experimentRationale","experimentTitle","armDescription",
                   "armName","protocolDescription","protocolFile",
                   "reagentDescription","contractName", "contractSummary",
                   "fcsAnnotationPanel","inclusionExclusion","programSummary",
                   "programTitle","glossaryDefinition","pubmedAuthors",
                   "compoundNameReported", "compountNameOtherNameReported" ];
            app.studyIds = [];
            for (var i = 0; i < solrJson.response.docs.length; i++) {
                var doc = solrJson.response.docs[i];
                var studyAccession = doc.studyAccession;
                app.studyIds.push(studyAccession);
                solrJson.response.docs[i].highlighting = [];
                for (var j = 0; j < highlightFields.length; j++) {
                    var field = highlightFields[j];
                    if (solrJson.highlighting[studyAccession][field]) {
                        solrJson.response.docs[i].highlighting.push(solrJson.highlighting[studyAccession][field][0]);
                    }
                }
                solrJson.response.docs[i].personnel = [];
                if (solrJson.response.docs[i].briefDescription.length > 400) {
                    solrJson.response.docs[i].briefDescription = solrJson.response.docs[i].briefDescription.substring(0,397) + "...";
                }
                for (var j= 0; j < doc.personnelLastName.length; j++) {
                    if (doc.personnelRoleInStudy[j] == 'Principal Investigator') {
                        var name = doc.personnelFirstName[j] + " " + doc.personnelLastName[j] + ", " + doc.personnelOrganization[j];
                        solrJson.response.docs[i].personnel.push(name);
                    }
                }
            }
            showResult(solrJson);
        }
    })
}

var buildFacetPanel = function(data) {
    var containerId = 'facet-container';
    var fields = [];
    var facetsTag = '';
    $('#' + containerId).text('');
    for (var property in data.facet_fields) {
        fields.push({ 
            name: property, 
            values: data.facet_fields[property]
        });
        facetsTag += '<div id="' + property +'Div" class="facet-tree-container"></div>';
    }
    $('#' + containerId).append(facetsTag);
    app.facets = [];
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        app.facets.push(field.name);
        buildFacetTree(field.name, field.values);
    }
}

var buildFacetTree = function(facetName, values) {
    //console.log('   ' + facetName + ' ' + values.length);
    var containerId = facetName + 'Div';
    var count = values.length / 2;
    var facetData = { 
        root: {
            expanded: true,
            cls: "folder",
            children: []
        }
    };
    var facetDisplayName = getFacetDisplayName(facetName);
    var facetCheckedValues = app.facetValueCol[facetName];
    for (var i = 0; i < count; i++) {
        var itemValue = values[2 * i];
        var itemValueCount = values[2 * i + 1];
        var isChecked = $.inArray(itemValue, facetCheckedValues) >= 0;
        facetData.root.children.push({
            id: itemValue,
            text: itemValue + ' (' + itemValueCount + ')',
            checked: isChecked,
            leaf: true
        });
    }
    var store = Ext.create('Ext.data.TreeStore', facetData);
    var treePanel = Ext.create('Ext.tree.Panel', {
        id: facetName + 'Tree',
        name: facetName,
        title: facetDisplayName,
        width: '100%',
        forceFit: true,
        //height: 160,
        useArrows: true,
        store: store,
        rootVisible: false,
        renderTo: containerId
        /*
        listeners: {
            click: {
                element: 'el', //bind to the underlying el property on the panel
                fn: function(event){ console.log('click el', event); }
            }
        }
        */
    });
    app.facetTreeCol[facetName] = treePanel;
    treePanel.on('checkchange', doFacetChange.bind(treePanel));
}

var getFacetDisplayName = function(facetName) {
    // capitalize first letter
    /*
    var letters = facetName.split('');
    letters[0] = letters[0].toUpperCase();
    return letters.join('');
    */
    var facetDisplayName = app.facetNameHash[facetName] || facetName;
    return facetDisplayName;
}

/*
setting:
    isShowSummary: isShowSummary,
    isShowHighlight: isShowHighlight,
*/
var updateSummaryDisplay = function(setting) {
    if (setting.isShowSummary) {
        $('.study-description').show();
    } else {
        $('.study-description').hide()
    }
    if (setting.isShowHighlight) {
        $('.study-highlight').show();
        //$('.search-highlight-mark').addClass('search-highlight-term');
    } else {
        $('.study-highlight').hide();
        //$('.search-highlight-mark').removeClass('search-highlight-term');
    }
}

var doFacetChange = function(node, checked, options) {
    //console.log('tree check change:', node.internalId, checked, this.name);
    app.facetValueCol = {};
    for (var i = 0; i < app.facets.length; i++) {
        var facetName = app.facets[i];
        var facetTree = app.facetTreeCol[facetName];
        var checkedNodes = facetTree.getChecked();
        
        var values = [];
        for (var j = 0; j < checkedNodes.length; j++) {
            values.push(checkedNodes[j].internalId);
            //console.log('  ' + facetName + ' ' + checkedNodes[j].internalId);
        }
        if (values.length > 0) {
            app.facetValueCol[facetName] = values;
        }
    }
    var q = getQueryString();
    solrSearch(q, app.facetValueCol, 0, 10);
};

// searchCondition example: { q:'', facetValueCol:null, start:0, rows:10 }
// result example: { hitCount: 58, 'qTime':371 }:
var updatePagingToolbar = function(searchCondition, result) {
    //console.log('>>>', searchCondition, result);
    var pagerButtons = $('.catalog-pager .pager-button');
    pagerButtons.off('click');
    pagerButtons.click(doPaging.bind(searchCondition));
    
    var start = searchCondition.start;
    var pagetotal = app.pager.pagesize * app.pager.pagebuttons;
    var pagenumber = Math.floor(start/pagetotal);
    var pageindex = Math.ceil((start % pagetotal) / app.pager.pagesize) + 1;
    app.pager.pagerstart = pagenumber * app.pager.pagebuttons;
    app.pager.pagemax = Math.ceil(result.hitCount / app.pager.pagesize);
    
    $('a.pager-button').removeClass('pager-active');
    $('a.pager-' + pageindex).addClass('pager-active');
    
    for (var i = 0; i < pagerButtons.length; i++) {
        var pagerButton = $(pagerButtons[i]);
        if (pagerButton.hasClass('pager-1')) {
            updatePagerButton(pagerButton, 1, app.pager.pagerstart, app.pager.pagemax);
        } else if (pagerButton.hasClass('pager-2')) {
            updatePagerButton(pagerButton, 2, app.pager.pagerstart, app.pager.pagemax);
        } else if (pagerButton.hasClass('pager-3')) {
            updatePagerButton(pagerButton, 3, app.pager.pagerstart, app.pager.pagemax);
        } else if (pagerButton.hasClass('pager-4')) {
            updatePagerButton(pagerButton, 4, app.pager.pagerstart, app.pager.pagemax);
        } else if (pagerButton.hasClass('pager-5')) {
            updatePagerButton(pagerButton, 5, app.pager.pagerstart, app.pager.pagemax);
        }
    }
}

var updatePagerButton = function(button, number, pagerstart, pagemax) {
    var pageNumber = app.pager.pagerstart + number;
    if (pageNumber <= pagemax) {
        button.html(pageNumber);
    } else {
        button.html('&nbsp;');
    }
}

var doPaging = function(event) {
    var pagetotal = app.pager.pagesize * app.pager.pagebuttons;
    var pagerButton = $(event.currentTarget);
    var pagevalue = pagerButton.text();
    
    if ($.isNumeric(pagevalue)) {
        var start = (parseInt(pagevalue) - 1) * app.pager.pagesize;
        solrSearch(this.q, this.facetValueCol, start, this.rows);
    } else if (pagerButton.hasClass('pager-prev')) {
        app.pager.pagerstart = (app.pager.pagerstart - app.pager.pagebuttons);
        if (app.pager.pagerstart < 0) {
            app.pager.pagerstart = 0;
        }
        start =  app.pager.pagerstart * app.pager.pagesize;
        solrSearch(this.q, this.facetValueCol, start, this.rows);
    } else if (pagerButton.hasClass('pager-next')) {
        var pagerstart = (app.pager.pagerstart +  app.pager.pagebuttons);
        if (pagerstart <= app.pager.pagemax) {
            app.pager.pagerstart = pagerstart;
            start =  app.pager.pagerstart * app.pager.pagesize;
            solrSearch(this.q, this.facetValueCol, start, this.rows);
        }
    }
}

function showResult(solrJson) {
    
    Ext.get('catalog-container').update();
    studyTemplate.append('catalog-container', solrJson.response.docs);
    buildFacetPanel(solrJson.facet_counts);
    
    var results = { 'hitCount': solrJson.response.numFound, 'qTime': solrJson.responseHeader.QTime };
    Ext.get('result-container').update();
    resultTemplate.append('result-container', results);
    
    // update paging toolbar
    updatePagingToolbar(app.searchCondition, results);
    
    // add click listener on sutdy title
    $('.study-title').off('click');
    $('.study-title').click(function(event) {
        var studyId = event.currentTarget.id;
        updateStudyPopup(studyId);
        $('#studyModal').modal();
    });
    
    // add click listener on image element for showing in light-box
    // Ext.select('.study-graph img', true).on('click', function(event) {
    $('.study-graph img').off('click');
    $('.study-graph img').click(function(event) {
        var image = event.currentTarget;
        var studyAccession = image.src.split('=')[1];
        displaySchematic(studyAccession);
        // show image in light-box
        //$('#studyLightbox img').attr('src', image.src);
        //$('#studyLightbox .lightbox-caption').text('Study ' + studyAccession);
        //$('#studyLightbox').lightbox({ backdrop:false, keyboard:true, show:true,resizeToFit:true });
    });
}

function doPopPrevStudy(event) {
    var studyId = $('#studyModelStudyId').text();
    var studyIndex = app.studyIds.indexOf(studyId);
    if (studyIndex > 0) {
        updateStudyPopup(app.studyIds[studyIndex - 1]);
    }
}

function doPopNextStudy(event) {
    var studyId = $('#studyModelStudyId').text();
    var studyIndex = app.studyIds.indexOf(studyId);
    if (studyIndex < app.studyIds.length - 1) {
        updateStudyPopup(app.studyIds[studyIndex + 1]);
    }
}

function updateStudyPopup(studyId) {
    var studyDetailPopupUrl = CONTEXT_ROOT + '/public/study/study/displayStudyDetail20/' + studyId;
    var studyContent = '<iframe class="study-detail-embed" src="' + studyDetailPopupUrl + '" frameborder="0"/>';
    $('#studyModelStudyId').html(studyId);
    $('#studyModelDetail').html(studyContent);
    // update navigation button
    var studyIndex = app.studyIds.indexOf(studyId);
    if (studyIndex == 0) {
    	$('#btnPopPrevStudy').prop('disabled', true);
    } else {
    	$('#btnPopPrevStudy').prop('disabled', false);
    }
    if (studyIndex + 1 == app.studyIds.length) {
    	$('#btnPopNextStudy').prop('disabled', true);
    } else {
    	$('#btnPopNextStudy').prop('disabled', false);
    }
}

function doPopOpenStudy(event) {
    var studyId = $('#studyModelStudyId').text();
    var studyDetailUrl = CONTEXT_ROOT + '/public/study/study/displayStudyDetail10/' + studyId;
    var detailWindow = window.open(studyDetailUrl, '_blank');
}

function doPopDownloadStudy(event) {
    var studyId = $('#studyModelStudyId').text();
    //var studyDownloadUrl = 'https://immport.niaid.nih.gov/immportWeb/home/display.do?content=' + studyId;
    // example: /immport-open/public/study/study/studyDownload/SDY180
    var studyDownloadUrl = CONTEXT_ROOT + '/public/study/study/studyDownload/' + studyId;
    window.open(studyDownloadUrl, '_blank');
    
}

var displaySchematic = function(studyAccession) {
    var imageUrl = '<img src=' + CONTEXT_ROOT + '/public/study/study/getStudyImage?' + 
        'studyAccession=' + studyAccession + ' height="690" width="690" />';
    Ext.create('Ext.window.Window',{
        width: 700,
        height: 700,
        constrain: true,
        model: true,
        resizable: false,
        draggable: false,
        title: "Study Schematic",
        items: [{
            xtype: 'panel',
            height: 700,
            width: 700,
            html: imageUrl
        }]
    }).show();
}


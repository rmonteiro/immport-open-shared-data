
var facetApp = {
	searchURL: CONTEXT_ROOT + "/search/shared_data/search-facet",
    studyIds: [],                   // array containing study ids
    facets: [],                     // stores facets from query results
    facetTreeCol: {},               // stores facet trees
    facetValueCol: {},              // stores checked values in facet trees
    facetNameHash: {},              // hash for facet display name, keyed by facet name
    searchCondition: { q:'', facetValueCol:null, start:0, rows:10 }, // store search condition
};

facetApp.facetNameHash = {
	'clinicalTrial': 'Clinical Trial',
	'subjectSpecies': 'Species',
	'biosampleType': 'Biosample Type',
    'experimentMeasurementTechnique': 'Assay Type',
    'studyType': 'Study Type',
    'researchFocus': 'Research Focus'
};


var buildFacetContainer = function(facets) {
	$('#facet-container').empty();
	var facetsContainer = '<div class="row">';
	
	// Control the layout of the Facet Panels
    facetsContainer += '<div class="col-md-4">';
    facetsContainer += buildFacetPanel('clinicalTrial',facets);
    facetsContainer += buildFacetPanel('studyType',facets);
    facetsContainer += buildFacetPanel('researchFocus',facets);
    facetsContainer += '</div>';
    facetsContainer += '<div class="col-md-4">';
    facetsContainer += buildFacetPanel('subjectSpecies',facets);
    facetsContainer += buildFacetPanel('biosampleType',facets);
    facetsContainer += '</div>';
    facetsContainer += '<div class="col-md-4">';
    facetsContainer += buildFacetPanel('experimentMeasurementTechnique',facets);
    facetsContainer += '</div>';
	facetsContainer += '</div>';
    $('#facet-container').append(facetsContainer);
}

var buildFacetPanel = function(facet,facets) {
	var facetsPanel = '<div class="panel-heading">';
    facetsPanel += '<div class="panel-title">' + getFacetDisplayName(facet) + '</div>';
    facetsPanel += '</div>';
    facetsPanel += '<div class="panel-body">';
    var facetCheckedValues = facetApp.facetValueCol[facet];
    var values = facets[facet];
    var count = values.length / 2;
    for (var i = 0; i < count; i++) {
        var itemValue = values[2 * i];
        var itemValueCount = values[2 * i + 1];
        var isChecked = $.inArray(itemValue, facetCheckedValues) >= 0;
        checkedStr = "";
        if (isChecked) {
        	checkedStr = "checked";
        }
        facetsPanel += '<div><input type="checkbox" class="facetCheckbox" value="' + facet + "::" + itemValue + '" ' + checkedStr + ' > ' + itemValue + ' (' + itemValueCount + ')</div>';
    }
    facetsPanel += '</div>';
    return facetsPanel;
}


var getFacetDisplayName = function(facetName) {
    var facetDisplayName = facetApp.facetNameHash[facetName] || facetName;
    return facetDisplayName;
}


/**
 * do Solr search
 * @param q string
 * @param facetValue object Example: { 'studyType': ['Observational'] }
 * @param start number
 * @param rows number 
 */
var facetSolrSearch = function(q, facetValueCol, start, rows) {
    
	// Send message to Google Analytics
    ga('send', 'pageview', {
        'page': '/solrSearch?q=' + q,
        'title': 'Solr Search'
    });
    
    var fqvalues = [];
    for (var key in facetValueCol) {
        var values = facetValueCol[key];
        var quotedValues = [];
        for (var i = 0; i < values.length; i++) {
            quotedValues.push('"' + values[i] + '"');
        }
        fqvalues.push(key + ':(' + quotedValues.join(' OR ') + ')');
    }
    
    $.ajax({
        url: facetApp.searchURL,
        traditional: true,
        data: {
            'fq': fqvalues,
            'hl.simple.pre': '<span class="search-highlight-term search-highlight-mark">',
            'hl.simple.post': '</\span>',
            'start': start,
            'rows': rows
        },
        success: function(response) {
            buildFacetContainer(response.facet_counts.facet_fields);
        }
    })
}



$().ready(function() {

    $('#btnSearch').click(function(event) {
        var searchValues = [];
        for (var key in facetApp.facetValueCol) {
            var values = facetApp.facetValueCol[key];
            if (values.length > 0) {
            	    searchValues.push(key + '=' + values);
            }
        }
        
      	var url = CONTEXT_ROOT + '/public/home/studySearch?' + searchValues.join('&');
        window.location = url;
    });
    
    $('#btnClearQuery').click(function(event) {
        facetApp.facetValueCol = {};
        facetSolrSearch("",facetApp.facetValueCol,0,10);
    });

    
    $('#facet-container').on('click','.facetCheckbox',function() {
    	var data = this.value.split("::");
    	if (this.checked) {
    		if (facetApp.facetValueCol[data[0]] == undefined) {
    			facetApp.facetValueCol[data[0]] = [];
    		}
    		facetApp.facetValueCol[data[0]].push(data[1]);
    	} else {
    		for (var i = 0; i < facetApp.facetValueCol[data[0]].length; i++) {
    	        if (data[1] === facetApp.facetValueCol[data[0]][i]) {
    	          facetApp.facetValueCol[data[0]].splice(i,1);
    	        }
    	    }
    		if (facetApp.facetValueCol[data[0]].length == 0) {
    			delete facetApp.facetValueCol[data[0]];
    		}
    	}
    	  
    	  facetSolrSearch("",facetApp.facetValueCol,0,10);
    });
   
    facetSolrSearch("", facetApp.facetValueCol, 0, 10);
});

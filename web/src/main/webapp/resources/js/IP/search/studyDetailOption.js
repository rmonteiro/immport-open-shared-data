var app = app || {};
app.studyAccession = studyAccession;

// This is the complete list for documenation purposes, 'design' is added later
//app.panelOrder = ['design','adverseevents','arm_or_cohorts','assessments','conmeds'
//'demographics','documentation','glossary','inclusionExclusion',
//                  'labtests','mechassays','publications','resources','study_files',
//                  'subject','summary','treatments','substanceUse'];
//
app.panelOrder = ['design','adverseevents','arm_or_cohorts','assessments','treatments',
                  'conmeds','demographics','documentation','labtests','mechassays',
                  'resources','study_files','subject','summary','substanceUse'
];

app.panels = study2Panels.toLowerCase().split(',');
for (i = 0; i < app.panels.length; i++) {
    app.panels[i] = app.panels[i].replace(/ /g,"_");	
}
console.log(app.panels);

app.panels.unshift('design'); // add design on top of array
app.panelCol = {
    summary: { 
        title: 'Summary', 
        url: CONTEXT_ROOT + '/public/study/study/displayStudyHtml/' + studyAccession 
    },
    design: { 
        title: 'Design', 
        url: CONTEXT_ROOT + '/public/study/studyDesign/displayStudyDesignHtml/' + studyAccession 
    },
    // inclusionExclusion
    inclusionexclusion: {
        title: "Inclusion Exclusion Criteria",
        url: CONTEXT_ROOT + '/public/clinical/inclusionExclusion/displayInclusionExclusionHtml/' + studyAccession
    },
    conmeds: {
        title: "Medications",
        url: CONTEXT_ROOT + '/public/clinical/substanceMerge/displaySubstanceMergeHtml/' + studyAccession
    },
    substanceuse: {
        title: "Substance Use",
        url: CONTEXT_ROOT + '/public/clinical/substanceUse/displaySubstanceUseHtml/' + studyAccession
    },
    treatments: {
        title: "Interventions",
        url: CONTEXT_ROOT + '/public/clinical/intervention/displayInterventionHtml/' + studyAccession
    },
    demographics: {
        title: "Demographics",
        url: CONTEXT_ROOT + '/public/research/subject/displaySubjectHtml/' + studyAccession
    },
    labtests: {
        title: "Lab Tests",
        url: CONTEXT_ROOT + '/public/clinical/labTest/displayLabTestHtml/' + studyAccession
    },
    adverseevents: {
        title: "Adverse Event",
        url: CONTEXT_ROOT + '/public/clinical/adverseEvent/displayAdverseEventHtml/' + studyAccession
    },
    assessments: {
        title: "Assessment",
        url: CONTEXT_ROOT + '/public/clinical/assessment/displayAssessmentHtml/' + studyAccession
    },
    glossary: {
        title: "Glossary",
        url: CONTEXT_ROOT + '/public/study/glossary/displayGlossaryHtml/' + studyAccession
    },
    publications: {
        title: "Publication",
        url: CONTEXT_ROOT + '/public/study/studyPubmed/displayStudyPubmedHtml/' + studyAccession
    },
    studylink: {
        title: "Study Links",
        url: CONTEXT_ROOT + '/public/study/studyLink/displayStudyLinkHtml/' + studyAccession
    },
    protocols: {
        title: "Protocols",
        url: CONTEXT_ROOT + '/public/research/protocol/displayProtocolHtml/' + studyAccession
    },
    study_files: {
        title: "Study Files",
        url: CONTEXT_ROOT + '/public/study/studyFile/displayStudyFileHtml/' + studyAccession
    },
    mechassays: {
        title: "Mechanistic Assays",
        url: CONTEXT_ROOT + '/public/research/mechanisticAssay/displayMechanisticAssayHtml/' + studyAccession
    }
};
// add tabId and panelId to app.panelCol
for (var key in app.panelCol) {
    var capKey = key.charAt(0).toUpperCase() + key.substr(1);
    app.panelCol[key].key = key;
    app.panelCol[key].tabId = 'tab' + capKey;
    app.panelCol[key].panelId = 'panel' + capKey;
}

$().ready(function() {
	// Listen for resize event to adjust grids.
	/*
	$(window).resize(function() {
        var grids = Ext.ComponentQuery.query('gridpanel')
        var containerWidth = $('.tabContainer').width();
        var gridWidth = containerWidth * 0.90;
        var gridHeight = 300;
        for (i = 0; i < grids.length; i++) {
	        grids[i].setSize(gridWidth,gridHeight);
	        grids[i].doLayout();
        }
	});
	*/
	
    // setup tabs, the other tabs will be added dynamically
    var navTabs = $('.tabContainer ul.nav-tabs');
    var navContent = $('.tabContainer .tab-content');
    
    // Loop through the panels in order, if they match a name
    // returned by the study2panel query add a tab.
    for (var i = 0; i < app.panelOrder.length; i++) {
        var panelName = app.panelOrder[i];
        if ($.inArray(panelName,app.panels) > -1 ) {
            var panelData = app.panelCol[panelName];
            if (panelName && panelData && panelName != 'summary') {
                var tabTag = '<li><a href="#' + panelData.tabId + '" data-toggle="tab">' + 
                    panelData.title + '</a></li>';
                navTabs.append(tabTag);
                var tabContentTag = '<div class="tab-pane" id="' + panelData.tabId + '">' +
                    '<div id="' + panelData.panelId + '"></div></div>';
                navContent.append(tabContentTag);
            }
        }
    }
    
    // fill first tab
    fillPanel('summary');
    // add tab listener to fill panel on demand
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (event) {
        var hashValue = $(event.currentTarget).attr('href');
        var tabKey = hashValue.replace('#tab', '').toLowerCase();
        var tabId = hashValue.replace('#', '');
        var panelId = tabId.replace('tab', 'panel');
        var tabName = $(event.currentTarget).text();
        
        if ($('#' + panelId).html() == '') {
            fillPanel(tabKey);
        }
    });
    
})

function fillPanel(panelKey) {
    var panelData = app.panelCol && app.panelCol[panelKey];
    if (panelKey == 'summary') {
    	var page = "displayStudyDetailModal/" + app.studyAccession;
    	ga('send','pageview',{
            'title': "Study Detail Modal",
            'page': page
          });
    }
    if (panelData) {
        var panelId = panelData.panelId;
        var panelUrl = panelData.url;
        ga('send', 'event', 'Study Detail Tabs', 'click', panelId);
        $('#' + panelId).load(panelUrl);
    }
}

app = app || {};
app.keyDelayTime = 500;    // timer delay on reaction to search bar key stroke
app.keyDelayTimer = -1;  // delay timer

function clearQueryString() {
    $('#queryString').val('');
}

function getQueryString() {
    return $('#queryString').val();
}

Ext.onReady(function() {
    Ext.get('queryString').addListener('keyup', function(event) {
        var q = Ext.get('queryString').getValue();
        if (q.length > 3) {
            if (app.keyDelayTimer > 0) {
                // reset delay timer
                clearTimeout(app.keyDelayTimer);
                app.keyDelayTimer = setTimeout(doDelayedSearch, app.keyDelayTime);
            } else {
                solrSearch(q, null, 0, 10);
                // set delay timer
                app.keyDelayTimer = setTimeout(doDelayedSearch, app.keyDelayTime);
            }
        }
        return true;
    });
});

function doDelayedSearch() {
    app.keyDelayTimer = -1;
    var q = getQueryString();
    solrSearch(q, null, 0, 10);
}


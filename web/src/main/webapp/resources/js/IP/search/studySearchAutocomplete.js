app = app || {};

/*
function clearQueryString() {
    app.searchCombo.clearValue();
}

function getQueryString() {
    return app.searchCombo.getValue();
}
*/

function clearQueryString() {
    $('#queryString').val('');
}

function getQueryString() {
    return $('#queryString').val();
}

Ext.onReady(function() {
    
    $('#queryString').typeahead([{
        name: 'study-search',
        remote: {
            url: CONTEXT_ROOT + '/search/shared_data/spell?page=1&start=0&limit=25&q=%QUERY',
            filter: function(response) {
                var results = [];
                var suggestions = response.spellcheck && response.spellcheck.suggestions;
                if (suggestions) {
                    for (var i = 2; i < suggestions.length; i++) {
                        var suggestion = suggestions[i].collation[0].collationQuery;
                        results.push(suggestion);
                    }
                }
                return results;
            }
        }
    }]);
    
    
    /*
    Ext.define('SolrReader', {
        extend: 'Ext.data.reader.Json',
        alias: 'reader.solrReader',
        readRecords: function(data) {
            var suggestions = [];
            for (var i = 0; i < data.spellcheck.suggestions.length; i++) {
                var obj = data.spellcheck.suggestions[i];
                if (obj['collation']) {
                    suggestions.push({ 
                        'term': obj['collation'][0]['collationQuery'], 
                        'count': obj['collation'][1]['hits']
                    });
                }   
            }
            data.suggestions = suggestions;
            data.metaData = { root: 'suggestions'}
            return this.callParent([data]);
        }
    });

    app.searchStore = Ext.create('Ext.data.Store', {
      queryMode: 'remote',
      fields: ['term','count'],
      proxy: {
          type: 'ajax',
          url: spellURL,
          reader: 'solrReader'
      }
    });
    
    app.searchCombo = Ext.create('Ext.form.field.ComboBox', {
      store: app.searchStore,
      fieldLabel: "",
      renderTo: 'searchCombo',
      hideTrigger: true,
      typeAhead: true,
      triggerAction: 'query',
      valueField: 'term',
      displayField: 'term',
      minChars: 3,
      queryParam: 'q',
      width: '710px',
      cls: 'form-control',
      id: 'queryString',
      listeners: {
          select: function(combo,selection) {
              solrSearch(combo.value, null, 0, 10);
          }
      }
    });
    */
    
});


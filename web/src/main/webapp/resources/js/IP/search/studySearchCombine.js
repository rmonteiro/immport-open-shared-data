app = app || {};
app.keyDelayTime = 500;    // timer delay on reaction to search bar key stroke
app.keyDelayTimer = -1;  // delay timer

function clearQueryString() {
    $('#queryString').val('');
}

function getQueryString() {
    return $('#queryString').val();
}

Ext.onReady(function() {
    Ext.get('queryString').addListener('keyup', function(event) {
        var q = Ext.get('queryString').getValue();
        if (q.length > 3) {
            if (app.keyDelayTimer > 0) {
                // reset delay timer
                clearTimeout(app.keyDelayTimer);
                app.keyDelayTimer = setTimeout(doDelayedSearch, app.keyDelayTime);
            } else {
                solrSearch(q, null, 0, 10);
                // set delay timer
                app.keyDelayTimer = setTimeout(doDelayedSearch, app.keyDelayTime);
            }
        }
        return true;
    });
    $('#queryString').typeahead([{
        name: 'study-search',
        remote: {
            url: CONTEXT_ROOT + '/search/shared_data/spell?page=1&start=0&limit=25&q=%QUERY',
            filter: function(response) {
                var results = [];
                var suggestions = response.spellcheck && response.spellcheck.suggestions;
                if (suggestions) {
                    for (var i = 2; i < suggestions.length; i++) {
                        var suggestion = suggestions[i].collation[0].collationQuery;
                        results.push(suggestion);
                    }
                }
                return results;
            }
        }
    }]);
});

function doDelayedSearch() {
    app.keyDelayTimer = -1;
    var q = getQueryString();
    solrSearch(q, null, 0, 10);
}


var searchURL = CONTEXT_ROOT + "/search/shared_data/search-facet";
var spellURL = CONTEXT_ROOT + "/search/shared_data/spell";

var app = {
    pager: { pagerstart:1, pagesize:10, pagebuttons:5, pagemax:0 },
    studyIds: [],                   // array containing study ids
    facets: [],                     // stores facets from query results
    facetValueCol: {},              // stores checked values in facet trees
    facetNameHash: {},              // hash for facet display name, keyed by facet name
    searchCondition: { q:'', facetValueCol:null, start:0, rows:10 }, // store search condition
    keyDelayTime: 500,              // timer delay on reaction to search bar key stroke
    keyDelayTimer: -1               // delay timer
};

app.facetNameHash = {
	'clinicalTrial': 'Clinical Trial',
	'subjectSpecies': 'Species',
	'biosampleType': 'Biosample Type',
    'experimentMeasurementTechnique': 'Assay Type',
    'studyType': 'Study Type',
    'researchFocus': 'Research Focus'
};

function clearQueryTerm() {
    $('#queryTerm').val('');
}

var buildFacetPanel = function(data) {
	$('#facet-container').empty();
	var facetsTag = '';
	for (var property in data.facet_fields) {
		var values = data.facet_fields[property];
        facetsTag += '<div class="panel panel-default">';
        facetsTag += '<div class="panel-heading">';
        facetsTag += '<div class="panel-title">' + getFacetDisplayName(property) + '</div>';
        facetsTag += '</div>';
        facetsTag += '<div class="panel-body">';
        var facetCheckedValues = app.facetValueCol[property];
        var count = values.length / 2;
        for (var i = 0; i < count; i++) {
            var itemValue = values[2 * i];
            var itemValueCount = values[2 * i + 1];
            var isChecked = $.inArray(itemValue, facetCheckedValues) >= 0;
            checkedStr = "";
            if (isChecked) {
            	checkedStr = "checked";
            }
            facetsTag += '<div><input type="checkbox" class="facetCheckbox" value="' + property + "::" + itemValue + '" ' + checkedStr + ' > ' + itemValue + ' (' + itemValueCount + ')</div>';
        }
        facetsTag += '</div></div>';
    }
    $('#facet-container').append(facetsTag);
}

var displaySchematic = function(studyAccession) {
	var imageUrl = '<img src=' + CONTEXT_ROOT + '/public/study/study/getStudyImage?' + 
    'studyAccession=' + studyAccession + ' />';
    $('#schematicModalStudyId').html(studyAccession);
    $('#schematicModalDetail').html(imageUrl);
	$('#schematicModal').modal();
}

var doPaging = function(event) {
    var pagetotal = app.pager.pagesize * app.pager.pagebuttons;
    var pagerButton = $(event.currentTarget);
    var pagevalue = pagerButton.text();
    
    if ($.isNumeric(pagevalue)) {
        var start = (parseInt(pagevalue) - 1) * app.pager.pagesize;
        solrSearch(this.q, this.facetValueCol, start, this.rows);
    } else if (pagerButton.hasClass('pager-prev')) {
        app.pager.pagerstart = (app.pager.pagerstart - app.pager.pagebuttons);
        if (app.pager.pagerstart < 0) {
            app.pager.pagerstart = 0;
        }
        start =  app.pager.pagerstart * app.pager.pagesize;
        solrSearch(this.q, this.facetValueCol, start, this.rows);
    } else if (pagerButton.hasClass('pager-next')) {
        var pagerstart = (app.pager.pagerstart +  app.pager.pagebuttons);
        if (pagerstart <= app.pager.pagemax) {
            app.pager.pagerstart = pagerstart;
            start =  app.pager.pagerstart * app.pager.pagesize;
            solrSearch(this.q, this.facetValueCol, start, this.rows);
        }
    }
}

function doPopOpenStudy(event) {
    var studyId = $('#studyModalStudyId').text();
    var studyDetailUrl = CONTEXT_ROOT + '/public/study/study/displayStudyDetail/' + studyId;
    var detailWindow = window.open(studyDetailUrl, '_blank');
}

function doPopDownloadStudy(event) {
    var studyId = $('#studyModalStudyId').text();
    var studyDownloadUrl = asperaDataBrowserUrl + studyId;
    //var studyDownloadUrl = CONTEXT_ROOT + '/public/study/study/studyDownload/' + studyId;
    window.open(studyDownloadUrl, '_data_browser');
}

var doDelayedSearch = function() {
    app.keyDelayTimer = -1;
    var q = getQueryTerm()
    solrSearch(q, app.facetValueCol, 0, 10);
}

function doPopPrevStudy(event) {
    var studyId = $('#studyModalStudyId').text();
    var studyIndex = app.studyIds.indexOf(studyId);
    if (studyIndex > 0) {
        updateStudyPopup(app.studyIds[studyIndex - 1]);
    }
}

function doPopNextStudy(event) {
    var studyId = $('#studyModalStudyId').text();
    var studyIndex = app.studyIds.indexOf(studyId);
    if (studyIndex < app.studyIds.length - 1) {
        updateStudyPopup(app.studyIds[studyIndex + 1]);
    }
}

function getQueryTerm() {
    return $('#queryTerm').val();
}

var getFacetDisplayName = function(facetName) {
    var facetDisplayName = app.facetNameHash[facetName] || facetName;
    return facetDisplayName;
}

function setupTemplates() {
    app.searchResultSource   = $("#search-result-template").html();
    app.searchResultTemplate = Handlebars.compile(app.searchResultSource);
    app.studyResultSource   = $("#study-result-template").html();
    app.studyResultTemplate = Handlebars.compile(app.studyResultSource);
}

/**
 * do Solr search
 * @param q string
 * @param facetValue object Example: { 'studyType': ['Observational'] }
 * @param start number
 * @param rows number 
 */
var solrSearch = function(q, facetValueCol, start, rows) {
    
	// Send message to Google Analytics
    ga('send', 'pageview', {
        'page': '/solrSearch?q=' + q,
        'title': 'Solr Search'
    });
    
    app.searchCondition = { q:q, facetValueCol:facetValueCol, start:start, rows:rows };
    var fqvalues = [];
    for (var key in facetValueCol) {
        var values = facetValueCol[key];
        var quotedValues = [];
        for (var i = 0; i < values.length; i++) {
            quotedValues.push('"' + values[i] + '"');
        }
        fqvalues.push(key + ':(' + quotedValues.join(' OR ') + ')');
    }
    
    $.ajax({
        url: searchURL,
        traditional: true,
        data: {
            'q': q,
            'fq': fqvalues,
            'hl.simple.pre': '<span class="search-highlight-term search-highlight-mark">',
            'hl.simple.post': '</\span>',
            'start': start,
            'rows': rows
        },
        success: function(response) {
            var highlightFields = [ "briefTitle","briefDescription",
                   "conditionStudied","description","endpoints",
                   "hypothesis","objectives","experimentDescription",
                   "experimentHypothesis", "experimentPurpose",
                   "experimentRationale","experimentTitle","armDescription",
                   "armName","protocolDescription","protocolFile",
                   "reagentDescription","contractName", "contractSummary",
                   "fcsAnnotationPanel","inclusionExclusion","programSummary",
                   "programTitle","glossaryDefinition","pubmedAuthors",
                   "compoundNameReported", "compountNameOtherNameReported",
                   "personnelConcat"];
            app.studyIds = [];
            for (var i = 0; i < response.response.docs.length; i++) {
                var doc = response.response.docs[i];
                var studyAccession = doc.studyAccession;
                app.studyIds.push(studyAccession);
                response.response.docs[i].highlighting = [];
                for (var j = 0; j < highlightFields.length; j++) {
                    var field = highlightFields[j];
                    if (response.highlighting[studyAccession][field]) {
                        response.response.docs[i].highlighting.push(response.highlighting[studyAccession][field][0]);
                    }
                }
                response.response.docs[i].personnel = [];
                if (response.response.docs[i].briefDescription &&  response.response.docs[i].briefDescription.length > 400) {
                    response.response.docs[i].briefDescription = response.response.docs[i].briefDescription.substring(0,397) + "...";
                }
               
                for (var j= 0; j < doc.personnelLastName.length; j++) {
                    if (doc.personnelRoleInStudy[j] == 'Principal Investigator') {
                        var name = doc.personnelFirstName[j] + " " + doc.personnelLastName[j] + ", " + doc.personnelOrganization[j];
                        response.response.docs[i].personnel.push(name);
                    }
                }
            }
            showResult(response);
        }
    })
}

function showResult(solrJson) {
    $('#catalog-container').empty();
    var studyResultHtml = app.studyResultTemplate({'studies': solrJson.response.docs, 'context_root': CONTEXT_ROOT, 'asperaDataBrowserUrl': asperaDataBrowserUrl});
    $('#catalog-container').append(studyResultHtml);
    //studyTemplate.append('catalog-container', solrJson.response.docs);
    buildFacetPanel(solrJson.facet_counts);
    
    var results = { 'hitCount': solrJson.response.numFound, 'qTime': solrJson.responseHeader.QTime };
    $('#result-container').empty();
    var searchResultHtml = app.searchResultTemplate(results);
    $('#result-container').append(searchResultHtml);
    
    // update paging toolbar
    updatePagingToolbar(app.searchCondition, results);
    
    // add click listener on study title
    $('.study-title').off('click');
    $('.study-title').click(function(event) {
        var studyId = event.currentTarget.id;
        updateStudyPopup(studyId);
        $('#studyModal').modal();
    });
    
    // add click listener on image element for showing in light-box
    // Ext.select('.study-graph img', true).on('click', function(event) {
    $('.study-graph img').off('click');
    $('.study-graph img').click(function(event) {
        var image = event.currentTarget;
        var studyAccession = image.src.split('=')[1];
        displaySchematic(studyAccession);
    });
}

var updatePagerButton = function(button, number, pagerstart, pagemax) {
    var pageNumber = app.pager.pagerstart + number;
    if (pageNumber <= pagemax) {
        button.html(pageNumber);
    } else {
        button.html('&nbsp;');
    }
}

var updatePagingToolbar = function(searchCondition, result) {
    var pagerButtons = $('.catalog-pager .pager-button');
    pagerButtons.off('click');
    pagerButtons.click(doPaging.bind(searchCondition));
    
    var start = searchCondition.start;
    var pagetotal = app.pager.pagesize * app.pager.pagebuttons;
    var pagenumber = Math.floor(start/pagetotal);
    var pageindex = Math.ceil((start % pagetotal) / app.pager.pagesize) + 1;
    app.pager.pagerstart = pagenumber * app.pager.pagebuttons;
    app.pager.pagemax = Math.ceil(result.hitCount / app.pager.pagesize);
    
    $('a.pager-button').removeClass('pager-active');
    $('a.pager-' + pageindex).addClass('pager-active');
    
    for (var i = 0; i < pagerButtons.length; i++) {
        var pagerButton = $(pagerButtons[i]);
        if (pagerButton.hasClass('pager-1')) {
            updatePagerButton(pagerButton, 1, app.pager.pagerstart, app.pager.pagemax);
        } else if (pagerButton.hasClass('pager-2')) {
            updatePagerButton(pagerButton, 2, app.pager.pagerstart, app.pager.pagemax);
        } else if (pagerButton.hasClass('pager-3')) {
            updatePagerButton(pagerButton, 3, app.pager.pagerstart, app.pager.pagemax);
        } else if (pagerButton.hasClass('pager-4')) {
            updatePagerButton(pagerButton, 4, app.pager.pagerstart, app.pager.pagemax);
        } else if (pagerButton.hasClass('pager-5')) {
            updatePagerButton(pagerButton, 5, app.pager.pagerstart, app.pager.pagemax);
        }
    }
}

function updateStudyPopup(studyId) {
    var studyDetailPopupUrl = CONTEXT_ROOT + '/public/study/study/displayStudyDetailModal/' + studyId;
    var studyContent = '<iframe class="study-detail-embed" src="' + studyDetailPopupUrl + '" frameborder="0"/>';
    $('#studyModalStudyId').html(studyId);
    $('#studyModalDetail').html(studyContent);
    // update navigation button
    var studyIndex = app.studyIds.indexOf(studyId);
    if (studyIndex == 0) {
    	$('#btnPopPrevStudy').prop('disabled', true);
    } else {
    	$('#btnPopPrevStudy').prop('disabled', false);
    }
    if (studyIndex + 1 == app.studyIds.length) {
    	$('#btnPopNextStudy').prop('disabled', true);
    } else {
    	$('#btnPopNextStudy').prop('disabled', false);
    }
}

var updateSummaryDisplay = function(setting) {
    if (setting.isShowSummary) {
        $('.study-description').show();
    } else {
        $('.study-description').hide()
    }
    if (setting.isShowHighlight) {
        $('.study-highlight').show();
    } else {
        $('.study-highlight').hide();
    }
}

$().ready(function() {
	if (typeof(clinicalTrial) != undefined && clinicalTrial != "") {
		app.facetValueCol['clinicalTrial'] = clinicalTrial;
	}
	if (typeof(researchFocus) != undefined && researchFocus != "") {
		app.facetValueCol['researchFocus'] = researchFocus;
	}
	if (typeof(studyType) != undefined && studyType != "") {
		app.facetValueCol['studyType'] = studyType;
	}
	if (typeof(subjectSpecies) != undefined && subjectSpecies != "") {
		app.facetValueCol['subjectSpecies'] = subjectSpecies;
	}
	if (typeof(biosampleType) != undefined && biosampleType != "") {
		app.facetValueCol['biosampleType'] = biosampleType;
	}
	if (typeof(experimentMeasurementTechnique) != undefined && experimentMeasurementTechnique != "") {
		app.facetValueCol['experimentMeasurementTechnique'] = experimentMeasurementTechnique;
	}
	
	setupTemplates();
	
    $('#queryTerm').on('keyup', function(event) {
        var q = getQueryTerm();
        if (q.length > 3) {
            if (app.keyDelayTimer > 0) {
                // reset delay timer
                clearTimeout(app.keyDelayTimer);
                app.keyDelayTimer = setTimeout(doDelayedSearch, app.keyDelayTime);
            } else {
                solrSearch(q, null, 0, 10);
                // set delay timer
                app.keyDelayTimer = setTimeout(doDelayedSearch, app.keyDelayTime);
            }
        }
        return true;
    });
    
    $('#queryTerm').typeahead([{
        name: 'study-search',
        placeholder: 'Influenza',
        remote: {
            url: spellURL + '?page=1&start=0&limit=25&q=%QUERY',
            filter: function(response) {
                var results = [];
                var suggestions = response.spellcheck && response.spellcheck.suggestions;
                if (suggestions) {
                    for (var i = 2; i < suggestions.length; i++) {
                        var suggestion = suggestions[i].collation[0].collationQuery;
                        results.push(suggestion);
                    }
                }
                return results;
            }
        }
    }]);
	
    $('#setting-container').click(function(event) {
        $('#dialogModal').modal();
    });
    
    $('#dialogModal .btn-next').click(function(event) {
        var isShowSummary = $('#checkboxShowSummary').is(':checked');
        var isShowHighlight= $('#checkboxShowHighlight').is(':checked');
        updateSummaryDisplay({
            isShowSummary: isShowSummary,
            isShowHighlight: isShowHighlight
        });
        $('#dialogModal').modal('hide');
    });
    
    // set study dialogbox height to 85% of browser height
    $('#studyModal').on('shown.bs.modal', function () {
        var heightAttribute = Math.floor($(window).height() * 0.85) + 'px';
        $(this).find('.modal-body').css({'height':heightAttribute});
    });
    
    
    $('#schematicModal').on('shown.bs.modal', function () {
        var heightAttribute = Math.floor($(window).height() * 0.85) + 'px';
        $(this).find('.modal-body').css({'height':heightAttribute});
    });
    
    
    $('#btnPopPrevStudy').click(doPopPrevStudy);
    $('#btnPopNextStudy').click(doPopNextStudy);
    $('#btnPopOpenStudy').click(doPopOpenStudy);
    $('#btnPopDownloadStudy').click(doPopDownloadStudy);
    $('#btnSearch').click(function(event) {
        var q = getQueryTerm();
        solrSearch(q, app.facetValueCol, 0, 10);
    });
    $('#btnClearQuery').click(function(event) {
        clearQueryTerm();
        var q = getQueryTerm()
        app.facetValueCol = {};
        solrSearch(q, app.facetValueCol, 0, 10);
    });
    
    $('#facet-container').on('click','.facetCheckbox',function() {
    	var data = this.value.split("::");
    	if (this.checked) {
    		if (app.facetValueCol[data[0]] == undefined) {
    			app.facetValueCol[data[0]] = [];
    		}
    		app.facetValueCol[data[0]].push(data[1]);
    	} else {
    		for (var i = 0; i < app.facetValueCol[data[0]].length; i++) {
    	        if (data[1] === app.facetValueCol[data[0]][i]) {
    	          app.facetValueCol[data[0]].splice(i,1);
    	        }
    	    }
    		if (app.facetValueCol[data[0]].length == 0) {
    			delete app.facetValueCol[data[0]];
    		}
    	}
    	var q = getQueryTerm();
    	solrSearch(q,app.facetValueCol,0,10);
    });
    
    
    if (searchTerm) {
    	$('#queryTerm').val(searchTerm);
    }
    var q = getQueryTerm();
    solrSearch(q, app.facetValueCol, 0, 10);
});

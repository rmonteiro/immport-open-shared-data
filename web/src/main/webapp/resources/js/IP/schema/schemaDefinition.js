var app = app || {};
/*
$().ready(function() {
	 $("#tableChoice").select2();
    setupTemplates();
    getData(tableName, updateDisplay);
    $("#tableChoice").on("change", function(e) {
    	var table = e.val;
    	$('.table-summary').empty();
        $('.table.table-definition tbody').empty();
        $('.table.index-definition tbody').empty();
        $('.table.reference-definition tbody').empty();
    	getData(table,updateDisplay);
    })
});
*/

function setupTemplates() {
    // study title
    app.tableSummarySource   = $("#table-summary-template").html();
    app.tableSummaryTemplate = Handlebars.compile(app.tableSummarySource);
    app.tableDefinitionSource = $("#table-definition-template").html();
    app.tableDefinitionTemplate = Handlebars.compile(app.tableDefinitionSource);
    app.indexDefinitionSource = $("#index-definition-template").html();
    app.indexDefinitionTemplate = Handlebars.compile(app.indexDefinitionSource);
    app.referenceDefinitionSource = $("#reference-definition-template").html();
    app.referenceDefinitionTemplate = Handlebars.compile(app.referenceDefinitionSource);
    
}

function getData(tableName, callback) {
    $('.table-summary').empty();
    $('.table.table-definition tbody').empty();
    $('.table.index-definition tbody').empty();
    $('.table.reference-definition tbody').empty();
    var url = CONTEXT_ROOT + '/resources/data/schemaDefinition/' + tableName + ".json";
    var parameter = {};
    $.get(url, function(data) {
    	callback && callback(data);
    });
}

function updateDisplay(data) {
    // Table Summary
    var tableSummaryHtml = app.tableSummaryTemplate(data);
    $('.table-summary').append(tableSummaryHtml);
    var tableDefinitionHtml = app.tableDefinitionTemplate(data);
    $('.table.table-definition tbody').append(tableDefinitionHtml);
    var indexDefinitionHtml = app.indexDefinitionTemplate(data);
    $('.table.index-definition tbody').append(indexDefinitionHtml);
    var referenceDefinitionHtml = app.referenceDefinitionTemplate(data);
    $('.table.reference-definition tbody').append(referenceDefinitionHtml);
}
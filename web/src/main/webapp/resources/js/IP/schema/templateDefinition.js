var app = app || {};

$().ready(function() {
    setupTemplates();
    getData();
    $("#templateChoice").on("change", function(e) {
    	$('#templateDiv').empty();
    	var templateName = e.val;
    	getTemplate(templateName);
    	updateTemplate();
    });
    $("#lkTableChoice").on("change", function(e) {
    	$('#lkTableDiv').empty();
    	var lkTableName = e.val;
    	getLkTable(lkTableName);
    	updateLkTable();
    });
    $("#templateDiv").on("click",".cvButton", function(e) {
        getLkTable($(this).attr("value"));
        updateLkTable();
        $('.nav-tabs a[href="#lkTables"]').tab("show");
    });
});

function setupTemplates() {
    app.introductionSource   = $("#introduction-template").html();
    app.introductionTemplate = Handlebars.compile(app.introductionSource);
    app.templateSource = $("#template-template").html();
    app.templateTemplate = Handlebars.compile(app.templateSource);
    app.lkTableSource = $("#lkTable-template").html();
    app.lkTableTemplate = Handlebars.compile(app.lkTableSource);
}

function format(item) { return item.tag;}

function getData() {
    var url1 = CONTEXT_ROOT + '/resources/data/templateDefinition/introduction.json';
	var url2 = CONTEXT_ROOT + '/resources/data/templateDefinition/templates.json';
	var url3 = CONTEXT_ROOT + '/resources/data/templateDefinition/lkTables.json';
	$.get(url1, function(data) {
	    app.introductionJson = data;
	    // Added to help Handlebars
	    for (var i = 0; i < app.introductionJson.length; i++) {
	    	if (i == 2) {
	    		app.introductionJson[i].isSection3 = true;
	    	}
	    	if (i == 3) {
	    		app.introductionJson[i].isSection4 = true;
	    	}
	    }
	    updateIntroduction();
	});
	$.get(url2, function(data) {
        app.templateJson = data;
        app.templateNames = [];
        for (var i = 0; i < app.templateJson.length; i++) {
        	app.templateNames.push({ 'id': app.templateJson[i].fileName, 'text': app.templateJson[i].fileName});
        	if (app.templateJson[i].templateType != "compound") {
        	    var columns = app.templateJson[i]['columns'];
        	    for ( var j = 0; j < columns.length; j++) {
        		    if (columns[j].cvTableName) {
        			    columns[j].cvTableName = '<input class="btn btn-info cvButton" type="button" value="' + columns[j].cvTableName + '">';
        		    }
        		    if (columns[j].pvTableName) {
        			    columns[j].pvTableName = '<input class="btn btn-info cvButton" type="button" value="' + columns[j].pvTableName + '">';
        		    }
        	    }
        	    app.templateJson[i]['columns'] = columns;
            } else {
            	for (var j = 0; j < app.templateJson[i]['templates'].length; j++) {
            		var columns = app.templateJson[i]['templates'][j]['columns'];
            	    for ( var k = 0; k < columns.length; k++) {
            		    if (columns[k].cvTableName) {
            			    columns[k].cvTableName = '<input class="btn btn-info cvButton" type="button" value="' + columns[k].cvTableName + '">';
            		    }
            		    if (columns[k].pvTableName) {
            			    columns[k].pvTableName = '<input class="btn btn-info cvButton" type="button" value="' + columns[k].pvTableName + '">';
            		    }
            	    }
            	    app.templateJson[i]['templates'][j]['columns'] = columns;
            	}
            }
        }
        getTemplate(app.templateNames[0].id);
        $('#templateChoice').select2({
        	data: app.templateNames
        });
       
	});
	$.get(url3, function(data) {
        app.lkTablesJson = data;
        app.lkTableNames = [];
        for (var i = 0; i < app.lkTablesJson.length; i++) {
        	app.lkTableNames.push({'id': app.lkTablesJson[i].name,'text': app.lkTablesJson[i].name});
        	var rows = app.lkTablesJson[i]['rows'];
        	for ( var j = 0; j < rows.length; j++) {
        		if (rows[j].link != "") {
        			rows[j].link = '<a href="' + rows[j].link + '" target="_blank">Link</a>';
        		}
        	}
        	app.lkTablesJson[i]['rows'] = rows;
        }
        getLkTable(app.lkTableNames[0].id);
        $('#lkTableChoice').select2({
        	data: app.lkTableNames
        });
	});
}

function getTemplate(templateName) {
	for (var i = 0; i < app.templateNames.length; i++) {
		var template = app.templateJson[i];
		if (templateName == template.fileName) {
			app.current_template = template;
			if (app.current_template.templateType === "compound") {
				app.current_template.isCompound = true;
			} else {
				app.current_template.isCompound = false;
			}
			break;
		};
	}
}

function getLkTable(lkTableName) {
	for (var i = 0; i < app.lkTableNames.length; i++) {
		var lkTable = app.lkTablesJson[i];
		if (lkTableName == lkTable.name) {
			app.current_lkTable = lkTable;
			break;
		};
	}
}

function updateIntroduction() {
    var introductionHtml = app.introductionTemplate(app.introductionJson);
    $('#introductionDiv').append(introductionHtml); 
}

function updateTemplate() {
	$('#templateDiv').empty();
	var templateHtml = app.templateTemplate(app.current_template);
	$('#templateDiv').append(templateHtml);
}

function updateLkTable() {
	$('#lkTableDiv').empty();
	var lkTableHtml = app.lkTableTemplate(app.current_lkTable);
	$('#lkTableDiv').append(lkTableHtml);
}
var schemaApp = schemaApp || {};

var addLegend = function(svg,legendX,legendY,typeScale) {
    var legend = svg.append("g")
                    .attr("class","legend")
                    .attr("transform", function(d) {
                      return "translate(" + legendX + "," + legendY + ")"; })

    legend.append("rect")
       .attr("x", 0)
       .attr("y", 0)
       .attr("height",150)
       .attr("width",300)
       .attr("fill", "none")
       .attr("stroke", "black")
     
       
    legend.append("text")
       .attr("x", 10)
       .attr("y", 15)
       .attr("class","node")
       .style("stroke", "black")
       .text("Overview of the relationship between tables.");

    legend.append("text")
       .attr("x", 10)
       .attr("y", 40)
       .attr("class","node")
       .style("stroke", "black")
       .text("Click on a node to display table details.");
                     
    legend.append("circle")
       .attr("cx",15)
       .attr("cy",60)
       .attr("r", 6)
       .style("stroke", function(d) { return typeScale("design"); })
       .style("fill", function(d) { return typeScale("design"); });

    legend.append("text")
       .attr("x",30)
       .attr("y",63)
       .attr("class","node")
       .style("stroke", typeScale("design"))
       .text("Design");

    legend.append("circle")
       .attr("cx",15)
       .attr("cy",80)
       .attr("r", 6)
       .style("stroke", function(d) { return typeScale("assay"); })
       .style("fill", function(d) { return typeScale("assay"); });

    legend.append("text")
       .attr("x",30)
       .attr("y",83)
       .attr("class","node")
       .style("stroke", typeScale("assay"))
       .text("Mechanistic");

    legend.append("circle")
       .attr("cx",15)
       .attr("cy",100)
       .attr("r", 6)
       .style("stroke", function(d) { return typeScale("clinical"); })
       .style("fill", function(d) { return typeScale("clinical"); });

    legend.append("text")
       .attr("x",30)
       .attr("y",103)
       .attr("class","node")
       .style("stroke", typeScale("clinical"))
       .text("Clinical");

    legend.append("circle")
       .attr("cx",15)
       .attr("cy",120)
       .attr("r", 6)
       .style("stroke", function(d) { return typeScale("annotation"); })
       .style("fill", function(d) { return typeScale("annotation"); });

    legend.append("text")
       .attr("x",30)
       .attr("y",123)
       .attr("class","node")
       .style("stroke", typeScale("annotation"))
       .text("Annotation");

    legend.append("circle")
       .attr("cx",15)
       .attr("cy",140)
       .attr("r", 6)
       .style("stroke", function(d) { return typeScale("protocol"); })
       .style("fill", function(d) { return typeScale("protocol"); });

    legend.append("text")
       .attr("x",30)
       .attr("y",143)
       .attr("class","node")
       .style("stroke", typeScale("protocol"))
       .text("Protocol");

	
}

var displaySchemaTree = function(treeData,element) {
    /* Set-up margins, based on margin-convention http://bl.ocks.org/mbostock/3019563 */
    var margin = { top: 20, right: 100, bottom: 20, left: 100 }
    var svgWidth = $(element).width();
    var svgHeight = $(element).height();
    var treeWidth = svgWidth - margin.left - margin.right;
    var treeHeight = svgHeight - margin.top - margin.bottom;
    var typeScale = d3.scale.category10().domain(['design','assay','clinical','annotation','protocol'])

    /* Set-up other constants and variables */
    var duration = 750;
    var maximumDepth = 0;

    /* Helper Functions */
    var linkDiagonal = d3.svg.diagonal().projection(function(d) { return [d.y,d.x];});

    /* Initialize SVG */
    var svg = d3.select(element).append("svg")
                .attr("width", svgWidth)
                .attr("height", svgHeight)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    /* Construct the Legend */
    var legendX = svgWidth - 300 - margin.right;
    var legendY = 25;
    if (svgHeight > 450 && svgWidth > 750) {
      addLegend(svg,legendX,legendY,typeScale)
    }

    var schemaTree = d3.layout.tree()
                       .size([treeHeight,treeWidth])

    var root = treeData.nodes;
    root.x0 = treeWidth / 2;
    root.y0 = 0;

    /* Data preparation, match the link information to the array order of the nodes */
    var nodesData = schemaTree.nodes(root);
    var nodesHash = {}
    for (var i = 0; i < nodesData.length; i++) {
        nodesHash[nodesData[i].link_name] = i;
    }
    var linksData = [];
    var links = treeData.links;
    for (var i = 0; i < links.length; i++) {
        var sourceId = nodesHash[links[i].source]
        var sourceObj = nodesData[sourceId]
        var targetId = nodesHash[links[i].target]
        var targetObj = nodesData[targetId]
        var obj = { "source": sourceObj, "target": targetObj }
        linksData.push(obj);
    }


    /* Find the maximum depth, then adjust display size based on depth */
    nodesData.forEach(function(d) { if (d.depth > maximumDepth) { maximumDepth = d.depth; } });
    nodesData.forEach(function(d) { d.y = d.depth * (treeWidth/(maximumDepth + 2)); });

    /* Bind the data to the nodes */
    var nodes = svg.selectAll("g.node").data(nodesData);
    var nodeEnter = nodes.enter()
                         .append("g")
                         .attr("class","node")
                         .attr("transform", function(d) {
                             return "translate(" + root.y0 + "," + root.x0 + ")"; })
                         .on("click",clickNode);

    /* Set up the nodes but hides the display, until everything is ready for transition */
    nodeEnter.append("circle")
             .attr("r", 1e-6)
             .style("stroke", function(d) { return typeScale(d.type); })
             .style("fill", function(d) { return typeScale(d.type); });

    nodeEnter.append("text")
             .attr("x", function(d) { return d.children || d._children ? -13 : 13; })
             .attr("dy", ".35em")
             .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
             .text(function(d) { return d.name; })
             .style("fill-opacity", 1e-6);


    /* Update the nodes and put in proper positions */
    var nodeUpdate = nodes.transition()
                          .duration(duration)
                          .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

    nodeUpdate.select("circle")
              .attr("r", 6)
              .style("stroke", function(d) { return typeScale(d.type); })
              .style("fill", function(d) { return typeScale(d.type); });

    nodeUpdate.select("text")
              .style("fill-opacity", 1);

    /* Transition exiting nodes */
    var nodeExit = nodes.exit()
                        .transition()
                        .duration(duration)
                        .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
                        .remove()

    nodeExit.select("circle")
            .attr("r",1e-6);

    nodeExit.select("text")
            .style("fill-opacity", 1e-6);


    /* Set up the links but hides the display, until everything is ready for transition */
    var links = svg.selectAll("path.link")
                   .data(linksData);

    links.enter()
         .insert("path", "g")
         .attr("class","link")
         .attr("d", function(d) {
              var o = { x: root.x0, y: root.y0};
             return linkDiagonal({source: o, target: o});
         });

    // Transition links to new position
    links.transition()
         .duration(duration)
         .attr("d",linkDiagonal);

    links.exit()
         .transition()
         .duration(duration)
         .attr("d", function(d) {
              var o = { x: root.x, y: root.y};
              return linkDiagonal({source: o, target: o});
         })

    nodesData.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
    });


};

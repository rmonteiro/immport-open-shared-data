package org.immport.open.web.search;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.ResourceBundle;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mitre.dsmiley.httpproxy.ProxyServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * This class tests the search functionality.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-httpproxy-servlet.xml"
})
public class SearchTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SearchTests.class);

    @Autowired
    String targetUrl;

    /** The bundle name for the test resources. */
    private String testResourcesBundleName = "tests";

    /** The test resources. */
    private ResourceBundle testResources;

    /** The Jetty server. */
    private Server server;

    /** The base URL. */
    private String baseUrl;

    /**
     * Set up the tests.
     */
    @Before
    public void setup() throws Exception {
        /*
         * Get the test resources
         */
        log.debug("testResourcesBundleName = " + testResourcesBundleName);
        testResources = ResourceBundle.getBundle(testResourcesBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = testResources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + testResources.getString(key));
            }
        }

        String portNumber = testResources.getString("portNumber");
        log.debug("portNumber = " + portNumber);
        String searchPath = testResources.getString("searchPath");
        log.debug("searchPath = " + searchPath);
        baseUrl = "http://localhost:" + portNumber + searchPath;
        log.info("baseUrl = " + baseUrl);

        /*
         * Instantiate the HTTP Proxy Servlet, a Servlet Holder, and a Servlet Handler
         */
        log.info("targetUrl = " + targetUrl);
        log.info("instantiating the HTTP Proxy Servlet");

        ServletHolder servletHolder = new ServletHolder();
        servletHolder.setServlet(new ProxyServlet());
        servletHolder.setInitParameter("targetUri", targetUrl);
        servletHolder.setInitParameter("log", "true");

        ServletHandler servletHandler = new ServletHandler();
        servletHandler.addServletWithMapping(servletHolder, searchPath + "/*");

        log.debug("creating a jetty server to listen on port " + portNumber);
        server = new Server(Integer.parseInt(portNumber));
        server.setHandler(servletHandler);
      
        log.debug("starting the jetty server");
        server.start();
        log.debug("state = " + server.getState());
    }
    

    @Test
    public void facetSearch() throws Exception {
        String facetSearchPath = testResources.getString("facetSearchPath");
        log.debug("facetSearchPath = " + facetSearchPath);
        String url = baseUrl + facetSearchPath;
        log.info("url = " + url);
        HttpClient httpClient = new HttpClient();
        httpClient.start();
        ContentResponse contentResponse = httpClient.GET(url);
        assertNotNull("The response is null.", contentResponse);
        int status = contentResponse.getStatus();
        log.info("status = " + status);
        assertTrue("The status is not 200.", status == 200);
        String response = contentResponse.getContentAsString();
        log.info(response);
    }


    /**
     * Tear down.
     */
    @After
    public void tearDown() throws Exception {
        log.debug("stopping the jetty server");
        server.stop();
        server.join(); // waits for the server to fully stop
        log.debug("state = " + server.getState());
    }


}

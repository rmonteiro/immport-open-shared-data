package org.immport.open.web.controller.exception;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.immport.open.web.controller.exception.ErrorController;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


/**
 * This class tests the error handling.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-dispatcher-servlet.xml"
})
public class ErrorControllerTests {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    /**
     * Set up the tests.
     */
    @Before
    public void setup() {
        /*
         * Instantiate the Dispatcher Servlet and add it to the Servlet Context
         */
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }
    

    @Test
    public void testException() throws Exception {
        mockMvc.perform(get("/throwException"))
                .andExpect(handler().handlerType(ErrorController.class))
                .andExpect(view().name("error"))
                .andDo(print());
    }


    /**
     * Tear down.
     */
    @After
    public void tearDown() {
        // empty
    }


}

package org.immport.data.shared.service;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.HashMap;
import java.util.List;

import org.immport.data.shared.model.AdverseEvent;
import org.immport.data.shared.model.AdverseEventDetail;
import org.immport.data.shared.model.AdverseEventSummary;
import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.AdverseEventRepository;
import org.immport.data.shared.service.GenericPivotServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the AdverseEvent object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class AdverseEventServiceImpl extends GenericPivotServiceImpl implements AdverseEventService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(AdverseEventServiceImpl.class);

    @Autowired
    private AdverseEventRepository adverseEventRepository;
    
   
    
    /**
     * Returns an AdverseEvent object by querying on the primary key.
     *
     * @param id primary key of the AdverseEvent object
     * @return the AdverseEvent object
     */
    public AdverseEvent findById(String adverseEventAccession) {
        AdverseEvent adverseEvent = adverseEventRepository.findById(adverseEventAccession); 
        return adverseEvent;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = adverseEventRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of AdverseEvent objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <AdverseEvent>
     */
    public List<AdverseEvent> getByStudy(String studyAccession) {
        List <AdverseEvent> results = adverseEventRepository.getByStudy(studyAccession);
        return results;
    }
    
    /**
     * Returns a List of Pivoted Concomitant Medication Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Concomitant Med Summary data)
     */
    public List<GenericPivotData> getAdverseEventSummaryByStudy(String studyAccession)
    {
         List<AdverseEventSummary> results = adverseEventRepository.getAdverseEventSummaryByStudy(studyAccession);

         List<GenericPivotData> objPivotList = new ArrayList<GenericPivotData>();
         if (results.size() > 0) {

				String dataType = "";   
				String tempDataType = "";
				//String armAccession;        	    
				Integer sortOrder;
				//String armColName;
				long dataCount;
				
     
			    HashMap<String, String> objHash = new HashMap<String, String>();
		        List<String> objTempArmNames = new ArrayList<String>();
		        getDistinctArmNames(studyAccession,objHash,objTempArmNames);	
			
			
				HashMap<String, String> objHashData = new HashMap<String, String>();				
				List<String> objBaseData = null;
			    List<String> objBaseNames = new ArrayList<String>();
			   
			    
			    
			    populateBaseColumnInformation("AdverseEventSummary", objBaseNames,
						null,null);
			      
			    NumberFormat numberFormatter = NumberFormat.getNumberInstance(Locale.US);
			
				for (int i = 0;i < results.size();i++) {
					AdverseEventSummary objSummary = (AdverseEventSummary)results.get(i);
					 if (i == 0) {
						 dataType = objSummary.getDataType();
					 }
					 else {
						 tempDataType =  objSummary.getDataType();
						 if (!dataType.equalsIgnoreCase(tempDataType)) {
							 
							objBaseData = new ArrayList<String>(); 	                       		
                        	objBaseData.add(dataType);
                        	 
                        	 addObjecttoList(
	                        			studyAccession,
	                        			objPivotList,
	      								objBaseData,
	      								objBaseNames,
	      								null,
	      								null,
	      								objHashData
	      								);
                        	
                        	
							 
							 dataType = tempDataType;
							 objHashData = new HashMap<String, String>();
						 }
					 }
				 	
					 sortOrder = objSummary.getSortOrder();
					 if (sortOrder != null){						
						 dataCount = objSummary.getDataCount();	 
						 String strDataCount = numberFormatter.format(dataCount);
						 objHashData.put(objSummary.getArmAccession(), strDataCount);
				    }
					 
			    	if (i == (results.size() - 1)) {
			    		objBaseData = new ArrayList<String>(); 	                       		
                    	objBaseData.add(dataType);
                    	
                    	 addObjecttoList(
                     			studyAccession,
                     			objPivotList,
   								objBaseData,
   								objBaseNames,
   								null,
   								null,
   								objHashData
   								);
			    		
                    	
			    		 
			    	 }
			    	 
			    	 
			     }
			
			    
				 populateListWithPlaceHolder(objPivotList,objTempArmNames,"0");
			 
			    
        
         } 
         

         return objPivotList;

    }
    
    
    
    /**
     * Returns a List of Pivoted Adverse Event Detail data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Adverse Event Detail data)
     */
    public PageableData getAdverseEventDetailByStudy(Integer start, Integer limit,
            String sort, String dir,String studyAccession)
    {
    	PageableData page = new PageableData();
        List<GenericPivotData>  objAdverseEventDetList = new ArrayList<GenericPivotData>();
        PageableData pageData =
        		adverseEventRepository.getAdverseEventDetailByStudy(start, limit, sort, dir, studyAccession);
        long iTotalCount = pageData.getTotalCount();
        if (iTotalCount > 0) {
	        List<AdverseEventDetail> objList =  (List<AdverseEventDetail>)pageData.getData();
	
	       
	
	        HashMap<String, String> objHash = new HashMap<String, String>();
	        List<String> objTempArmNames = new ArrayList<String>();
	        getDistinctArmNames(studyAccession,objHash,objTempArmNames);
	        
	        
	       //Pivoting Data
	        String nameReported = "";
	        String severityReported = "";
	        String tempNameReported = "";
	        String tempSeverityReported = "";
	        
	        long iTotalBy = 0;
	
	       
	       
	        HashMap<String, String> objHashArm = new HashMap<String, String>();
	       
	       
	        List<String> objBaseData = null;
	        List<String> objBaseNames = new ArrayList<String>();
	        List<String> objBaseNamesSort = new ArrayList<String>();
			List<String> baseNamesSortParameter = new ArrayList<String>();
		        
			populateBaseColumnInformation("AdverseEventDetail", objBaseNames,
						objBaseNamesSort,baseNamesSortParameter);
	        
	      
	         
	        long dataCount = 0;
	        
	        for (int i=0;i< objList.size();i++) {
	        	AdverseEventDetail objAdverseEvent = (AdverseEventDetail)objList.get(i);
	                if (i==0) {
	                		nameReported = objAdverseEvent.getNameReported();
	                		severityReported = objAdverseEvent.getSeverityReported();
	                		
	                		objHashArm = new HashMap<String, String>();
	                }
	                else {
	                		tempNameReported = objAdverseEvent.getNameReported();
	                		tempSeverityReported = objAdverseEvent.getSeverityReported();
	                        if (!((nameReported.equalsIgnoreCase((tempNameReported))) 
	                        		&& (severityReported.equalsIgnoreCase((tempSeverityReported))))) {	 
	                        	
	                        	  objBaseData = new ArrayList<String>(); 	                       		
	                        	  objBaseData.add(nameReported);
	                        	  objBaseData.add(severityReported);
	                        	  objBaseData.add(String.valueOf(iTotalBy));
	                        	  
	                        	  addObjecttoList(
	                        			studyAccession,
	                        			objAdverseEventDetList,
	      								objBaseData,
	      								objBaseNames,
	      								objBaseNamesSort,
	      								baseNamesSortParameter,
	      								objHashArm
	      								);
	                             
	                        		
	                        	
	                                nameReported = tempNameReported;
	                                severityReported =tempSeverityReported;
	                                iTotalBy = 0;
	                                objHashArm = new HashMap<String, String>();
	                               
	                        }
	                }
	
	                iTotalBy = iTotalBy  + objAdverseEvent.getSubjAccNo();
	                String stArm =          objAdverseEvent.getarmAccession();
	                //String  armColName = (String)objHash.get(stArm);
		        	 
		        	dataCount = objAdverseEvent.getSubjAccNo();	        	
		        	objHashArm.put(stArm, String.valueOf(dataCount));
	
	              
	                if (i == (objList.size() - 1)) {   
	                	
	                	  objBaseData = new ArrayList<String>(); 	                       		
                    	  objBaseData.add(nameReported);
                    	  objBaseData.add(severityReported);
                    	  objBaseData.add(String.valueOf(iTotalBy));
                		
                    	  addObjecttoList(
                      			studyAccession,
                      			objAdverseEventDetList,
    							objBaseData,
    							objBaseNames,
    							objBaseNamesSort,
    							baseNamesSortParameter,
    							objHashArm
    							);
	                   
	                 
	                }
	        }
	        
	        
	      
	        
	        populateListWithPlaceHolder(objAdverseEventDetList,objTempArmNames,"");
	
	        page.setData(objAdverseEventDetList);
	        page.setTotalCount(pageData.getTotalCount());
        }
        else
        {
        	page.setData(objAdverseEventDetList);
        	page.setTotalCount(pageData.getTotalCount());        	
        }
        return page;
    }   
    
    /**
     * Returns a List of Arms that are linked to the Adverse Event for that
     * study.
     *
     * @param studyAccession accession for the study
     * @return List (Arms)
     */
    protected List<ArmCohortSortOrder> getDistinctArmNameForStudy(String studyAccession)
    {
    	   List<ArmCohortSortOrder> objArm = (List<ArmCohortSortOrder>)adverseEventRepository.getDistinctArmNameForAdverseEvent(studyAccession);
    	   return objArm;
    }
    
}

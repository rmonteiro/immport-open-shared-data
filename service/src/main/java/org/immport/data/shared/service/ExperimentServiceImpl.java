package org.immport.data.shared.service;

import java.util.ArrayList;
import java.util.List;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Experiment;
import org.immport.data.shared.model.MechanisticAssay;
import org.immport.data.shared.repository.ExperimentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Service class for the Experiment object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class ExperimentServiceImpl implements ExperimentService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ExperimentServiceImpl.class);

    @Autowired
    private ExperimentRepository experimentRepository;
    
    @Autowired
    private ExpsampleService expsampleService;
    
    /**
     * Returns an Experiment object by querying on the primary key.
     *
     * @param id primary key of the Experiment object
     * @return the Experiment object
     */
    public Experiment findById(String experimentAccession) {
        Experiment experiment = experimentRepository.findById(experimentAccession); 
        return experiment;
    }
    
    /**
     * Returns an Experiment object by querying on the primary key.
     *
     * @param id primary key of the Experiment object
     * @return the Experiment object
     */
    public Experiment getExperimentDetail(String experimentAccession) {
    	Experiment experiment = experimentRepository.getExperimentDetail(experimentAccession);
    	return experiment;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = experimentRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of Experiment objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Experiment>
     */
    public List<Experiment> getByStudy(String studyAccession) {
        List <Experiment> results = experimentRepository.getByStudy(studyAccession);
        return results;
    }
    
    /**
     * Returns a List of Assays objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Assays>
     */
    @Cacheable(value="MechanisticAssays", key="#p0")
    public List<MechanisticAssay> getMechanisticAssays(String studyAccession)
    {
    	  List <Experiment> results = experimentRepository.getByStudy(studyAccession);
    	  
    	  List<MechanisticAssay> returnList = new ArrayList<MechanisticAssay>();
    	  
    	  for (int i=0;i<results.size();i++)
    	  {
    		  Experiment exp =(Experiment)results.get(i);
    		  
    		  long countExpSample = expsampleService.getCountByExperiment(exp.getExperimentAccession());
    		  
    		  MechanisticAssay objAssay = new MechanisticAssay(exp.getExperimentAccession(),
    				  											exp.getName(),exp.getPurpose(),
    				  											exp.getMeasurementTechnique(),
    				  											String.valueOf(countExpSample));
    		  returnList.add(objAssay);
    		  
    	  }
    	  
    	  
    	  return returnList;
    }
    
}

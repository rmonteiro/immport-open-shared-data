package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.HlaTypingResult;

/**
 * Service interface for the HlaTypingResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface HlaTypingResultService {
 
    /**
     * Returns a List of HlaTypingResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <HlaTypingResult>
     */
    public List<HlaTypingResult> getByStudy(String studyAccession);
}

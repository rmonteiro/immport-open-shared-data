package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.ElispotResult;
import org.immport.data.shared.repository.ElispotResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the ElispotResult object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class ElispotResultServiceImpl implements ElispotResultService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ElispotResultServiceImpl.class);

    @Autowired
    private ElispotResultRepository elispotResultRepository;
    

    /**
     * Returns a List of ElispotResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ElispotResult>
     */
    public List<ElispotResult> getByStudy(String studyAccession) {
        List <ElispotResult> results = elispotResultRepository.getByStudy(studyAccession);
        return results;
    }
}

/**
 * 
 */
package org.immport.data.shared.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.GenericPivotData;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author fmonteiro
 *
 */
public abstract class GenericPivotServiceImpl {
	
	private  static Map objMapBaseColInfo;
	
	
	protected void getDistinctArmNames(String studyAccession,Map objHash,List<String> objTempArmNames)
	{
		//get only the required Arms for the Adverse Event Detail for that study in the right order for display
	    List<ArmCohortSortOrder> objArm = getDistinctArmNameForStudy(studyAccession);  
	    int iArmlength = objArm.size();
	   // objHash = new HashMap();
	  //  objTempArmNames = new ArrayList();
	   // String armCohort = "";
	   // String sortOrder = "";
	    for (int j = 0;j < iArmlength ; j++){
	    	ArmCohortSortOrder armSort = (ArmCohortSortOrder)objArm.get(j);	        	
	        objHash.put(armSort.getarmAccession(), "ARM" + armSort.getSortOrder());
	        objTempArmNames.add(armSort.getarmAccession());
	    }
	}
    
	
	
	protected void populateListWithPlaceHolder(List<GenericPivotData> lstPivotData,List<String> objTempArmNames,String placeHolder)
	{
		 Iterator<GenericPivotData> it = lstPivotData.iterator();	         
         while(it.hasNext()) {
        	 GenericPivotData objPivot = (GenericPivotData)it.next();
        	 Map<String, String> objHashPivot =  objPivot.getArmCoValues();
        	 ArrayList<String> objArmData = new ArrayList<String>();
        	
        	 for (int j= 0; j < objTempArmNames.size();j++) {
        		 String armName = objTempArmNames.get(j);
        		 
        		 String armValue = (String)objHashPivot.get(armName);
        		 if (armValue == null)
        		 {
        			 objArmData.add(placeHolder);	        			 
        		 }
        		 else
        		 {
        			 objArmData.add(armValue); 
        		 }
        	 }
        	 objPivot.setArmData(objArmData);
        	 objPivot.setArmNames(objTempArmNames);
         }
	}
	
	
    protected abstract List<ArmCohortSortOrder> getDistinctArmNameForStudy(String studyAccession);
    
    
    protected void populateBaseColumnInformation(String baseColumnType,List<String> objBaseNames,
    											List<String> objBaseNamesSort,List<String> baseNamesSortParameter)
    {
    	
    	if (objMapBaseColInfo == null){
			ApplicationContext context = new ClassPathXmlApplicationContext("/spring/applicationContext-services.xml");
			CollectionInjection baseColInfo = (CollectionInjection)context.getBean("CollectionInjectionBean");	    	
			objMapBaseColInfo = baseColInfo.getMaps();
		}
        String stBaseNames = (String)objMapBaseColInfo.get(baseColumnType + ".BaseNames");
        String stBaseNamesSort = (String)objMapBaseColInfo.get(baseColumnType + ".BaseNamesSort");
        String stBaseNamesParameter = (String)objMapBaseColInfo.get(baseColumnType + ".BaseNamesParameter");
        StringTokenizer stToken = null;
        if (objBaseNames != null){
        	stToken = new StringTokenizer(stBaseNames,",");
        	while (stToken.hasMoreTokens()) {
        		objBaseNames.add(stToken.nextToken());
        	}
        }
        
    	if (objBaseNamesSort != null){
	    	stToken = new StringTokenizer(stBaseNamesSort,",");
	    	while (stToken.hasMoreTokens()) {
	    		objBaseNamesSort.add(stToken.nextToken());
	    	}
    	}
    	
    	if (baseNamesSortParameter != null){
	    	stToken = new StringTokenizer(stBaseNamesParameter,",");
	    	while (stToken.hasMoreTokens()) {
	    		baseNamesSortParameter.add(stToken.nextToken());
	    	}
    	}
        
        
    }
    
    protected void addObjecttoList(String studyAccession,
    								List<GenericPivotData>  objMainList,
    								List<String> objBaseData,
    								List<String> objBaseNames,
    								List<String> objBaseNamesSort,
    								List<String> baseNamesSortParameter,
    								Map<String, String> objHashArm
    								)
    {
    	
    	GenericPivotData objFinal = new GenericPivotData(studyAccession);
		objFinal.setBaseData(objBaseData);
		if (objMainList.size() == 0)
		{
			objFinal.setBaseNames(objBaseNames);
			objFinal.setBaseNamesSort(objBaseNamesSort);
			objFinal.setBaseNamesSortParameter(baseNamesSortParameter);
		}
        objFinal.setArmCoValues(objHashArm);
        objMainList.add(objFinal);      	
    	
    }
	
}

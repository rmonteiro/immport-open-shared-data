package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.KirTypingResult;
import org.immport.data.shared.repository.KirTypingResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the KirTypingResult object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class KirTypingResultServiceImpl implements KirTypingResultService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(KirTypingResultServiceImpl.class);

    @Autowired
    private KirTypingResultRepository kirTypingResultRepository;
    
    
    /**
     * Returns a List of KirTypingResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <KirTypingResult>
     */
    public List<KirTypingResult> getByStudy(String studyAccession) {
        List <KirTypingResult> results = kirTypingResultRepository.getByStudy(studyAccession);
        return results;
    }
}

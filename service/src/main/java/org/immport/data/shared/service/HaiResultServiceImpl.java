package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.HaiResult;
import org.immport.data.shared.repository.HaiResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the HaiResult object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class HaiResultServiceImpl implements HaiResultService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(HaiResultServiceImpl.class);

    @Autowired
    private HaiResultRepository haiResultRepository;
    
    
    /**
     * Returns a List of HaiResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <HaiResult>
     */
    public List<HaiResult> getByStudy(String studyAccession) {
        List <HaiResult> results = haiResultRepository.getByStudy(studyAccession);
        return results;
    }
}

package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.HlaTypingResult;
import org.immport.data.shared.repository.HlaTypingResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the HlaTypingResult object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class HlaTypingResultServiceImpl implements HlaTypingResultService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(HlaTypingResultServiceImpl.class);

    @Autowired
    private HlaTypingResultRepository hlaTypingResultRepository;
    
    
    /**
     * Returns a List of HlaTypingResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <HlaTypingResult>
     */
    public List<HlaTypingResult> getByStudy(String studyAccession) {
        List <HlaTypingResult> results = hlaTypingResultRepository.getByStudy(studyAccession);
        return results;
    }
}

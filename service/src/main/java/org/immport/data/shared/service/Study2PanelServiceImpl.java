package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.Study2Panel;
import org.immport.data.shared.model.StudyFile;
import org.immport.data.shared.repository.Study2PanelRepository;
import org.immport.data.shared.repository.StudyFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the StudyFile object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class Study2PanelServiceImpl implements Study2PanelService {
	
	
	 /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(Study2PanelServiceImpl.class);

    @Autowired
    private Study2PanelRepository study2PanelRepository;
    
    /**
     * Returns a List of StudyFile objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyFile>
     */
    public List<Study2Panel> getByStudy(String studyAccession) {
        List <Study2Panel> results = study2PanelRepository.getByStudy(studyAccession);
        return results;
    }

}

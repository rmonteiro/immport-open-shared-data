package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.LabTest;
import org.immport.data.shared.model.LabTestPanel;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;

/**
 * Service interface for the LabTest object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface LabTestService {
    /**
     * Returns an LabTest object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the LabTest object
     */
    public LabTestPanel findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of LabTest objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <LabTest>
     */
    public List<LabTestPanel> getByStudy(String studyAccession);
    
    /**
     * Returns a List of Lab Test Panel Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Summary data)
     */
    public List<GenericPivotData> getLabTestPanelSummaryByStudy(String studyAccession);
    
    /**
     * Returns a List of Lab Test Component that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Lab Test Component)
     */
    public PageableData getLabTestComponentListByStudy(Integer start, Integer limit,    		
            String sort, String dir,String studyAccession); 
}


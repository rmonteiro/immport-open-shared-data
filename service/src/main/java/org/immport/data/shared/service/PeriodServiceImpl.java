package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.Period;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.PeriodRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the Period object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class PeriodServiceImpl implements PeriodService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(PeriodServiceImpl.class);

    @Autowired
    private PeriodRepository periodRepository;
    

    /**
     * Returns an Period object by querying on the primary key.
     *
     * @param id primary key of the Period object
     * @return the Period object
     */
    public Period findById(String periodAccession) {
        Period period = periodRepository.findById(periodAccession); 
        return period;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = periodRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of Period objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Period>
     */
    public List<Period> getByStudy(String studyAccession) {
        List <Period> results = periodRepository.getByStudy(studyAccession);
        return results;
    }
}

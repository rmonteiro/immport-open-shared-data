package org.immport.data.shared.service;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.List;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.AssessmentPanel;
import org.immport.data.shared.model.AssessmentSummary;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.BiosampleStudyTime;
import org.immport.data.shared.repository.AssessmentPanelRepository;
import org.immport.data.shared.service.GenericPivotServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Service class for the Assessment object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class AssessmentServiceImpl  extends GenericPivotServiceImpl implements AssessmentService {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(AssessmentServiceImpl.class);

    @Autowired
    private AssessmentPanelRepository assessmentRepository;
  
    /**
     * Returns an Assessment object by querying on the primary key.
     *
     * @param id primary key of the Assessment object
     * @return the Assessment object
     */
    public AssessmentPanel findById(String assessmentAccession) {
    	AssessmentPanel assessment = assessmentRepository.findById(assessmentAccession); 
        return assessment;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = assessmentRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of Assessment objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Assessment>
     */
    public List<AssessmentPanel> getByStudy(String studyAccession) {
        List <AssessmentPanel> results = assessmentRepository.getByStudy(studyAccession);
        return results;
    }
      
    /**
     * Returns a List of Pivoted Assessment Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Summary data)
     */
    @Cacheable(value="AssessmentSummary", key="#p4.concat('-').concat(#p0).concat('-').concat(#p2).concat('-').concat(#p3)")
    public PageableData getAssessmentSummaryByStudy(Integer start, Integer limit,            
            String sort, String dir,String studyAccession) {
        PageableData pageRet = new PageableData();
        PageableData page = assessmentRepository.getAssessmentPanelSummaryByStudy(start,limit,sort,dir,studyAccession);
        pageRet.setTotalCount(page.getTotalCount());
        List<AssessmentSummary> results = (List<AssessmentSummary>)page.getData(); 
        List<GenericPivotData> objAssessmentPivotList = new ArrayList<GenericPivotData>();
        if (results.size() > 0) { 
            String stAssessment = "Assessment Components";
            String stSubjects = "Subjects";
            HashMap<String, String> objHash = new HashMap<String, String>();
            List<String> objTempArmNames = new ArrayList<String>();
            getDistinctArmNames(studyAccession,objHash,objTempArmNames);
            HashMap<String, String> objHashAssessmentData = new HashMap<String, String>();
            HashMap<String, String> objHashSubjectData = new HashMap<String, String>();
                 
            String panelNameReported = "";
            String tempPanelNameReported = "";
            long assessmentCount = 0;
            long subjectCount = 0;
            List<String> objBaseData = new ArrayList<String>();
            List<String> objBaseNames = new ArrayList<String>();
            List<String> objBaseNamesSort = new ArrayList<String>();
            List<String> baseNamesSortParameter = new ArrayList<String>();
                    
            populateBaseColumnInformation("AssessmentSummary", objBaseNames,
                    objBaseNamesSort,baseNamesSortParameter);
            NumberFormat numberFormatter = NumberFormat.getNumberInstance(Locale.US);
            for(int i=0;i < results.size();i++) {
                 AssessmentSummary objAssessmentSummary = (AssessmentSummary)results.get(i);
                 if (i == 0) {
                     panelNameReported = objAssessmentSummary.getPanelNameReported();
                 } else {
                     tempPanelNameReported =  objAssessmentSummary.getPanelNameReported();
                     if (!panelNameReported.equalsIgnoreCase(tempPanelNameReported)) {                   
                         objBaseData = new ArrayList<String>();
                         objBaseData.add(panelNameReported);
                         objBaseData.add(stSubjects);
                         addObjecttoList(
                                      studyAccession,
                                      objAssessmentPivotList,
                                      objBaseData,
                                      objBaseNames,
                                      objBaseNamesSort,
                                      baseNamesSortParameter,
                                      objHashSubjectData);
                             
                         objBaseData = new ArrayList<String>();
                         objBaseData.add(panelNameReported);
                         objBaseData.add(stAssessment);
                         addObjecttoList(
                                     studyAccession,
                                     objAssessmentPivotList,
                                     objBaseData,
                                     objBaseNames,
                                     objBaseNamesSort,
                                     baseNamesSortParameter,
                                     objHashAssessmentData);
                             
                         panelNameReported = tempPanelNameReported;
                         objHashAssessmentData = new HashMap<String, String>();
                         objHashSubjectData = new HashMap<String, String>(); 
                     }                         
                 }
                                     
                 String stArm = objAssessmentSummary.getArmAccession();
                 assessmentCount = objAssessmentSummary.getAssessmentNo();
                 subjectCount = objAssessmentSummary.getSubjAccNo();
                 String strAssessmentCount = numberFormatter.format(assessmentCount);
                 String strSubjectCount = numberFormatter.format(subjectCount);
                 objHashAssessmentData.put(stArm, strAssessmentCount);
                 objHashSubjectData.put(stArm, strSubjectCount);
                     
                 if (i == (results.size() -1)) {
                     objBaseData = new ArrayList<String>();
                     objBaseData.add(panelNameReported);
                     objBaseData.add(stSubjects);                      
                     addObjecttoList(
                                 studyAccession,
                                 objAssessmentPivotList,
                                 objBaseData,
                                 objBaseNames,
                                 objBaseNamesSort,
                                 baseNamesSortParameter,
                                 objHashSubjectData);
                       
                     objBaseData = new ArrayList<String>();
                     objBaseData.add(panelNameReported);
                     objBaseData.add(stAssessment);                      
                     addObjecttoList(
                                 studyAccession,
                                 objAssessmentPivotList,
                                 objBaseData,
                                 objBaseNames,
                                 objBaseNamesSort,
                                 baseNamesSortParameter,
                                 objHashAssessmentData);
                 }
             }   
             populateListWithPlaceHolder(objAssessmentPivotList,objTempArmNames,"0");
        }
        pageRet.setData(objAssessmentPivotList);
        return pageRet;
    }
    
    
    /**
     * Returns a List of Assessment Component that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Component)
     */
    @Cacheable(value="AssessmentComponent", key="#p4.concat('-').concat(#p0).concat('-').concat(#p2).concat('-').concat(#p3)")
    public PageableData getAssessmentComponentListByStudy(Integer start, Integer limit,            
            String sort, String dir,String studyAccession) {
        PageableData pageRet = null;
        pageRet = assessmentRepository.getAssessmentComponentListByStudy(start,limit,sort,dir,studyAccession);
        return pageRet;
    }
    

    public PageableData getStudyTimeCollectedByAssessmentsPageable(Integer start, Integer limit,            
            String sort, String dir,String studyAccession) {
        PageableData pageRet = null;
        pageRet = assessmentRepository.getStudyTimeCollectedByAssessmentsPageable(start,limit,sort,dir,studyAccession);
        return pageRet;
    }    


     /**
     * Returns a List of Arms that are linked to the Assessments for that
     * study.
     *
     * @param studyAccession accession for the study
     * @return List (Arms)
     */
    protected List<ArmCohortSortOrder> getDistinctArmNameForStudy(String studyAccession) {
        List<ArmCohortSortOrder> objArm = (List<ArmCohortSortOrder>)assessmentRepository.getDistinctArmNameForAssessmentPanel(studyAccession);
        return objArm;
    }
    
    
    /**
     * Returns a List of BiosampleStudyTime objects that are linked to
     * a study via Assessments
     *
     * @param studyAccession accession for the study
     * @return List <BiosampleStudyTime>
     */
    public List<BiosampleStudyTime> getStudyTimeCollectedByAssessments(String studyAccession) {
        return assessmentRepository.getStudyTimeCollectedByAssessments(studyAccession);
        
    }
}

package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.MbaaResult;
import org.immport.data.shared.repository.MbaaResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the MbaaResult object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class MbaaResultServiceImpl implements MbaaResultService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(MbaaResultServiceImpl.class);

    @Autowired
    private MbaaResultRepository mbaaResultRepository;
    
    
    /**
     * Returns a List of MbaaResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <MbaaResult>
     */
    public List<MbaaResult> getByStudy(String studyAccession) {
        List <MbaaResult> results = mbaaResultRepository.getByStudy(studyAccession);
        return results;
    }
}

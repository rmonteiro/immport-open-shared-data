package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.PlannedVisit;
import org.immport.data.shared.model.PlannedVisitData;
import org.immport.data.shared.model.PageableData;

/**
 * Service interface for the PlannedVisit object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface PlannedVisitService {
    /**
     * Returns an PlannedVisit object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the PlannedVisit object
     */
    public PlannedVisit findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of PlannedVisit objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <PlannedVisit>
     */
    public List<PlannedVisit> getByStudy(String studyAccession);
    
    /**
     * Returns a List of PlannedVisit objects By Experiment that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return void
     */
    public PlannedVisitData getPlannedVisitsByType(String studyAccession, String type);
    
 
}


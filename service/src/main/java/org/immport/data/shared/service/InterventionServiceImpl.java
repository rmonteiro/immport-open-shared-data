package org.immport.data.shared.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;






import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.ConcomitantMedicationDetail;
import org.immport.data.shared.model.ConcomitantMedicationSummary;
import org.immport.data.shared.model.StudyIntervention;
import org.immport.data.shared.model.Intervention;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;



import org.immport.data.shared.repository.InterventionRepository;
import org.immport.data.shared.service.GenericPivotServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the SubstanceMerge object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class InterventionServiceImpl extends GenericPivotServiceImpl implements InterventionService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(InterventionServiceImpl.class);

    @Autowired
    private InterventionRepository interventionRepository;
    
   // @Autowired
  //  private ArmOrCohortRepository armOrCohortRepository;
    
    //@Autowired
    //private ArmOrCohortService armOrCohortService;
    
    private String type ="";
    
    
    /**
     * Returns an SubstanceMerge object by querying on the primary key.
     *
     * @param id primary key of the SubstanceMerge object
     * @return the SubstanceMerge object
     */
    public Intervention findById(String interventionAccession) {
    	Intervention intervention = interventionRepository.findById(interventionAccession); 
        return intervention;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = interventionRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of SubstanceMerge objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <SubstanceMerge>
     */
    public List<Intervention> getByStudy(String studyAccession) {
        List <Intervention> results = interventionRepository.getByStudy(studyAccession);
        return results;
    }
    
    /**
     * Returns a List of Pivoted Concomitant Medication Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Concomitant Med Summary data)
     */
    public List<GenericPivotData> getConcomitantMedicationByStudy(String studyAccession)
    {
         List<ConcomitantMedicationSummary> results = interventionRepository.getConcomitantMedicationByStudy(studyAccession);
         this.type = "ConcomitantMedication";
         List<GenericPivotData> objConMedList = getSummaryDatabyType(studyAccession, results);
         return objConMedList;

    }
    
    
    /** 
     * Returns a List of Substance Use Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Substance Use Summary data)
     */
    public List<GenericPivotData> getSubstanceUseSummaryByStudy(String studyAccession)
    {
    	 List<ConcomitantMedicationSummary> results = interventionRepository.getSubstanceUseSummaryByStudy(studyAccession);
         this.type = "SubstanceUse";
         List<GenericPivotData> objConMedList = getSummaryDatabyType(studyAccession, results);
         return objConMedList;
    }

    /**
     * Returns a List of Pivoted Concomitant Medication Detail data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Concomitant Med Detail data)
     */
    public PageableData getConcomitantMedDetailByStudy(Integer start, Integer limit,
            String sort, String dir,String studyAccession)
    {
    	PageableData pageData =
    			interventionRepository.getConcomitantMedDetailByStudy(start, limit, sort, dir, studyAccession);
        this.type = "ConcomitantMedication";
        
        PageableData page = getDetailDatabyType(studyAccession,pageData);
        return page;
    }   
    
    
    /**
     * Returns a List of Pivoted Substance Use Detail data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Concomitant Med Detail data)
     */
    public PageableData getSubstanceUseDetailByStudy(Integer start, Integer limit,
            String sort, String dir,String studyAccession)
    {
    	PageableData pageData =
    			interventionRepository.getSubstanceUseDetailByStudy(start, limit, sort, dir, studyAccession);
        this.type = "SubstanceUse";
        
        PageableData page = getDetailDatabyType(studyAccession,pageData);
        return page;
    }   
    
    
    /**
     * Returns a List of Pivoted Intervention Detail data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Intervention Detail data)
     */
    public PageableData getInterventionDetailByStudy(Integer start, Integer limit,
            String sort, String dir,String studyAccession)
    {
    	PageableData page = new PageableData();
    	this.type = "Intervention";
        List<GenericPivotData>  objInterventionDetList = new ArrayList<GenericPivotData>();
        PageableData pageData =
        		interventionRepository.getStudyInterventionDetailByStudy(start, limit, sort, dir, studyAccession);
        long iTotalCount = pageData.getTotalCount();
        if (iTotalCount > 0) {
	        List<StudyIntervention> objList =  (List<StudyIntervention>)pageData.getData();
	
	       
	        
	        HashMap<String, String> objHash = new HashMap<String, String>();
	        List<String> objTempArmNames = new ArrayList<String>();
	        getDistinctArmNames(studyAccession,objHash,objTempArmNames);
	
	       //Pivoting Data
	        String compoundRole = "";
	        String compoundNameReported = "";
	        String mergeNameReported = "";
	        String tempCompoundRole = "";
	        String tempCompoundNameReported = "";
	        String tempMergeNameReported = "";
	        
	        long iTotalBy = 0;
	
	       
	       
	        HashMap<String, String> objHashArm = new HashMap<String, String>();
	        //List<String> objArmData = null;
	       
	        List<String> objBaseData = null;
	        List<String> objBaseNames = new ArrayList<String>();
	        List<String> objBaseNamesSort = new ArrayList<String>();
			List<String> baseNamesSortParameter = new ArrayList<String>();
	        
			populateBaseColumnInformation(this.type, objBaseNames,
					objBaseNamesSort,baseNamesSortParameter);
			
	      
	         
	        long dataCount = 0;
	        
	        for (int i=0;i< objList.size();i++) {
	        	StudyIntervention objIntervention = (StudyIntervention)objList.get(i);
	                if (i==0) {
	                	    compoundRole = objIntervention.getCompoundRole();
	                	    compoundNameReported = objIntervention.getCompoundNameReported();
	                	    mergeNameReported = objIntervention.getMergeNameReported();
	                		
	                		objHashArm = new HashMap<String, String>();
	                }
	                else {
	                		tempCompoundRole = objIntervention.getCompoundRole();
	                		tempCompoundNameReported = objIntervention.getCompoundNameReported();
	                		tempMergeNameReported = objIntervention.getMergeNameReported();
	                        if (!((compoundRole.equalsIgnoreCase((tempCompoundRole))) 
	                        		&& (compoundNameReported.equalsIgnoreCase((tempCompoundNameReported)))
	                        		&& (mergeNameReported.equalsIgnoreCase((tempMergeNameReported))))) {	                               
	                        	
	                                
	                                objBaseData = new ArrayList<String>(); 	                       		
		   	                        objBaseData.add(mergeNameReported);
		   	                        objBaseData.add(compoundNameReported);
		   	                     
		   	                        objBaseData.add(String.valueOf(iTotalBy));
		   	                        
		   	                     
		   	                    	
		   	                    	
		   	                        addObjecttoList(
		                        			studyAccession,
		                        			objInterventionDetList,
		      								objBaseData,
		      								objBaseNames,
		      								objBaseNamesSort,
		      								baseNamesSortParameter,
		      								objHashArm
		      								);
	                                
	                                compoundRole = tempCompoundRole;
	                                compoundNameReported =tempCompoundNameReported;
	                                mergeNameReported = tempMergeNameReported;
	                                iTotalBy = 0;
	                                objHashArm = new HashMap<String, String>();
	                               
	                        }
	                }
	
	                iTotalBy = iTotalBy  + objIntervention.getSubjAccNo();
	                String stArm =          objIntervention.getArmAccession();
	                //String  armColName = (String)objHash.get(stArm);
		        	 
		        	dataCount = objIntervention.getSubjAccNo();	        	
		        	objHashArm.put(stArm, String.valueOf(dataCount));
	
	              
	                if (i == (objList.size() - 1)) {    
	                	
	                			
	                		objBaseData = new ArrayList<String>(); 	                       		
		   	                objBaseData.add(mergeNameReported);
		   	                objBaseData.add(compoundNameReported);		   	                
		   	                objBaseData.add(String.valueOf(iTotalBy));		   	                   
		   	                     
		   	                addObjecttoList(
	                        			studyAccession,
	                        			objInterventionDetList,
	      								objBaseData,
	      								objBaseNames,
	      								objBaseNamesSort,
	      								baseNamesSortParameter,
	      								objHashArm
	      								);
	                }
	        }
	        
	        populateListWithPlaceHolder(objInterventionDetList,objTempArmNames,"");
	        
	      
	
	        page.setData(objInterventionDetList);
	        page.setTotalCount(pageData.getTotalCount());
        }
        else
        {
        	page.setData(objInterventionDetList);
        	page.setTotalCount(pageData.getTotalCount());        	
        }
        return page;
    }
    
    /**
     * Returns a List of Arms that are linked to the Substance merge for that
     * study.
     *
     * @param studyAccession accession for the study
     * @return List (Arms)
     */
    protected List<ArmCohortSortOrder> getDistinctArmNameForStudy(String studyAccession)
    {
    	   List<ArmCohortSortOrder> objArm = (List<ArmCohortSortOrder>)interventionRepository.getDistinctArmNameForCompoundRole(studyAccession,this.type);
    	   return objArm;
    }
    
    
    
    private List<GenericPivotData> getSummaryDatabyType(String studyAccession,List<ConcomitantMedicationSummary> results) {
    	 List<GenericPivotData> objConMedList = new ArrayList<GenericPivotData>();
         if (results.size() > 0) {
	        // String l_stStudyAccNum = "";
	      //   String l_stConMed = "Concomitant Medications";
	      //   String l_stSubjects = "Subjects";
	      //   String l_stArmStrConMed = "";
	       //  String l_stArmSubjects = "";
	      //   String l_stDataValueConMed = "";
	      //   String l_stDataValueSubjects = "";
	
	        // Iterator objIterator = results.iterator();
	      //   int iCount = 0;
	
	         
	         HashMap<String, String> objHash = new HashMap<String, String>();
		     List<String> objTempArmNames = new ArrayList<String>();
		     getDistinctArmNames(studyAccession,objHash,objTempArmNames);	
	
	         //pivoting the data
		     HashMap<String, String> objHashConMedArmData = new HashMap<String, String>();
		     HashMap<String, String> objHashSubjectArmData = new HashMap<String, String>();
	        // List<String> objConMedArmData = new ArrayList<String>();
	       //  List<String> objSubjectArmData = new ArrayList<String>();
	       
	         //int count = 1;
	         String armName = "";
	        // String tempArmName = "";
	        
	         for (int i=0;i< results.size();i++) {
	         
	        	 ConcomitantMedicationSummary objConMedSummary = (ConcomitantMedicationSummary)results.get(i);
	             armName = objConMedSummary.getarmAccession();
	             
	            // String  armColName = (String)objHash.get(armName);
		         objHashConMedArmData.put(armName, String.valueOf(objConMedSummary.getCompReportNo()));
		         objHashSubjectArmData.put(armName, String.valueOf(objConMedSummary.getSubjAccNo()));
	         }
	        	   
		         List<String> objBaseData = new ArrayList<String>();
				 List<String> objBaseNames = new ArrayList<String>();
				 objBaseNames.add("Totals By");			
				 
		         objBaseData = new ArrayList<String>(); 	                       		
	             objBaseData.add("Concomitant Medications");
		         
	             addObjecttoList(
             			studyAccession,
             			objConMedList,
						objBaseData,
						objBaseNames,
						null,
						null,
						objHashConMedArmData
						);
		         
		         
		        			 			
		         objBaseData = new ArrayList<String>(); 	                       		
	             objBaseData.add("Subjects");
	             
	             addObjecttoList(
	             			studyAccession,
	             			objConMedList,
							objBaseData,
							objBaseNames,
							null,
							null,
							objHashSubjectArmData
							);
		         
		
	        	 
	             populateListWithPlaceHolder(objConMedList,objTempArmNames,"0");                    
         }
         return objConMedList;
    }
    
    
    
    private PageableData getDetailDatabyType(String studyAccession,PageableData pageData) {
    	PageableData page = new PageableData();
        List<GenericPivotData>  objConMedDetList = new ArrayList<GenericPivotData>();
        long iTotalCount = pageData.getTotalCount();
        if (iTotalCount > 0) {
	        List<ConcomitantMedicationDetail> objList =  (List<ConcomitantMedicationDetail>)pageData.getData();
	
	        
	        HashMap<String, String> objHash = new HashMap<String, String>();
		     List<String> objTempArmNames = new ArrayList<String>();
		     getDistinctArmNames(studyAccession,objHash,objTempArmNames);       	
	        
	       //Pivoting Data
	        String stCompoundName = "";
	        String stTempCompoundName = "";
	        long iTotalBy = 0;
	
	       
	        
	      
	        HashMap<String, String> objHashArm = new HashMap<String, String>();
	        List<String> objBaseData = null;
			List<String> objBaseNames = new ArrayList<String>();
			List<String> objBaseNamesSort = new ArrayList<String>();
			List<String> baseNamesSortParameter = new ArrayList<String>();
			
			
			populateBaseColumnInformation(this.type, objBaseNames,
					objBaseNamesSort,baseNamesSortParameter);
			
			
			
			long dataCount = 0;
	    
	        for (int i=0;i< objList.size();i++) {
	                ConcomitantMedicationDetail objConMed = (ConcomitantMedicationDetail)objList.get(i);
	                if (i==0) {
	                        stCompoundName = objConMed.getCompoundNameReported();
	                     
	                }
	                else {
	                        stTempCompoundName = objConMed.getCompoundNameReported();
	                        if (!stCompoundName.equalsIgnoreCase((stTempCompoundName))) {
	                                
	                                
	                              objBaseData = new ArrayList<String>(); 	                       		
		   	                      objBaseData.add(stCompoundName);
		   	                      objBaseData.add(String.valueOf(iTotalBy));		   	                       
		   	                      
		   	                    	
		   	                      addObjecttoList(
		                        			studyAccession,
		                        			objConMedDetList,
		      								objBaseData,
		      								objBaseNames,
		      								objBaseNamesSort,
		      								baseNamesSortParameter,
		      								objHashArm
		      								);
		   	                    	
	                                stCompoundName = stTempCompoundName;
	                                iTotalBy = 0;	                                
	                                objHashArm = new HashMap<String, String>();
	                        }
	                }
	
	                iTotalBy = iTotalBy  + objConMed.getSubjAccNo();               
	                String stArm =  objConMed.getarmAccession();
	              //  String  armColName = (String)objHash.get(stArm);		        	 
		        	dataCount = objConMed.getSubjAccNo();      	
		        	objHashArm.put(stArm, String.valueOf(dataCount));
		        	
	                if (i == (objList.size() - 1)) {
	                        
	                     
                         objBaseData = new ArrayList<String>(); 	                       		
   	                     objBaseData.add(stCompoundName);
   	                     objBaseData.add(String.valueOf(iTotalBy));		   	                       
   	                      	                    	
   	                     addObjecttoList(
                     			studyAccession,
                     			objConMedDetList,
   								objBaseData,
   								objBaseNames,
   								objBaseNamesSort,
   								baseNamesSortParameter,
   								objHashArm
   								);
	
	                             
	                }
	        }
	
	        populateListWithPlaceHolder(objConMedDetList,objTempArmNames,"");
	        page.setData(objConMedDetList);
	        page.setTotalCount(pageData.getTotalCount());
        }
        else
        {
        	page.setData(objConMedDetList);
        	page.setTotalCount(pageData.getTotalCount());        	
        }
        return page;
    }
    
}

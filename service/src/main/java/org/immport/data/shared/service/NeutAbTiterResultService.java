package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.NeutAbTiterResult;

/**
 * Service interface for the NeutAbTiterResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface NeutAbTiterResultService {
    /**
     * Returns a List of NeutAbTiterResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <NeutAbTiterResult>
     */
    public List<NeutAbTiterResult> getByStudy(String studyAccession);
}
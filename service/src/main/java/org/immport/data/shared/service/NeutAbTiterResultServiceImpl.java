package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.NeutAbTiterResult;
import org.immport.data.shared.repository.NeutAbTiterResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the NeutAbTiterResult object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class NeutAbTiterResultServiceImpl implements NeutAbTiterResultService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(NeutAbTiterResultServiceImpl.class);

    @Autowired
    private NeutAbTiterResultRepository neutAbTiterResultRepository;
    
    
    /**
     * Returns a List of NeutAbTiterResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <NeutAbTiterResult>
     */
    public List<NeutAbTiterResult> getByStudy(String studyAccession) {
        List <NeutAbTiterResult> results = neutAbTiterResultRepository.getByStudy(studyAccession);
        return results;
    }
}

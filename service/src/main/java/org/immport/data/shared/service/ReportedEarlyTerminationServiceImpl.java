package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.ReportedEarlyTermination;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.ReportedEarlyTerminationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the ReportedEarlyTermination object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class ReportedEarlyTerminationServiceImpl implements ReportedEarlyTerminationService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ReportedEarlyTerminationServiceImpl.class);

    @Autowired
    private ReportedEarlyTerminationRepository reportedEarlyTerminationRepository;
    

    /**
     * Returns an ReportedEarlyTermination object by querying on the primary key.
     *
     * @param id primary key of the ReportedEarlyTermination object
     * @return the ReportedEarlyTermination object
     */
    public ReportedEarlyTermination findById(String reportedEarlyTerminationAccession) {
        ReportedEarlyTermination reportedEarlyTermination = reportedEarlyTerminationRepository.findById(reportedEarlyTerminationAccession); 
        return reportedEarlyTermination;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = reportedEarlyTerminationRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of ReportedEarlyTermination objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ReportedEarlyTermination>
     */
    public List<ReportedEarlyTermination> getByStudy(String studyAccession) {
        List <ReportedEarlyTermination> results = reportedEarlyTerminationRepository.getByStudy(studyAccession);
        return results;
    }
}

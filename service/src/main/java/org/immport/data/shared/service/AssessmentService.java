package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.AssessmentPanel;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.BiosampleStudyTime;

/**
 * Service interface for the Assessment object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface AssessmentService {
    /**
     * Returns an Assessment object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the Assessment object
     */
    public AssessmentPanel findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of Assessment objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Assessment>
     */
    public List<AssessmentPanel> getByStudy(String studyAccession);
    
    /** 
     * Returns a List of Assessment Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Summary data)
     */
    public PageableData getAssessmentSummaryByStudy(Integer start, Integer limit,    		
            String sort, String dir,String studyAccession);
    
    /**
     * Returns a List of Assessment Component that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Component)
     */
    public PageableData getAssessmentComponentListByStudy(Integer start, Integer limit,    		
            String sort, String dir,String studyAccession); 
    
     public PageableData getStudyTimeCollectedByAssessmentsPageable(Integer start, Integer limit,    		
            String sort, String dir,String studyAccession);	    

    /**
     * Returns a List of BiosampleStudyTime objects that are linked to
     * a study via Lab Tests
     *
     * @param studyAccession accession for the study
     * @return List <BiosampleStudyTime>
     */
    public List<BiosampleStudyTime> getStudyTimeCollectedByAssessments(String studyAccession);
    
}


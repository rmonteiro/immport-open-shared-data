package org.immport.data.shared.service;

import java.util.ArrayList;
import java.util.List;

import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.Biosample;

import org.immport.data.shared.model.TreatmentToExpSampleInfo;
import org.immport.data.shared.repository.BiosampleRepository;
import org.immport.data.shared.repository.TreatmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the Biosample object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class TreatmentServiceImpl implements TreatmentService {
	
	
	  /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(TreatmentServiceImpl.class);

    @Autowired
    private TreatmentRepository treatmentRepository;
    
    
    /**
     * Returns an Biosample object by querying on the primary key.
     *
     * @param id primary key of the Biosample object
     * @return the GenericPivotData
     */
    public List<TreatmentToExpSampleInfo> getTreatmentToExpSample(String studyAccession) {
    
    	List<TreatmentToExpSampleInfo> lstTreatmentToExpSample = treatmentRepository.getTreatmentToExpSample(studyAccession);
    	
        return lstTreatmentToExpSample;
    }

}

package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.ArmOrCohort;
import org.immport.data.shared.repository.ArmOrCohortRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the ArmOrCohort object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class ArmOrCohortServiceImpl implements ArmOrCohortService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ArmOrCohortServiceImpl.class);

    @Autowired
    private ArmOrCohortRepository armOrCohortRepository;
    
    
    /**
     * Returns an ArmOrCohort object by querying on the primary key.
     *
     * @param id primary key of the ArmOrCohort object
     * @return the ArmOrCohort object
     */
    public ArmOrCohort findById(String armOrCohortAccession) {
        ArmOrCohort armOrCohort = armOrCohortRepository.findById(armOrCohortAccession); 
        return armOrCohort;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = armOrCohortRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of ArmOrCohort objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmOrCohort>
     */
    public List<ArmOrCohort> getByStudy(String studyAccession) {
        List <ArmOrCohort> results = armOrCohortRepository.getByStudy(studyAccession);
        return results;
    }
    
    /**
     * Returns a List of Armaccession by sort order that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmOrCohort>
     */
    public List<String> getArmAccessionByStudy(String studyAccession)
    {
         List <String> results = armOrCohortRepository.getArmAccessionByStudy(studyAccession);
         return results;
    }   
    
    
    /**
     * Returns a List of ArmCohortSortOrder by sort order that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmOrCohort>
     */    
    public List<ArmCohortSortOrder> getArmOrderAndDescByStudy (String studyAccession) {
    	List <ArmCohortSortOrder> results = armOrCohortRepository.getArmOrderAndDescByStudy(studyAccession);
        return results;
    }
    
}

package org.immport.data.shared.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.DemograhicSummary;
import org.immport.data.shared.model.Subject;
import org.immport.data.shared.model.SubjectDemographic;



import org.immport.data.shared.repository.SubjectRepository;
import org.immport.data.shared.service.GenericPivotServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the Subject object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class SubjectServiceImpl extends GenericPivotServiceImpl implements SubjectService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SubjectServiceImpl.class);

    @Autowired
    private SubjectRepository subjectRepository;
    
    
    /**
     * Returns an Subject object by querying on the primary key.
     *
     * @param id primary key of the Subject object
     * @return the Subject object
     */
    public Subject findById(String subjectAccession) {
        Subject subject = subjectRepository.findById(subjectAccession); 
        return subject;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = subjectRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of Subject objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Subject>
     */
    public List<Subject> getByStudy(String studyAccession) {
        List <Subject> results = subjectRepository.getByStudy(studyAccession);
        return results;
    }
    
    /** 
     * Returns a List of Demographic Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Demographic Summary data)
     */
    public List<GenericPivotData> getDemograhicSummaryByStudy(String studyAccession)
    {
    	 List<DemograhicSummary> results = subjectRepository.getDemograhicSummaryByStudy(studyAccession);

         List<GenericPivotData> objPivotList = new ArrayList<GenericPivotData>();
         if (results.size() > 0) {

 	       
 	
 	       HashMap objHash = new HashMap();
		   List<String> objTempArmNames = new ArrayList<String>();
		   getDistinctArmNames(studyAccession,objHash,objTempArmNames);
 	        
 	       //Pivoting Data
 	        String nameReported = ""; 	      
 	        String tempNameReported = "";
 	        String unit = "";
 	       String tempUnit = "";
 	        
 	       // long iTotalBy = 0;
 	
 	       
 	       
 	        HashMap<String, String> objHashArm = new HashMap<String, String>();
 	        //List<String> objArmData = new ArrayList<String>();
 	       
 	       List<String> objBaseData = null;
		   List<String> objBaseNames = new ArrayList<String>();
		   objBaseNames.add("Demographic Statistic");
		   //objBaseNames.add("Unit");
 	     
 	        
 	        for (int i=0;i< results.size();i++) {
 	        	DemograhicSummary objSummary = (DemograhicSummary)results.get(i);
 	                if (i==0) {
 	                	nameReported = objSummary.getStatistic();
 	                	unit = objSummary.getUnit();
 	                	objHashArm = new HashMap();
 	                }
 	                else {
 	                		tempNameReported = objSummary.getStatistic();
 	                		tempUnit = objSummary.getUnit();
 	                        if (!(nameReported.equalsIgnoreCase((tempNameReported)) 
 	                        		)) {	                               
 	                        	
 	                               
 	                               
 	                             objBaseData = new ArrayList<String>(); 
 	                            /* if (nameReported.equalsIgnoreCase("Min Age") 
 	                            		 || nameReported.equalsIgnoreCase("Mean Age") 
 	                            		 || nameReported.equalsIgnoreCase("Max Age"))
 	                             {
 	                            	nameReported = nameReported + " (" + unit + ")";
 	                             }*/
 	                             
 	                             if ((nameReported.indexOf("Age")) > 0)
 	                             {
 	                            	nameReported = nameReported + " (" + unit + ")";
 	                             }
 		                         objBaseData.add(nameReported);
 		                       
 		                         //objBaseData.add(unit);
 		                         //GenericPivotData objFinal = new GenericPivotData(studyAccession);
 		                    	 //objFinal.setBaseData(objBaseData);
 		                    	 //objFinal.setBaseNames(objBaseNames);							
 		                    	 //objFinal.setArmCoValues(objHashArm);
 		                    	 //objPivotList.add(objFinal);
 	                             
 		                    	 addObjecttoList(
 		                     			studyAccession,
 		                     			objPivotList,
 		   								objBaseData,
 		   								objBaseNames,
 		   								null,
 		   								null,
 		   								objHashArm
 		   								);
 	                               
 	                                
 	                              nameReported = tempNameReported;
 	                              unit =tempUnit; 	                               
 	                              objHashArm = new HashMap<String, String>();
 	                               
 	                        }
 	                }
 	
 	              
 	                String stArm =          objSummary.getArmAccession();
 	                //String  armColName = (String)objHash.get(stArm);
 		        	 
 		        	String sDisplayData = objSummary.getDisplayData();	        	
 		        	objHashArm.put(stArm, sDisplayData);
 	
 	              
 	                if (i == (results.size() - 1)) {    
 	                	
 	                    
 	                   
                         objBaseData = new ArrayList<String>(); 
                         /*if (nameReported.equalsIgnoreCase("Min Age") 
                         		 || nameReported.equalsIgnoreCase("Mean Age") 
                         		 || nameReported.equalsIgnoreCase("Max Age"))
                          {
                         	nameReported = nameReported + " (" + unit + ")";
                          }*/
                         
                         if ((nameReported.indexOf("Age")) > 0)
                         {
                         	nameReported = nameReported + " (" + unit + ")";
                         }
                         objBaseData.add(nameReported);                         
                         //objBaseData.add(unit);
                         //GenericPivotData objFinal = new GenericPivotData(studyAccession);
                    	 //objFinal.setBaseData(objBaseData);
                    	 //objFinal.setBaseNames(objBaseNames);							
                    	 //objFinal.setArmCoValues(objHashArm);
                    	 //objPivotList.add(objFinal);
                    	 
                    	 addObjecttoList(
	                     			studyAccession,
	                     			objPivotList,
	   								objBaseData,
	   								objBaseNames,
	   								null,
	   								null,
	   								objHashArm
	   								);
 	                }
 	        }
 	        
 	       populateListWithPlaceHolder(objPivotList,objTempArmNames,"0");
 	      
        
         } 
         

         return objPivotList;
    }
    
    
    /**
     * Returns a List of Arms that are linked to the Subjects for that
     * study.
     *
     * @param studyAccession accession for the study
     * @return List (Arms)
     */
    protected List<ArmCohortSortOrder> getDistinctArmNameForStudy(String studyAccession)
    {
    	   List<ArmCohortSortOrder> objArm = (List<ArmCohortSortOrder>)subjectRepository.getDistinctArmNameForDemograhics(studyAccession);
    	   return objArm;
    }
    
    public List<SubjectDemographic> getSubjectDemographicsByStudy(String studyAccession) {
 	   return subjectRepository.getSubjectDemographicsByStudy(studyAccession);
    }
}

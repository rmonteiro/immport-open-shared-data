package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.ProtocolDeviation;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.ProtocolDeviationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the ProtocolDeviation object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class ProtocolDeviationServiceImpl implements ProtocolDeviationService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ProtocolDeviationServiceImpl.class);

    @Autowired
    private ProtocolDeviationRepository protocolDeviationRepository;
    

    /**
     * Returns an ProtocolDeviation object by querying on the primary key.
     *
     * @param id primary key of the ProtocolDeviation object
     * @return the ProtocolDeviation object
     */
    public ProtocolDeviation findById(String protocolDeviationAccession) {
        ProtocolDeviation protocolDeviation = protocolDeviationRepository.findById(protocolDeviationAccession); 
        return protocolDeviation;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = protocolDeviationRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of ProtocolDeviation objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ProtocolDeviation>
     */
    public List<ProtocolDeviation> getByStudy(String studyAccession) {
        List <ProtocolDeviation> results = protocolDeviationRepository.getByStudy(studyAccession);
        return results;
    }
}

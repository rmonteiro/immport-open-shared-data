package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.StudyLink;
import org.immport.data.shared.repository.StudyLinkRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the StudyLink object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class StudyLinkServiceImpl implements StudyLinkService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyLinkServiceImpl.class);

    @Autowired
    private StudyLinkRepository studyLinkRepository;
    
    
    /**
     * Returns an StudyLink object by querying on the primary key.
     *
     * @param id primary key of the StudyLink object
     * @return the StudyLink object
     */
    public StudyLink findById(Integer studyLinkAccession) {
        StudyLink studyLink = studyLinkRepository.findById(studyLinkAccession); 
        return studyLink;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = studyLinkRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of StudyLink objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyLink>
     */
    public List<StudyLink> getByStudy(String studyAccession) {
        List <StudyLink> results = studyLinkRepository.getByStudy(studyAccession);
        return results;
    }
}

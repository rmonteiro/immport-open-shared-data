package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Study2Panel;
import org.immport.data.shared.model.StudyFile;


/**
 * Service interface for the StudyFile object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface Study2PanelService  {
    
    /**
     * Returns a List of StudyFile objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyFile>
     */
    public List<Study2Panel> getByStudy(String studyAccession);
}


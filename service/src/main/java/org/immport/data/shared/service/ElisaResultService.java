package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.ElisaResult;

/**
 * Service interface for the ElisaMbaaResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface ElisaResultService {
 
    /**
     * Returns a List of ElisaMbaaResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ElisaMbaaResult>
     */
    public List<ElisaResult> getByStudy(String studyAccession);
}
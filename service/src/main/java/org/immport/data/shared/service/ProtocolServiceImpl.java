package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Protocol;
import org.immport.data.shared.repository.ProtocolRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the Protocol object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class ProtocolServiceImpl implements ProtocolService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ProtocolServiceImpl.class);

    @Autowired
    private ProtocolRepository protocolRepository;
    
    
    /**
     * Returns an Protocol object by querying on the primary key.
     *
     * @param id primary key of the Protocol object
     * @return the Protocol object
     */
    public Protocol findById(String protocolAccession) {
        Protocol protocol = protocolRepository.findById(protocolAccession); 
        return protocol;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = protocolRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of Protocol objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Protocol>
     */
    public List<Protocol> getByStudy(String studyAccession) {
        List <Protocol> results = protocolRepository.getByStudy(studyAccession);
        return results;
    }
}

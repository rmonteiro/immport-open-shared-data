package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.StudyPubmed;
import org.immport.data.shared.repository.StudyPubmedRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the StudyPubmed object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class StudyPubmedServiceImpl implements StudyPubmedService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyPubmedServiceImpl.class);

    @Autowired
    private StudyPubmedRepository studyPubmedRepository;
    

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = studyPubmedRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of StudyPubmed objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <StudyPubmed>
     */
    public List<StudyPubmed> getByStudy(String studyAccession) {
        List <StudyPubmed> results = studyPubmedRepository.getByStudy(studyAccession);
        return results;
    }
}

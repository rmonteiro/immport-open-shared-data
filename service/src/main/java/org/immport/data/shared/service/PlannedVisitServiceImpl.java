package org.immport.data.shared.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.PlannedVisit;
import org.immport.data.shared.model.PlannedVisitBin;
import org.immport.data.shared.model.PlannedVisitData;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.BiosampleStudyTime;
import org.immport.data.shared.repository.PlannedVisitRepository;
import org.immport.data.shared.service.GenericPivotServiceImpl;
import org.immport.data.shared.service.BiosampleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the PlannedVisit object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class PlannedVisitServiceImpl extends GenericPivotServiceImpl implements PlannedVisitService {
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(PlannedVisitServiceImpl.class);

    @Autowired
    private PlannedVisitRepository plannedVisitRepository;
    
    @Autowired
    private BiosampleService biosampleService;
    
    @Autowired
    private AssessmentService assessmentService;

    private int SUCCESS = 0;
    private int FAIL = 1;
    private int DATA_NOT_AVAILABLE = 2;

    /**
     * Returns an PlannedVisit object by querying on the primary key.
     *
     * @param id primary key of the PlannedVisit object
     * @return the PlannedVisit object
     */
    public PlannedVisit findById(String plannedVisitAccession) {
        PlannedVisit plannedVisit = plannedVisitRepository.findById(plannedVisitAccession); 
        return plannedVisit;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
        PageableData page = plannedVisitRepository.getPageableData(start, limit, sort, dir);
        return page;
    }
    
    /**
     * Returns a List of PlannedVisit objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <PlannedVisit>
     */
    public List<PlannedVisit> getByStudy(String studyAccession) {
        List <PlannedVisit> results = plannedVisitRepository.getByStudy(studyAccession);
        return results;
    }
    
    /**
     * Returns a List of PlannedVisit objects By Experiment,LabTest or
     * Assessment that are linked to a study.
     *
     * @param studyAccession accession for the study
     * @return void
     */
    public PlannedVisitData getPlannedVisitsByType(String studyAccession, String type) {
        List<PlannedVisit> plannedVisits = plannedVisitRepository.getPlannedVisits(studyAccession);
        PlannedVisitData plannedVisitData = new PlannedVisitData();
        plannedVisitData.setPlannedVisits(plannedVisits);
        int errorCode = 0;
        List<String> objBaseNames = new ArrayList<String>();
        List<PlannedVisitBin> plannedVisitBins = new ArrayList<PlannedVisitBin> (); 
        errorCode = createCalculatedBins( plannedVisits,objBaseNames,
                plannedVisitData,plannedVisitBins,type);

        if (errorCode == 0) {
            List<BiosampleStudyTime> biosampleStudyTimes = new ArrayList();
            if (type.equalsIgnoreCase("EXPERIMENT")) {            
                biosampleStudyTimes = biosampleService.getStudyTimeCollectedByExperiment(studyAccession);
            } else if (type.equalsIgnoreCase("LABTEST")){            
                 biosampleStudyTimes = biosampleService.getStudyTimeCollectedByLabTests(studyAccession);
            } else {
                 biosampleStudyTimes = assessmentService.getStudyTimeCollectedByAssessments(studyAccession);
            }
            String experimentAccession = "";
            String title = "";
            String tempExperimentAccession = "";
            String tempTitle = "";
            double studyTimeCollected = 0.0;
            String plannedVisitAccession="";
            long countBioSamples = 0;
            HashMap objHash = new HashMap();
            List<GenericPivotData> objPivotList = new ArrayList<GenericPivotData>();
            for (int k=0;k<biosampleStudyTimes.size();k++){
                BiosampleStudyTime biosampleStudyTime = biosampleStudyTimes.get(k);
                if (k == 0) {
                    experimentAccession = biosampleStudyTime.getExperimentAccessionOrPanelName();
                    title = biosampleStudyTime.getTitle();
                } else {
                    tempExperimentAccession = biosampleStudyTime.getExperimentAccessionOrPanelName();
                    tempTitle =  biosampleStudyTime.getTitle();
                    if (!experimentAccession.equalsIgnoreCase(tempExperimentAccession)) {
                        List<String> objBaseData = fillBaseData(experimentAccession,title,plannedVisits,objHash,type);
                        addObjecttoList(studyAccession,objPivotList,objBaseData,
                                      objBaseNames,null,null,objHash);
                             
                        objHash = new HashMap();
                        experimentAccession = tempExperimentAccession;
                        title = tempTitle;
                    }
                }
                plannedVisitAccession = biosampleStudyTime.getPlannedVisitAccession();
                countBioSamples = biosampleStudyTime.getCountOfBioSamples();
                     
                for (int i =0; i < plannedVisits.size(); i++) {
                    PlannedVisit plannedVisitBin = plannedVisits.get(i);
                    String plannedAcessionNoFromBin = plannedVisitBin.getPlannedVisitAccession();
                    if (plannedAcessionNoFromBin.equalsIgnoreCase(plannedVisitAccession)) {                             
                        objHash.put(i, countBioSamples);
                             break;
                    }     
                }
            }

            if (biosampleStudyTimes.size() > 0){
                List<String> objBaseData = fillBaseData(experimentAccession,title,plannedVisits,objHash,type);
                addObjecttoList(studyAccession,objPivotList,objBaseData,
                            objBaseNames,null,null,objHash);
            } else {
                plannedVisitData.setErrorCode(DATA_NOT_AVAILABLE);
                if (type.equalsIgnoreCase("EXPERIMENT")) {  
                    plannedVisitData.setErrorMessage("Planned Visit Data is not available for Study Time Collected By Experiment");
                } else if (type.equalsIgnoreCase("LABTEST")){
                    plannedVisitData.setErrorMessage("Planned Visit Data is not available for Study Time Collected By Lab Tests");
                } else {
                    plannedVisitData.setErrorMessage("Planned Visit Data is not available for Study Time Collected By Assessments");

                }
            }
            plannedVisitData.setGenericPivotData(objPivotList);
        }
        return plannedVisitData;
    }
    
    private List<String> fillBaseData(String experimentAccession,String title,
                                    List<PlannedVisit> plannedVisitBins,
                                    Map objHash, String type) {
        List<String> objBaseData = new ArrayList<String>();
        objBaseData.add(experimentAccession);
        if (type.equalsIgnoreCase("EXPERIMENT")) {  
            objBaseData.add(title);
        }
        for(int l =0; l < plannedVisitBins.size(); l++) {
            Long countBioSample = (Long)objHash.get(l);
            if (countBioSample == null) {
                objBaseData.add("0");
            } else {
                objBaseData.add(String.valueOf(countBioSample));
            }
        }
            
        return objBaseData;
    }
    
    public int createCalculatedBins( List<PlannedVisit> plannedVisits,
                                        List<String> objBaseNames,
                                        PlannedVisitData plannedVisitData,
                                         List<PlannedVisitBin> plannedVisitBins,
                                         String type) {
        int errorCode = 0;
        String errorMessage = "";
        if (type.equalsIgnoreCase("EXPERIMENT")) {
            objBaseNames.add("Exp Accession");
            objBaseNames.add("Exp Title");            
        }
        else if (type.equalsIgnoreCase("LABTEST"))    {
            objBaseNames.add("Lab Test Panel Name");            
        } else {
            objBaseNames.add("Assessment Panel Name");
        }
           
        for(int i =0;i<plannedVisits.size();i++){
            PlannedVisit plannedVisit = (PlannedVisit)plannedVisits.get(i);   
            objBaseNames.add(plannedVisit.getPlannedVisitAccession());
        }
        return errorCode;
    }
    
    protected List<ArmCohortSortOrder> getDistinctArmNameForStudy(String studyAccession) {
        return null;
    }
}

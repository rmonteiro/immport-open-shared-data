package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Experiment;
import org.immport.data.shared.model.MechanisticAssay;

/**
 * Service interface for the Experiment object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface ExperimentService {
    /**
     * Returns an Experiment object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the Experiment object
     */
    public Experiment findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of Experiment objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Experiment>
     */
    public List<Experiment> getByStudy(String studyAccession);
    
    
    /**
     * Returns a List of Assays objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Assays>
     */
    List<MechanisticAssay> getMechanisticAssays(String studyAccession);
    
    /**
     * Returns an Experiment object by querying on the primary key.
     *
     * @param id primary key of the Experiment object
     * @return the Experiment object
     */
    public Experiment getExperimentDetail(String id);
}

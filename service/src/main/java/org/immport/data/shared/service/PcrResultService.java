package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.PcrResult;

/**
 * Service interface for the PcrResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface PcrResultService {

    /**
     * Returns a List of PcrResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <PcrResult>
     */
    public List<PcrResult> getByStudy(String studyAccession);
}
package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.ElispotResult;

/**
 * Service interface for the ElispotResult object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface ElispotResultService {

    /**
     * Returns a List of ElispotResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ElispotResult>
     */
    public List<ElispotResult> getByStudy(String studyAccession);
}
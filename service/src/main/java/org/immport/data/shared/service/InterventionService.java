package org.immport.data.shared.service;

import java.util.List;




import org.immport.data.shared.model.Intervention;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;

/**
 * Service interface for the SubstanceMerge object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface InterventionService {
    /**
     * Returns an SubstanceMerge object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the SubstanceMerge object
     */
    public Intervention findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of SubstanceMerge objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <SubstanceMerge>
     */
    public List<Intervention> getByStudy(String studyAccession);
    
    /** 
     * Returns a List of Concomitant Medication Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Concomitant Medication Summary data)
     */
    public List<GenericPivotData> getConcomitantMedicationByStudy(String studyAccession);
    
    /** 
     * Returns a List of Substance Use Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Substance Use Summary data)
     */
    public List<GenericPivotData> getSubstanceUseSummaryByStudy(String studyAccession);

    /**
     * Returns a List of Concomitant Medication Detail data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Concomitant Med Detail data)
     */
    public PageableData getConcomitantMedDetailByStudy(Integer start, Integer limit,
            String sort, String dir,String studyAccession);
    
    /**
     * Returns a List of Substance Use Detail data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Substance Use Detail data)
     */
    public PageableData getSubstanceUseDetailByStudy(Integer start, Integer limit,
            String sort, String dir,String studyAccession);
    
    /**
     * Returns a List of Pivoted Intervention Detail data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Intervention Detail data)
     */
    public PageableData getInterventionDetailByStudy(Integer start, Integer limit,
            String sort, String dir,String studyAccession);
    
}


package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.FcsAnalyzedResult;
import org.immport.data.shared.repository.FcsAnalyzedResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the FcsAnalyzedResult object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class FcsAnalyzedResultServiceImpl implements FcsAnalyzedResultService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(FcsAnalyzedResultServiceImpl.class);

    @Autowired
    private FcsAnalyzedResultRepository fcsAnalyzedResultRepository;
    
    
    /**
     * Returns a List of FcsAnalyzedResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <FcsAnalyzedResult>
     */
    public List<FcsAnalyzedResult> getByStudy(String studyAccession) {
        List <FcsAnalyzedResult> results = fcsAnalyzedResultRepository.getByStudy(studyAccession);
        return results;
    }
}

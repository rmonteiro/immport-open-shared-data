package org.immport.data.shared.service;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.List;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.LabTest;
import org.immport.data.shared.model.LabTestPanel;
import org.immport.data.shared.model.LabTestPanelSummary;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.LabTestPanelRepository;
import org.immport.data.shared.service.GenericPivotServiceImpl;
import org.immport.data.shared.service.ArmOrCohortService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the LabTest object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class LabTestServiceImpl extends GenericPivotServiceImpl implements LabTestService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(LabTestServiceImpl.class);

    @Autowired
    private LabTestPanelRepository labTestRepository;
    

    
    
    /**
     * Returns an LabTest object by querying on the primary key.
     *
     * @param id primary key of the LabTest object
     * @return the LabTest object
     */
    public LabTestPanel findById(String labTestAccession) {
        LabTestPanel labTest = labTestRepository.findById(labTestAccession); 
        return labTest;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = labTestRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of LabTest objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <LabTest>
     */
    public List<LabTestPanel> getByStudy(String studyAccession) {
        List <LabTestPanel> results = labTestRepository.getByStudy(studyAccession);
        return results;
    }
    
    /**
     * Returns a List of Lab Test Panel Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Assessment Summary data)
     */
    public List<GenericPivotData> getLabTestPanelSummaryByStudy(String studyAccession)
    {
	    	
    		List<LabTestPanelSummary> results= labTestRepository.getLabTestPanelSummaryByStudy(studyAccession);
	    	 
	    	 
	        List<GenericPivotData> objLabTestPanelPivotList = new ArrayList<GenericPivotData>();
	         if (results.size() > 0) {
		         //String l_stStudyAccNum = "";
		         String stBioSample = "Biological Samples";
		         String stSubjects = "Subjects";
		         String stLabTests = "Lab Tests"; 
		       //  String l_stArmStrBioSample = "";
		       //  String l_stArmSubjects = "";
		       //  String l_stArmLabTests  = "";
		       //  String l_stDataValueBioSample = "";
		       //  String l_stDataValueSubjects = "";
		       //  String l_stDataValueLabTests = "";
		
		         
		        // int iCount = 0;
		         
		        
			
	
		         HashMap objHash = new HashMap();
			     List<String> objTempArmNames = new ArrayList<String>();
			     getDistinctArmNames(studyAccession,objHash,objTempArmNames);	
		
		         //pivoting the data
		        // List<String> objBioSampleArmData = new ArrayList<String>();
		       //  List<String> objSubjectArmData = new ArrayList<String>();
		      //   List<String> objLabTestsArmData = new ArrayList<String>();
		         HashMap<String, String> objHashBioSampleData = new HashMap();
		         HashMap<String, String> objHashSubjectData = new HashMap();
		         HashMap<String, String> objHashLabTestsData = new HashMap();
		         
		         String panelNameReported = "";
		         String tempPanelNameReported = "";
		         long bioSampleCount = 0;
		         long subjectCount = 0;
		         long labTestsCount = 0;
		         
		         List<String> objBaseData = new ArrayList<String>();
				 List<String> objBaseNames = new ArrayList<String>();
				 List<String> objBaseNamesSort = new ArrayList<String>();
				 List<String> baseNamesSortParameter = new ArrayList<String>();
				        
				 populateBaseColumnInformation("LabTestSumary", objBaseNames,
								objBaseNamesSort,baseNamesSortParameter);
					
				
				 
				 
		        
				 NumberFormat numberFormatter = NumberFormat.getNumberInstance(Locale.US);
		         
		         for(int i=0;i < results.size();i++) {
		        	 LabTestPanelSummary objLabTestPanelSummary = (LabTestPanelSummary)results.get(i);
		        	 
		        	 if (i == 0){
		        		 panelNameReported = objLabTestPanelSummary.getPanelNameReported();
		        	 }
		        	 else {
		        		 tempPanelNameReported =  objLabTestPanelSummary.getPanelNameReported();
		        		 if (!panelNameReported.equalsIgnoreCase(tempPanelNameReported)) {
		        			 

		        			
		        			 objBaseData = new ArrayList<String>(); 	                       		
	                         objBaseData.add(panelNameReported);
	                         objBaseData.add(stLabTests);
	                      
		        			 
	                    	 addObjecttoList(
	                        			studyAccession,
	                        			objLabTestPanelPivotList,
	      								objBaseData,
	      								objBaseNames,
	      								objBaseNamesSort,
	      								baseNamesSortParameter,
	      								objHashLabTestsData
	      								);
		        			 
		        			 
		        			 
		        			 objBaseData = new ArrayList<String>(); 	                       		
	                         objBaseData.add(panelNameReported);
	                         objBaseData.add(stBioSample);
	                      
	                    	 
	                    	 addObjecttoList(
	                        			studyAccession,
	                        			objLabTestPanelPivotList,
	      								objBaseData,
	      								objBaseNames,
	      								objBaseNamesSort,
	      								baseNamesSortParameter,
	      								objHashBioSampleData
	      								);
		        			 
		        			
		        	        
		        			 objBaseData = new ArrayList<String>(); 	                       		
	                         objBaseData.add(panelNameReported);
	                         objBaseData.add(stSubjects);
	                      
	                    	 addObjecttoList(
	                        			studyAccession,
	                        			objLabTestPanelPivotList,
	      								objBaseData,
	      								objBaseNames,
	      								objBaseNamesSort,
	      								baseNamesSortParameter,
	      								objHashSubjectData
	      								);
		        			 
		        			 panelNameReported = tempPanelNameReported;
		        			 objHashBioSampleData = new HashMap();
		        			 objHashSubjectData = new HashMap();
		        			 objHashLabTestsData = new HashMap();
		        	         
		        			 
		        		 }
		        		 
		        	 }
		        	
		        	 
		        	 String stArm =          objLabTestPanelSummary.getArmAccession();
		             //String  armColName = (String)objHash.get(stArm);
		             
		             bioSampleCount = 	 objLabTestPanelSummary.getBioSampleNo();
		             String stBioSampleCount = numberFormatter.format(bioSampleCount);
		             
		             subjectCount = objLabTestPanelSummary.getSubjAccNo();	
		             String stSubjectCount = numberFormatter.format(subjectCount);
		             
		             labTestsCount = objLabTestPanelSummary.getLabTestPanelNo();
		             String stLabTestsCount = numberFormatter.format(labTestsCount);
		             
		             objHashBioSampleData.put(stArm, stBioSampleCount);
		             objHashSubjectData.put(stArm, stSubjectCount);
		             objHashLabTestsData.put(stArm, stLabTestsCount);
		             
		             if (i == (results.size() -1)) {
		            
		            	 
		            	 objBaseData = new ArrayList<String>(); 	                       		
                         objBaseData.add(panelNameReported);
                         objBaseData.add(stLabTests);
                     
                    	 addObjecttoList(
                     			studyAccession,
                     			objLabTestPanelPivotList,
   								objBaseData,
   								objBaseNames,
   								objBaseNamesSort,
   								baseNamesSortParameter,
   								objHashLabTestsData
   								);
                    	 
                    	 objBaseData = new ArrayList<String>(); 	                       		
                         objBaseData.add(panelNameReported);
                         objBaseData.add(stBioSample);
                     
	        			
                    	 addObjecttoList(
                     			studyAccession,
                     			objLabTestPanelPivotList,
   								objBaseData,
   								objBaseNames,
   								objBaseNamesSort,
   								baseNamesSortParameter,
   								objHashBioSampleData
   								);
                    	 
                    	 objBaseData = new ArrayList<String>(); 	                       		
                         objBaseData.add(panelNameReported);
                         objBaseData.add(stSubjects);
                    	 
                    	 addObjecttoList(
                     			studyAccession,
                     			objLabTestPanelPivotList,
   								objBaseData,
   								objBaseNames,
   								objBaseNamesSort,
   								baseNamesSortParameter,
   								objHashSubjectData
   								);
	        			
	        			
	        			 
		             }
		         }   
		         
		         populateListWithPlaceHolder(objLabTestPanelPivotList,objTempArmNames,"0");
		         
		       
		         
		         
	         }
	         
	        return objLabTestPanelPivotList;
    }
    
    
    /**
     * Returns a List of Lab Test Component that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Lab Test Component)
     */
    public PageableData getLabTestComponentListByStudy(Integer start, Integer limit,    		
            String sort, String dir,String studyAccession)
    {
    	PageableData pageRet = labTestRepository.getLabTestComponentListByStudy(start,limit,sort,dir,studyAccession);
   	 	return pageRet;
    }
    
    
    /**
     * Returns a List of Arms that are linked to the Adverse Event for that
     * study.
     *
     * @param studyAccession accession for the study
     * @return List (Arms)
     */
    protected List<ArmCohortSortOrder> getDistinctArmNameForStudy(String studyAccession)
    {
    	   List<ArmCohortSortOrder> objArm = (List<ArmCohortSortOrder>)labTestRepository.getDistinctArmNameForLabTestPanel(studyAccession);
    	   return objArm;
    }
    
}

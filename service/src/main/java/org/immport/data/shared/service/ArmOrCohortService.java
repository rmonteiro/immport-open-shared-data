package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.ArmCohortSortOrder;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.ArmOrCohort;

/**
 * Service interface for the ArmOrCohort object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface ArmOrCohortService {
    /**
     * Returns an ArmOrCohort object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the ArmOrCohort object
     */
    public ArmOrCohort findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of ArmOrCohort objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmOrCohort>
     */
    public List<ArmOrCohort> getByStudy(String studyAccession);
    
    /**
     * Returns a List of Armaccession by sort order that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmOrCohort>
     */
    public List<String> getArmAccessionByStudy(String studyAccession);  
    
    /**
     * Returns a List of ArmCohortSortOrder by sort order that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ArmOrCohort>
     */    
    public List<ArmCohortSortOrder> getArmOrderAndDescByStudy (String studyAccession);
    
    
}
package org.immport.data.shared.service;



import java.util.List;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Study;
import org.immport.data.shared.model.StudyImage;
import org.immport.data.shared.model.StudySummary;
import org.immport.data.shared.repository.StudyPersonnelRepository;
import org.immport.data.shared.repository.StudyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service class for the Study object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class StudyServiceImpl implements StudyService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(StudyServiceImpl.class);

    @Autowired
    private StudyRepository studyRepository;
    
    @Autowired
    private StudyPersonnelRepository studyPersonnelRepository;
    
    
    /**
     * Returns an Study object by querying on the primary key.
     *
     * @param id primary key of the Study object
     * @return the Study object
     */
    
    public Study findById(String studyAccession) {
        Study study = studyRepository.findById(studyAccession);
        return study;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = studyRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    public StudyImage getStudyImage(String studyAccession) {
    	return studyRepository.getStudyImage(studyAccession);
    }
    
    
    /**
     * Returns an Study Data for the Study Tab.
     *
     * @param studyAccession primary key of the object
     * @return the Study object
     */
    public Study getStudyDataForSummaryTab(String studyAccession)
    {
    	Study study = studyRepository.findById(studyAccession);
    	//getting the lazy data
        study.getArmOrCohorts().size();    
        study.getStudyPersonnels().size();
        return study;
    }
    
    /**
     * Returns an Contact Grant for the Study.
     *
     * @param studyAccession primary key of the object
     * @return the String
     */
    public String getContactGrant(String studyAccession)
    {
    	return studyRepository.getContactGrant(studyAccession);
    }
    
    /**
     * Returns an Study object by querying on the primary key.
     *
     * @param id primary key of the Study object
     * @return the Study object
     */
    public StudySummary getStudySummary(String id) {
        return studyRepository.getStudySummary(id);
    }
    
    /**
     * Returns a List of StudySummary objects
     *
     * @return List <StudySummary> objects
     */
    public List getStudySummaryAll() {
       return studyRepository.getStudySummaryAll();
    }
    
    /**
     * Returns a List of StudySummary objects
     *
     * @return List <StudySummary> objects
     */
    public List getStudySummaryAllDetail() {
    	List <StudySummary> studies = studyRepository.getStudySummaryAll();
    	for (StudySummary study: studies) {
    		study.setResearchFocus(studyRepository.getResearchFocus(study.getStudyAccession()));
    		study.setSubjectSpecies(studyRepository.getSubjectSpecies(study.getStudyAccession()));
    		study.setBiosampleType(studyRepository.getBiosampleType(study.getStudyAccession()));
    		study.setExperimentMeasurementTechnique(studyRepository.getExperimentMeasurementTechnique(study.getStudyAccession()));
    		study.setPersonnel(studyPersonnelRepository.getByStudy(study.getStudyAccession()));
    	}
       return studies;
    }
    
    /**
     * Returns List of Strings representing ResearchFocus.
     *
     * @return the List of Strings representing ResearchFocus
     */
    public List <String> getResearchFocus(String studyAccession)
    {
    	return studyRepository.getResearchFocus(studyAccession);
    }
    
    /**
     * Returns List of Strings representing Species.
     *
     * @return the List of Strings representing Species
     */
    public List <String> getSubjectSpecies(String studyAccession)
    {
    	return studyRepository.getSubjectSpecies(studyAccession);
    }
    
    /**
     * Returns List of Strings representing BiosampleType.
     *
     * @return the List of Strings representing BiosampleType
     */
    public List <String> getBiosampleType(String studyAccession)
    {
    	return studyRepository.getBiosampleType(studyAccession);
    }
    
    /**
     * Returns List of Strings representing ResearchFocus.
     *
     * @return the List of Strings representing ResearchFocus
     */
    public List <String> getExperimentMeasurementTechnique(String studyAccession)
    {
    	return studyRepository.getExperimentMeasurementTechnique(studyAccession);
    }
}

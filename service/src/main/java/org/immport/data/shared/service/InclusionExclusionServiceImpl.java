package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.InclusionExclusion;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.InclusionExclusionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the InclusionExclusion object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class InclusionExclusionServiceImpl implements InclusionExclusionService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(InclusionExclusionServiceImpl.class);

    @Autowired
    private InclusionExclusionRepository inclusionExclusionRepository;
    
    
    /**
     * Returns an InclusionExclusion object by querying on the primary key.
     *
     * @param id primary key of the InclusionExclusion object
     * @return the InclusionExclusion object
     */
    public InclusionExclusion findById(String inclusionExclusionAccession) {
        InclusionExclusion inclusionExclusion = inclusionExclusionRepository.findById(inclusionExclusionAccession); 
        return inclusionExclusion;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = inclusionExclusionRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of InclusionExclusion objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <InclusionExclusion>
     */
    public List<InclusionExclusion> getByStudy(String studyAccession) {
        List <InclusionExclusion> results = inclusionExclusionRepository.getByStudy(studyAccession);
        return results;
    }
}

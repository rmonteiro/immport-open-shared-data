package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.SubjectMeasureResult;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.SubjectMeasureResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the SubjectMeasureResult object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class SubjectMeasureResultServiceImpl implements SubjectMeasureResultService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SubjectMeasureResultServiceImpl.class);

    @Autowired
    private SubjectMeasureResultRepository subjectMeasureResultRepository;
    
    
    /**
     * Returns an SubjectMeasureResult object by querying on the primary key.
     *
     * @param id primary key of the SubjectMeasureResult object
     * @return the SubjectMeasureResult object
     */
    public SubjectMeasureResult findById(String subjectMeasureResultAccession) {
        SubjectMeasureResult subjectMeasureResult = subjectMeasureResultRepository.findById(subjectMeasureResultAccession); 
        return subjectMeasureResult;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = subjectMeasureResultRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of SubjectMeasureResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <SubjectMeasureResult>
     */
    public List<SubjectMeasureResult> getByStudy(String studyAccession) {
        List <SubjectMeasureResult> results = subjectMeasureResultRepository.getByStudy(studyAccession);
        return results;
    }
}

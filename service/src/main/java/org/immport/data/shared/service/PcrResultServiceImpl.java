package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.PcrResult;
import org.immport.data.shared.repository.PcrResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the PcrResult object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class PcrResultServiceImpl implements PcrResultService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(PcrResultServiceImpl.class);

    @Autowired
    private PcrResultRepository pcrResultRepository;
    
    
    /**
     * Returns a List of PcrResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <PcrResult>
     */
    public List<PcrResult> getByStudy(String studyAccession) {
        List <PcrResult> results = pcrResultRepository.getByStudy(studyAccession);
        return results;
    }
}

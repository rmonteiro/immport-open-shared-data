package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.AdverseEvent;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;

/**
 * Service interface for the AdverseEvent object. Contains methods
 * for retrieving information based on different query parameters
 */

public interface AdverseEventService {
    /**
     * Returns an AdverseEvent object by querying on the primary key.
     *
     * @param id primary key of the object
     * @return the AdverseEvent object
     */
    public AdverseEvent findById(String id);

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
        String sort, String dir);

    /**
     * Returns a List of AdverseEvent objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <AdverseEvent>
     */
    public List<AdverseEvent> getByStudy(String studyAccession);
    
    
    /** 
     * Returns a List of AdverseEvent Summary data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (AdverseEvent Summary data)
     */
    public List<GenericPivotData> getAdverseEventSummaryByStudy(String studyAccession);
    
    /**
     * Returns a List of Pivoted Adverse Event Detail data that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List (Adverse Event Detail data)
     */
    public PageableData getAdverseEventDetailByStudy(Integer start, Integer limit,
            String sort, String dir,String studyAccession);

}

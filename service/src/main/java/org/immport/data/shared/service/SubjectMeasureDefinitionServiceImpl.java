package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.SubjectMeasureDefinition;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.SubjectMeasureDefinitionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the SubjectMeasureDefinition object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class SubjectMeasureDefinitionServiceImpl implements SubjectMeasureDefinitionService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SubjectMeasureDefinitionServiceImpl.class);

    @Autowired
    private SubjectMeasureDefinitionRepository subjectMeasureDefinitionRepository;
    
    
    /**
     * Returns an SubjectMeasureDefinition object by querying on the primary key.
     *
     * @param id primary key of the SubjectMeasureDefinition object
     * @return the SubjectMeasureDefinition object
     */
    public SubjectMeasureDefinition findById(String subjectMeasureDefinitionAccession) {
        SubjectMeasureDefinition subjectMeasureDefinition = subjectMeasureDefinitionRepository.findById(subjectMeasureDefinitionAccession); 
        return subjectMeasureDefinition;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = subjectMeasureDefinitionRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of SubjectMeasureDefinition objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <SubjectMeasureDefinition>
     */
    public List<SubjectMeasureDefinition> getByStudy(String studyAccession) {
        List <SubjectMeasureDefinition> results = subjectMeasureDefinitionRepository.getByStudy(studyAccession);
        return results;
    }
}

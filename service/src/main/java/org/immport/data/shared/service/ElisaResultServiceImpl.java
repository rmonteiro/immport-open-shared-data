package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.ElisaResult;
import org.immport.data.shared.repository.ElisaResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the ElisaMbaaResult object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class ElisaResultServiceImpl implements ElisaResultService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ElisaResultServiceImpl.class);

    @Autowired
    private ElisaResultRepository elisaResultRepository;
    
    
    /**
     * Returns a List of ElisaMbaaResult objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ElisaMbaaResult>
     */
    public List<ElisaResult> getByStudy(String studyAccession) {
        List <ElisaResult> results = elisaResultRepository.getByStudy(studyAccession);
        return results;
    }
}

package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Expsample;
import org.immport.data.shared.repository.ExpsampleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the Expsample object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class ExpsampleServiceImpl implements ExpsampleService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ExpsampleServiceImpl.class);

    @Autowired
    private ExpsampleRepository expsampleRepository;
    
    
    /**
     * Returns an Expsample object by querying on the primary key.
     *
     * @param id primary key of the Expsample object
     * @return the Expsample object
     */
    public Expsample findById(String expsampleAccession) {
        Expsample expsample = expsampleRepository.findById(expsampleAccession); 
        return expsample;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = expsampleRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of Expsample objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <Expsample>
     */
    public List<Expsample> getByStudy(String studyAccession) {
        List <Expsample> results = expsampleRepository.getByStudy(studyAccession);
        return results;
    }
    
    
    /**
     * Returns a count of Expsample that are linked to
     * a experimentAccession.
     *
     * @param experimentAccession
     * @return long
     */
    public long getCountByExperiment(String experimentAccession)
    {
    	  long count = expsampleRepository.getCountByExperiment(experimentAccession);
    	  return count;
    }
}

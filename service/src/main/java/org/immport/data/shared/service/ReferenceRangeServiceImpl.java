package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.ReferenceRange;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.repository.ReferenceRangeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the ReferenceRange object. Contains methods
 * for retrieving information based on different query parameters
 * by making repository method calls.
 */
@Service
public class ReferenceRangeServiceImpl implements ReferenceRangeService {
    
    
    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ReferenceRangeServiceImpl.class);

    @Autowired
    private ReferenceRangeRepository referenceRangeRepository;
    
    
    /**
     * Returns an ReferenceRange object by querying on the primary key.
     *
     * @param id primary key of the ReferenceRange object
     * @return the ReferenceRange object
     */
    public ReferenceRange findById(String referenceRangeAccession) {
        ReferenceRange referenceRange = referenceRangeRepository.findById(referenceRangeAccession); 
        return referenceRange;
    }

    /**
     * Returns a PageableData object containing the count of total rows
     * returned by the query, and a list of objects returned by the query.
     *
     * @param start starting row in the query results
     * @param limit number of rows to return
     * @param sort  column to sort on
     * @param dir   direction of the sort ("asc","desc")
     * @return PageableData
     */
    public PageableData getPageableData(Integer start, Integer limit,
            String sort, String dir) {
            PageableData page = referenceRangeRepository.getPageableData(start, limit, sort, dir);
            return page;
    }
    
    /**
     * Returns a List of ReferenceRange objects that are linked to
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <ReferenceRange>
     */
    public List<ReferenceRange> getByStudy(String studyAccession) {
        List <ReferenceRange> results = referenceRangeRepository.getByStudy(studyAccession);
        return results;
    }
}

package org.immport.data.shared.service;

import java.util.List;

import org.immport.data.shared.model.GenericPivotData;

import org.immport.data.shared.model.TreatmentToExpSampleInfo;

public interface TreatmentService {
	
	
	/**
     * Returns a List of TreatmentToExpSample Info are linked to this
     * a study.
     *
     * @param studyAccession accession for the study
     * @return List <TreatmentToExpSample>
     */
    public List<TreatmentToExpSampleInfo> getTreatmentToExpSample(String studyAccession);


}

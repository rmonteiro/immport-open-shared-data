package org.immport.data.shared.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Biosample;
import org.immport.data.shared.service.BiosampleService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the Biosample Service.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-services.xml"
})
public class BiosampleServiceTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(BiosampleServiceTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The Biosample service. */
    @Autowired
    BiosampleService biosampleService;


    /**
     * Set up the tests.
     * 
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the Biosample object by id.
     * 
     */
    @Test
    public void findById() {
        String id = resources.getString("biosample");
        log.info("id = " + id);
        Biosample biosample = biosampleService.findById(id);
        assertNotNull("The Biosample object is null.", biosample);
    }

    /**
     * Test getting a PageableData of Biosample objects
     *
     */
    @Test
    public void getPageableData() {
        PageableData page = biosampleService.getPageableData(0,10,"b.biosampleAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The Biosample count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of Biosample objects by study accession
     *
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <Biosample> results = biosampleService.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The Biosample count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }

    /**
     * Tear down.
     *
     */
    @After
    public void tearDown() {
        // empty
    }
}

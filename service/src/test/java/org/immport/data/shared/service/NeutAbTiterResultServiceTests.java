package org.immport.data.shared.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.NeutAbTiterResult;
import org.immport.data.shared.service.NeutAbTiterResultService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the NeutAbTiterResult Service.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-services.xml"
})
public class NeutAbTiterResultServiceTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(NeutAbTiterResultServiceTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The NeutAbTiterResult service. */
    @Autowired
    NeutAbTiterResultService neutAbTiterResultService;


    /**
     * Set up the tests.
     * 
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }


    /**
     * Test getting a List of NeutAbTiterResult objects by study accession
     *
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("neutAbTiterStudyAccession");
        List <NeutAbTiterResult> results = neutAbTiterResultService.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The NeutAbTiterResult count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }

    /**
     * Tear down.
     *
     */
    @After
    public void tearDown() {
        // empty
    }
}

package org.immport.data.shared.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.LabTest;
import org.immport.data.shared.model.LabTestPanel;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.service.LabTestService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the LabTest Service.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-services.xml"
})
public class LabTestServiceTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(LabTestServiceTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The LabTest service. */
    @Autowired
    LabTestService labTestService;


    /**
     * Set up the tests.
     * 
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the LabTest object by id.
     * 
     */
    @Test
    public void findById() {
        String id = resources.getString("labTestPanel");
        log.info("id = " + id);
        LabTestPanel labTest = labTestService.findById(id);
        assertNotNull("The LabTest object is null.", labTest);
    }

    /**
     * Test getting a PageableData of LabTest objects
     *
     */
    @Test
    public void getPageableData() {
        PageableData page = labTestService.getPageableData(0,10,"l.labTestPanelAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The LabTest count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of LabTest objects by study accession
     *
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <LabTestPanel> results = labTestService.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The LabTest count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }

    
    @Test    
    public void getLabTestPanelSummaryByStudy()
    {
    	 String id = resources.getString("studyAccession");
    	 List <GenericPivotData> results = labTestService.getLabTestPanelSummaryByStudy(id);
    	 
    	
    	 
        
         
         if (results != null) {
	         Iterator<GenericPivotData> it = results.iterator();
	         int count = 1;
	         while (it.hasNext()) {
	        	 GenericPivotData objCon = (GenericPivotData)it.next();
	        	 
	        	 String armData = "";   
	        	 String armName = "";
	        	 List<String> objArmList = objCon.getArmData(); 
	        	 List<String> objArmNames = objCon.getArmNames();    
	        	 
	        	 String baseValues = "";
	        	 List<String> objBaseData= objCon.getBaseData();
	        	 
	        	 for (int j=0;j < objBaseData.size();j++)
	        	 {
	        		 baseValues = baseValues + " , " + objBaseData.get(j);
	        	 }
	        	 
	        	 
	        	 for(int i =0; i < objArmList.size();i++){
	        		 armData = armData + ", " + objArmList.get(i);
	        		 armName = armName + ", " + objArmNames.get(i);
	        	 }
	        	 
	        	 log.info("Row " + count + " :" + baseValues
	        			 						 + ", " + armName +  ", " + armData);
	        	 count++;
	        			 
	         }
         }
    	 
    	
    }
    
    /**
     * Tear down.
     *
     */
    @After
    public void tearDown() {
        // empty
    }
}

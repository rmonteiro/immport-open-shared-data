package org.immport.data.shared.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.PlannedVisit;
import org.immport.data.shared.model.PlannedVisitBin;
import org.immport.data.shared.model.PlannedVisitData;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.service.PlannedVisitService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the PlannedVisit Service.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-services.xml"
})
public class PlannedVisitServiceTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(PlannedVisitServiceTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The PlannedVisit service. */
    @Autowired
    PlannedVisitService plannedVisitService;


    /**
     * Set up the tests.
     * 
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the PlannedVisit object by id.
     * 
     */
    @Test
    public void findById() {
        String id = resources.getString("plannedVisit");
        log.info("id = " + id);
        PlannedVisit plannedVisit = plannedVisitService.findById(id);
        assertNotNull("The PlannedVisit object is null.", plannedVisit);
    }

    /**
     * Test getting a PageableData of PlannedVisit objects
     *
     */
    @Test
    public void getPageableData() {
        PageableData page = plannedVisitService.getPageableData(0,10,"p.plannedVisitAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The PlannedVisit count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of PlannedVisit objects by study accession
     *
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <PlannedVisit> results = plannedVisitService.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The PlannedVisit count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }
    
    /**
     * Test getting a List of PlannedVisit objects by Experiment by study accession
     *
     */
    @Test
    public void getPlannedVisitsByExperiment() {
        PlannedVisitData plannedVisitData = plannedVisitService.getPlannedVisitsByType("SDY6", "EXPERIMENT");
        assertNotNull("The results is null.", plannedVisitData);
        
        if (plannedVisitData.getErrorCode() == 0) {
            List<PlannedVisitBin> plannedVisitBins = plannedVisitData.getPlannedVisitBins();
            for(int k=0;k < plannedVisitBins.size();k++) {
                PlannedVisitBin plannedVisitBin = (PlannedVisitBin)plannedVisitBins.get(k);
                log.info("Min:" + plannedVisitBin.getMinStartDay() + " Max:" + plannedVisitBin.getMaxStartDay());
            }

            List <GenericPivotData> results = ( List <GenericPivotData>)plannedVisitData.getGenericPivotData();
             
            if (results != null) {
                 Iterator<GenericPivotData> it = results.iterator();
                 int count = 1;
                 while (it.hasNext()) {
                     GenericPivotData objCon = (GenericPivotData)it.next();
                     String baseValues = "";
                     List<String> objBaseData= objCon.getBaseData();
                     
                     for (int j=0;j < objBaseData.size();j++) {
                         baseValues = baseValues + " , " + objBaseData.get(j);
                     }
                     log.info("Row " + count + " :" + baseValues);
                     count++;
                 }
             }
        } else {
            String errorMessage = plannedVisitData.getErrorMessage();
            log.info("Message:" + errorMessage);
        }
    }
    
    /**
     * Test getting a List of PlannedVisit objects by LabTest by study accession
     *
     */
    @Test
    public void getPlannedVisitsByLabTests() {
        PlannedVisitData plannedVisitData = plannedVisitService.getPlannedVisitsByType("SDY10", "LABTEST");
        assertNotNull("The results is null.", plannedVisitData);
        if (plannedVisitData.getErrorCode() == 0) {
            List<PlannedVisitBin> plannedVisitBins = plannedVisitData.getPlannedVisitBins();
            
            for(int k=0;k < plannedVisitBins.size();k++) {
                PlannedVisitBin plannedVisitBin= (PlannedVisitBin)plannedVisitBins.get(k);
                log.info("Min:" + plannedVisitBin.getMinStartDay() + " Max:" + plannedVisitBin.getMaxStartDay());
            }

            List <GenericPivotData> results = ( List <GenericPivotData>)plannedVisitData.getGenericPivotData();
             
            if (results != null) {
                Iterator<GenericPivotData> it = results.iterator();
                int count = 1;
                while (it.hasNext()) {
                    GenericPivotData objCon = (GenericPivotData)it.next();
                    String baseValues = "";
                    List<String> objBaseData= objCon.getBaseData();
                     
                    for (int j=0;j < objBaseData.size();j++) {
                         baseValues = baseValues + " , " + objBaseData.get(j);
                    }

                    log.info("Row " + count + " :" + baseValues);
                    count++;
                }
            }
        } else {
            String errorMessage = plannedVisitData.getErrorMessage();
            log.info("Message:" + errorMessage);
        }
    }
    
    /**
     * Test getting a List of PlannedVisit objects by Assessment by study accession
     *
     */
    @Test
    public void getPlannedVisitsByAssessments() {
        PlannedVisitData plannedVisitData = plannedVisitService.getPlannedVisitsByType("SDY10", "ASSESSMENT");
        assertNotNull("The results is null.", plannedVisitData);
        if (plannedVisitData.getErrorCode() == 0) {
            List<PlannedVisitBin> plannedVisitBins = plannedVisitData.getPlannedVisitBins();
            
            for(int k=0;k < plannedVisitBins.size();k++) {
                PlannedVisitBin plannedVisitBin= (PlannedVisitBin)plannedVisitBins.get(k);
                log.info("Min:" + plannedVisitBin.getMinStartDay() + " Max:" + plannedVisitBin.getMaxStartDay());
            }

            List <GenericPivotData> results = ( List <GenericPivotData>)plannedVisitData.getGenericPivotData();
             
            if (results != null) {
                Iterator<GenericPivotData> it = results.iterator();
                int count = 1;
                while (it.hasNext()) {
                    GenericPivotData objCon = (GenericPivotData)it.next();
                    String baseValues = "";
                    List<String> objBaseData= objCon.getBaseData();
                     
                    for (int j=0;j < objBaseData.size();j++) {
                         baseValues = baseValues + " , " + objBaseData.get(j);
                    }

                    log.info("Row " + count + " :" + baseValues);
                    count++;
                }
            }
        } else {
            String errorMessage = plannedVisitData.getErrorMessage();
            log.info("Message:" + errorMessage);
        }
    }

    /**
     * Tear down.
     *
     */
    @After
    public void tearDown() {
        // empty
    }
}

package org.immport.data.shared.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.model.Experiment;
import org.immport.data.shared.model.MechanisticAssay;
import org.immport.data.shared.service.ExperimentService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the Experiment Service.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-services.xml"
})
public class ExperimentServiceTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(ExperimentServiceTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The Experiment service. */
    @Autowired
    ExperimentService experimentService;


    /**
     * Set up the tests.
     * 
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the Experiment object by id.
     * 
     */
    @Test
    public void findById() {
        String id = resources.getString("experiment");
        log.info("id = " + id);
        Experiment experiment = experimentService.findById(id);
        assertNotNull("The Experiment object is null.", experiment);
    }

    /**
     * Test getting a PageableData of Experiment objects
     *
     */
    @Test
    public void getPageableData() {
        PageableData page = experimentService.getPageableData(0,10,"e.experimentAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The Experiment count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of Experiment objects by study accession
     *
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <Experiment> results = experimentService.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The Experiment count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }
    
    
    @Test
    public void getMechanisticAssays() {
    	String id = resources.getString("studyAccession");
        List <MechanisticAssay> results = experimentService.getMechanisticAssays("SDY2");
        
        for(int i=0;i<results.size();i++)
        {
        	MechanisticAssay obj = (MechanisticAssay)results.get(i);
        	log.info("Row " + i + "-" + obj.getExperimentAccession() + " , " + obj.getTitle() 
        			 + " , " + obj.getTitle() + " , " + obj.getPurpose() + " , " +
        			obj.getMeasurementTechnique() + " , " + obj.getCountExpSamples()); 
        }
    }

    /**
     * Tear down.
     *
     */
    @After
    public void tearDown() {
        // empty
    }
}

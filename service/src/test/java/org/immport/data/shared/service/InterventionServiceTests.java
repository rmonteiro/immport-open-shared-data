package org.immport.data.shared.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;








import org.immport.data.shared.model.Intervention;
import org.immport.data.shared.model.GenericPivotData;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.service.InterventionService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the SubstanceMerge Service.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-services.xml"
        
})
public class InterventionServiceTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(InterventionServiceTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The SubstanceMerge service. */
    @Autowired
    InterventionService interventionService;


    /**
     * Set up the tests.
     * 
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the SubstanceMerge object by id.
     * 
     */
    @Test
    public void findById() {
        String id = resources.getString("substanceMerge");
        log.info("id = " + id);
        Intervention intervention = interventionService.findById(id);
        assertNotNull("The Intervention object is null.", intervention);
    }

    /**
     * Test getting a PageableData of SubstanceMerge objects
     *
     */
    @Test
    public void getPageableData() {
        PageableData page = interventionService.getPageableData(0,10,"s.interventionAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The SubstanceMerge count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of SubstanceMerge objects by study accession
     *
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <Intervention> results = interventionService.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The SubstanceMerge count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }
    
    
    /**
     * Test getting a List of Concomitant Medication Summary by study accession
     *
     */
    @Test
    public void getConcomitantMedicationByStudy()
    {
    	 String id = resources.getString("studyAccession");
         List <GenericPivotData> results = interventionService.getConcomitantMedicationByStudy("SDY14");
         assertNotNull("The results is null.", results);
         assertTrue("The ConcomitantMedication count is greater than zero.", results.size() > 0);
         log.info("Count: " + results.size());
         
         Iterator<GenericPivotData> it = results.iterator();
         int count = 1;
         while (it.hasNext()) {
        	 GenericPivotData objCon = (GenericPivotData)it.next();        	 
        	 String armData = "";
        	 
        	 String baseValues = "";
    		 List<String> objBaseData= objCon.getBaseData();
        	 
        	 for (int j=0;j < objBaseData.size();j++)
        	 {
        		 baseValues = baseValues + " , " + objBaseData.get(j);
        	 }
        	 
        	 List<String> objArmList = objCon.getArmData();        	 
        	 for(int i =0; i < objArmList.size();i++){
        		
        		 
        		 armData = armData + ", " + objArmList.get(i);
        	 }
        	 
        	 log.info("Row " + count + " :" + baseValues + ", " + armData);
        	 count++;
         }
         
         
    }
    
    
    /**
     * Test getting a List of Concomitant Medication Summary by study accession
     *
     */
    @Test
    public void getSubstanceUseSummaryByStudy()
    {
    	 String id = resources.getString("studyAccession");
         List <GenericPivotData> results = interventionService.getSubstanceUseSummaryByStudy("SDY14");
         assertNotNull("The results is null.", results);
         assertTrue("The getSubstanceUseSummaryByStudy count is greater than zero.", results.size() > 0);
         log.info("Count: " + results.size());
         
         Iterator<GenericPivotData> it = results.iterator();
         int count = 1;
         while (it.hasNext()) {
        	 GenericPivotData objCon = (GenericPivotData)it.next();        	 
        	 String armData = "";
        	 
        	 String baseValues = "";
    		 List<String> objBaseData= objCon.getBaseData();
        	 
        	 for (int j=0;j < objBaseData.size();j++)
        	 {
        		 baseValues = baseValues + " , " + objBaseData.get(j);
        	 }
        	 
        	 List<String> objArmList = objCon.getArmData();        	 
        	 for(int i =0; i < objArmList.size();i++){
        		
        		 
        		 armData = armData + ", " + objArmList.get(i);
        	 }
        	 
        	 log.info("Row " + count + " :" + baseValues + ", " + armData);
        	 count++;
         }
         
         
    }
    
    
    /**
     * Test getting a List of Concomitant Medication Detail pivoted data by study accession
     *
     */
    @Test    
    public void getConcomitantMedDetailByStudy()
    {
    	 String id = resources.getString("studyAccession");
    	 PageableData page = interventionService.getConcomitantMedDetailByStudy(0, 10,
		            "compoundNameReported", "", id);
    	 
    	 List <GenericPivotData> results = ( List <GenericPivotData>)page.getData();
    	 
         log.info("Total count:" + page.getTotalCount());
         
         if (results != null) {
	         Iterator<GenericPivotData> it = results.iterator();
	         int count = 1;
	         while (it.hasNext()) {
	        	 GenericPivotData objCon = (GenericPivotData)it.next();        	 
	        	 String armData = "";  
	        	 String baseValues = "";
	        	 List<String> objArmList = objCon.getArmData();
	        	 List<String> objBaseData= objCon.getBaseData();
	        	 
	        	 for (int j=0;j < objBaseData.size();j++)
	        	 {
	        		 baseValues = baseValues + " , " + objBaseData.get(j);
	        	 }
	        	 
	        	 for(int i =0; i < objArmList.size();i++){
	        		 armData = armData + ", " + objArmList.get(i);
	        	 }
	        	 
	        	 log.info("Row " + count + " :" + baseValues + ", " + armData);
	        	 count++;
	        			 
	         }
         }
    	 
    	
    }
    
    
    /**
     * Test getting a List of Concomitant Medication Detail pivoted data by study accession
     *
     */
    @Test    
    public void getSubstanceUseDetailByStudy()
    {
    	 String id = resources.getString("studyAccession");
    	 PageableData page = interventionService.getSubstanceUseDetailByStudy(0, 10,
		            "compoundNameReported", "", id);
    	 
    	 List <GenericPivotData> results = ( List <GenericPivotData>)page.getData();
    	 
         log.info("Total count:" + page.getTotalCount());
         
         if (results != null) {
	         Iterator<GenericPivotData> it = results.iterator();
	         int count = 1;
	         while (it.hasNext()) {
	        	 GenericPivotData objCon = (GenericPivotData)it.next();        	 
	        	 String armData = "";  
	        	 String baseValues = "";
	        	 List<String> objArmList = objCon.getArmData();
	        	 List<String> objBaseData= objCon.getBaseData();
	        	 
	        	 for (int j=0;j < objBaseData.size();j++)
	        	 {
	        		 baseValues = baseValues + " , " + objBaseData.get(j);
	        	 }
	        	 
	        	 for(int i =0; i < objArmList.size();i++){
	        		 armData = armData + ", " + objArmList.get(i);
	        	 }
	        	 
	        	 log.info("Row " + count + " :" + baseValues + ", " + armData);
	        	 count++;
	        			 
	         }
         }
    	 
    	
    }
    
    
    /**
     * Test getting a List of Concomitant Medication Detail pivoted data by study accession
     *
     */
    @Test    
    public void getInterventionDetailByStudy()
    {
    	 String id = resources.getString("studyAccession");
    	 PageableData page = interventionService.getInterventionDetailByStudy(0, 10,
		            "nameReported", "", id);
    	 
    	 List <GenericPivotData> results = ( List <GenericPivotData>)page.getData();
    	 
         log.info("Total count:" + page.getTotalCount());
         
         if (results != null) {
	         Iterator<GenericPivotData> it = results.iterator();
	         int count = 1;
	         while (it.hasNext()) {
	        	 GenericPivotData objCon = (GenericPivotData)it.next();
	        	 
	        	 String armData = "";   
	        	 String armName = "";
	        	 String baseValues = "";
	        	 List<String> objArmList = objCon.getArmData(); 
	        	 List<String> objArmNames = objCon.getArmNames();    
	        	 List<String> objBaseData= objCon.getBaseData();
	        	 
	        	 for (int j=0;j < objBaseData.size();j++)
	        	 {
	        		 baseValues = baseValues + " , " + objBaseData.get(j);
	        	 }
	        	 
	        	 for(int i =0; i < objArmList.size();i++){
	        		 armData = armData + ", " + objArmList.get(i);
	        		 armName = armName + ", " + objArmNames.get(i);
	        	 }
	        	 
	        	 log.info("Row " + count + " :" + baseValues + " , " + armName +  ", " + armData);
	        	 count++;
	        			 
	         }
         }
    	 
    	
    }

    /**
     * Tear down.
     *
     */
    @After
    public void tearDown() {
        // empty
    }
}

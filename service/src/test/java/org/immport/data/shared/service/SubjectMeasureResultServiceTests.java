package org.immport.data.shared.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.immport.data.shared.model.SubjectMeasureResult;
import org.immport.data.shared.model.PageableData;
import org.immport.data.shared.service.SubjectMeasureResultService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class tests the SubjectMeasureResult Service.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/spring/applicationContext-services.xml"
})
public class SubjectMeasureResultServiceTests {

    /** The logger. */
    private static final Logger log = LoggerFactory.getLogger(SubjectMeasureResultServiceTests.class);

    /** The resource bundle name. */
    private String resourceBundleName = "tests";

    /** The resources. */
    private ResourceBundle resources;

    /** The SubjectMeasureResult service. */
    @Autowired
    SubjectMeasureResultService subjectMeasureResultService;


    /**
     * Set up the tests.
     * 
     */
    @Before
    public void setUp() {
        log.debug("resourceBundleName = " + resourceBundleName);
        resources = ResourceBundle.getBundle(resourceBundleName);
        if (log.isDebugEnabled()) {
            Iterator<?> iterator = resources.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                log.debug(key + " = " + resources.getString(key));
            }
        }
    }

    /**
     * Tests getting the SubjectMeasureResult object by id.
     * 
     */
    @Test
    public void findById() {
        String id = resources.getString("subjectMeasureResult");
        log.info("id = " + id);
        SubjectMeasureResult subjectMeasureResult = subjectMeasureResultService.findById(id);
        assertNotNull("The SubjectMeasureResult object is null.", subjectMeasureResult);
    }

    /**
     * Test getting a PageableData of SubjectMeasureResult objects
     *
     */
    @Test
    public void getPageableData() {
        PageableData page = subjectMeasureResultService.getPageableData(0,10,"s.subjectMeasureResAccession","desc");
        assertNotNull("The page object is null.", page);
        assertTrue("The SubjectMeasureResult count is greater than zero.", page.getData().size() > 0);
        log.info("Count: " + page.getTotalCount());
    }

    /**
     * Test getting a List of SubjectMeasureResult objects by study accession
     *
     */
    @Test
    public void getByStudy() {
        String id = resources.getString("studyAccession");
        List <SubjectMeasureResult> results = subjectMeasureResultService.getByStudy(id);
        assertNotNull("The results is null.", results);
        assertTrue("The SubjectMeasureResult count is greater than zero.", results.size() > 0);
        log.info("Count: " + results.size());
    }

    /**
     * Tear down.
     *
     */
    @After
    public void tearDown() {
        // empty
    }
}
